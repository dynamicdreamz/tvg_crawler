$(document).ready(function() {
	/*==switch-toggle-start==*/

$('.btn-toggle').on('click',function() {
    $(this).find('.btn').toggleClass('active');
});
    $(".sortby-button button").click(function(){
        $(this).toggleClass("dec");
    });
    $(".toggle-menu").click(function(){
        $(this).toggleClass("active");
        $("body").toggleClass("drawer-open");
    });
/*==vertical-tab-start==*/
    $("div.vertical-tab-menu>ul.list-group>li").click(function(e) {
        e.preventDefault();
        $(this).siblings('li.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.vertical-tab>div.vertical-tab-content").removeClass("active");
        $("div.vertical-tab>div.vertical-tab-content").eq(index).addClass("active");
    });


});

function success_toast(msg) {
    $.toast({
        heading: 'Success',
        text: msg,
        position: 'top-right',
        icon: 'success',
        stack: false
    })
}

function error_toast(msg) {
    $.toast({
        heading: 'Error',
        text: msg,
        position: 'top-right',
        icon: 'error',
        stack: false
    })
}