<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Imgfly::routes();

/* Testing Route*/
Route::get('/cronJob', function (){
    Artisan::call('Jpmorgan:cron');
});


/* Error Log File */
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get("/", 'JobController@index');
Route::get("/{search}", 'JobController@index');
Route::get("job-detail/{id}", "JobController@show")->name("jobs.show");

Route::get("/admin", 'AdminController@index');
Route::post("/admin/login", 'AdminController@postLogin');
Route::get("/admin/login", 'AdminController@getLogin');
Route::post('/admin/clone-jobs', 'AdminController@postCloneJobs')->name('clone-jobs');
Route::group(['middleware'=>'adminauth'],function(){

    //Change profile
    Route::get("/admin/my-account", 'AdminController@my_account');
    Route::post('/admin/my-account', 'AdminController@my_account_update');
    //profile setting end

    Route::get("/admin/dashboard", 'AdminController@dashboard');
    Route::get("/admin/configuration/{id}", 'AdminController@configuration');
    Route::get("/admin/companies", 'AdminController@getCompanies');
    Route::get("/admin/profile_keywords/{id}", 'AdminController@profile_keywords');
    Route::post("/admin/add-weight", 'AdminController@postAddWeight');
    Route::post("/admin/weights", 'AdminController@getWeights');
    Route::post("/admin/delete-weight/{id}", 'AdminController@postDeleteWeight');
    Route::post("/admin/delete-weight/{id}", 'AdminController@postDeleteWeight');

    Route::post("/admin/create-profile", 'AdminController@postCreateProfile');
    Route::post("/admin/profiles", 'AdminController@getProfiles');
    Route::post("/admin/update-profile", 'AdminController@postUpdateProfile');
    Route::post("/admin/delete-profiles", 'AdminController@postDeleteProfiles');
    Route::post("/admin/delete-profile/{id}", 'AdminController@postDeleteProfile');

    Route::post("/admin/add-keyword", 'AdminController@postAddKeyword');
    Route::post("/admin/keywords", 'AdminController@getKeywords');
    Route::post("/admin/update-keyword", 'AdminController@postUpdateKeyword');
    Route::post("/admin/delete-keywords", 'AdminController@postDeleteKeywords');
    Route::post("/admin/delete-keyword/{id}", 'AdminController@postDeleteKeyword');

    /* bulk keyword */
    Route::post("/admin/upload_keyword_csv", 'AdminController@upload_bulk_csv_keywords');
    Route::post("/admin/add-bulk-keyword", 'AdminController@postBulkKeyword');
    Route::post("/admin/profile-bulk-keyword", 'AdminController@postBulkProfileKeyword');

    Route::post("/admin/add-profile-keyword", 'AdminController@postAddProfileKeyword');
    Route::post("/admin/profile-keywords", 'AdminController@getProfileKeywords');
    Route::get("/admin/job-matched", 'AdminController@getJobMatched');
    Route::post("/admin/job-profiles", 'AdminController@getJobProfiles');
    Route::post("/admin/profile-jobs", 'AdminController@postProfileJobs');
    Route::post("/admin/profileapproved-jobs", 'AdminController@postProfileapprovedJobs');
    Route::post("/admin/profileignored-jobs", 'AdminController@postProfileignoredJobs');
    Route::get("/admin/profile-jobs/{id}", 'AdminController@getProfileJobs');

    Route::get("/admin/jobs", 'AdminController@getJobs');
    Route::post("/admin/job-status", 'AdminController@postJobStatus');
    Route::post("/admin/job-status-all", 'AdminController@postJobStatusAll');
    Route::get("/admin/profile/{profileId}/job-details/{jobId}", 'AdminController@getJobDetails');
    Route::get("/admin/logout", 'AdminController@getLogout');
    Route::get("/admin/profile/{profileId}/editjob/{id}", 'AdminController@getEditjob');
    Route::post("/admin/update-job", 'AdminController@postUpdateJob');
    Route::post("/admin/create-job", 'AdminController@postCreateJob');
    Route::get("/admin/add-job", 'AdminController@getAddJob');
    Route::post("/admin/find-keyword", 'AdminController@postFindKeyword');
    Route::post("/admin/add-jobkeyword", 'AdminController@postAddJobkeyword');
    Route::post("/admin/delete-jobkeyword", 'AdminController@postDeleteJobkeyword');
    Route::post("/admin/update-cron", 'AdminController@postUpdateCron');
    Route::post("/admin/execute-genericcron", 'AdminController@postExecutegenericCron');
    Route::post("/admin/execute-profilecron", 'AdminController@postExecuteprofileCron');
    Route::get("/admin/cron-status", 'AdminController@getCronStatus');
    Route::post("/admin/location-rating", 'AdminController@postLocationRating');
    Route::get("/admin/location-rating", 'AdminController@getLocationRating');

    Route::post("/admin/company-rating", 'AdminController@postCompanyRating');
    Route::get("/admin/company-rating", 'AdminController@getCompanyRating');
    Route::post("/admin/company-delete/{id}", 'AdminController@postCompanyDelete');

    Route::post("/admin/location-delete/{country}", 'AdminController@postLocationDelete');
    Route::get("/admin/all-jobtype-rating", 'AdminController@getAllJobtypeRating');
    Route::post("/admin/jobtype-rating", 'AdminController@postJobtypeRating');
    Route::post("/admin/jobtype-rating-update", 'AdminController@postJobtypeRatingUpdate');
    Route::get("/admin/jobtype-rating/{id}", 'AdminController@getJobtypeRating');

    Route::post("/admin/experience-rating", 'AdminController@postExperienceRating');
    Route::get("/admin/experience-rating", 'AdminController@getExperienceRating');
    Route::post("/admin/experience-delete/{id}", 'AdminController@postExperienceDelete');

    Route::get('/admin/users', 'AdminController@getUsers');
    Route::post('/admin/add-user', 'AdminController@postAddUser');

    Route::post('/admin/update-user', 'AdminController@postUpdateUser');
    Route::post('/admin/delete-user/{id}', 'AdminController@postDeleteUser');
    Route::post('/admin/delete-users', 'AdminController@postDeleteUsers');
    Route::post('/admin/all-users', 'AdminController@getAllUsers');
    Route::post('/admin/admin-data', 'AdminController@getAdminData');
    Route::get('/admin/selected-jobs', 'AdminController@getSelectedJobs');
    Route::post('/admin/send-newsletter', 'AdminController@postSendNewsletter');
    Route::get('/admin/my-settings', 'AdminController@getSettings');
    Route::post('/admin/save-settings', 'AdminController@postSettings');
    Route::post('/admin/exportApproveJobs','AdminController@excelExportApprovedData');
    Route::post('/admin/job-to-profile','AdminController@postJobToProfle');
    Route::post('/admin/job-transfer-profile','JobController@postJobBasedProfile');
    Route::post('/admin/export-relevant-jobs','AdminController@getExportRelevantJobs');
});


Route::get('admin/password/email','AdminController@getResetAdminPassword')->name('password.reset');
Route::post('admin/password/email','AdminController@postResetAdminPassword')->name('password.reset');
Route::get('admin/resetPassword/{email}/{verificationLink}','AdminController@resetPassword');
Route::post('admin/resetPassword','AdminController@newPassword');

  /*
   |--------------------------------------------------------------------------
   | AJAX calls
   |--------------------------------------------------------------------------
   */
  

Route::prefix('ajax')->group(function (){
    Route::post('/jobs/index', 'JobController@indexQuery')->name("ajax.jobs.index");
    Route::post('/admin/jobs', 'AdminController@indexQuery')->name("ajax.admin.jobs");
});

