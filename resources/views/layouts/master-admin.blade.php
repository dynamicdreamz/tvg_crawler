<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include("inc.admin.header")
</head>
<body>
<div id="divLoading"></div>
{{--@include('inc.admin.icons-svg')--}}

<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="header-inner">
                    <div class="logo"><a href="{{ URL::to('/') }}/admin/dashboard"><h2>Nova</h2><h3>Jobs Data Management System</h3></a></div><div class="dropdown userdropdown">
                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-img"><img src="{{ URL::to('/') }}/uploads/thumb/{{\Auth::guard('admin')->user()->profile_pic}}"></div> <span class="user-name"><strong>Welcome,</strong> {{\Auth::guard('admin')->user()->first_name}} {{\Auth::guard('admin')->user()->last_name}}</span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <li><a href="{{ URL::to('/') }}/admin/logout">Logout</a></li>
                            <li><a href="{{ URL::to('/') }}/admin/my-account">My account</a></li>
                            <li><a href="{{ URL::to('/') }}/admin/my-settings">My settings</a></li>
                            @if(\Auth::guard('admin')->user()->type=="superadmin")
                            <li><a href="{{ URL::to('/') }}/admin/users">Users</a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-wrapper">
    <div class="container-fluid">
        <div class="row">
    {{-- Sidebar menu for the regular user.--}}

        @include('inc.admin.sidenav')


    <div class="content-wrapper">
        @include('inc.admin.errors')

        @include('flash.message')

        @yield('content')
    </div>

    @include('inc.admin.footer')
</div>
    </div>
</div>
@stack("javascript")
</body>
</html>
