<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include("inc.header")
</head>
<body>
<div id="divLoading"></div>
{{--@include('inc.admin.icons-svg')--}}

<header class="header frontend-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="header-inner">
                    <div class="logo"><a href="{{ URL::to('/') }}"><h2>Nova</h2><h3>Jobs Data Management System</h3></a></div><div class="dropdown userdropdown">
                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-img"><img src="{{ URL::to('/') }}/img/user-img.png"></div> <span class="user-name"><strong>Welcome,</strong> James Martin</span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="top-noticebar frontend-noticebar">
        <div class="container-fluid">
            <div class="row">
                <div class="toggle-menu">
                    <i class="fal fa-bars"></i>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <h4 class="text-white "><span class="job-list">0</span> Jobs Listing</h4>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6">
                    <div class="pull-right searchbar">
                        <div class="search-box">
                            <input type="text" placeholder="Search" id="search_job" value="">
                            <button><i class="far fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="main-wrapper frontend-mainwrapper">
    <div class="container-fluid">
        <div class="row">
    {{-- Sidebar menu for the regular user.--}}

        @include('inc.sidenav')


    <div class="content-wrapper">
        @include('inc.errors')

        @include('flash.message')

        @yield('content')
    </div>

    @include('inc.footer')
</div>
    </div>
</div>
@stack("javascript")
</body>
</html>
