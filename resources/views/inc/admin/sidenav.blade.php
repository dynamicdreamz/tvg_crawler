<div class="side-menu-fixed side-menu-scroll mCustomScrollbar">
    <div class="side-menu">
        <a class="all-job-text" href="{{ URL::to('/') }}/admin/dashboard">All Jobs <span class="all_jobs"></span></a>
        <h4>Skillsets</h4>
        <ul class="sidebarnav" id="sidebarnav">

        </ul>
        <h4>Filter By:</h4>
        <ul class="sidebarnav frontendsidenav">
            <li>
                <h5 class="text-white">job type</h5>
                <div class="jobtype-box">
                    <p class="text-white">Full Time</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" name="job_types" class="job_type_filter" data-job_type="1" value="1"><span class="checkmark"></span></label>
                    </div>
                </div>
                <div class="jobtype-box">
                    <p class="text-white">Part Time</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" name="job_types" class="job_type_filter" data-job_type="2" value="2"><span class="checkmark"></span></label>
                    </div>
                </div>
                <div class="jobtype-box">
                    <p class="text-white">Internship</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" name="job_types" class="job_type_filter" data-job_type="3" value="3"><span class="checkmark"></span></label>
                    </div>
                </div>
                {{--<div class="jobtype-box">
                    <p class="text-white">Internship</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox"><span class="checkmark"></span></label>
                    </div>
                </div>--}}
            </li>
            <li>
                <h5 class="text-white">COUNTRY</h5>
                <div class="location-box">
                    <input type="text" id="country" class="form-control typeahead" placeholder="Set Location">
                    <button><i class="fas fa-map-marker-alt"></i></button>
                </div>
            </li>
            <li>
                <h5 class="text-white">COMPANY</h5>
                <div class="select-wrapper">
                    <select id="company" name="company">
                    </select>
                </div>
            </li>
            <li>
                <a href="#" class="btn btn-block bg-white apply-btn">APPLY</a>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(function () { 

        function getProfiles() {
            //$("#divLoading").addClass('show');
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-profiles") }}',
                contentType: 'application/json',
                success: function (result) {
                    var total_jobs = result.total_profiles;
                        result = result.data;
                    var innerhtml = '';

                    var profile_id = window.location.pathname.split("/").pop();

                    for (var i = 0; i < result.length; i++) {
                        var class_text = "";
                        if(profile_id == result[i]['id']){
                            class_text="active";
                        }
                        innerhtml += '<li class="'+class_text+'"><a href="{{ URL::to('/') }}/admin/profile-jobs/'+result[i]['id']+'">'+ result[i]['name'] +'<span>('+ result[i]['jobs'] +')</span></a></li>';
                    }

                    $(".all_jobs").text('('+total_jobs+')');

                    $("#sidebarnav").html(innerhtml);
                }
            });

            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/companies") }}',
                contentType: 'application/json',
                success: function (result) {
                   var companies = result.companies;
                   var innerhtml = '<option value="">Select</option>';
                   for (i=0 ;i < companies.length; i++){
                    var seleted_company_val = $.cookie('seleted_company');
      //  console.log("seleted_company_val",seleted_company_val);

                        
        if (seleted_company_val  != undefined) {
            var chk_ar = seleted_company_val;
            chk_ar = $.parseJSON(chk_ar);
            $.each(chk_ar, function (index, value) {
            //    console.log("VAL",value);
                if(value==companies[i].company){
                innerhtml += '<option value="'+companies[i].company+'" selected>'+companies[i].company+'</option>';
                }else{
                    innerhtml += '<option value="'+companies[i].company+'">'+companies[i].company+'</option>';
                }
               
            });

        }else{
            innerhtml += '<option value="'+companies[i].company+'">'+companies[i].company+'</option>';
        }
                      
                    }

                    $("#company").html(innerhtml);
                }
            });
        }

        getProfiles(0, '');


    })
</script> 