<div class="side-menu-fixed">
    <div class="side-menu">
        <div class="toggle-menu close">
            <i class="far fa-times"></i>
        </div>
        <ul class="sidebarnav frontendsidenav">
            <li>
                <h5 class="text-white">Filter By:</h5>
                <div class="sortby">
                    <p class="text-white">Relevance</p>
                    <input type="number" id="relevance" >
                </div>
                <div class="sortby">
                    <p class="text-white">Date</p>
                    <input type="text" id="date_filter" data-date-format="dd-mm-yyyy" readonly>
                </div>
            </li>
            <li>
                <h5 class="text-white">Location</h5>
                <div class="location-box">
                    <input type="text" class="form-control typeahead" placeholder="Set Location">
                    <button><i class="fas fa-map-marker-alt"></i></button>
                </div>
            </li>
            <li>
                <h5 class="text-white">job type</h5>
                <div class="jobtype-box">
                    <p class="text-white">Full Time</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" class="job_type_filter" value="1"><span class="checkmark"></span></label>
                    </div>
                </div>
                <div class="jobtype-box">
                    <p class="text-white">Part Time</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox"  class="job_type_filter" value="2"><span class="checkmark"></span></label>
                    </div>
                </div>
                <div class="jobtype-box">
                    <p class="text-white">Internship</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" class="job_type_filter" value="3"><span class="checkmark"></span></label>
                    </div>
                </div>
                {{--<div class="jobtype-box">
                    <p class="text-white">Internship</p>
                    <div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox"><span class="checkmark"></span></label>
                    </div>
                </div>--}}
            </li>
            <li>
                <a href="#" class="btn btn-block bg-white apply-btn">APPLY</a>
            </li>
        </ul>
    </div>
</div>