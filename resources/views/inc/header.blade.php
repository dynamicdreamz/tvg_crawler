<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('page_title', config('app.name', 'Tvg') )</title>



<link rel="shortcut icon" type="image/png" href="{{ asset('favicon/nova-favicon.png') }}"/>
<link rel="shortcut icon" type="image/png" href="{{ asset('favicon/nova-favicon.png') }}"/>


<!-- Bootstrap -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet">
<link href="{{ asset('css/jquery.toast.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>