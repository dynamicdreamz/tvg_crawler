@extends("layouts.master-admin")
@section("content")
<div class="main-wrapper joblistingall-page">
    <div class="container">
        <div class="jobs-tag">
            <div class="row ">
                <div class="col-sm-12">
                    <div class="pull-left">
                        <h5 class="text-white mb-0"> <span>74</span>  jobs  collected</h5>
                        <p class="text-white mb-0">Last crawling on Monday 28, october. 7:00am</p>
                    </div>
                    <div class="pull-right">
                        <a class="mb-0 text-white" href="#">Crawler options</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="vertical-tab-container">
            <div class="row">
                <div class="col-sm-3 vertical-tab-menu">
                    <ul class="list-group">
                        <li href="#" class="list-group-item active">
                            <a>All jobs <span>(74)</span></a>
                        </li>
                        <li href="#" class="list-group-item">
                            <a>Business  <span>(20)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Marketing  <span>(15)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Finance <span>(8)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Technology  <span>(10)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Product <span>(3)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Management   <span>(25)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Law   <span>(13)</span></a>
                        </li>
                        <li href="#" class="list-group-item ">
                            <a>Engineering   <span>(7)</span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-9 vertical-tab ">
                    <div class="vertical-tab-content active">
                        <div class="row all-job-header">
                            <div class="col-sm-12">
                                <div class="pull-left">
                                    <div class="sub-title d-i">
                                        <h5>All jobs</h5>
                                    </div>
                                    <div class="edit-profile-link d-i">
                                        <a href="#">Edit profiles (AI)</a>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="serchbar">
                                        <input name="" id="search_all" placeholder="Search" type="search">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row common-tab business-job-tab">
                            <div class="col-sm-12">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#relevant">5  <span>relevant</span> </a></li>
                                    <li><a data-toggle="pill" href="#approved">0  <span>approved</span></a></li>
                                    <li><a data-toggle="pill" href="#ignored">15  <span>ignored</span></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="relevant" class="tab-pane fade in active">
                                        <div class="row business-tab-header">
                                            <div class="col-sm-12">
                                                <div class="pull-left">
                                                    <div class="checkbox-btn common-checkbox">
                                                        <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                        <label for="rememberme" class="acccke"></label>
                                                    </div>
                                                    <button type="button" class="outline-btn btn">Ignore</button>
                                                    <button type="button" class="outline-btn btn">Approve</button>
                                                </div>
                                                <div class="pull-right">
                                                    <a class="outline-btn btn sort-by-weight" id="sort-by-match" data-weight="asc">Sort
                                                        by weight<img src="{{ URL::to('/') }}/img/dropdownarrow-gray.png"> </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="all_jobs" class="business-job-part">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="approved" class="tab-pane fade">
                                        <h3>Menu 1</h3>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    </div>
                                    <div id="ignored" class="tab-pane fade">
                                        <h3>Menu 2</h3>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- flight section -->
                    <div class="vertical-tab-content ">
                        <div class="row business-job-header">
                            <div class="col-sm-12">
                                <div class="pull-left">
                                    <div class="sub-title d-i">
                                        <h5>{{$profile_name}}</h5>
                                    </div>
                                    <div class="edit-profile-link d-i">
                                        <a href="#">Edit profiles (AI)</a>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <div class="serchbar">
                                        <input name="" placeholder="Search" type="search">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row common-tab business-job-tab">
                            <div class="col-sm-12">
                                <ul class="nav nav-pills">
                                    <li class="active"><a data-toggle="pill" href="#relevant">5  <span>relevant</span> </a></li>
                                    <li><a data-toggle="pill" href="#approved">0  <span>approved</span></a></li>
                                    <li><a data-toggle="pill" href="#ignored">15  <span>ignored</span></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div id="relevant" class="tab-pane fade in active">
                                        <div class="row business-tab-header">
                                            <div class="col-sm-12">
                                                <div class="pull-left">
                                                    <div class="checkbox-btn common-checkbox">
                                                        <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                        <label for="rememberme" class="acccke"></label>
                                                    </div>
                                                    <button type="button" class="outline-btn btn">Ignore</button>
                                                    <button type="button" class="outline-btn btn">Approve</button>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="select-wrapper common-sortby">
                                                        <select class="form-control" id="sel1">
                                                            <option>Sort by match</option>
                                                            <option>Sort by weigh1</option>
                                                            <option>Sort by weigh2</option>
                                                            <option>Sort by weigh3</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="business-job-part">
                                                    <div class="business-job-content">
                                                        <div class="business-job-content-left">
                                                            <div class="checkbox-btn common-checkbox">
                                                                <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                                <label for="rememberme" class="acccke"></label>
                                                            </div>
                                                            <div class="business-job-link">
                                                                <h3>Executive Trainee</h3>
                                                                <p class="mb-0">on <span class="date">Aug 22, 2018</span> from <span class="online-link">Indeed.com</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="business-job-content-right">
                                                            <div class="job-yearreport text-center d-i">
                                                                <h4 class="mb-0">80%</h4>
                                                                <p class="mb-0">MATCH</p>
                                                            </div>
                                                            <button type="button" class="dark-btn btn">Ignore</button>
                                                            <button type="button" class="dark-btn btn">Approve</button>
                                                        </div>
                                                    </div>
                                                    <div class="business-job-content">
                                                        <div class="business-job-content-left">
                                                            <div class="checkbox-btn common-checkbox">
                                                                <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                                <label for="rememberme" class="acccke"></label>
                                                            </div>
                                                            <div class="business-job-link">
                                                                <h3>Senior consultant business for SAP </h3>
                                                                <p class="mb-0">on <span class="date">Aug 22, 2018</span> from <span class="online-link">Linkedin.com</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="business-job-content-right">
                                                            <div class="job-yearreport text-center d-i">
                                                                <h4 class="mb-0">79%</h4>
                                                                <p class="mb-0">MATCH</p>
                                                            </div>
                                                            <button type="button" class="dark-btn btn">Ignore</button>
                                                            <button type="button" class="dark-btn btn">Approve</button>
                                                        </div>
                                                    </div>
                                                    <div class="business-job-content">
                                                        <div class="business-job-content-left">
                                                            <div class="checkbox-btn common-checkbox">
                                                                <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                                <label for="rememberme" class="acccke"></label>
                                                            </div>
                                                            <div class="business-job-link">
                                                                <h3>Executive Assistant</h3>
                                                                <p class="mb-0">on <span class="date">Aug 22, 2018</span> from <span class="online-link">job.com</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="business-job-content-right">
                                                            <div class="job-yearreport text-center d-i">
                                                                <h4 class="mb-0">75%</h4>
                                                                <p class="mb-0">MATCH</p>
                                                            </div>
                                                            <button type="button" class="dark-btn btn">Ignore</button>
                                                            <button type="button" class="dark-btn btn">Approve</button>
                                                        </div>
                                                    </div>
                                                    <div class="business-job-content">
                                                        <div class="business-job-content-left">
                                                            <div class="checkbox-btn common-checkbox">
                                                                <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                                <label for="rememberme" class="acccke"></label>
                                                            </div>
                                                            <div class="business-job-link">
                                                                <h3>Senior consultant business for SAP </h3>
                                                                <p class="mb-0">on <span class="date">Aug 22, 2018</span> from <span class="online-link">Indeed.com</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="business-job-content-right">
                                                            <div class="job-yearreport text-center d-i">
                                                                <h4 class="mb-0">71%</h4>
                                                                <p class="mb-0">MATCH</p>
                                                            </div>
                                                            <button type="button" class="dark-btn btn">Ignore</button>
                                                            <button type="button" class="dark-btn btn">Approve</button>
                                                        </div>
                                                    </div>
                                                    <div class="business-job-content">
                                                        <div class="business-job-content-left">
                                                            <div class="checkbox-btn common-checkbox">
                                                                <input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">
                                                                <label for="rememberme" class="acccke"></label>
                                                            </div>
                                                            <div class="business-job-link">
                                                                <h3>Senior consultant business for SAP </h3>
                                                                <p class="mb-0">on <span class="date">Aug 22, 2018</span> from <span class="online-link">Indeed.com</span></p>
                                                            </div>
                                                        </div>
                                                        <div class="business-job-content-right">
                                                            <div class="job-yearreport text-center d-i">
                                                                <h4 class="mb-0">71%</h4>
                                                                <p class="mb-0">MATCH</p>
                                                            </div>
                                                            <button type="button" class="dark-btn btn">Ignore</button>
                                                            <button type="button" class="dark-btn btn">Approve</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="approved" class="tab-pane fade">
                                        <h3>Menu 1</h3>
                                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    </div>
                                    <div id="ignored" class="tab-pane fade">
                                        <h3>Menu 2</h3>
                                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- marketing search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">marketing</h3>
                        </center>
                    </div>
                    <!-- finace search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">finace</h3>
                        </center>
                    </div>
                    <!-- technology search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">technology</h3>
                        </center>
                    </div>
                    <!-- product search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">product</h3>
                        </center>
                    </div>
                    <!-- managenment search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">managenment</h3>
                        </center>
                    </div>
                    <!-- law search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">law</h3>
                        </center>
                    </div>
                    <!-- engineering search -->
                    <div class="vertical-tab-content">
                        <center>
                            <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                            <h3 style="margin-top: 0;color:#55518a">engineering</h3>
                        </center>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            function get_jobs(offset) {
                 $("div#divLoading").addClass('show');
                var search_text = $('#search_all').val();
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: '{"offset": ' + offset + ', "pagesize": "20", "search_text": "'+search_text+'"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '{{ url("/ajax/admin/jobs") }}',
                    success: function (data) {
                        //console.log(data);
                        $("#divLoading").addClass('show');
                        var result = data;

                        var innerhtml = $("#all_jobs").html();
                        if(search_text !='' && offset <= 0){
                            innerhtml = '';
                        }
                        for (var i = 0; i < result.length; i++) {

                            innerhtml += '<div class="business-job-content">'+
                                '<div class="business-job-content-left">'+
                                '<div class="checkbox-btn common-checkbox">'+
                                '<input class="input-checkbox" name="rememberme" id="rememberme" type="checkbox">'+
                                '<label for="rememberme" class="acccke"></label>'+
                                '</div>'+
                                '<div class="business-job-link">'+
                                '<h3>'+result[i]['job_title']+'</h3>'+
                                '<p class="mb-0">on <span class="date">'+result[i]['posted_on']+'</span> from <span class="online-link">'+result[i]['website']+'</span></p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="business-job-content-right">'+
                                '<div class="job-yearreport text-center d-i">'+
                                '<h4 class="mb-0">'+result[i]['relevant_match']+'%</h4>'+
                                '<p class="mb-0">MATCH</p>'+
                                '</div>'+
                                '<button type="button" class="dark-btn btn ignore_all_job" data-jobid="'+result[i]['id']+'">Ignore</button>'+
                                '<button type="button" class="dark-btn btn approve_all_job" data-jobid="'+result[i]['id']+'">Approve</button>'+
                                '</div>'+
                                '</div>';


                        }

                        $("#all_jobs").html(innerhtml);

                        function sortUsingNestedText(parent, childSelector) {
                            var items = parent.children(childSelector).sort(function(a, b) {
                                var vA = parseInt($(b).find('.business-job-content-right h4').text());
                                var vB = parseInt($(a).find('.business-job-content-right h4').text());

                                return (vA > vB) ? 1 : (vA < vB) ? -1 : 0;

                            });
                            parent.append(items);
                        }
                        sortUsingNestedText($('#all_jobs'), "div");

                        $("#divLoading").removeClass('show');

                    }
                });
            }

            get_jobs(0);
            var count = 0;
            $("#paginate").on("click", function () {
                count += 1;
                get_jobs(count * 20);
            })

            $("#search_all").on("keyup", function(){
                get_jobs(0);
            })

            $(document).on('click','.ignore_all_job',function(){
                var job_id = $(this).data('jobid');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/job-status") }}',
                    data: '{"action":"ignore","job_id":'+job_id+'}',
                    contentType: 'application/json',
                    success: function (data) {
                        get_jobs(0);
                        $("#divLoading").removeClass('show');
                        success_toast(data.msg);

                    }
                })
            })

            $(document).on('click','.approve_all_job',function(){
                var job_id = $(this).data('jobid');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/job-status") }}',
                    data: '{"action":"approve","job_id":'+job_id+'}',
                    contentType: 'application/json',
                    success: function (data) {
                        get_jobs(0);
                        $("#divLoading").removeClass('show');
                        success_toast(data.msg);

                    }
                })
            })
        })
    </script>
@endsection