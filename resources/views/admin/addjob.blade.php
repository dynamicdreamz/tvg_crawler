<?php
$job_type = [1=>"Full Time",2=>"Part Time",3=>"Internship"];
$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
?>
@extends("layouts.master-admin")
@section("content")
<div class="row">
    <div class="col-sm-12">
        <h2>Create New Job</h2>
    </div>
</div>

<div class="row">
    <form id="create_new_job">
    <div class="col-sm-12 col-md-8 col-lg-8">
        <div class="custom-block mb-2">
            <div class="create-job">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location">Title</label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location">Website</label>
                                <input type="url" class="form-control" id="website" name="website" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location">Category</label>
                                <input type="text" class="form-control" id="category" name="category" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location">Location</label>
                                <select class="form-control" id="location" name="location" required>
                                    <option>Select</option>
                                    @foreach($countries as $country )
                                        <option value="{{$country}}">{{$country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="type">Type</label>
                                <select id="type" class="form-control" name="type">
                                    <option>Select</option>
                                    @foreach($job_type as $k=>$type)
                                        <option value="{{$k}}">{{$type}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="comapny">Company</label>
                                <input type="text" class="form-control" id="comapny" name="company" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="contact">Contact Name</label>
                                <input type="text" class="form-control" id="contact" name="contact_name" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="contact">Contact Email</label>
                                <input type="text" class="form-control" id="contact" name="contact_email" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="contact">Contact Phone</label>
                                <input type="text" class="form-control" id="contact" name="contact_phone" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="contact">Reference Id</label>
                                <input type="text" class="form-control" id="reference_id" name="reference_id" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="contact">Source Url</label>
                                <input type="text" class="form-control" id="source_url" name="source_url" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="start">Start</label>
                                <input type="text" class="form-control" size="16" id="start" name="start" data-date-format="dd-mm-yyyy" readonly required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="deadline">Deadline</label>
                                <input type="text" class="form-control" size="16" id="deadline" name="deadline" data-date-format="dd-mm-yyyy" readonly required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="experience">Experience From</label>
                                <input type="number" min="0" class="form-control" id="experience_from" name="experience_from" required>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="experience">Experience To</label>
                                <input type="number" min="0" class="form-control" id="experience_to" name="experience_to" required>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <div class="custom-block mb-2 job-dec">
            <h4>DESCRIPTION</h4>
            <textarea id="editor1" name="description" class="form-control" rows="5" >
                </textarea>
        </div>
        <button class="green-bg btn text-white">Create Job</button>
    </div>
    </form>
    <div class="col-sm-12 col-md-4 col-lg-4">

        <div class="custom-block mb-2">

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <button type="button" class="broder-button btn btn-block addkeyword"  data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="keywordModal" tabindex="-1" role="dialog" aria-labelledby="keywordModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="keywordModalLabel">Add Keyword</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="generic_form_add_keyword">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="keyword_title" name="keyword_title" required>
                    </div>
                    <div class="form-group">
                        <label for="profile" class="col-form-label">Profile:</label>
                        <div class="select-wrapper2">
                            <select class="form-control" id="sel2" name="keyword_profile" required>
                                <option value="0">Generic</option>
                                @foreach($profiles as $profile)
                                    <option value="{{$profile->id}}">{{$profile->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="weight" class="col-form-label">Weight:</label>
                        <div class="select-wrapper2">
                            <select class="form-control" id="sel1" name="keyword_weight" required>
                                @foreach($weights as $weight)
                                    <option value="{{$weight->id}}">{{$weight->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="balance" class="col-form-label">Profile Balance:</label>
                        <div class="btn-group btn-toggle btn-group-design">
                            <button type="button" class="btn keyword_balance" value="1">Positive</button>
                            <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_keyword">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('editor1');

        $('#deadline,#start').datepicker();
    })


    var keyword_exist = false;
    var keyword_id = 0;
    $("#keyword_title").on("keyup", function () {
        var title = $(this).val();
        var profile_id = 0;
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/find-keyword") }}',
            data: '{"keyword_title":"' + title + '","profile_id":' + profile_id + '}',
            contentType: 'application/json',
            success: function (data) {
                $("#divLoading").removeClass('show');
                keyword_exist = data.exist;
                keyword_id = data.keyword_id;
                if (keyword_exist) {
                    $("#keyword_title").parent().nextAll(".form-group").hide();
                } else {
                    $("#keyword_title").parent().nextAll(".form-group").show();
                }
            }
        })
    })

    $("#create_new_job").on('submit', function (e) {
        e.preventDefault();
        if ($("#experience_from").val() > $("#experience_to").val()) {
            error_toast("experience from should be less than experience to.");
            return false;
        }
        $("#divLoading").addClass('show');

        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }

        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/create-job") }}',
            data: $(this).serialize()+'&'+$("#generic_form_add_keyword").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val()+'&profile_id=0&keyword_id='+keyword_id+'&keyword_exist='+keyword_exist,
            success: function (data) {
                $("#divLoading").removeClass('show');
                success_toast(data.msg);
                $("#create_new_job").trigger("reset");
                $("#generic_form_add_keyword").trigger("reset");
                CKEDITOR.instances.editor1.setData('');
            }
        })
    });

    $("#generic_form_add_keyword").on("submit",function (e) {
        e.preventDefault();
        $("#keywordModal").modal('hide');
    })

       /* $("#generic_form_add_keyword").on('submit',function (e) {
            e.preventDefault();
            $("#divLoading").addClass('show');

            var profile_id = 0;
            var job_id = 0;

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/add-jobkeyword") }}',
                data: $("#generic_form_add_keyword").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val()+'&profile_id='+profile_id+'&job_id='+job_id+'&keyword_id='+keyword_id+'&keyword_exist='+keyword_exist,
                success: function (data) {
                    if(data.success){
                        $("#divLoading").removeClass('show');
                        $("#keywordModal").modal('hide');
                        success_toast(data.msg);
                        $('#generic_form_add_keyword').trigger("reset");
                        location.reload();
                    }else{
                        $("#divLoading").removeClass('show');
                        //$("#keywordModal").modal('hide');
                        error_toast(data.msg);
                    }

                }
            })
        });*/


</script>
@endsection
