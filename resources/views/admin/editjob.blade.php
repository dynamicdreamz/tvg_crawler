<?php
$job_type = [1=>"Full Time",2=>"Part Time",3=>"Internship"];
$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

?>
@extends("layouts.master-admin")
@section("content")
    <div class="row">
        <div class="col-sm-12">
            <h2>Update Job</h2>
        </div>
    </div>

    <div class="row">
        <form id="form_edit_job">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="custom-block mb-2">
                <div class="create-job">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" id="title" name="title" value="{{$jobDetail->job_title}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <input type="text" class="form-control" id="website" name="website" value="{{$jobDetail->website}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="category">Category</label>
                                    <input type="text" class="form-control" id="category" name="category" value="{{$jobDetail->category}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <select class="form-control" id="location" name="location" required>
                                        <option>Select</option>
                                        @foreach($countries as $country )
                                        <option value="{{$country}}" {{ ( $country == $jobDetail->country) ? 'selected' : '' }}>{{$country}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <select id="type" class="form-control" name="type">
                                        <option>Select</option>
                                        @foreach($job_type as $k=>$type)
                                        <option value="{{$k}}" {{$k == $jobDetail->job_type ? "selected":""}}>{{$type}}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="comapny">Company</label>
                                    <input type="text" class="form-control" id="company" name="company" value="{{$jobDetail->company}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="contact">Contact Name</label>
                                    <input type="text" class="form-control" id="contact" name="contact_name" value="{{$jobDetail->contact_name}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="contact">Contact Email</label>
                                    <input type="text" class="form-control" id="contact" name="contact_email" value="{{$jobDetail->contact_email}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="contact">Contact Phone</label>
                                    <input type="text" class="form-control" id="contact" name="contact_phone" value="{{$jobDetail->contact_phone}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="contact">Reference Id</label>
                                    <input type="text" class="form-control" id="reference_id" name="reference_id"  value="{{$jobDetail->reference_id}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="contact">Source Url</label>
                                    <input type="text" class="form-control" id="source_url" name="source_url" value="{{$jobDetail->source_url}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="start">Start</label>
                                    <input type="text" class="form-control" id="start" name="start" value="{{$jobDetail->posted_on}}" data-date-format="dd-mm-yyyy" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="deadline">Deadline</label>
                                    <input type="text" class="form-control" id="deadline" name="deadline" value="{{$jobDetail->close_on}}" data-date-format="dd-mm-yyyy" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="experience">Experience From</label>
                                    <input type="number" min="0" class="form-control" id="experience_from" name="experience_from" value="{{$jobDetail->experience_from}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="experience">Experience To</label>
                                    <input type="number" min="0" class="form-control" id="experience_to" name="experience_to" value="{{$jobDetail->experience_to}}">
                                </div>
                            </div>
                        </div>

                </div>
            </div>
            <div class="custom-block mb-2 job-dec">
                <h4>DESCRIPTION</h4>
                <textarea id="editor1" name="description" class="form-control" rows="5" >
                    {!!html_entity_decode($jobDetail->description, ENT_QUOTES, 'UTF-8')!!}
                </textarea>
            </div>
            <input type="hidden" name="job_id" value="{{$jobDetail->id}}">
            </form>
            <a class="green-bg button createjob-btn" href="#">Update Job</a>
        </div>
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="custom-block mb-2">
            <a href="javascript:void(0);" class="btn-block button green-bg f-18 update_cron" data-job_profile_id="{{$jobDetail->profile_id}}" style="margin-bottom: 25px">Update Cron</a>
            <ul class="inline-list">
                <li><div class="jobmatchperc"><h2>{{$jobDetail->points}}</h2><h4>MATCH</h4></div></li>

                <li><a class="button green-bg ignore_job" href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>

                <li><a class="button green-bg" href="javascript:void(0);" data-toggle="modal" data-target="#PreviewModal"><img src="{{ URL::to('/') }}/img/eye.svg"> Preview</a></li>
            </ul>

            <a href="javascript:void(0);" class="btn-block button green-bg f-18 approve_job">Approve</a>

        </div>
        <div class="custom-block mb-2">
            <h4>Add Job To Profile</h4>
            <form>
                <div class="form-group">
                    <div class="select-wrapper2">
                        <select class="form-control" id="job_profile_id" name="keyword_profile" required>
                            <option>Select</option>
                            @foreach($profiles as $profile)
                                <option value="{{$profile->id}}">{{$profile->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="button" id="add_job_to_profile" class="broder-button btn btn-block">Save</button>
            </form>
        </div>
        <div class="custom-block mb-2">
            <h4>Related Keywords</h4>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs design-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#positive_tags" aria-controls="positive_tags" role="tab" data-toggle="tab">Positive ({{$jobDetail->positive_keywords_count}})</a></li>
                <li role="presentation"><a href="#negative_tags" aria-controls="negative_tags" role="tab" data-toggle="tab">Negative ({{$jobDetail->negative_keywords_count}})</a></li>
                <li role="presentation"><a href="#deleted_tags" aria-controls="deleted_tags" role="tab" data-toggle="tab">Deleted ({{count($jobDetail->deleted_keywords)}})</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="positive_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->positive_keywords as $id=>$pos_keyword)
                                <li>
                                    <a href="#" class="tag-input " data-keywordid="{{$id}}" data-profileid="{{$jobDetail->profile_id}}">{{$pos_keyword}}<span class="remove"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
                <div role="tabpanel" class="tab-pane" id="negative_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->negative_keywords as $id=>$neg_keyword)
                                <li>
                                    <a href="#" class="tag-input {{strpos($neg_keyword, '-') !== false ? "red":""}}" data-keywordid="{{$id}}" data-profileid="{{$jobDetail->profile_id}}">{{$neg_keyword}}<span class="remove"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
                <div role="tabpanel" class="tab-pane" id="deleted_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->deleted_keywords as $del_keyword)
                                <li>
                                    <a href="#" class="tag-input red" data-keywordid="{{$del_keyword->keyword_id}}" data-profileid="{{isset($del_keyword->profile_id) ? $del_keyword->profile_id:0}}">{{$del_keyword->generic_title}}<span class="add"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
            </div>
        </div>
        @if($jobDetail->matching_profiles)
        <div class="custom-block mb-2 p-lr">
            <div class="col-md-12"><h4>Other Matchings</h4></div>
            <ul>
                @foreach($jobDetail->matching_profiles as $profile)
                    @if($profile['points'] > $threshold)
                        <li>
                            <div class="other-matching-side tooltip-base">
                                <h3 class="pull-left">{{$profile['points']}}</h3><span>{{$profile['name']}}</span><a class="button green-bg pull-right" href="{{ URL::to('/') }}/admin/profile-jobs/{{$profile['profile_id']}}"><img src="{{ URL::to('/') }}/img/eye.svg"> Preview</a>
                            </div>
                            <div class="tooltip">
                                <div class="tooltiptext">
                                    <div class="tooltip-header">
                                        <p>Score Calculation:</p>
                                    </div>
                                    <ul>
                                        @foreach($profile['keywords'] as $k=>$keywords)
                                            <li><p class="text-black">{{$keywords}}: <span class="{{ strpos($keywords, '-') !== false ? "text-red":"text-green"}}">{{$profile['keyword_points'][$k]}} Points</span></p></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="btn-block button green-bg f-18 update_cron" data-job_profile_id="{{$profile["profile_id"]}}" style="margin-bottom: 25px">Update Cron</a>
                            <div class="custom-block mb-2">
                                <h4>Related Keywords</h4>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs design-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#positive_tags{{$profile["profile_id"]}}" aria-controls="positive_tags" role="tab" data-toggle="tab">Positive ({{$profile['positive_keywords_count']}})</a></li>
                                    <li role="presentation"><a href="#negative_tags{{$profile["profile_id"]}}" aria-controls="negative_tags" role="tab" data-toggle="tab">Negative ({{$profile['negative_keywords_count']}})</a></li>
                                    <li role="presentation"><a href="#deleted_tags{{$profile["profile_id"]}}" aria-controls="deleted_tags" role="tab" data-toggle="tab">Deleted ({{count($profile['deleted_keywords'])}})</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="positive_tags{{$profile["profile_id"]}}">
                                        <div class="jlb-tags-part">
                                            <ul class="list-inline">
                                                @foreach($profile['positive_keywords'] as $id=>$pos_keyword)
                                                    <li>
                                                        <a href="#" class="tag-input " data-keywordid="{{$id}}" data-profileid="{{$profile["profile_id"]}}">{{$pos_keyword}}<span class="remove"></span></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="negative_tags{{$profile["profile_id"]}}">
                                        <div class="jlb-tags-part">
                                            <ul class="list-inline">
                                                @foreach($profile['negative_keywords'] as $id=>$neg_keyword)
                                                    <li>
                                                        <a href="#" class="tag-input {{strpos($neg_keyword, '-') !== false ? "red":""}}" data-keywordid="{{$k}}" data-profileid="{{$profile["profile_id"]}}">{{$neg_keyword}}<span class="remove"></span></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="deleted_tags{{$profile["profile_id"]}}">
                                        <div class="jlb-tags-part">
                                            <ul class="list-inline">
                                                @foreach($profile['deleted_keywords'] as $del_keyword)
                                                    <li>
                                                        <a href="#" class="tag-input red" data-keywordid="{{$del_keyword->keyword_id}}" data-profileid="{{isset($del_keyword->profile_id) ? $del_keyword->profile_id:0}}">{{$del_keyword->generic_title}}<span class="add"></span></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>

        </div>
            @endif
    </div>
    </div>



    <div class="modal fade" id="keywordModal" tabindex="-1" role="dialog" aria-labelledby="keywordModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="keywordModalLabel">Add Keyword</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="generic_form_add_keyword">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" id="keyword_title" name="keyword_title" required>
                        </div>
                        <div class="form-group">
                            <label for="profile" class="col-form-label">Profile:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="sel2" name="keyword_profile" required>
                                    <option value="0">Generic</option>
                                    @foreach($profiles as $profile)
                                        <option value="{{$profile->id}}">{{$profile->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Weight:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="sel1" name="keyword_weight" required>
                                    @foreach($weights as $weight)
                                        <option value="{{$weight->id}}">{{$weight->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="balance" class="col-form-label">Profile Balance:</label>
                            <div class="btn-group btn-toggle btn-group-design">
                                <button type="button" class="btn keyword_balance" value="1">Positive</button>
                                <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_keyword">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="PreviewModal" tabindex="-1" role="dialog" aria-labelledby="PreviewModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="keywordModalLabel">Preview Job </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="custom-block mb-2">
                        <ul class="inline-list">
                            <li><h3 class="pull-left mr-1">{{$jobDetail->job_title}}</h3></li>
                            <li><div class="rating-block">{{--<a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a>--}}{{--<a href=""><img src="{{ URL::to('/') }}/img/star-gray.svg"></a>--}}</div></li><br><li><span class="f-w-5">Posted on <strong>{{$jobDetail->posted_on}}</strong> from <a href="{{$jobDetail->source_url}}" target="_blank" class="text-green"><u>{{$jobDetail->source_url}}</u></a></span></li>

                        </ul>
                    </div>
                    <div class="custom-block mb-2">
                            <ul class="inline-list icon-block-ul">
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/placeholder.svg"></div>
                                        <div class="text-detail"><h4>Location</h4><p>{{$jobDetail->country}}</p></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/clock.svg"></div>
                                        <div class="text-detail"><h4>Type</h4><p>{{$jobDetail->job_type == 1 ? "Full Time": ($jobDetail->job_type == 2 ? "Part Time":($jobDetail->job_type == 3 ? "Internship":""))}}</p></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/suitcase.svg"></div>
                                        <div class="text-detail"><h4>Company</h4><p>{{$jobDetail->company}}</p></div>
                                    </div>
                                </li>
                                @if($jobDetail->contact_name || $jobDetail->contact_phone)
                                    <li>
                                        <div class="iconandtext">
                                            <div class="icon-left"><img src="{{ URL::to('/') }}/img/man-user.svg"></div>
                                            <div class="text-detail"><h4>Contact Person</h4><p>{{$jobDetail->contact_name}},{{$jobDetail->contact_phone}}</p></div>
                                        </div>
                                    </li>
                                @endif
                                <hr>
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/power-button.svg"></div>
                                        <div class="text-detail"><h4>Start</h4><p>{{$jobDetail->posted_on}}</p></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/deadline.svg"></div>
                                        <div class="text-detail"><h4>Deadline</h4><p>{{$jobDetail->close_on}}</p></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="iconandtext">
                                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/lightbulb.svg"></div>
                                        @if($jobDetail->experience_from > 0 || $jobDetail->experience_to > 0)
                                            <div class="text-detail"><h4>Experience</h4><p>{{$jobDetail->experience_from}}-{{$jobDetail->experience_to}} yrs</p></div>
                                        @else
                                            <div class="text-detail"><h4>Experience</h4><p>----</p></div>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </div>
                    <div class="custom-block mb-2 job-dec">
                        <h4>DESCRIPTION</h4>
                        {{-- <textarea class="form-control" rows="5" >
                                             </textarea>--}}
                        <p>
                            {!!html_entity_decode($jobDetail->description, ENT_QUOTES, 'UTF-8')!!}
                        </p>
                    </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            CKEDITOR.replace( 'editor1' );
            $('#deadline,#start').datepicker();

            $("#sel2").val('{{$jobDetail->profile_id}}');
        });


        var status = '{{$jobDetail->status}}';

        $('.approve_job').hide();
        $('.ignore_job').hide();
        if(status == 1 || status == 0){
            $('.ignore_job').show();
        }
        if(status == 2 || status == 0){
            $('.approve_job').show();
        }
        $('.ignore_job').on('click', function () {
            var job_id = '{{$jobDetail->id}}';
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-status") }}',
                data: '{"action":"ignore","job_id":' + job_id + '}',
                contentType: 'application/json',
                success: function (data) {
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);
                    $('.ignore_job').hide();
                    $('.approve_job').show();

                }
            })
        })

        $('.approve_job').on('click', function () {
            var job_id = '{{$jobDetail->id}}';
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-status") }}',
                data: '{"action":"approve","job_id":' + job_id + '}',
                contentType: 'application/json',
                success: function (data) {
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);
                    $('.ignore_job').show();
                    $('.approve_job').hide();
                }
            })
        })

        $(".createjob-btn").on('click',function (e) {
            if ($("#experience_from").val() > $("#experience_to").val()) {
                error_toast("experience from should be less than experience to.");
                return false;
            }
            $("#divLoading").addClass('show');

            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/update-job") }}',
                data: $("#form_edit_job").serialize(),
                success: function (data) {
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);
                    var profile_id = '{{$jobDetail->profile_id}}';
                    var job_id = '{{$jobDetail->id}}';
                    window.location.href = '{{ url("/") }}/admin/profile/'+profile_id+'/job-details/'+job_id;
                }
            })
        });

        var keyword_exist  = false;
        var keyword_id  = 0;
        $("#keyword_title").on("keyup",function () {
            var title = $(this).val();
            var profile_id = '{{$jobDetail->profile_id}}';
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/find-keyword") }}',
                data: '{"keyword_title":"' + title + '","profile_id":'+profile_id+'}',
                contentType: 'application/json',
                success: function (data) {
                    $("#divLoading").removeClass('show');
                    keyword_exist = data.exist;
                    keyword_id = data.keyword_id;
                    if(keyword_exist){
                        $("#keyword_title").parent().nextAll(".form-group").hide();
                    }else{
                        $("#keyword_title").parent().nextAll(".form-group").show();
                    }
                }
            })
        })


        $("#generic_form_add_keyword").on('submit',function (e) {
            e.preventDefault();
            $("#divLoading").addClass('show');

            var profile_id = '{{$jobDetail->profile_id}}';
            var job_id = '{{$jobDetail->id}}';

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/add-jobkeyword") }}',
                data: $("#generic_form_add_keyword").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val()+'&profile_id='+profile_id+'&job_id='+job_id+'&keyword_id='+keyword_id+'&keyword_exist='+keyword_exist,
                success: function (data) {
                    if(data.success){
                        $("#divLoading").removeClass('show');
                        $("#keywordModal").modal('hide');
                        success_toast(data.msg);
                        $('#generic_form_add_keyword').trigger("reset");
                        location.reload();
                    }else{
                        $("#divLoading").removeClass('show');
                        //$("#keywordModal").modal('hide');
                        error_toast(data.msg);
                    }

                }
            })
        });

        $(".remove").on("click",function () {

            $(this).closest('li').hide();
            var keyword_id = $(this).parent().data("keywordid");
            var profile_id = $(this).parent().data("profileid");
            var job_id = '{{$jobDetail->id}}';
            $("#divLoading").addClass('show');
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/delete-jobkeyword") }}',
                data: '{"keyword_id":'+keyword_id+',"profile_id":'+profile_id+',"job_id":'+job_id+',"remove":1}',
                contentType: 'application/json',
                success: function (data) {
                    success_toast(data.msg);
                    $("#divLoading").removeClass('show');
                }
            })
        })

        $(".add").on("click",function () {

            $(this).closest('li').hide();
            var keyword_id = $(this).parent().data("keywordid");
            var profile_id = $(this).parent().data("profileid");
            var job_id = '{{$jobDetail->id}}';
            $("#divLoading").addClass('show');
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/delete-jobkeyword") }}',
                data: '{"keyword_id":'+keyword_id+',"profile_id":'+profile_id+',"job_id":'+job_id+',"remove":0}',
                contentType: 'application/json',
                success: function (data) {
                    success_toast(data.msg);
                    $("#divLoading").removeClass('show');
                }
            })
        })

        $("#update_cron").on("click",function () {
            $("#divLoading").addClass('show');
            var profile_id = '{{$jobDetail->profile_id}}';
            var job_id = '{{$jobDetail->id}}'
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/update-cron") }}',
                data: '{"profile_id":'+profile_id+',"job_id":'+job_id+'}',
                contentType: 'application/json',
                success: function (data) {
                    success_toast(data.msg);
                    $("#divLoading").removeClass('show');
                    location.reload();
                }
            })
        })

        $("#add_job_to_profile").on("click",function () {
            $("#divLoading").addClass('show');
            var profile_id = $("#job_profile_id").val();
            var job_id = '{{$jobDetail->id}}'
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-to-profile") }}',
                data: '{"profile_id":'+profile_id+',"job_id":'+job_id+'}',
                contentType: 'application/json',
                success: function (data) {
                    $("#divLoading").removeClass('show');
                    if(data.success){
                        success_toast(data.msg);
                    }else{
                        error_toast(data.msg);
                    }
                }
            })
        })
    </script>
@endsection
