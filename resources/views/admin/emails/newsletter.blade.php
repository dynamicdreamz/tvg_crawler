<!doctype html>

<html>
<head></head>

<body>

<header style="padding-bottom: 0px;padding-top: 5px;padding-bottom: 5px;position: fixed;top: 0;left: 0;width: 100%;background: #fff;z-index: 1;box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);">
    <div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
        <div style="margin-right: -15px;margin-left: -15px;">
            <div style="width: 100%;padding-right: 15px;padding-left: 15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"">
            <div style="padding: 0px 15px;">
                <div><a href="#" style="display: inline-block;text-decoration: none;">
                        <h2 style="font-weight: bold;font-size: 58px;line-height: 1;color: #0EB0A3;margin-bottom: 5px;border-bottom: 6px solid #0EB0A3;padding-bottom: 0;width: intrinsic;width: -moz-max-content;width: -webkit-max-content;margin-top: 0;font-family: 'Roboto', sans-serif;">Nova</h2>
                        <h3 style="font-weight: normal;font-size: 16px;color: #757575;margin-bottom: 0;margin-top: 0;font-family: 'Roboto', sans-serif;">Jobs Data Management System</h3></a></div>
            </div>
        </div>
    </div>
    </div>
</header>

<div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;padding-top: 65px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">

    <div style="margin-right: -15px;margin-left: -15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
        <div style="width: 100%;padding-right: 15px;padding-left: 15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">
           @foreach($jobs as $job)

            <div>
                <div style="background: #FFFFFF;box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.15);margin-bottom: 20px;width: 100%;vertical-align: middle;display: block;">
                    <div>
                        <div style="padding: 20px;">
                            <div style="font-family: 'Roboto', sans-serif;">
                                <h2 style="color: #3F3F3F;font-size: 24px;margin: 0;line-height: 1.5;"><a href="#" style="color: #3F3F3F;text-decoration: none;">{{$job->job_title}}</a></h2>
                                <div style="font-size: 13px;font-weight: bold;color: #6B6B6B;margin-bottom: 10px;"> <a href="{{$job->source_url}}" target="_blank" style="color: #6B6B6B;text-decoration: none;">{{$job->source_url}}</a></div>
                                <div><p style="font-weight: 300;font-size: 14px;color: #4C4C4C;line-height: 1.5;margin-bottom: 0;">{{$job->description}}</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               @endforeach
        </div>
    </div>

</div>

</body>

</html>