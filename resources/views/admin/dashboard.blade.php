@extends("layouts.master-admin")
@section("content")
<div class="top-noticebar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <h4 id="jobs_collected">74 Job Collected </h4><span>Last Crawling on {{$crawl_date}}</span>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                {{-- <a class="pull-right option-btn" href="#">Crawler Options</a>--}}
                <a class="pull-right option-btn" href="#" id="execute_generic_cron" style="margin-right: 10px;">Generic
                    Cron</a>
                <a class="pull-right option-btn" href="#" id="execute_profile_cron" style="margin-right: 10px;">Profile
                    Cron</a>
                <a href="{{ URL::to('/') }}/admin/add-job" class="pull-right option-btn"
                   style="margin-right: 10px;">Create New Job</a>
            </div>
        </div>
    </div>
</div>
<div class="row mb-2">
    <div class="col-sm-6 col-md-6 col-lg-6">
        <ul class="inline-list">
            <li><h3>{{$profile_name}} Jobs</h3></li>
            <li><a href="{{ URL::to('/') }}/admin/configuration/{{$profile_id}}{{$profile_id > 0 ?'#profiles':''}}"><img
                            src="{{ URL::to('/') }}/img/pencil.svg"> Edit Profiles (AI)</a></li>

        </ul>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="search-box">
            <input type="search" name="" placeholder="Search" id="search_relevant_profile">
            <button><i class="far fa-search"></i></button>
        </div>
    </div>
</div>
<div class="row mb-2">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs design-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#relevant" id="relevant_count" aria-controls="active"
                                                      role="tab" data-toggle="tab">0 Relevant</a></li>
            {{-- @if($profile_id > 0)--}}
            <li role="presentation"><a href="#approved" id="approved_count" aria-controls="approved" role="tab"
                                       data-toggle="tab">0 Approved</a></li>
            <li role="presentation"><a href="#ignored" id="ignored_count" aria-controls="ignored" role="tab"
                                       data-toggle="tab">0 Ignored</a></li>
            {{--@endif--}}
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="relevant">
                <div class="row mb-2">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <ul class="inline-list pull-left relevant_left">
                            {{--@if($profile_id > 0)--}}
                            <li>
                                <div class="checkbox-btn">
                                    <label class="checkbox-design"><input type="checkbox" id="relevant_check"><span
                                                class="checkmark"></span></label>
                                </div>
                            </li>
                            <li><a class="button ignore_all" href="#"><img
                                            src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>
                            <li><a class="button approve_all" href="#"><img
                                            src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>
                            {{--@endif--}}
                        </ul>
                        {{--
                        <div class="sortby-button pull-right">
                            <button class="btn dec" data-weight="desc" id="sort_relevant">Sort by match</button>
                        </div>
                        --}}

<!--                        <div class="select-wrapper pull-right">-->
<!--                            <select id="sort_relevant">-->
<!--                                <option value="">Sort by match</option>-->
<!--                                <option value="desc">Sort by high-to-low</option>-->
<!--                                <option value="asc">Sort by low-to-high</option>-->
<!--                            </select>-->
<!--                        </div>-->
                        {{--
                        <div class="select-wrapper pull-right" style="margin-right: 20px">
                            <select id="relevant_companies">
                                <option value="">Companies</option>
                                @foreach($companies as $company)
                                <option value="{{$company->company}}">{{$company->company}}</option>
                                @endforeach
                            </select>
                        </div>
                        --}}

                        <ul class="inline-list pull-right">
                            <li>
                                <a class="button export_relevant_jobs" href="javascript:">Export Excel</a></li>
                            <li>
                                <a class="button generate_html" href="#" data-toggle="modal"
                                   data-target="#newsletterModalRelevant">Generate HTML for Newsletter</a>
                            </li>
                            <li>
                                <div class="select-wrapper pull-right">
                                    <select id="sort_relevant">
                                        <option value="">Sort by match</option>
                                        <option value="desc">Sort by high-to-low</option>
                                        <option value="asc">Sort by low-to-high</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <input type="hidden" id="profile_id" value="{{$profile_id}}">
                        <div class="job-list-saection" id="relevant_jobs"></div>
                        <div class="text-center">
                            <a href="javascript:void(0);" class="button" id="paginate_relavant_jobs">Show More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="approved">
                <ul class="row mb-2">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <ul class="inline-list pull-left approved_left">
                            <li>
                                <div class="checkbox-btn">
                                    <label class="checkbox-design"><input type="checkbox" id="approved_check"><span
                                                class="checkmark"></span></label>
                                </div>
                            </li>
                            <li><a class="button ignore_all" href="#"><img
                                            src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>
                            {{--
                            <li><a class="button approve_all" href="#"><img
                                            src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>
                            --}}
                        </ul>
                        <ul class="inline-list pull-right">
                            <li>
                                <form action="{{URL::to('/')}}/admin/exportApproveJobs" method="post" id="exportExcel">
                                    @csrf
                                    <input type="hidden" name="profile_id" id="profile_id" value="{{$profile_id}}">
                                    <button class="btn submit" id="export_approved_data">Export Excel</button>
                                </form>
                            </li>
                            <li><a class="button" href="#" id="generate_html" data-toggle="modal"
                                   data-target="#newsletterModal">Generate HTML for Newsletter</a></li>

                            <li>
                                <div class="select-wrapper">
                                    <select id="sort_approved">
                                        <option value="">Sort by match</option>
                                        <option value="desc">Sort by high-to-low</option>
                                        <option value="asc">Sort by low-to-high</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                        {{--
                        <div class="select-wrapper pull-right" style="margin-right: 20px">
                            <select id="approved_companies">
                                <option value="">Companies</option>
                                @foreach($companies as $company)
                                <option value="{{$company->company}}">{{$company->company}}</option>
                                @endforeach
                            </select>
                        </div>
                        --}}

                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <input type="hidden" id="profile_id" value="{{$profile_id}}">
                            <div class="job-list-saection" id="approved_jobs">


                            </div>
                            <div class="text-center">
                                <a href="javascript:void(0);" class="button text-center" id="paginate_approved_jobs">Show
                                    More</a>
                            </div>
                        </div>
                    </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="ignored">
                <div class="row mb-2">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <ul class="inline-list pull-left ignored_left">
                            <li>
                                <div class="checkbox-btn">
                                    <label class="checkbox-design"><input type="checkbox" id="ignored_check"><span
                                                class="checkmark"></span></label>
                                </div>
                            </li>
                            {{--
                            <li><a class="button ignore_all" href="#"><img src="{{ URL::to('/') }}/img/hand-paper.svg">
                                    Ignore</a></li>
                            --}}
                            <li><a class="button approve_all" href="#"><img
                                            src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>
                        </ul>
                        {{--
                        <div class="sortby-button pull-right">
                            <button class="btn dec" data-weight="desc" id="sort_ignored">Sort by match</button>
                        </div>
                        --}}
                        <div class="select-wrapper pull-right">
                            <select id="sort_ignored">
                                <option value="">Sort by match</option>
                                <option value="desc">Sort by high-to-low</option>
                                <option value="asc">Sort by low-to-high</option>
                            </select>
                        </div>
                        {{--
                        <div class="select-wrapper pull-right" style="margin-right: 20px">
                            <select id="ignored_companies">
                                <option value="">Companies</option>
                                @foreach($companies as $company)
                                <option value="{{$company->company}}">{{$company->company}}</option>
                                @endforeach
                            </select>
                        </div>
                        --}}
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <input type="hidden" id="profile_id" value="{{$profile_id}}">
                        <div class="job-list-saection" id="ignored_jobs">
                        </div>
                        <div class="text-center">
                            <a href="javascript:void(0);" class="button" id="paginate_ignored_jobs">Show More</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!----------------------------Newsletter Modal ------------------>
<div class="modal fade " id="newsletterModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Newsletter Html</h4>
            </div>
            <div class="modal-body" id="newsletter_text">

            </div>
            <div class="modal-footer">
                <ul class="inline-list pull-left">
                    <li>
                        <input type="email" class="form-control" name="receiver_email" id="receiver_email">
                    </li>
                    <li>
                    <li>
                        <button type="button" class="btn btn-danger send_newsletter">Test Mail</button>
                    </li>
                    </li>
                </ul>
                <ul class="inline-list pull-right">
                    <li>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-primary copy_newsletter">Copy</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-warning reset_newsletter">Reset</button>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>
<!--------------Newsletter Modal End------------------------->

<script type="text/javascript">

    $(document).on('ready', function () {
        var cookieData = $.cookie('job_type');
        if (cookieData != undefined) {
            var chk_ar = cookieData;
            chk_ar = $.parseJSON(chk_ar);
            $.each(chk_ar, function (index, value) {
                $('input').filter('[data-job_type="' + value + '"]').prop('checked', true);
            });
        }

        var seleted_country_data = $.cookie('seleted_country');

        console.log("1", seleted_country_data);

        if (seleted_country_data != undefined) {
            var chk_ar = seleted_country_data;
            chk_ar = $.parseJSON(chk_ar);
            $.each(chk_ar, function (index, value) {
                $('input.typeahead.tt-input').val(value);
            });
        }

    });
    /*$(function () {*/
    var total_jobs_collected = 0;

    function getRelevantProfileJobs(sort_by = "", offset = 0) {
        $("#divLoading").addClass('show');
        var profile_id = $("#profile_id").val();
        var search_text = $('#search_relevant_profile').val();
        var company = '';
        var country = '';
        if (typeof $('input.typeahead.tt-input').val() != 'undefined') {
            country = $('input.typeahead.tt-input').val();
        } else {
            var seleted_country_data = $.cookie('seleted_country');
            console.log("2", seleted_country_data);
            if (seleted_country_data != undefined) {
                var chk_ar = $.parseJSON(seleted_country_data);
                country = chk_ar[0];
            }
        }


        if ($("#company").val()) {
            company = $("#company").val();
        } else {
            var seleted_company_data = $.cookie('seleted_company');
            if (seleted_company_data != undefined) {
                var chk_ar = $.parseJSON(seleted_company_data);
                company = chk_ar[0];
            }
        }

        var job_type = '';
        if ($('.job_type_filter:checked').map(function () {
            return this.value;
        }).get().join(',')) {
            job_type = $('.job_type_filter:checked').map(function () {
                return this.value;
            }).get().join(',');
        } else {

            var cookieData = $.cookie('job_type');
            if (cookieData != undefined) {
                var chk_ar = cookieData;
                chk_ar = $.parseJSON(chk_ar);
                job_type = chk_ar.join();
            }
        }


        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/profile-jobs") }}',
            data: '{"offset": ' + offset + ', "pagesize": "50","profile_id" : ' + profile_id + ',"search_text": "' + search_text + '","sort_by":"' + sort_by + '","company":"' + company + '","country":"' + country + '","job_type":"' + job_type + '"}',
            contentType: 'application/json',
            success: function (result) {

                total_jobs_collected = result.total_jobs;
                result = result.data;
                var innerhtml = '';


                for (var i = 0; i < result.length; i++) {


                    innerhtml += '<div class="job-list-block" ><div class="jlb-div">' +
                        '<div class="jlb-left">';
                    /*if(result[i]['profile_id'] > 0) {*/
                    innerhtml += '<div class="checkbox-btn">' +
                        '<label class="checkbox-design"><input class="input-checkbox relevant-checkbox" id="relevant_check' + i + '" type="checkbox" name="relevant_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                        '</div>';
                    /*}*/

                    if (result[i]['posted_on'] == "Jan 01, 1970") {
                        result[i]['posted_on'] = '';
                    }
                    var email = '';
                    if(result[i]['contact_email'] != ""){
                        email = '<img src="{{ URL::to('/') }}/img/mail.svg" />';
                    }
                    innerhtml += '<div class="job-details">' +
                        '<h2 class="job-title"><a href="{{ URL::to('/') }}/admin/profile/0/job-details/' + result[i]['id'] + '">' + result[i]['job_title'] + '</a>'+email+'</h2>' +
                        '<div class="job-date">on <a>' + result[i]['posted_on'] + '</a> from <a href="' + result[i]['website'] + '" target="_blank">' + result[i]['website'] + '</a></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-right">' +
                        '<ul class="inline-list">' +
                        '<li><div class="company-logo"><strong>' + result[i]['company'] + '</strong></div></li>' +
                        '<li><div class="job-match tooltip-base">' +
                        '<h3>' + result[i]['relevant_match'] + ' <span>Match</span></h3>' +
                        '<div class="job-progress">' +
                        '<div class="progress">' +
                        '<div class="progress-bar" role="progressbar" aria-valuenow="' + result[i]['relevant_match'] + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + result[i]['relevant_match'] + ';">  </div>' +
                        '</div>' +
                        '</div>' +
                        '</div><div class="tooltip"><div class="tooltiptext">\n' +
                        '  <div class="tooltip-header">\n' +
                        '  <p>Score Calculation:</p>\n' +
                        '  </div>\n' +
                        '  <ul>\n';
                    for (var t = 0; t < result[i]['match_points'].length; t++) {
                        if (result[i]['keywords'][t]) {
                            var point_tag = result[i]['keywords'][t].indexOf('-') > -1 ? "text-red" : "text-green";
                            innerhtml += '<li><p class="text-black">' + result[i]['keywords'][t] + ': <span class="' + point_tag + '">' + result[i]['match_points'][t] + ' Points</span></p></li>\n';

                        }
                    }
                    var changeWord = '';
                    for (var t = 0; t < result[i]['rating_types'].length; t++) {
                        if(result[i]['rating_types'][t] == "location"){
                            changeWord = 'location ('+ result[i]['country'] +')';
                        }
                        else if(result[i]['rating_types'][t] == "jobtype"){
                            if(result[i]['job_type'] == 1){
                                changeWord = 'jobtype (Full Time)';
                            }
                            else if(result[i]['job_type'] == 2){
                                changeWord = 'jobtype (Part Time)';
                            }
                            else{
                                changeWord = 'jobtype (Internship)';
                            }

                        }
                        else{
                            changeWord =  result[i]['rating_types'][t]
                        }
                        innerhtml += '<li><p class="text-black">' + changeWord + ': <span class="text-green">' + result[i]['rating_points'][t] + ' Points</span></p></li>\n';
                    }
                    innerhtml += '</ul>\n' +
                        /*'<div class="tooltop-footer">\n' +
                        '<a>Load More</a>\n' +
                        '</div>\n' +*/
                        '</div></div></li>';
                    /*if(result[i]['profile_id'] > 0) {*/
                    innerhtml += '<li><a class="button ignore_job" data-jobid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>' +
                        '<li><a class="button approve_job" data-jobid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>';
                    /*}*/
                    innerhtml += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-tags-part">' +
                        '<ul class="list-inline">';
                    var tags = 0;
                    for (var k = 0; k < result[i]['keywords'].length; k++) {
                        var red_tag = result[i]['keywords'][k].indexOf('-') > -1 ? "red" : "";
                        var hide = "";
                        if (k > 3) {
                            hide = "hide";
                            tags++;
                        }
                        innerhtml +=
                            '<li class="' + hide + '">' +
                            '<a href="#" class="tag-input ' + red_tag + '" data-keywordId="' + result[i]['keyword_ids'][k] + '"  data-profileId="' + result[i]['profile_id'] + '">' + result[i]['keywords'][k] + '<!--<span class="remove"></span>--></a>' +
                            '</li>';

                    }
                    if (tags > 0) {
                        innerhtml +=
                            '<li>' +
                            '<div class="plusmoretsf">' +
                            '<img class="img-responsive" src="{{ URL::to('/') }}/img/round-close.png" alt="close"><p>' + tags + ' More Tags</p>' +
                            '</div>' +
                            '</li>';
                    }

                    innerhtml += '</ul>' +
                        '</div></div>';


                }

                if (offset > 0) {
                    $("#relevant_jobs").append(innerhtml);
                } else {
                    $("#relevant_jobs").html(innerhtml);
                }


                $("#relevant_count").text(total_jobs_collected + ' Relevant');
                if (total_jobs_collected <= 50 || result.length < 50) {
                    $("#paginate_relavant_jobs").hide();
                } else {
                    $("#paginate_relavant_jobs").show();
                }
                $(".relevant_left").show();
                if (result.length <= 0) {
                    $(".relevant_left").hide();
                }

                $("#divLoading").removeClass('show');

            }
        })
    }

    getRelevantProfileJobs("", 0);
    var count = 0;
    $("#paginate_relavant_jobs").on("click", function () {
        count += 1;
        getRelevantProfileJobs("", count * 50);
    })

    $("#search_relevant_profile").on("keyup", function () {
        getRelevantProfileJobs("", 0);
        getApprovedProfileJobs("", 0)
        getIgnoredProfileJobs("", 0);
    });

    function getApprovedProfileJobs(sort_by = "", offset = 0) {
        // $("#divLoading").addClass('show');
        var profile_id = $("#profile_id").val();
        var search_text = $('#search_relevant_profile').val();
        var company = '';
        var country = '';
        if (typeof $('input.typeahead.tt-input').val() != 'undefined') {
            country = $('input.typeahead.tt-input').val();
        } else {
            var seleted_country_data = $.cookie('seleted_country');
            console.log("a", seleted_country_data);
            if (seleted_country_data != undefined) {
                var chk_ar = $.parseJSON(seleted_country_data);
                country = chk_ar[0];
            }
        }


        if ($("#company").val()) {
            company = $("#company").val();
        } else {
            var seleted_company_data = $.cookie('seleted_company');
            if (seleted_company_data != undefined) {
                var chk_ar = $.parseJSON(seleted_company_data);
                company = chk_ar[0];
            }
        }

        var job_type = '';
        if ($('.job_type_filter:checked').map(function () {
            return this.value;
        }).get().join(',')) {
            job_type = $('.job_type_filter:checked').map(function () {
                return this.value;
            }).get().join(',');
        } else {

            var cookieData = $.cookie('job_type');
            if (cookieData != undefined) {
                var chk_ar = cookieData;
                chk_ar = $.parseJSON(chk_ar);
                job_type = chk_ar.join();
            }
        }
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/profileapproved-jobs") }}',
            data: '{"offset": ' + offset + ', "pagesize": "50","profile_id" : ' + profile_id + ',"search_text": "' + search_text + '","sort_by":"' + sort_by + '","company":"' + company + '","country":"' + country + '","job_type":"' + job_type + '"}',
            contentType: 'application/json',
            success: function (result) {
                if (result['total_keywords'] > 10) {
                    $("#paginate_profile").show();
                } else {
                    $("#paginate_profile").hide();
                }
                total_jobs_collected = result.total_jobs;
                result = result.data;
                var innerhtml = '';

                for (var i = 0; i < result.length; i++) {
                    if (result[i]['posted_on'] == "Jan 01, 1970") {
                        result[i]['posted_on'] = '';
                    }
                    var email = '';
                    if(result[i]['contact_email'] != ""){
                        email = '<img src="{{ URL::to('/') }}/img/mail.svg" />';
                    }
                    innerhtml += '<div class="job-list-block" ><div class="jlb-div">' +
                        '<div class="jlb-left">' +
                        '<div class="checkbox-btn">' +
                        '<label class="checkbox-design"><input class="input-checkbox approved-checkbox" id="relevant_check' + i + '" type="checkbox" name="relevant_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                        '</div>' +
                        '<div class="job-details">' +
                        '<h2 class="job-title"><a href="{{ URL::to('/') }}/admin/profile/0/job-details/' + result[i]['id'] + '">' + result[i]['job_title'] + '</a>'+email+'</h2>' +
                        '<div class="job-date">on <a>' + result[i]['posted_on'] + '</a> from <a href="' + result[i]['website'] + '" target="_blank">' + result[i]['website'] + '</a></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-right">' +
                        '<ul class="inline-list">' +
                        '<li><div class="company-logo"><strong>' + result[i]['company'] + '</strong></div></li>' +
                        '<li><div class="job-match tooltip-base">' +
                        '<h3>' + result[i]['relevant_match'] + ' <span>Match</span></h3>' +
                        '<div class="job-progress">' +
                        '<div class="progress">' +
                        '<div class="progress-bar" role="progressbar" aria-valuenow="' + result[i]['relevant_match'] + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + result[i]['relevant_match'] + ';">  </div>' +
                        '</div>' +
                        '</div>' +
                        '</div><div class="tooltip"><div class="tooltiptext">\n' +
                        '  <div class="tooltip-header">\n' +
                        '  <p>Score Calculation:</p>\n' +
                        '  </div>\n' +
                        '  <ul>\n';
                    for (var t = 0; t < result[i]['match_points'].length; t++) {
                        if (result[i]['keywords'][t]) {
                            var point_tag = result[i]['keywords'][t].indexOf('-') > -1 ? "text-red" : "text-green";
                            innerhtml += '<li><p class="text-black">' + result[i]['keywords'][t] + ': <span class="' + point_tag + '">' + result[i]['match_points'][t] + ' Points</span></p></li>\n';
                        }

                    }
                    var changeWord = '';
                    for (var t = 0; t < result[i]['rating_types'].length; t++) {
                        if(result[i]['rating_types'][t] == "location"){
                            changeWord = 'location ('+ result[i]['country'] +')';
                        }
                        else if(result[i]['rating_types'][t] == "jobtype"){
                            if(result[i]['job_type'] == 1){
                                changeWord = 'jobtype (Full Time)';
                            }
                            else if(result[i]['job_type'] == 2){
                                changeWord = 'jobtype (Part Time)';
                            }
                            else{
                                changeWord = 'jobtype (Internship)';
                            }

                        }
                        else{
                            changeWord =  result[i]['rating_types'][t]
                        }
                        innerhtml += '<li><p class="text-black">' + changeWord + ': <span class="text-green">' + result[i]['rating_points'][t] + ' Points</span></p></li>\n';
                    }
                    innerhtml += '</ul>\n' +
                        /*    '<div class="tooltop-footer">\n' +
                            '<a>Load More</a>\n' +
                            '</div>\n' +*/
                        '</div></div></li>';
                    /*if(result[i]['profile_id'] > 0) {*/
                    innerhtml += '<li><a class="button ignore_job" data-jobid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>';
                    /*'<li><a class="button approve_job" data-jobid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>'*/
                    ;
                    /*}*/
                    innerhtml += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-tags-part">' +
                        '<ul class="list-inline">';
                    var tags = 0;
                    for (var k = 0; k < result[i]['keywords'].length; k++) {
                        var red_tag = result[i]['keywords'][k].indexOf('-') > -1 ? "red" : "";
                        var hide = "";
                        if (k > 3) {
                            hide = "hide";
                            tags++;
                        }
                        innerhtml +=
                            '<li class="' + hide + '">' +
                            '<a href="#" class="tag-input ' + red_tag + '" data-keywordId="' + result[i]['keyword_ids'][k] + '"  data-profileId="' + result[i]['profile_id'] + '">' + result[i]['keywords'][k] + '<!--<span class="remove"></span>--></a>' +
                            '</li>';

                    }
                    if (tags > 0) {
                        innerhtml +=
                            '<li>' +
                            '<div class="plusmoretsf">' +
                            '<img class="img-responsive" src="{{ URL::to('/') }}/img/round-close.png" alt="close"><p>' + tags + ' More Tags</p>' +
                            '</div>' +
                            '</li>';
                    }

                    innerhtml += '</ul>' +
                        '</div></div>';

                }

                if (offset > 0) {
                    $("#approved_jobs").append(innerhtml);
                } else {
                    $("#approved_jobs").html(innerhtml);
                }

                $("#approved_count").text(total_jobs_collected + ' Approved');
                if (total_jobs_collected <= 50 || result.length < 50) {
                    $("#paginate_approved_jobs").hide();
                } else {
                    $("#paginate_approved_jobs").show();
                }

                $(".approved_left").show();
                if (result.length <= 0) {
                    $(".approved_left").hide();
                }


                // $("#divLoading").removeClass('show');
            }
        })
    }

    getApprovedProfileJobs("", 0);
    count = 0;
    $("#paginate_approved_jobs").on("click", function () {
        count += 1;
        getApprovedProfileJobs("", count * 50);
    });

    function getIgnoredProfileJobs(sort_by = "", offset = 0) {

        //$("#divLoading").addClass('show');
        var profile_id = $("#profile_id").val();
        var search_text = $('#search_relevant_profile').val();
        var company = '';
        var country = '';
        if (typeof $('input.typeahead.tt-input').val() != 'undefined') {
            country = $('input.typeahead.tt-input').val();
        } else {
            var seleted_country_data = $.cookie('seleted_country');
            console.log("a2", seleted_country_data);
            if (seleted_country_data != undefined) {
                var chk_ar = $.parseJSON(seleted_country_data);
                country = chk_ar[0];
            }
        }

        if ($("#company").val()) {
            company = $("#company").val();
        } else {
            var seleted_company_data = $.cookie('seleted_company');
            console.log("a1", seleted_company_data);
            if (seleted_company_data != undefined) {
                var chk_ar = $.parseJSON(seleted_company_data);
                company = chk_ar[0];
            }
        }


        var job_type = '';
        if ($('.job_type_filter:checked').map(function () {
            return this.value;
        }).get().join(',')) {
            job_type = $('.job_type_filter:checked').map(function () {
                return this.value;
            }).get().join(',');
        } else {

            var cookieData = $.cookie('job_type');
            if (cookieData != undefined) {
                var chk_ar = cookieData;
                chk_ar = $.parseJSON(chk_ar);
                job_type = chk_ar.join();
            }
        }


        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/profileignored-jobs") }}',
            data: '{"offset": ' + offset + ', "pagesize": "50","profile_id" : ' + profile_id + ',"search_text": "' + search_text + '","sort_by":"' + sort_by + '","company":"' + company + '","country":"' + country + '","job_type":"' + job_type + '"}',
            contentType: 'application/json',
            success: function (result) {


                total_jobs_collected = result.total_jobs;
                result = result.data;
                var innerhtml = '';

                for (var i = 0; i < result.length; i++) {
                    if (result[i]['posted_on'] == "Jan 01, 1970") {
                        result[i]['posted_on'] = '';
                    }
                    var email = '';
                    if(result[i]['contact_email'] != ""){
                        email = '<img src="{{ URL::to('/') }}/img/mail.svg" />';
                    }
                    innerhtml += '<div class="job-list-block" ><div class="jlb-div">' +
                        '<div class="jlb-left">' +
                        '<div class="checkbox-btn">' +
                        '<label class="checkbox-design"><input class="input-checkbox ignored-checkbox" id="relevant_check' + i + '" type="checkbox" name="relevant_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                        '</div>' +
                        '<div class="job-details">' +
                        '<h2 class="job-title"><a href="{{ URL::to('/') }}/admin/profile/0/job-details/' + result[i]['id'] + '">' + result[i]['job_title'] + '</a>'+email+'</h2>' +
                        '<div class="job-date">on <a>' + result[i]['posted_on'] + '</a> from <a href="' + result[i]['website'] + '" target="_blank">' + result[i]['website'] + '</a></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-right">' +
                        '<ul class="inline-list">' +
                        '<li><div class="company-logo"><strong>' + result[i]['company'] + '</strong></div></li>' +
                        '<li><div class="job-match tooltip-base">' +
                        '<h3>' + result[i]['relevant_match'] + ' <span>Match</span></h3>' +
                        '<div class="job-progress">' +
                        '<div class="progress">' +
                        '<div class="progress-bar" role="progressbar" aria-valuenow="' + result[i]['relevant_match'] + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + result[i]['relevant_match'] + ';">  </div>' +
                        '</div>' +
                        '</div>' +
                        '</div><div class="tooltip"><div class="tooltiptext">\n' +
                        '  <div class="tooltip-header">\n' +
                        '  <p>Score Calculation:</p>\n' +
                        '  </div>\n' +
                        '  <ul>\n';
                    for (var t = 0; t < result[i]['match_points'].length; t++) {
                        if (result[i]['keywords'][t]) {
                            var point_tag = result[i]['keywords'][t].indexOf('-') > -1 ? "text-red" : "text-green";
                            innerhtml += '<li><p class="text-black">' + result[i]['keywords'][t] + ': <span class="' + point_tag + '">' + result[i]['match_points'][t] + ' Points</span></p></li>\n';
                        }
                    }
                    var changeWord = '';
                    for (var t = 0; t < result[i]['rating_types'].length; t++) {
                        if(result[i]['rating_types'][t] == "location"){
                            changeWord = 'location ('+ result[i]['country'] +')';
                        }
                        else if(result[i]['rating_types'][t] == "jobtype"){
                            if(result[i]['job_type'] == 1){
                                changeWord = 'jobtype (Full Time)';
                            }
                            else if(result[i]['job_type'] == 2){
                                changeWord = 'jobtype (Part Time)';
                            }
                            else{
                                changeWord = 'jobtype (Internship)';
                            }

                        }
                        else{
                            changeWord =  result[i]['rating_types'][t]
                        }
                        innerhtml += '<li><p class="text-black">' + changeWord + ': <span class="text-green">' + result[i]['rating_points'][t] + ' Points</span></p></li>\n';
                    }
                    innerhtml += '</ul>\n' +
                        /* '<div class="tooltop-footer">\n' +
                         '<a>Load More</a>\n' +
                         '</div>\n' +*/
                        '</div></div></li>';
                    /*if(result[i]['profile_id'] > 0) {*/
                    innerhtml += '<li><a class="button approve_job" data-jobid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/thumb-up-sign.svg"> Approve</a></li>';
                    /*}*/
                    innerhtml += '</ul>' +
                        '</div>' +
                        '</div>' +
                        '<div class="jlb-tags-part">' +
                        '<ul class="list-inline">';
                    var tags = 0;
                    for (var k = 0; k < result[i]['keywords'].length; k++) {
                        var red_tag = result[i]['keywords'][k].indexOf('-') > -1 ? "red" : "";
                        var hide = "";
                        if (k > 3) {
                            hide = "hide";
                            tags++;
                        }
                        innerhtml +=
                            '<li class="' + hide + '">' +
                            '<a href="#" class="tag-input ' + red_tag + '" data-keywordId="' + result[i]['keyword_ids'][k] + '"  data-profileId="' + result[i]['profile_id'] + '">' + result[i]['keywords'][k] + '<!--<span class="remove"></span>--></a>' +
                            '</li>';

                    }
                    if (tags > 0) {
                        innerhtml +=
                            '<li>' +
                            '<div class="plusmoretsf">' +
                            '<img class="img-responsive" src="{{ URL::to('/') }}/img/round-close.png" alt="close"><p>' + tags + ' More Tags</p>' +
                            '</div>' +
                            '</li>';
                    }

                    innerhtml += '</ul>' +
                        '</div></div>';

                }

                if (offset > 0) {
                    $("#ignored_jobs").append(innerhtml);
                } else {
                    $("#ignored_jobs").html(innerhtml);
                }

                $("#ignored_count").text(total_jobs_collected + ' Ignored');
                if (total_jobs_collected <= 50 || result.length < 49) {
                    $("#paginate_ignored_jobs").hide();
                } else {
                    $("#paginate_ignored_jobs").show();
                }

                $(".ignored_left").show();
                if (result.length <= 0) {
                    $(".ignored_left").hide();
                }

                //$("#divLoading").removeClass('show');
            }
        })
    }

    getIgnoredProfileJobs("", 0);
    count = 0;
    $("#paginate_ignored_jobs").on("click", function () {
        count += 1;
        getIgnoredProfileJobs("", count * 50);
    });

    $("#jobs_collected").text(total_jobs_collected + ' Job Collected');

    $(document).on('click', '.ignore_job', function () {
        $("#divLoading").addClass('show');
        var job_id = $(this).data('jobid');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/job-status") }}',
            data: '{"action":"ignore","job_id":' + job_id + '}',
            contentType: 'application/json',
            success: function (data) {
                getRelevantProfileJobs();
                getApprovedProfileJobs();
                getIgnoredProfileJobs();
                $("#divLoading").removeClass('show');
                success_toast(data.msg);

            }
        })
    })

    $(document).on('click', '.approve_job', function () {
        $("#divLoading").addClass('show');
        var job_id = $(this).data('jobid');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/job-status") }}',
            data: '{"action":"approve","job_id":' + job_id + '}',
            contentType: 'application/json',
            success: function (data) {
                getRelevantProfileJobs();
                getApprovedProfileJobs();
                getIgnoredProfileJobs();
                $("#divLoading").removeClass('show');
                success_toast(data.msg);

            }
        })
    })

    $("#relevant_check").on("change", function () {
        $(".input-checkbox").prop('checked', false);
        if ($(this).is(':checked')) {
            $(".relevant-checkbox").prop('checked', true);
        } else {
            $(".relevant-checkbox").prop('checked', false);
        }
    });
    $("#approved_check").on("change", function () {
        $(".input-checkbox").prop('checked', false);
        if ($(this).is(':checked')) {
            $(".approved-checkbox").prop('checked', true);
        } else {
            $(".approved-checkbox").prop('checked', false);
        }
    });
    $("#ignored_check").on("change", function () {
        $(".input-checkbox").prop('checked', false);
        if ($(this).is(':checked')) {
            $(".ignored-checkbox").prop('checked', true);
        } else {
            $(".ignored-checkbox").prop('checked', false);
        }
    })

    $(document).on('click', '.ignore_all', function () {
        var selected_profiles = $('.input-checkbox:checked').serialize();
        if (selected_profiles) {
            $("#divLoading").addClass('show');
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-status-all") }}',
                data: selected_profiles + '&action=ignore',
                success: function (data) {
                    getRelevantProfileJobs("", 0);
                    getApprovedProfileJobs("", 0);
                    getIgnoredProfileJobs("", 0);
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);

                }
            })
        } else {
            error_toast("Please select atleast one job.");
        }
    });

    $(document).on('click', '.approve_all', function () {
        var selected_profiles = $('.input-checkbox:checked').serialize();
        if (selected_profiles) {
            $("#divLoading").addClass('show');
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/job-status-all") }}',
                data: selected_profiles + '&action=approve',
                success: function (data) {
                    getRelevantProfileJobs("", 0);
                    getApprovedProfileJobs("", 0);
                    getIgnoredProfileJobs("", 0);
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);

                }
            })
        } else {
            error_toast("Please select atleast one job.");
        }
    });


    $('#execute_generic_cron').on('click', function () {
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/execute-genericcron") }}',
            success: function (data) {
                if (data.success) {
                    success_toast(data.msg);
                } else {
                    error_toast(data.msg);
                }

            }
        })

        success_toast("Cron execution started. It will take approx 20 to 30 mins to complete.");


    });

    $('#execute_profile_cron').on('click', function () {
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/execute-profilecron") }}',
            success: function (data) {
                if (data.success) {
                    success_toast(data.msg);
                } else {
                    error_toast(data.msg);
                }
            }
        })

        success_toast("Cron execution started. It will take approx 15 mins to complete.");


    });

    /*$(document).on('click', '.tag-input', function () {
        $(this).css('background-color', 'red');
        console.log('keywordid', $(this).data('keywordid'));
    });*/


    $("#sort_relevant").on("change", function () {

        if ($(this).val() != "") {
            getRelevantProfileJobs($(this).val());
        }

    });
    $("#sort_approved").on("change", function () {

        if ($(this).val() != "") {
            getApprovedProfileJobs($(this).val());
        }

    });
    $("#sort_ignored").on("change", function () {

        if ($(this).val() != "") {
            getIgnoredProfileJobs($(this).val());
        }

    });

    $(document).on("click", ".plusmoretsf", function () {

        if ($(this).children('img').attr('alt') == 'close') {
            $(this).children('img').attr('alt', 'open');
            $(this).children('img').attr('src', "{{ URL::to('/') }}/img/round-minus.png");
        } else {
            $(this).children('img').attr('alt', 'close');
            $(this).children('img').attr('src', "{{ URL::to('/') }}/img/round-close.png");
        }

        $(this).closest("ul").find("li").toggleClass('hide');
        $(this).closest("ul").find("li:nth-child(1),li:nth-child(2),li:nth-child(3),li:nth-child(4),li:last-child").toggleClass('hide');
    })

    $(function () {
        var substringMatcher = function (strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function (i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        var countries = ['{!!html_entity_decode($countries, ENT_QUOTES, 'UTF-8')!!}'];

        $('.typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            },
            {
                name: 'countries',
                source: substringMatcher(countries)
            });

        $('.apply-btn').on('click', function () {
            var favorite = [];
            var city = [];
            var country = [];
            $.each($("input[name='job_types']:checked"), function () {
                favorite.push($(this).val());
            });
            $.cookie('job_type', JSON.stringify(favorite));


            var seleted_company = $("#company").val();
            city.push(seleted_company);
            $.cookie('seleted_company', JSON.stringify(city));


            var seleted_country = $('input.typeahead.tt-input').val();
            country.push(seleted_country);
            $.cookie('seleted_country', JSON.stringify(country));

            getRelevantProfileJobs();
            getApprovedProfileJobs();
            getIgnoredProfileJobs();
        });

        function getSelectedJobs(type = "html") {
            var selected_profiles = $('.approved-checkbox:checked').serialize();
            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/selected-jobs") }}',
                data: selected_profiles,
                success: function (result) {
                    var jobs = result.data;
                    var myvar = '<!doctype html>' +
                        '	' +
                        '<html>' +
                        '<head></head>' +
                        '' +
                        '	<body>' +
                        '' +
                        '		<header style="padding-bottom: 0px;padding-top: 5px;padding-bottom: 5px;position: fixed;top: 0;left: 0;width: 100%;background: #fff;z-index: 1;box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.1);">' +
                        '		    <div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">' +
                        '		        <div style="margin-right: -15px;margin-left: -15px;">' +
                        '		            <div style="width: 100%;padding-right: 15px;padding-left: 15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;"">' +
                        '		                <div style="padding: 0px 15px;">' +
                        '		                    <div><a href="#" style="display: inline-block;text-decoration: none;">' +
                        '		                    	<h2 style="font-weight: bold;font-size: 58px;line-height: 1;color: #0EB0A3;margin-bottom: 5px;border-bottom: 6px solid #0EB0A3;padding-bottom: 0;width: intrinsic;width: -moz-max-content;width: -webkit-max-content;margin-top: 0;font-family: \'Roboto\', sans-serif;">Nova</h2>' +
                        '		                    	<h3 style="font-weight: normal;font-size: 16px;color: #757575;margin-bottom: 0;margin-top: 0;font-family: \'Roboto\', sans-serif;">Jobs Data Management System</h3></a></div>' +
                        '		                </div>' +
                        '		            </div>' +
                        '		        </div>' +
                        '		    </div>' +
                        '		</header>' +
                        '' +
                        '		<div style="padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;padding-top: 65px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">' +
                        '' +
                        '			<div style="margin-right: -15px;margin-left: -15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">' +
                        '	            <div style="width: 100%;padding-right: 15px;padding-left: 15px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;">';

                    var innerhtml = '';

                    for (var i = 0; i < jobs.length; i++) {
                        innerhtml += '<div>' +
                            '					   <div style="background: #FFFFFF;box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.15);margin-bottom: 20px;width: 100%;vertical-align: middle;display: block;">' +
                            '					      <div>' +
                            '					         <div style="padding: 20px;">' +
                            '					            <div style="font-family: \'Roboto\', sans-serif;">' +
                            '					               <h2 style="color: #3F3F3F;font-size: 24px;margin: 0;line-height: 1.5;"><a href="#" style="color: #3F3F3F;text-decoration: none;">' + jobs[i]["job_title"] + '</a></h2>' +
                            '					               <div style="font-size: 13px;font-weight: bold;color: #6B6B6B;margin-bottom: 10px;"> <a href="' + jobs[i]["source_url"] + '" target="_blank" style="color: #6B6B6B;text-decoration: none;">' + jobs[i]["source_url"] + '</a></div>' +
                            '					               <div><p style="font-weight: 300;font-size: 14px;color: #4C4C4C;line-height: 1.5;margin-bottom: 0;">' + jobs[i]["description"] + '</p></div>' +
                            '					            </div>' +
                            '					         </div>' +
                            '					      </div>' +
                            '					   </div>' +
                            '					</div>';
                    }

                    myvar += innerhtml;

                    myvar += '	            </div>' +
                        '	        </div>' +
                        '' +
                        '		</div>' +
                        '		' +
                        '	</body>' +
                        '' +
                        '</html>';

                    if (innerhtml != "") {
                        if (type == "html") {
                            $("#newsletter_text").html(myvar);
                        } else {
                            $("#newsletter_text").text(myvar);
                        }
                    } else {
                        $("#newsletter_text").text("No jobs selected, please select jobs from the job list.");
                    }
                }
            })
        }


        $('#newsletterModal').on('show.bs.modal', function (event) {
            getSelectedJobs();
        });

        $(".copy_newsletter").on('click', function () {
            getSelectedJobs("text");
            var range = document.createRange();
            range.selectNode(document.getElementById("newsletter_text"));
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(range);
            document.execCommand("copy")
        });

        $(".reset_newsletter").on('click', function () {
            getSelectedJobs("html");
        });

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        $(".send_newsletter").on('click', function () {
            var selected_profiles = $('.approved-checkbox:checked').serialize();
            var email = $("#receiver_email").val();

            if (isEmail(email)) {
                $("#divLoading").addClass('show');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/send-newsletter") }}',
                    data: selected_profiles + "&email=" + email,
                    success: function (result) {
                        $("#divLoading").removeClass('show');
                        if (result.success) {
                            success_toast(result.msg)
                        } else {
                            error_toast(result.msg)
                        }
                    }
                })
            } else {
                error_toast("Invalid email.");
            }

        })

    })
    $('.export_relevant_jobs').on('click', function () {
        var selected_jobs = $('.relevant-checkbox:checked').serializeArray();
        $("#divLoading").addClass('show');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            },
            url: '{{ url("/admin/export-relevant-jobs") }}',
            data: selected_jobs,
            success: function (result) {
                if (result.success) {
                    success_toast(result.msg)
                    var data = result.data;
                    $("#divLoading").removeClass('show');
                    var csv = 'id, company, website, job_title, posted_on, country, source_url, job_type\n';
                    $.each(data,function (key, value) {

                        csv += value['id']+ ',';
                        csv += value['company']+ ',';
                        csv += value['website']+ ',';
                        csv += value['job_title']+ ',';
                        csv += value['posted_on']+ ',';
                        csv += value['country'] + ',';
                        csv += value['source_url'] + ',';
                        csv += value['job_type'] + ',';
                        csv += "\n";
                    });

                    var element = document.createElement('a');
                    element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv));
                    element.setAttribute('download', 'RelevantJobs.csv');

                    element.style.display = 'none';
                    document.body.appendChild(element);

                    element.click();

                    document.body.removeChild(element);

                } else {
                    $("#divLoading").removeClass('show');
                    error_toast(result.msg)
                }
            }
        })

    });

    /* })*/
</script>
@endsection
