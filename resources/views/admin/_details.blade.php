@extends("layouts.master-admin")
@section("content")
<div class="row mb-2">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ul class="inline-list">
            <li><h3 class="pull-left mr-1">{{$jobDetail->job_title}}</h3></li>
            <li><div class="rating-block">{{--<a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a><a href=""><img src="{{ URL::to('/') }}/img/star-orange.svg"></a>--}}{{--<a href=""><img src="{{ URL::to('/') }}/img/star-gray.svg"></a>--}}</div></li><li> <a href="{{ URL::to('/admin') }}/profile/{{$jobDetail->profile_id}}/editjob/{{$jobDetail->id}}"><img src="{{ URL::to('/') }}/img/pencil.svg"> Edit Job</a></li><br><li><span class="f-w-5">Posted on <strong>{{$jobDetail->posted_on}}</strong> from <a href="{{$jobDetail->source_url}}" target="_blank" class="text-green"><u>{{$jobDetail->source_url}}</u></a></span></li>

        </ul>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-8 col-lg-8">
        <div class="custom-block mb-2">
            <ul class="inline-list icon-block-ul">
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/placeholder.svg"></div>
                        <div class="text-detail"><h4>Location</h4><p>{{$jobDetail->country}}</p></div>
                    </div>
                </li>
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/clock.svg"></div>
                        <div class="text-detail"><h4>Type</h4><p>{{$jobDetail->job_type == 1 ? "Full Time": ($jobDetail->job_type == 2 ? "Part Time":($jobDetail->job_type == 3 ? "Internship":""))}}</p></div>
                    </div>
                </li>
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/suitcase.svg"></div>
                        <div class="text-detail"><h4>Company</h4><p>{{$jobDetail->company}}</p></div>
                    </div>
                </li>
                @if($jobDetail->contact_name || $jobDetail->contact_phone)
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/man-user.svg"></div>
                        <div class="text-detail"><h4>Contact Person</h4><p>{{$jobDetail->contact_name}},{{$jobDetail->contact_phone}}</p></div>
                    </div>
                </li>
                @endif
                <hr>
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/power-button.svg"></div>
                        @if($jobDetail->posted_on == "Jan 01, 1970") 
                        <div class="text-detail"><h4>Start</h4><p></p></div>
                        @else
                        <div class="text-detail"><h4>Start</h4><p>{{$jobDetail->posted_on}}</p></div>
                        @endif                        
                    </div>
                </li>
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/deadline.svg"></div>
                        @if($jobDetail->close_on == "Jan 01, 1970") 
                        <div class="text-detail"><h4>Deadline</h4><p></p></div>
                        @else
                        <div class="text-detail"><h4>Deadline</h4><p>{{$jobDetail->close_on}}</p></div>
                        @endif   
                    </div>
                </li>
                <li>
                    <div class="iconandtext">
                        <div class="icon-left"><img src="{{ URL::to('/') }}/img/lightbulb.svg"></div>
                        @if($jobDetail->experience_from > 0 || $jobDetail->experience_to > 0)
                        <div class="text-detail"><h4>Experience</h4><p>{{$jobDetail->experience_from}}-{{$jobDetail->experience_to}} yrs</p></div>
                            @else
                            <div class="text-detail"><h4>Experience</h4><p>----</p></div>
                            @endif
                    </div>
                </li>
            </ul>
        </div>
        <div class="custom-block mb-2 job-dec">
            <h4>DESCRIPTION</h4>
           {{-- <textarea class="form-control" rows="5" >
								</textarea>--}}
            <p>
                {!!html_entity_decode($jobDetail->description, ENT_QUOTES, 'UTF-8')!!}
            </p>
        </div>
        <div class="custom-block mb-2">
            <h4>FEEDBACK CHANNEL <span class="float-right feedch-pubtext">Published on <span class="text-gray"> {{$jobDetail->posted_on}}</span></span></h4>
            <ul class="inline-list">
                <li><div class="checkbox-btn"><label class="checkbox-design">All<input type="checkbox"><span class="checkmark"></span></label></div></li>
                <li><div class="checkbox-btn"><label class="checkbox-design">Shortcut<input type="checkbox"><span class="checkmark"></span></label></div></li>
                <li><div class="checkbox-btn"><label class="checkbox-design">Hivebrite<input type="checkbox"><span class="checkmark"></span></label></div></li>
                <li><div class="checkbox-btn"><label class="checkbox-design">Email Only<input type="checkbox"><span class="checkmark"></span></label></div></li>
                <li><div class="checkbox-btn"><label class="checkbox-design">Social Media<input type="checkbox"><span class="checkmark"></span></label></div></li>
            </ul>
        </div>
    </div>
    
    <div class="col-sm-12 col-md-4 col-lg-4">
        <div class="custom-block mb-2">
            <a href="javascript:void(0);" class="btn-block button green-bg f-18 update_cron" data-job_profile_id="{{$jobDetail->profile_id}}" style="margin-bottom: 25px">Update Cron</a>
            <ul class="inline-list">
                <li><div class="jobmatchperc"><h2>{{$jobDetail->relevant_match}}</h2><h4>MATCH</h4></div></li>

                <li><a class="button green-bg ignore_job" href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/hand-paper.svg"> Ignore</a></li>

                {{--<li><a class="button green-bg" href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/eye.svg"> Preview</a></li>--}}
            </ul>

                <a href="javascript:void(0);" class="btn-block button green-bg f-18 approve_job">Approve</a>

        </div>
        <div class="custom-block mb-2">
            <h4>Related Keywords</h4>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs design-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#positive_tags" aria-controls="positive_tags" role="tab" data-toggle="tab">Positive ({{$jobDetail->positive_keywords_count}})</a></li>
                <li role="presentation"><a href="#negative_tags" aria-controls="negative_tags" role="tab" data-toggle="tab">Negative ({{$jobDetail->negative_keywords_count}})</a></li>
                <li role="presentation"><a href="#deleted_tags" aria-controls="deleted_tags" role="tab" data-toggle="tab">Deleted ({{count($jobDetail->deleted_keywords)}})</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="positive_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->positive_keywords as $id=>$pos_keyword)
                                <li>
                                    <a href="#" class="tag-input " data-keywordid="{{$id}}" data-profileid="{{$jobDetail->profile_id}}">{{$pos_keyword}}<span class="remove"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
                <div role="tabpanel" class="tab-pane" id="negative_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->negative_keywords as $id=>$neg_keyword)
                                <li>
                                    <a href="#" class="tag-input {{strpos($neg_keyword, '-') !== false ? "red":""}}" data-keywordid="{{$id}}" data-profileid="{{$jobDetail->profile_id}}">{{$neg_keyword}}<span class="remove"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
                <div role="tabpanel" class="tab-pane" id="deleted_tags">
                    <div class="jlb-tags-part">
                        <ul class="list-inline">
                            @foreach($jobDetail->deleted_keywords as $del_keyword)
                                <li>
                                    <a href="#" class="tag-input red" data-keywordid="{{$del_keyword->keyword_id}}" data-profileid="{{isset($del_keyword->profile_id) ? $del_keyword->profile_id:0}}">{{$del_keyword->generic_title}}<span class="add"></span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                </div>
            </div>
        </div>
        <div class="custom-block mb-2 p-lr">
            <div class="col-md-12"><h4>Other Matchings</h4></div>
            <ul>
                @foreach($jobDetail->matching_profiles as $profile)

                    @if($profile['points'] > $threshold)
                <li>
                    <div class="other-matching-side tooltip-base">
                        <h3 class="pull-left">{{$profile['points']}}</h3><span>{{$profile['name']}}</span><a class="button green-bg pull-right" href="{{ URL::to('/') }}/admin/profile-jobs/{{$profile['profile_id']}}"><img src="{{ URL::to('/') }}/img/eye.svg"> Preview</a>
                    </div>
                    <div class="tooltip">
                        <div class="tooltiptext">
                            <div class="tooltip-header">
                                <p>Score Calculation:</p>
                            </div>
                            <ul>
                                @foreach($profile['keywords'] as $k=>$keywords)
                                <li><p class="text-black">{{$keywords}}: <span class="{{ strpos($keywords, '-') !== false ? "text-red":"text-green"}}">{{$profile['keyword_points'][$k]}} Points</span></p></li>
                                @endforeach

                                    @if($profile['rating_types'])
                                    @foreach($profile['rating_types'] as $r=>$ratings)
                                        <li><p class="text-black">{{$ratings}}: <span class="text-green">{{$profile['rating_points'][$r]}} Points</span></p></li>
                                    @endforeach
                                        @endif
                            </ul>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="btn-block button green-bg f-18 update_cron" data-job_profile_id="{{$profile["profile_id"]}}" style="margin-bottom: 25px">Update Cron</a>
                    <div class="custom-block mb-2">
                        <h4>Related Keywords</h4>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs design-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#positive_tags{{$profile["profile_id"]}}" aria-controls="positive_tags" role="tab" data-toggle="tab">Positive ({{$profile['positive_keywords_count']}})</a></li>
                            <li role="presentation"><a href="#negative_tags{{$profile["profile_id"]}}" aria-controls="negative_tags" role="tab" data-toggle="tab">Negative ({{$profile['negative_keywords_count']}})</a></li>
                            <li role="presentation"><a href="#deleted_tags{{$profile["profile_id"]}}" aria-controls="deleted_tags" role="tab" data-toggle="tab">Deleted ({{count($profile['deleted_keywords'])}})</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="positive_tags{{$profile["profile_id"]}}">
                                <div class="jlb-tags-part">
                                    <ul class="list-inline">
                                        @foreach($profile['positive_keywords'] as $id=>$pos_keyword)
                                            <li>
                                                <a href="#" class="tag-input " data-keywordid="{{$id}}" data-profileid="{{$profile["profile_id"]}}">{{$pos_keyword}}<span class="remove"></span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="negative_tags{{$profile["profile_id"]}}">
                                <div class="jlb-tags-part">
                                    <ul class="list-inline">
                                        @foreach($profile['negative_keywords'] as $id=>$neg_keyword)
                                            <li>
                                                <a href="#" class="tag-input {{strpos($neg_keyword, '-') !== false ? "red":""}}" data-keywordid="{{$k}}" data-profileid="{{$profile["profile_id"]}}">{{$neg_keyword}}<span class="remove"></span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="deleted_tags{{$profile["profile_id"]}}">
                                <div class="jlb-tags-part">
                                    <ul class="list-inline">

                                        @foreach($profile['deleted_keywords'] as $del_keyword)
                                            <li>
                                                <a href="#" class="tag-input red" data-keywordid="{{$del_keyword->keyword_id}}" data-profileid="{{isset($del_keyword->profile_id) ? $del_keyword->profile_id:0}}">{{$del_keyword->generic_title}}<span class="add"></span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <button type="button" class="broder-button btn btn-block addkeyword" data-toggle="modal" data-target="#keywordModal" >Add keyword</button>
                            </div>
                        </div>
                    </div>
                </li>
                    @endif
                    @endforeach
                </ul>

        </div>
    </div>
</div>


<div class="modal fade" id="keywordModal" tabindex="-1" role="dialog" aria-labelledby="keywordModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="keywordModalLabel">Add Keyword</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="generic_form_add_keyword">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="keyword_title" name="keyword_title" required>
                    </div>
                    <div class="form-group">
                        <label for="profile" class="col-form-label">Profile:</label>
                        <div class="select-wrapper2">
                            <select class="form-control" id="sel2" name="keyword_profile" required>
                                <option value="0">Generic</option>
                                @foreach($profiles as $profile)
                                    <option value="{{$profile->id}}">{{$profile->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="weight" class="col-form-label">Weight:</label>
                        <div class="select-wrapper2">
                            <select class="form-control" id="sel1" name="keyword_weight" required>
                                @foreach($weights as $weight)
                                    <option value="{{$weight->id}}">{{$weight->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="balance" class="col-form-label">Profile Balance:</label>
                        <div class="btn-group btn-toggle btn-group-design">
                            <button type="button" class="btn keyword_balance" value="1">Positive</button>
                            <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_keyword">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    var status = '{{$jobDetail->status}}';
    $("#sel2").val('{{$jobDetail->profile_id}}');

    $('.approve_job').hide();
    $('.ignore_job').hide();
    if(status == 1 || status == 0){
        $('.ignore_job').show();
    }
    if(status == 2 || status == 0){
        $('.approve_job').show();
    }
    $('.ignore_job').on('click', function () {
        var job_id = '{{$jobDetail->id}}';
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/job-status") }}',
            data: '{"action":"ignore","job_id":' + job_id + '}',
            contentType: 'application/json',
            success: function (data) {
                $("#divLoading").removeClass('show');
                success_toast(data.msg);
                $('.ignore_job').hide();
                $('.approve_job').show();

            }
        })
    })

    $('.approve_job').on('click', function () {
        var job_id = '{{$jobDetail->id}}';
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/job-status") }}',
            data: '{"action":"approve","job_id":' + job_id + '}',
            contentType: 'application/json',
            success: function (data) {
                $("#divLoading").removeClass('show');
                success_toast(data.msg);
                $('.ignore_job').show();
                $('.approve_job').hide();
            }
        })
    })

    var keyword_exist  = false;
    var keyword_id  = 0;
    $("#keyword_title").on("keyup",function () {
        var title = $(this).val();
        var profile_id = '{{$jobDetail->profile_id}}';
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/find-keyword") }}',
            data: '{"keyword_title":"' + title + '","profile_id":'+profile_id+'}',
            contentType: 'application/json',
            success: function (data) {
                $("#divLoading").removeClass('show');
                keyword_exist = data.exist;
                keyword_id = data.keyword_id;
                if(keyword_exist){
                    $("#keyword_title").parent().nextAll(".form-group").hide();
                }else{
                    $("#keyword_title").parent().nextAll(".form-group").show();
                }
            }
        })
    })


    $("#generic_form_add_keyword").on('submit',function (e) {
        e.preventDefault();
        $("#divLoading").addClass('show');

        var profile_id = '{{$jobDetail->profile_id}}';
        var job_id = '{{$jobDetail->id}}';

        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/add-jobkeyword") }}',
            data: $("#generic_form_add_keyword").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val()+'&profile_id='+profile_id+'&job_id='+job_id+'&keyword_id='+keyword_id+'&keyword_exist='+keyword_exist,
            success: function (data) {
                if(data.success){
                    $("#divLoading").removeClass('show');
                    $("#keywordModal").modal('hide');
                    success_toast(data.msg);
                    $('#generic_form_add_keyword').trigger("reset");
                    location.reload();
                }else{
                    $("#divLoading").removeClass('show');
                    //$("#keywordModal").modal('hide');
                    error_toast(data.msg);
                }

            }
        })
    });

    $(".remove").on("click",function () {

        $(this).closest('li').hide();
        var keyword_id = $(this).parent().data("keywordid");
        var profile_id = $(this).parent().data("profileid");
        var job_id = '{{$jobDetail->id}}';
        $("#divLoading").addClass('show');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/delete-jobkeyword") }}',
            data: '{"keyword_id":'+keyword_id+',"profile_id":'+profile_id+',"job_id":'+job_id+',"remove":1}',
            contentType: 'application/json',
            success: function (data) {
                success_toast(data.msg);
                $("#divLoading").removeClass('show');
            }
        })
    })

    $(".add").on("click",function () {

        $(this).closest('li').hide();
        var keyword_id = $(this).parent().data("keywordid");
        var profile_id = $(this).parent().data("profileid");
        var job_id = '{{$jobDetail->id}}';
        $("#divLoading").addClass('show');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/delete-jobkeyword") }}',
            data: '{"keyword_id":'+keyword_id+',"profile_id":'+profile_id+',"job_id":'+job_id+',"remove":0}',
            contentType: 'application/json',
            success: function (data) {
                success_toast(data.msg);
                $("#divLoading").removeClass('show');
            }
        })
    })

    $(".update_cron").on("click",function () {
        //$("#divLoading").addClass('show');
        var profile_id = $(this).data('job_profile_id');
        var job_id = '{{$jobDetail->id}}';
        //console.log("profile_id:-"+profile_id,"job_id:-"+job_id);
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/update-cron") }}',
            data: '{"profile_id":'+profile_id+',"job_id":'+job_id+'}',
            contentType: 'application/json',
            success: function (data) {
                success_toast(data.msg);
                $("#divLoading").removeClass('show');
                location.reload();
            }
        })
    })

</script>
@endsection
