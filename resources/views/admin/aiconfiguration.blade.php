@extends("layouts.master-admin")
@section("content")
    <div class="row mb-2">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3 class="pull-left mr-1">AI Configuration</h3>
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs design-tabs" role="tablist" id="myTab">
                <li role="presentation" class="active">
                    <a href="#generickeyword" aria-controls="home" role="tab" data-toggle="tab">Generic Keywords</a>
                </li>
                <li role="presentation">
                    <a href="#profiles" aria-controls="profile" role="tab" data-toggle="tab">Profiles</a>
                </li>
                <li role="presentation">
                    <a href="#weights" aria-controls="profile" role="tab" data-toggle="tab">Weights</a>
                </li>
                <li role="presentation">
                    <a href="#companies" aria-controls="profile" role="tab" data-toggle="tab">Company</a>
                </li>
                <li role="presentation">
                    <a href="#location" aria-controls="profile" role="tab" data-toggle="tab">Location</a>
                </li>
                <li role="presentation">
                    <a href="#jobtype" aria-controls="profile" role="tab" data-toggle="tab">Job Type</a>
                </li>
<!--                <li role="presentation">-->
<!--                    <a href="#experience" aria-controls="profile" role="tab" data-toggle="tab">Experience</a>-->
<!--                </li>-->
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="generickeyword">

                    <div class="row mb-2">
                        <div class="col-sm-4 col-md-5 col-lg-5">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Generic Keywords</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_keywords">0</strong>
                                        Keywords Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-8 col-md-7 col-lg-7">
                            <ul class="inline-list pull-right ui_modify">
                                <li>
                                    <div class="search-box">
                                        <input type="search" id="search_keyword" name="" placeholder="Search">
                                        <button><i class="far fa-search"></i></button>
                                    </div>
                                </li>
                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#keywordModal"> Add Keyword</a></li>
                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#bulkKeywordModal"> Bulk Keyword</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <ul class="inline-list pull-left">
                                <li>
                                    <div class="checkbox-btn">
                                        <label class="checkbox-design"><input type="checkbox" class="input-checkbox"
                                                                              name="keyword_check"
                                                                              id="keyword_check"><span
                                                    class="checkmark"></span></label>
                                    </div>
                                </li>
                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#keyworddeleteAllModal"> Remove</a></li>

                            </ul>
                            <div class="pull-right">
                                <a class="outline-btn btn sort-by-weight" id="sort-by-weight" data-weight="asc">Sort by name<img src="{{ URL::to('/') }}/img/dropdownarrow-gray.svg"> </a>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="keywordtable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>NAME</th>
                                        <th>WEIGHT</th>
                                        <th>PROFILE BALANCE</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profiles">
                    <div class="row mb-2">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Profile Listing</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/male.svg"> <strong class="total_profiles">0</strong>
                                        Profiles Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <ul class="inline-list pull-right">
                                <li>
                                    <div class="search-box">
                                        <input type="search" id="search_profile" name="" placeholder="Search">
                                        <button><i class="far fa-search"></i></button>
                                    </div>
                                </li>
                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#profileModal"> Create Profile</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <ul class="inline-list pull-left">
                                <li>
                                    <div class="checkbox-btn">
                                        <label class="checkbox-design"><input type="checkbox" class="input-checkbox"
                                                                              name="profile_check"
                                                                              id="profile_check"><span
                                                    class="checkmark"></span></label>
                                    </div>
                                </li>
                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#profiledeleteAllModal"> Unlink Keywords</a></li>
                            </ul>
                            <div class="pull-right">
                                <a class="outline-btn btn sort-by-weight" id="sort-by-name" data-weight="asc">Sort by name<img src="{{ URL::to('/') }}/img/dropdownarrow-gray.svg"> </a>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="profiletable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>NAME</th>
                                        <th># KEYWORDS</th>
                                        <th>LAST CHANGE</th>
                                        <th>ACTION</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="weights">
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Weights</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_weights">0</strong> Weights
                                        Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list pull-right">
                                {{-- <li>
                                     <div class="search-box">
                                         <input type="search" name="" placeholder="Search">
                                         <button><i class="far fa-search"></i></button>
                                     </div>
                                 </li>--}}

                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#weightModal"> Create Weight</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="weighttable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>SR NO</th>
                                        <th>NAME</th>
                                        <th>RATING</th>
                                        <th>ORDER</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="companies">
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Companies</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_companies">0</strong> Companies
                                        Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list pull-right">
                                {{-- <li>
                                     <div class="search-box">
                                         <input type="search" name="" placeholder="Search">
                                         <button><i class="far fa-search"></i></button>
                                     </div>
                                 </li>--}}

                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#companyModal"> Add Company</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="companytable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>SR NO</th>
                                        <th>Company</th>
                                        <th>RATING</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="location">
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Locations</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_locations">0</strong> Locations
                                        Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list pull-right">
                                {{-- <li>
                                     <div class="search-box">
                                         <input type="search" name="" placeholder="Search">
                                         <button><i class="far fa-search"></i></button>
                                     </div>
                                 </li>--}}

                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#locationModal"> Add Location</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="locationtable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>SR NO</th>
                                        <th>COUNTRY</th>
                                        <th>RATING</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="jobtype">
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list">
                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                                <li><h4>Job Type</h4></li>
                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_jobtypes">0</strong> Job Type Ratings
                                        Found</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <ul class="inline-list pull-right">
                                {{-- <li>
                                     <div class="search-box">
                                         <input type="search" name="" placeholder="Search">
                                         <button><i class="far fa-search"></i></button>
                                     </div>
                                 </li>--}}

                                <li><a class="button green-bg" data-toggle="modal"
                                       data-target="#ajobtypeModal"> Add Job type</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="listing-table table-responsive" id="jobtypetable">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>SR NO</th>
                                        <th>TYPE</th>
                                        <th>RATING</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div role="tabpanel" class="tab-pane" id="experience">-->
<!--                    <div class="row mb-2">-->
<!--                        <div class="col-sm-12 col-md-12 col-lg-6">-->
<!--                            <ul class="inline-list">-->
<!--                                {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}-->
<!--                                <li><h4>Experience</h4></li>-->
<!--                                <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_experiences">0</strong> Experience Ratings-->
<!--                                        Found</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                        <div class="col-sm-12 col-md-12 col-lg-6">-->
<!--                            <ul class="inline-list pull-right">-->
<!--                                {{-- <li>-->
<!--                                     <div class="search-box">-->
<!--                                         <input type="search" name="" placeholder="Search">-->
<!--                                         <button><i class="far fa-search"></i></button>-->
<!--                                     </div>-->
<!--                                 </li>--}}-->
<!---->
<!--                                <li><a class="button green-bg" data-toggle="modal"-->
<!--                                       data-target="#experienceModal"> Add Experience</a></li>-->
<!--                            </ul>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="row mb-2">-->
<!--                        <div class="col-sm-12 col-md-12 col-lg-12">-->
<!--                            <div class="listing-table table-responsive" id="experiencetable">-->
<!--                                <table>-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th>SR NO</th>-->
<!--                                        <th>EXPERIENCE</th>-->
<!--                                        <th>RATING</th>-->
<!--                                        <th>ACTION</th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!---->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>


    <!------------Create Generic Keyword Modal--------------------------->
    <div class="modal fade" id="keywordModal" tabindex="-1" role="dialog" aria-labelledby="keywordModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="keywordModalLabel">Add Keyword</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="generic_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" id="keyword_title" name="keyword_title" required>
                        </div>
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Weight:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="sel1" name="keyword_weight" required>
                                    @foreach($weights as $weight)
                                        <option value="{{$weight->id}}">{{$weight->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="balance" class="col-form-label">Profile Balance:</label>
                            <div class="btn-group btn-toggle btn-group-design">
                                <button type="button" class="btn keyword_balance" value="1">Positive</button>
                                <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_keyword">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Create Generic Keyword Modal End--------------------------->

    <!------------Create bulk  Keyword Modal--------------------------->
    <div class="modal fade" id="bulkKeywordModal" tabindex="-1" role="dialog" aria-labelledby="keywordModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <form id="bulk_generic_form" class="bulk_keywords">
                    <div class="modal-body">
                        <div class="modal_title">
                            <h4 class="modal-title" id="keywordModalLabel">Add Keyword(s)</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-form-label">1. Keyword Name</label>
<!--                            <div class="keyword_title" id="keyword_title" name="keyword_title"></div>-->
                            <input type="text" data-role="tagsinput" class="keyword_title" id="keyword_title" name="keyword_title">
                            <div class="group_tags file-upload">
                                <span class="keyword_error">Upload at least more than one keyword!</span>
                                <div class="upload-btn-wrapper">
                                    <button class="file-btn">Upload Only CSV File</button>
                                    <input type="file" name="upload_group_tags" id="upload_group_tags"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="name" class="col-form-label">2. Associate to profiles or generic list</label>
                            <div class="select-option">
                                <select name="profiles" id="profiles" class="form-control" required>
                                    <option value="" disabled selected>Select Generic or  Profiles</option>
                                    <option value="">Generic</option>
                                    @if(isset($profile))
                                        @foreach($profile as $value)
                                            <option value="{{$value->id}}">{{$value->name}} </option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="name" class="col-form-label">3. Set balance and weight</label>
                            <div class="btn-group btn-toggle btn-group-design small-space">
                                <button type="button" class="btn keyword_balance" value="1">Positive</button>
                                <button type="button" class="btn keyword_balance active" value="-1">Negative</button>
                            </div>
                            <div class="select-option">
                                <select class="form-control bulk_weight" id="sel1" name="keyword_weight" required>
                                    <option value="" disabled selected>Select Weight</option>
                                    @foreach($weights as $weight)
                                        <option value="{{$weight->id}}">{{$weight->title}} ({{$weight->rating}}
                                            points)
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="footer_button">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                            <button type="submit" class="btn btn-primary" id="bulk_keyword">Add Keywords</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Create bulk Keyword Modal End--------------------------->
    <!----------------------------Generic Keyword Delete Modal ------------------>
    <div class="modal fade" id="keyworddeleteModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Keyword</h4>
                </div>
                <div class="modal-body keyworddelete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_keyword">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--------------Generic Keyword Delete Modal End------------------------->

    <!----------------------------Generic Keyword All Delete Modal ------------------>
    <div class="modal fade" id="keyworddeleteAllModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Keyword</h4>
                </div>
                <div class="modal-body keyworddelete_confirm">
                    <p>Confirm delete keywords?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete_keywords">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--------------Generic Keyword All Delete Modal End------------------------->

    <!------------Create Profile Modal--------------------------->
    <div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="profileModalLabel">Create Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="profile_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name:</label>
                            <input type="text" class="form-control" id="profile_title" name="profile_title" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="create_profile">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Create Profile Modal End--------------------------->


    <!----------------------------Profile Delete Modal ------------------>
    <div class="modal fade" id="profiledeleteModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Profile</h4>
                </div>
                <div class="modal-body profiledelete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_profile">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--------------Profile Delete Modal End------------------------->

    <!----------------------------Profile All Delete Modal ------------------>
    <div class="modal fade" id="profiledeleteAllModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Profile</h4>
                </div>
                <div class="modal-body profiledelete_confirm">
                    <p>Confirm delete profiles?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger delete_profiles">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--------------Profile All Delete Modal End------------------------->

    <!------------Weight Modal--------------------------->
    <div class="modal fade" id="weightModal" tabindex="-1" role="dialog" aria-labelledby="weightModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="weightModalLabel">Add Weight</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="weight_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Title:</label>
                            <input type="text" class="form-control" id="title" name="title" required>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Rating:</label>
                            <input type="number" class="form-control" min="0" name="rating">
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Sort Order:</label>
                            <input type="number" class="form-control" min="0" name="sort">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_weight">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Weight Modal End--------------------------->

    <!--Weight Delete Modal -->
    <div class="modal fade" id="deleteModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Weight</h4>
                </div>
                <div class="modal-body delete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_weight">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--Weight Delete Modal End-->

    <!------------Company Modal--------------------------->
    <div class="modal fade" id="companyModal" tabindex="-1" role="dialog" aria-labelledby="companyModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="companyModalLabel">Add Job Type Rating</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="company_rating_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Company:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="company" name="company" required>
                                    <option value="">Select</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->company}}">{{$company->company}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Rating:</label>
                            <input type="number" id="company_rating" class="form-control" min="0" name="rating" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_company_rating">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Company Modal End--------------------------->

    <!--Company Delete Modal -->
    <div class="modal fade" id="deleteCompanyModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Company</h4>
                </div>
                <div class="modal-body delete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_company">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--Company Delete Modal End-->

    <!------------Location Modal--------------------------->
    <div class="modal fade" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="weightModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LocationModalLabel">Add Location Rating</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="location_rating_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Country:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="sel1" name="country" required>
                                    <option value="">Select</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country}}">{{$country}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Rating:</label>
                            <input type="number" class="form-control" min="0" max="100" name="rating"  required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_location_rating">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Location Modal End--------------------------->

    <!--Location Delete Modal -->
    <div class="modal fade" id="deleteLocationModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Location</h4>
                </div>
                <div class="modal-body delete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_location">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--Location Delete Modal End-->

    <!------------Job Type Add Modal--------------------------->
    <div class="modal fade" id="ajobtypeModal" tabindex="-1" role="dialog" aria-labelledby="jobtypeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LocationModalLabel">Add Job Type Rating</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="ajobtype_rating_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Job Type:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="ajob_type" name="job_type" required>
                                    <option value="">Select</option>
                                    <option value="1">Full Time</option>
                                    <option value="2">Part Time</option>
                                    <option value="3">Internship</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Rating:</label>
                            <input type="number" id="ajobtype_rating" class="form-control" min="0"  name="rating" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="add_jobtype_rating">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Job Type Add Modal End--------------------------->

    <!------------Job Type Update Modal--------------------------->
    <div class="modal fade" id="jobtypeModal" tabindex="-1" role="dialog" aria-labelledby="jobtypeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="LocationModalLabel">Edit Job Type Rating</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="jobtype_rating_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="weight" class="col-form-label">Job Type:</label>
                            <div class="select-wrapper2">
                                <select class="form-control" id="job_type" name="job_type" required>
                                    <option value="">Select</option>
                                    <option value="1">Full Time</option>
                                    <option value="2">Part Time</option>
                                    <option value="3">Internship</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-form-label">Rating:</label>
                            <input type="number" id="jobtype_rating" class="form-control" min="0" name="rating" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="update_jobtype_rating">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!------------Job Type Modal End--------------------------->

    <!------------Experience Modal--------------------------->
{{--    <div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="experienceModalLabel"--}}
{{--         aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="LocationModalLabel">Add Experience Rating</h5>--}}
{{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <form id="experience_rating_form">--}}
{{--                    <div class="modal-body">--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="sort" class="col-form-label">Experience From:</label>--}}
{{--                            <input type="number" class="form-control" min="0" id="experience_from"  name="experience_from" required>--}}
{{--                                            <span id="experience_from-error1" class="error"></span>--}}

{{--                       </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="sort" class="col-form-label">Experience To:</label>--}}
{{--                            <input type="number" class="form-control" min="0" id="experience_to" name="experience_to" required>--}}
{{--                                            <span id="experience_to-error1" class="error"></span>--}}

{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <label for="sort" class="col-form-label">Rating:</label>--}}
{{--                            <input type="number" class="form-control" min="0" name="rating" required>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>--}}
{{--                        <button type="submit" class="btn btn-primary" id="add_experience_rating">Add</button>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!------------Experience Modal End--------------------------->

    <!--Experience Delete Modal -->
    <div class="modal fade" id="deleteExperienceModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Delete Experience</h4>
                </div>
                <div class="modal-body delete_confirm">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="delete_experience">Delete</button>
                </div>
            </div>

        </div>
    </div>
    <!--Experience Delete Modal End-->


    <script type="text/javascript">
        $(function () {

            /********************************************Weight*************************************************/
            function getWeights() {
                //$("#divLoading").addClass('show');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/weights") }}',
                    contentType: 'application/json',
                    success: function (result) {
                        $('.total_weights').text(result.total_weight);
                        var result = result.data;
                        var innerhtml = '';
                        for (var i = 0; i < result.length; i++) {
                            innerhtml += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td><p class="name-text">' + result[i]['title'] + '</p></td>' +
                                '<td>' + result[i]['rating'] + '</td>' +
                                '<td>' + result[i]['sort'] + '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +

                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteModal" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';
                        }

                        $("#weighttable tbody").html(innerhtml);


                        // $("#divLoading").removeClass('show');
                    }
                })
            }



//----------------------


$('#add_experience_rating').prop('disabled', true);

         var formstatus = true;

  $("#experience_from").keyup(function(){
          var experience_from = parseFloat($('#experience_from').val());
          var experience_to = parseFloat($('#experience_to').val());
   
     
  var charLength = $('#experience_from').val().length;
        if(charLength > 2){
                formstatus = true;            
                $("#experience_from-error1").text("Please enter no more than 2 characters.");
          
        }else{
              formstatus = false;
                $("#experience_from-error1").text("");
        }

            if(experience_from >= experience_to){
                formstatus = true;
                console.log("Must be less than or equal to max carat");
                $("#experience_from-error1").text("Must be less than Experience To");
            }
            if(experience_to <= experience_from){
                formstatus = true;
                     console.log("Must be less than or equal to max carat");
                $("#experience_from-error1").text("Must be less than Experience To");
            }
            if(experience_from < experience_to){
                formstatus = false;

                $("#experience_from-error1").text("");
            }
            if(experience_to > experience_from){
                formstatus = false;

                $("#experience_to-error1").text("");
            }
            // if(experience_to == experience_from ){
            //     formstatus = false;

            //     $("#experience_to-error1").text("");
            //     $("#experience_from-error1").text("");
            // }       
           

         if(experience_from==NaN){
                formstatus = false;
                $("#experience_from-error1").text("");
                
            }   


            console.log("Status1",formstatus);

            if(formstatus==true){
    $('#add_experience_rating').prop('disabled', true);   

             }else{
    $('#add_experience_rating').prop('disabled', false);   

             }   
    });
   

    var formstatus = true;

    $("#experience_to").keyup(function(){
     
        var experience_from = parseFloat($('#experience_from').val());
          var experience_to = parseFloat($('#experience_to').val());

           var charLength = $('#experience_to').val().length;


        if(charLength > 2){

                formstatus = false;            
                $("#experience_to-error1").text("Please enter no more than 2 characters.");
          
        }else{
              formstatus = true;
                $("#experience_to-error1").text("");
        }

         if(experience_to <= experience_from){
                formstatus = false;
                console.log("Must be greater than or equal to min carat");
                $("#experience_to-error1").text("Must be greater than Experience From");
            }



        
            // if(experience_to < experience_from){
            //     formstatus = false;
            //     console.log("Must be greater than or equal to min carat");
            //     $("#experience_to-error1").text("Must be greater than or equal to Experience From");
            // }
            // if(experience_to > experience_from){
            //     formstatus = true;

            //     $("#experience_to-error1").text("");
            // }
            // if(experience_to == experience_from){
            //     formstatus = true;

            //     $("#experience_to-error1").text("");
            //     $("#experience_from-error1").text("");
            // }
            // if(experience_from < experience_to){
            //     formstatus = true;

            //     $("#experience_from-error1").text("");
            // }
         
 
            // if(experience_to==NaN){
            //     formstatus = true;
            //     $("#experience_to-error1").text("");
            // }    



             if(formstatus==false){
    $('#add_experience_rating').prop('disabled', true);   

             }else{
    $('#add_experience_rating').prop('disabled', false);   
            console.log("Status2",formstatus);

             }        
           
    });
  



//------------------------------------------------------------

//-------------------experience_rating_form--------------------

       var experience_rating_form_error;

                    jQuery.validator.setDefaults({
                      debug: true,
                      success: "valid"
                    });
                    $( "#experience_rating_form" ).validate({
                      rules: {
                        experience_from: {
                          required: true,
                          // maxlength: 2
                        },
                        experience_to: {
                          required: true,
                          // maxlength: 2,                        
                        
                      },
                       rating: {
                          required: true,
                           max: 1000
                        }, 
                  },
                    showErrors: function (errorMap, errorList) {
                    experience_rating_form_error = errorList.length;
			            if(experience_rating_form_error>0){
			           $('#add_experience_rating').prop('disabled', true); 
			            }else{
			           $('#add_experience_rating').prop('disabled', false); 
			              }

			        var errors = this.numberOfInvalids();
			        if (errors) {
			           $('#add_experience_rating').prop('disabled', true); 
			        } else {
			           $('#add_experience_rating').prop('disabled', false); 
			        }
			       this.defaultShowErrors();
			    },
		          submitHandler: function(form) {
		                form.submit();
		            }
                    });

//-------------------experience_rating_form  end--------------------

//------------------------------------------------------------------

//-------------------location_rating_form--------------------------


            var location_rating_form_error;


   $( "#location_rating_form" ).validate({
                      rules: {
                        rating: {
                          required: true,
                           max: 1000
                        },                     
                  },
                    showErrors: function (errorMap, errorList) {

  console.log("errorList",errorList);

             location_rating_form_error = errorList.length;
            if(location_rating_form_error>0){
           $('#add_location_rating').prop('disabled', true); 
            }else{
           $('#add_location_rating').prop('disabled', false); 
              }

        var errors = this.numberOfInvalids();
        if (errors) {
           $('#add_location_rating').prop('disabled', true); 
        } else {
           $('#add_location_rating').prop('disabled', false); 
        }
       this.defaultShowErrors();
    },
          submitHandler: function(form) {
                form.submit();
            }
                    });



//-------------------location_rating_form  end--------------------

//-----------------------------------------------------------------

//-------------------company_rating_form--------------------------

      var company_rating_form_error;


   $( "#company_rating_form" ).validate({
                      rules: {
                        rating: {
                          required: true,
                           max: 1000
                        },                     
                  },
                    showErrors: function (errorMap, errorList) {

  console.log("errorList",errorList);

             company_rating_form_error = errorList.length;
            if(company_rating_form_error>0){
           $('#add_company_rating').prop('disabled', true); 
            }else{
           $('#add_company_rating').prop('disabled', false); 
       }
 var errors = this.numberOfInvalids();
        if (errors) {
           $('#add_company_rating').prop('disabled', true); 
        } else {
           $('#add_company_rating').prop('disabled', false); 
        }
       
       this.defaultShowErrors();
    },
          submitHandler: function(form) {
                form.submit();
            }
                    });


//-------------------company_rating_form end--------------------------

//-------------------------------------------------------------------

//-------------------ajobtype_rating_form----------------------------

   var ajobtype_rating_form_error;


   $( "#ajobtype_rating_form" ).validate({
                      rules: {
                        rating: {
                          required: true,
                           max: 1000
                        },                     
                  },
                    showErrors: function (errorMap, errorList) {

  console.log("errorList",errorList);

             ajobtype_rating_form_error = errorList.length;
            if(ajobtype_rating_form_error>0){
           $('#add_jobtype_rating').prop('disabled', true); 
            }else{
           $('#add_jobtype_rating').prop('disabled', false); 
       }
var errors = this.numberOfInvalids();
        if (errors) {
           $('#add_jobtype_rating').prop('disabled', true); 
        } else {
           $('#add_jobtype_rating').prop('disabled', false); 
        }
       
       this.defaultShowErrors();
    },
          submitHandler: function(form) {
                form.submit();
            }
                    });


//-------------------ajobtype_rating_form end----------------------------



//-------------------jobtype_rating_form----------------------------

   var jobtype_rating_form_error;


   $( "#jobtype_rating_form" ).validate({
                      rules: {
                        rating: {
                          required: true,
                           max: 1000
                        },                     
                  },
                    showErrors: function (errorMap, errorList) {

  console.log("errorList",errorList);

             jobtype_rating_form_error = errorList.length;
            if(jobtype_rating_form_error>0){
           $('#update_jobtype_rating').prop('disabled', true); 
            }else{
           $('#update_jobtype_rating').prop('disabled', false); 
       }
var errors = this.numberOfInvalids();
        if (errors) {
           $('#update_jobtype_rating').prop('disabled', true); 
        } else {
           $('#update_jobtype_rating').prop('disabled', false); 
        }
       
       this.defaultShowErrors();
    },
          submitHandler: function(form) {
                form.submit();
            }
                    });


//-------------------jobtype_rating_form end----------------------------





//-------------------jobtype_rating_form----------------------------

   var weight_form_error;


   $( "#weight_form" ).validate({
                      rules: {
                        rating: {
                          required: true,
                           max: 1000
                        },                     
                  },
                    showErrors: function (errorMap, errorList) {

  console.log("errorList",errorList);

             weight_form_error = errorList.length;
            if(weight_form_error>0){
           $('#add_weight').prop('disabled', true); 
            }else{
           $('#add_weight').prop('disabled', false); 
       }
var errors = this.numberOfInvalids();
        if (errors) {
           $('#add_weight').prop('disabled', true); 
        } else {
           $('#add_weight').prop('disabled', false); 
        }
       
       this.defaultShowErrors();
    },
          submitHandler: function(form) {
                form.submit();
            }
                    });


//-------------------jobtype_rating_form end----------------------------






            getWeights();

            $("#weight_form").on("submit", function (e) {
                e.preventDefault();

                 if(weight_form_error<=0){
                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/add-weight") }}',
                    data: $("#weight_form").serialize(),
                    success: function (data) {
                        $("#divLoading").removeClass('show');
                        $("#weightModal").modal('hide');
                        success_toast(data.msg);
                        getWeights();
                        $('#weight_form').trigger("reset");
                        location.reload();
                    }
                });
            }
            else{
            	return;
            }
            });

            var weight_id = 0;
            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                weight_id = button.data('id');
                $('.delete_confirm p').html('Confirm delete weight <strong>' + button.parents('tr').find('td:nth-child(2) > .name-text').text() + '</strong>?');
                //$("#divLoading").addClass('show');
            })

            $('#delete_weight').on('click', function () {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/delete-weight/") }}/' + weight_id,
                    success: function (data) {
                        $("#divLoading").removeClass('show');
                        $("#deleteModal").modal('hide');
                        success_toast(data.msg);
                        getWeights();
                        location.reload();
                    }
                })
            })

            /*********************************************Weight End*************************************************/

            /********************************************Profile***************************************************/

            function getProfiles(offset, order_by) {
                //$("#divLoading").addClass('show');
                var search_text = $('#search_profile').val();
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/profiles") }}',
                    data: '{"offset": ' + offset + ', "pagesize": "10", "order_by": "' + order_by + '", "search_text": "' + search_text + '"}',
                    contentType: 'application/json',
                    success: function (result) {
                        if (result['total_keywords'] > 10) {
                            $("#paginate_profile").show();
                        } else {
                            $("#paginate_profile").hide();
                        }
                        result = result.data;
                        var innerhtml = '';

                        for (var i = 0; i < result.length; i++) {

                            if (result[i]['balance'] == 1) {
                                var positive = "active";
                                var negative = "";
                            } else {
                                positive = "";
                                negative = "active";
                            }

                            var d = new Date(result[i]['updated_at']);

                            var curr_date = d.getDate();

                            var curr_month = d.getMonth() + 1;

                            var curr_year = d.getFullYear();

                            var last_change = curr_date + '/' + curr_month + '/' + curr_year;


                            innerhtml += '<tr>' +
                                '<td>' +
                                '<div class="checkbox-btn">' +
                                '<label class="checkbox-design"><input class="input-checkbox profile-checkbox" id="profile_check' + i + '" type="checkbox" name="profile_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                                '</div></td>' +
                                '<td class="profile_name" id="'+ result[i]['id'] +'">' + result[i]['name'] + '</td>' +
                                '<td>' + result[i]['keywords'] + '</td>' +
                                '<td>' + last_change + '</td>' +
                                '<td class="res-btn-nav">' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg edit_btn" href="#"><img src="{{ URL::to('/') }}/img/pencil-white.svg"> Edit</a></li>' +
                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#profiledeleteModal" data-id="' + result[i]['id'] + '" data-keyword= "'+result[i]['keywords']+'"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '<td><a href="{{ url("/admin/profile_keywords") }}/' + result[i]['id'] + '"></a> </td>' +
                                '</td>' +
                                '</tr>';
                        }

                        if (offset > 0) {
                            $("#profiletable tbody").append(innerhtml);
                        } else {
                            $("#profiletable tbody").html(innerhtml);
                        }

                        for (var i = 0; i < result.length; i++) {
                            $("#profile_weight_" + result[i]['id']).val(result[i]['weight']);
                        }
                        $(".total_profiles").text($("#profiletable tbody").find('tr').length);
                        //$("#divLoading").removeClass('show');
                    }
                })
            }

            getProfiles(0, '');
            var count = 0;
            $("#paginate_profile").on("click", function () {
                count += 1;
                getProfiles(count * 10, 'asc');
            })


            $("#search_profile").on("keyup", function () {
                getProfiles(0, 'asc');
            });

            $("#profile_form").on("submit", function (e) {
                e.preventDefault();

                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/create-profile") }}',
                    data: $("#profile_form").serialize(),
                    success: function (data) {
                        getProfiles(0, 'asc');
                        $("#divLoading").removeClass('show');
                        $("#profileModal").modal('hide');
                        success_toast(data.msg);
                        $('#profile_form').trigger("reset");
                    }
                })
            });

            $(document).on("change", ".profile_weight", function () {
                var profile_weight = $(this).val();
                var profile_id = $(this).attr('name')
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/update-profile") }}',
                    data: '{"profile_weight":' + profile_weight + ',"profile_id":' + profile_id + ',"action":"update_profile_weight"}',
                    contentType: 'application/json',
                    success: function (data) {
                        getProfiles(0, 'asc');
                        $("#divLoading").removeClass('show');
                        success_toast(data.msg);

                    }
                })
            });

            $(document).on("click", ".btn-toggle", function () {
                $(this).find('.profile_bal').toggleClass('active');
                var profile_balance = $(this).find('.profile_bal.active').val();
                var profile_id = $(this).parents('tr').find('.profile_weight').attr('name');
                if (profile_id) {
                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/update-profile") }}',
                        data: '{"profile_balance":' + profile_balance + ',"profile_id":' + profile_id + ',"action":"update_profile_balance"}',
                        contentType: 'application/json',
                        success: function (data) {
                            getProfiles(0, 'asc');
                            $("#divLoading").removeClass('show');
                            success_toast(data.msg);

                        }
                    })
                }
            });

            $("#profile_check").on("change", function () {
                if ($(this).is(':checked')) {
                    $(".profile-checkbox").prop('checked', true);
                } else {
                    $(".profile-checkbox").prop('checked', false);
                }
            })

            $(".delete_profiles").on("click", function () {
                var selected_profiles = $('.profile-checkbox:checked').serialize();
                if (selected_profiles) {
                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/delete-profiles") }}',
                        data: selected_profiles,
                        success: function (data) {
                            getProfiles(0, 'asc');
                            $("#divLoading").removeClass('show');
                            $("#profiledeleteAllModal").modal('hide');
                            success_toast(data.msg);

                        }
                    })
                } else {
                    error_toast('No keyword selected, please select a keyword.');
                }
            })

            var profile_id = 0;
            var keyword_count = 0;
            $('#profiledeleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                profile_id = button.data('id');
                keyword_count = button.data('keyword');
                $('.profiledelete_confirm p').html('Confirm delete Profile <strong>' + button.parents('tr').find('td:nth-child(2) > .name-text').text() + '</strong>?');

            })

            $(document).on("click", "#delete_profile", function () {
              //  console.log('keyword_count',keyword_count);
                if(parseInt(keyword_count) ==  0){
                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/delete-profile") }}/' + profile_id,
                        success: function (data) {
                            getProfiles(0, 'asc');
                            $("#divLoading").removeClass('show');
                            $('#profiledeleteModal').modal('hide');
                            success_toast(data.msg);

                        }
                    })
                }else{
                    error_toast("Please delete profile keywords first.");
                }


            })

            var k_profile_id = '{{$profile_id}}';
            if('{{$profile_id}}' > 0){
                $("#profiles").load("{{ URL::to('/') }}/admin/profile_keywords/"+k_profile_id);
            }

            $(document).on('click', '.edit_btn', function (e) {
                var href = $(this).parents('tr').find("td:last a").attr("href");
                var profile_id = $(this).parents('tr').find("td:nth-child(2)").attr("id");
                k_profile_id =profile_id;

                $("#profiles").load(href);
            });

            $("#sort-by-name").on("click", function () {
                if ($(this).data('weight') == 'asc') {
                    $(this).data('weight', 'desc')
                    $('.sort-by-weight img').css('transform', 'rotate(180deg)');
                } else {
                    $(this).data('weight', 'asc')
                    $('.sort-by-weight img').css('transform', 'rotate(360deg)');
                }
                count = 0;
                getProfiles(0, $(this).data('weight'));
            });

            /********************************************Profile End***************************************************/

            /********************************************Generic Keyword***************************************************/

            function getKeywords(offset, order_by) {
                //$("#divLoading").addClass('show');
                var search_text = $('#search_keyword').val();
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/keywords") }}',
                    data: '{"offset": ' + offset + ', "pagesize": "10","order_by": "' + order_by + '", "search_text": "' + search_text + '"}',
                    contentType: 'application/json',
                    success: function (result) {
                        if (result['total_keywords'] > 10) {
                            $("#paginate_keyword").show();
                        } else {
                            $("#paginate_keyword").hide();
                        }

                        result = result.data;
                        var innerhtml = '';

                        for (var i = 0; i < result.length; i++) {

                            if (result[i]['generic_balance'] == 1) {
                                var positive = "active";
                                var negative = "";
                            } else {
                                positive = "";
                                negative = "active";
                            }


                            innerhtml += '<tr>' +
                                '<td>' +
                                '<div class="checkbox-btn">' +
                                '<label class="checkbox-design"><input class="input-checkbox keyword-checkbox" id="keyword_check' + i + '" type="checkbox" name="keyword_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                                '</div>' +
                                '</td>' +
                                '<td><p class="name-text">' + result[i]['generic_title'] + '</p></td>' +
                                '<td>' +
                                '<div class="select-wrapper2">' +
                                '<select class="keyword_weight" id="keyword_weight_' + result[i]['id'] + '" name="' + result[i]['id'] + '">' +
                                '@foreach($weights as $weight)' +
                                '<option value="{{$weight->id}}">{{$weight->title}}</option>' +
                                '@endforeach' +
                                '</select>' +
                                '</div>' +
                                '</td>' +
                                '<td>' +
                                '<div class="btn-group btn-toggle btn-group-design">' +
                                '<button class="btn keyword_bal ' + positive + '" value="1">Positive</button>' +
                                '<button class="btn keyword_bal ' + negative + '" value="-1">Negative</button>' +
                                '</div>' +
                                '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg" data-toggle="modal" data-target="#keyworddeleteModal" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';

                        }

                        if (offset > 0) {
                            $("#keywordtable tbody").append(innerhtml);
                        } else {
                            $("#keywordtable tbody").html(innerhtml);
                        }

                      //  console.log(result);
                        for (var i = 0; i < result.length; i++) {
                            $("#keyword_weight_" + result[i]['id']).val(result[i]['generic_weight']);
                        }
                        $(".total_keywords").text($("#keywordtable tbody").find('tr').length);

                        //$("#divLoading").removeClass('show');
                    }
                })
            }

            getKeywords(0, 'asc');

            count = 0;
            $("#paginate_keyword").on("click", function () {
                count += 1;
                getKeywords(count * 10, 'asc');
            })

            $("#search_keyword").on("keyup", function () {
                getKeywords(0, 'asc');
            });

            $("#generic_form").on("submit", function (e) {
                e.preventDefault();

                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/add-keyword") }}',
                    data: $("#generic_form").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val(),
                    success: function (data) {
                        getKeywords(0, 'asc');
                        $("#divLoading").removeClass('show');
                        $("#keywordModal").modal('hide');
                        success_toast(data.msg);
                        $('#generic_form').trigger("reset");
                    }
                })
            });

            $(document).on("change", ".keyword_weight", function () {
                var keyword_weight = $(this).val();
                var keyword_id = $(this).attr('name')
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/update-keyword") }}',
                    data: '{"keyword_weight":' + keyword_weight + ',"keyword_id":' + keyword_id + ',"action":"update_keyword_weight"}',
                    contentType: 'application/json',
                    success: function (data) {
                        getKeywords(0, 'asc');
                        $("#divLoading").removeClass('show');
                        success_toast(data.msg);

                    }
                })
            });

            $(document).on("click", ".btn-toggle", function () {
                $(this).find('.keyword_bal').toggleClass('active');
                var keyword_balance = $(this).find('.keyword_bal.active').val();
                var keyword_id = $(this).parents('tr').find('.keyword_weight').attr('name');
                if (keyword_id) {
                    $("#divLoading").addClass('show');
                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/update-keyword") }}',
                        data: '{"keyword_balance":' + keyword_balance + ',"keyword_id":' + keyword_id + ',"action":"update_keyword_balance"}',
                        contentType: 'application/json',
                        success: function (data) {
                            //getKeywords(0, 'asc');
                            $("#divLoading").removeClass('show');
                            success_toast(data.msg);

                        }
                    })
                }
            });

            $("#keyword_check").on("change", function () {
                if ($(this).is(':checked')) {
                    $(".keyword-checkbox").prop('checked', true);
                } else {
                    $(".keyword-checkbox").prop('checked', false);
                }
            })

            $(".delete_keywords").on("click", function () {
                var selected_keywords = $('.keyword-checkbox:checked').serialize();
                if (selected_keywords) {
                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/delete-keywords") }}',
                        data: selected_keywords,
                        success: function (data) {
                            getKeywords(0, 'asc');
                            $("#divLoading").removeClass('show');
                            $('#keyworddeleteAllModal').modal('hide');
                            success_toast(data.msg);

                        }
                    })
                } else {
                    error_toast('No keyword selected, please select a keyword.');
                }

            })

            var keyword_id = 0;
            $('#keyworddeleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                keyword_id = button.data('id');
                $('.keyworddelete_confirm p').html('Confirm delete Profile <strong>' + button.parents('tr').find('td:nth-child(2) > .name-text').text() + '</strong>?');

            });

            $(document).on("click", "#delete_keyword", function () {

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/delete-keyword") }}/' + keyword_id,
                    success: function (data) {
                        getKeywords(0, 'asc');
                        $("#divLoading").removeClass('show');
                        $('#keyworddeleteModal').modal('hide');
                        success_toast(data.msg);

                    }
                })

            })

            $("#sort-by-weight").on("click", function () {
                if ($(this).data('weight') == 'asc') {
                    $(this).data('weight', 'desc')
                    $('.sort-by-weight img').css('transform', 'rotate(180deg)');
                } else {
                    $(this).data('weight', 'asc')
                    $('.sort-by-weight img').css('transform', 'rotate(360deg)');
                }
                count = 0;
                getKeywords(0, $(this).data('weight'));
            });

            /********************************************Generic Keyword End***************************************************/

            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.nav-pills > li > a").on("shown.bs.tab", function (e) {
                var id = $(e.target).attr("href").substr(1);
                if (id == 'profiles') {
                    getProfiles(0, 'asc');
                } else if (id == 'weights') {
                    getWeights(0, 'asc');
                } else {
                    getKeywords(0, 'asc');
                }
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#myTab a[href="' + hash + '"]').tab('show');

            $(document).on("dblclick",".profile_name", function (e) {
                e.stopPropagation();
                var currentEle = $(this);
                var value = $(this).html();
                updateVal(currentEle, value);
            });


            function updateVal(currentEle, value) {
                $(currentEle).html('<input class="thVal" type="text" value="' + value + '" />');
                $(".thVal").focus();
                $(".thVal").keyup(function (event) {
                    if (event.keyCode == 13) {
                        $(currentEle).html($(".thVal").val());
                    }
                });

                $(document).click(function () {
                    var profile_name = $(".thVal").val();
                    $(currentEle).html($(".thVal").val());
                    if(profile_name){
                        $.ajax({
                            method: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '{{ url("/admin/update-profile") }}',
                            data: '{"profile_name":"' + profile_name + '","profile_id":' + $(currentEle).attr('id') + ',"action":"update_profile_name"}',
                            contentType: 'application/json',
                            success: function (data) {
                                // getProfiles(0, 'asc');
                                //$("#divLoading").removeClass('show');
                                success_toast(data.msg);

                            }
                        })
                    }
                });
            }

            function getCompanies() {
                //$("#divLoading").addClass('show');
                console.log('k_profile_id',k_profile_id);
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/company-rating") }}',
                    contentType: 'application/json',
                    data: {"profile_id":k_profile_id},
                    success: function (result) {                   	
                        $('.total_companies').text(result.total_companies);
                        result = result.data;
                        var innerhtml = '';
                        for (var i = 0; i < result.length; i++) {
                            innerhtml += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td class="name-text">' + result[i]['company'] + '</td>' +
                                '<td>' + result[i]['rating'] + '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteCompanyModal" data-company="' + result[i]['company'] + '" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';
                        }

                        $("#companytable tbody").html(innerhtml);


                        // $("#divLoading").removeClass('show');
                    }
                })
            }
            getCompanies()

            $("#company_rating_form").on("submit", function (e) {
                e.preventDefault();


                if(company_rating_form_error<=0){

                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/company-rating") }}',
                    data: $("#company_rating_form").serialize()+'&profile_id='+k_profile_id,
                    success: function (data) {
                        getCompanies();
                        $("#divLoading").removeClass('show');
                        $("#companyModal").modal('hide');
                        success_toast(data.msg);
                        $('#company_rating_form').trigger("reset");
                    }
                })
            }else{
            	return;
            }
            });

            var company = '';
            var company_id = 0;
            $('#deleteCompanyModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                company = button.data('company');
                company_id = button.data('id');
                console.log(company);
                $('.delete_confirm p').html('Confirm delete company <strong>' + company + '</strong>?');
                //$("#divLoading").addClass('show');
            })

            $('#delete_company').on('click', function () {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/company-delete/") }}/' + company_id,
                    data: {"profile_id":k_profile_id},
                    success: function (data) {
                        $("#divLoading").removeClass('show');
                        $("#deleteCompanyModal").modal('hide');
                        success_toast(data.msg);
                        getCompanies();
                    }
                })
            })

            function getLocations() {
                //$("#divLoading").addClass('show');
                console.log('k_profile_id',k_profile_id);
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/location-rating") }}',
                    contentType: 'application/json',
                    data: {"profile_id":k_profile_id},
                    success: function (result) {
                        $('.total_locations').text(result.total_locations);
                        var result = result.data;
                        var innerhtml = '';
                        for (var i = 0; i < result.length; i++) {
                            innerhtml += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td class="name-text">' + result[i]['country'] + '</td>' +
                                '<td>' + result[i]['rating'] + '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteLocationModal" data-country="' + result[i]['country'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';
                        }

                        $("#locationtable tbody").html(innerhtml);


                        // $("#divLoading").removeClass('show');
                    }
                })
            }

            getLocations();

            $("#location_rating_form").on("submit", function (e) {
                e.preventDefault();


console.log("error_length00",location_rating_form_error);

       if(location_rating_form_error<=0){
       
                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/location-rating") }}',
                    data: $("#location_rating_form").serialize()+'&profile_id='+k_profile_id,
                    success: function (data) {
                        getLocations();
                        $("#divLoading").removeClass('show');
                        $("#locationModal").modal('hide');
                        success_toast(data.msg);
                        $('#location_rating_form').trigger("reset");
                    }
                })


            }
            else{
            	return;
            }
            });


            var country = '';
            $('#deleteLocationModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                country = button.data('country');
                console.log(country);
                $('.delete_confirm p').html('Confirm delete location <strong>' + country + '</strong>?');
                //$("#divLoading").addClass('show');
            })

            $('#delete_location').on('click', function () {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/location-delete/") }}/' + country,
                    data: {"profile_id":k_profile_id},
                    success: function (data) {
                        $("#divLoading").removeClass('show');
                        $("#deleteLocationModal").modal('hide');
                        success_toast(data.msg);
                        getLocations();
                    }
                })
            })

            function getJobtypes() {
                //$("#divLoading").addClass('show');
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/all-jobtype-rating") }}',
                    contentType: 'application/json',
                    data: {"profile_id":k_profile_id},
                    success: function (result) {
                        $('.total_jobtypes').text(result.total_jobtypes);
                        var result = result.data;
                        var innerhtml = '';
                        for (var i = 0; i < result.length; i++) {
                            var jobtype  = result[i]['jobtype'] == 1 ? "Full Time": result[i]['jobtype'] == 2 ? "Part Time":"Internship";
                            innerhtml += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td class="name-text">' + jobtype + '</td>' +
                                '<td>' + result[i]['rating'] + '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#jobtypeModal" data-jobtypeid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/pencil-white.svg"> Edit</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';
                        }

                        $("#jobtypetable tbody").html(innerhtml);


                        // $("#divLoading").removeClass('show');
                    }
                })
            }

            getJobtypes();
            var jobtype = 0;
            $('#jobtypeModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                jobtype = button.data('jobtypeid');
                console.log(jobtype);
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/jobtype-rating") }}/'+jobtype,
                    data: {"profile_id":k_profile_id},
                    success: function (data) {
                        data = data.data;
                        console.log(data);

                        $("#jobtype_rating").val(data.rating);
                        $("#job_type").val(data.jobtype);
                    }
                })

            })

            $('#update_jobtype_rating').on('click', function () {

            	
            	if(jobtype_rating_form_error<=0){

                $("#divLoading").addClass('show');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/jobtype-rating-update") }}',
                    data: $("#jobtype_rating_form").serialize()+'&id='+jobtype+'&profile_id='+k_profile_id,
                    success: function (data) {
                        getJobtypes();
                        $("#divLoading").removeClass('show');
                        $("#jobtypeModal").modal('hide');
                        if(data.success){
                            success_toast(data.msg);
                        }else{
                            error_toast(data.msg);
                        }
                    }
                });
            }else{
            	return;
            }
            });

            $('#ajobtype_rating_form').on('submit', function (e) {
                e.preventDefault();
                if(ajobtype_rating_form_error<=0){
                $("#divLoading").addClass('show');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/jobtype-rating") }}',
                    data: $("#ajobtype_rating_form").serialize()+'&profile_id='+k_profile_id,
                    success: function (data) {
                        getJobtypes();
                        $("#divLoading").removeClass('show');
                        $("#ajobtypeModal").modal('hide');
                        $("#ajobtype_rating_form").trigger("reset");
                        if(data.success){
                            success_toast(data.msg);
                        }else{
                            error_toast(data.msg);
                        }
                    }
                })
}
else{
	return;
}
            })


            function getExperiences() {
                //$("#divLoading").addClass('show');
                $.ajax({
                    method: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/experience-rating") }}',
                    contentType: 'application/json',
                    data: {"profile_id":k_profile_id},
                    success: function (result) {
                        $('.total_experiences').text(result.total_experiences);
                        result = result.data;
                        var innerhtml = '';
                        for (var i = 0; i < result.length; i++) {
                            innerhtml += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td class="name-text">' + result[i]['experience_from'] + '-' + result[i]['experience_to'] + ' yrs</td>' +
                                '<td>' + result[i]['rating'] + '</td>' +
                                '<td>' +
                                '<ul class="inline-list">' +
                                '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteExperienceModal" data-id="' + result[i]['id'] + '" data-from="' + result[i]['experience_from'] + '" data-to="' + result[i]['experience_to'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                                '</ul>' +
                                '</td>' +
                                '</tr>';
                        }

                        $("#experiencetable tbody").html(innerhtml);


                        // $("#divLoading").removeClass('show');
                    }
                })
            }

            getExperiences();

            $("#experience_rating_form").on("submit", function (e) {
                e.preventDefault();

                  if(experience_rating_form_error<=0){

                $("#divLoading").addClass('show');

                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/experience-rating") }}',
                    data: $(this).serialize()+'&profile_id='+k_profile_id,
                    success: function (data) {
                        getExperiences();
                        $("#divLoading").removeClass('show');
                        $("#experienceModal").modal('hide');
                        success_toast(data.msg);
                        $('#experience_rating_form').trigger("reset");
                    }
                })
            }
            else{
            	return;
            }
            });

            var experience_id = 0;
            $('#deleteExperienceModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                experience_id = button.data('id');
                //console.log(country);
                $('.delete_confirm p').html('Confirm delete experience <strong>' + button.data('from') + '-' + button.data('to') + ' yrs</strong>?');
                //$("#divLoading").addClass('show');
            })

            $('#delete_experience').on('click', function () {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/experience-delete/") }}/' + experience_id,
                    data: {"profile_id":k_profile_id},
                    success: function (data) {
                        $("#divLoading").removeClass('show');
                        $("#deleteExperienceModal").modal('hide');
                        success_toast(data.msg);
                        getExperiences();
                    }
                })
            })

            /* bulk keyword */

            $("#bulk_generic_form").on("submit", function (e) {
                e.preventDefault();

                $("#divLoading").addClass('show');

                var result = $('#bulk_generic_form .keyword_title').val();
                if(!result){
                    error_toast("Upload or insert at least one keyword!");
                    $('.keyword_error').show();
                    $("#divLoading").removeClass('show');
                }
                else{

                    var balance = $("#bulk_generic_form .keyword_balance.active").val();
                    var profile = $('#bulk_generic_form #profiles').val();
                    var weight = $('#bulk_generic_form .bulk_weight').val();

                    $.ajax({
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ url("/admin/add-bulk-keyword") }}',
                        data: {
                            "keyword_balance" :balance,
                            "keyword_title" : result,
                            "profiles" :profile,
                            "keyword_weight" : weight,

                        },
                        success: function (data) {
                            getKeywords(0, 'asc');
                            $("#divLoading").removeClass('show');
                            $("#bulkKeywordModal").modal('hide');
                            success_toast(data.msg);
                            $('#bulk_generic_form').trigger("reset");
                        }
                    });
                }

            });

            $("#upload_group_tags").change(function() {
                var fd = new FormData();
                fd.append('file', this.files[0]);

                $.ajax({
                    url: '{{ url("/admin/upload_keyword_csv") }}',
                    type: "post",
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: fd,
                    success: function(response) {

                        if(response.status == "error"){
                            error_toast("Upload only CSV file!");
                        }
                        var html = '';
                        $.each(response.data,function (key, value) {
                            $('#bulk_generic_form .keyword_title').tagsinput('add',value);
                        });

                        $('.keyword_error').hide();
                    },
                    error: function() {
                        error_toast("An error occured, please try again.");
                    }
                });
            });

            $('#bulkKeywordModal').on('hidden.bs.modal', function () {
                $('.keyword_data').remove();
                $('.keyword_error').hide();
                $(this).find("select").val('').end();
                $('#bulk_generic_form').trigger("reset");
                $('#bulk_generic_form .keyword_title').tagsinput('removeAll');
            });

        });

    </script>
@endsection
