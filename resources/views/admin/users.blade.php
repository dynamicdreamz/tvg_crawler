@extends("layouts.master-admin")
@section("content")
<div class="row mb-2">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="pull-left mr-1">Admin Users</h3>
    </div>
</div>
<div class="row mb-2">
    <div class="col-sm-12 col-md-12 col-lg-6">
        <ul class="inline-list">
            {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}

            <li><a href="javascript:void(0);"><strong class="total_users">0</strong>
                    users Found</a></li>
        </ul>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6">
        <ul class="inline-list pull-right">
            <li>
                <div class="search-box">
                    <input type="search" id="search_user" name="" placeholder="Search">
                    <button><i class="far fa-search"></i></button>
                </div>
            </li>
            <li><a class="button green-bg" data-toggle="modal"
                   data-target="#userModal"> Add User</a></li>
        </ul>

    </div>
</div>
<div class="row mb-2">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <ul class="inline-list pull-left">
            <li>
                <div class="checkbox-btn">
                    <label class="checkbox-design"><input type="checkbox" class="input-checkbox"
                                                          name="user_check"
                                                          id="user_check"><span
                                class="checkmark"></span></label>
                </div>
            </li>
            <li><a class="button green-bg" data-toggle="modal"
                   data-target="#userdeleteAllModal"> Remove</a></li>

        </ul>
        {{--<div class="pull-right">
            <a class="outline-btn btn sort-by-weight" id="sort-by-weight" data-weight="asc">Sort by name<img src="{{ URL::to('/') }}/img/dropdownarrow-gray.svg"> </a>
        </div>--}}
    </div>
</div>
<div class="row mb-2">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="listing-table table-responsive" id="usertable">
            <table>
                <thead>
                <tr>
                    <th></th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>PROFILE PIC</th>
                    <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalLabel">Add User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="user_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username" class="col-form-label">Username:</label>
                        <input type="text" class="form-control" id="username" name="username" autocomplete="new-username" required>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="col-form-label">First Name:</label>
                        <input type="text" class="form-control" id="user_first_name" name="user_first_name" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-form-label">Last Name:</label>
                        <input type="text" class="form-control" id="user_last_name" name="user_last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <input type="email" class="form-control" id="user_email" name="user_email" required>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password:</label>
                        <input type="password" class="form-control" id="user_password" name="user_password" autocomplete="new-password" required>
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-form-label">Profile Pic:</label>
                        <input type="file" class="form-control" id="user_image" name="user_image" accept="image/jpeg,image/png,image/jpg" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_user">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="userEditModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalLabel">Edit User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="user_update_form">
                <div class="modal-body">
                  <div class="form-group">
                        <label for="first_name" class="col-form-label">User name:</label>
                        <input type="text" class="form-control" name="username" id="username_edit">
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="col-form-label">First Name:</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" >
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-form-label">Last Name:</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-form-label">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-form-label">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="file" class="col-form-label">Profile Pic:</label>
                        <input type="file" class="form-control" id="image" name="user_image" accept="image/jpeg,image/png,image/jpg">
                        <br>
                        <img src="" id="admin_image" >
                    </div>
                    <input type="hidden" id="user_id" name="user_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="update_user">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!----------------------------User Delete Modal ------------------>
<div class="modal fade" id="userdeleteModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body userdelete_confirm">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="delete_user">Delete</button>
            </div>
        </div>

    </div>
</div>
<!--------------User Delete Modal End------------------------->

<div class="modal fade" id="userdeleteAllModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete user</h4>
            </div>
            <div class="modal-body userdelete_confirm">
                <p>Confirm delete users?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete_users">Delete</button>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    function getUsers(offset=0, order_by='desc') {
        //$("#divLoading").addClass('show');
        var search_text = $('#search_user').val();
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/all-users") }}',
            data: '{"offset": ' + offset + ', "pagesize": "50","order_by": "' + order_by + '", "search_text": "' + search_text + '"}',
            contentType: 'application/json',
            success: function (result) {
                if (result.total_users > 50) {
                    $("#paginate_user").show();
                } else {
                    $("#paginate_user").hide();
                }

                result = result.data;
                var innerhtml = '';

                for (var i = 0; i < result.length; i++) {

                    innerhtml += '<tr>' +
                        '<td>' +
                        '<div class="checkbox-btn">' +
                        '<label class="checkbox-design"><input class="input-checkbox user-checkbox" id="user_check' + i + '" type="checkbox" name="user_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>' +
                        '</div>' +
                        '</td>' +
                        '<td><p>' + result[i]['first_name'] + ' ' + result[i]['last_name'] + '</p></td>' +
                        '<td><p>' + result[i]['email'] + '</p></td>' +
                        '<td><img src="{{ URL::to('/') }}/uploads/thumb/' + result[i]['profile_pic'] + '" alt="'+ result[i]['image'] +'"></td>' +
                        '<td>' +
                        '<ul class="inline-list">' +
                        '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#userEditModal" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/pencil-white.svg"> Edit</a></li>' +
                        '<li><a class="button green-bg" data-toggle="modal" data-target="#userdeleteModal" data-name="'+ result[i]['first_name'] + ' ' + result[i]['last_name'] +'" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                        '</ul>' +
                        '</td>' +
                        '</tr>';

                }

                if (offset > 0) {
                    $("#usertable tbody").append(innerhtml);
                } else {
                    $("#usertable tbody").html(innerhtml);
                }

                //  console.log(result);
                for (var i = 0; i < result.length; i++) {
                    $("#user_weight_" + result[i]['id']).val(result[i]['generic_weight']);
                }
                $(".total_users").text($("#usertable tbody").find('tr').length);

                //$("#divLoading").removeClass('show');
            }
        })
    }
    getUsers();
    $("#search_user").on("keyup", function () {
        getUsers();
    });

    $("#user_form").on("submit", function (e) {
        e.preventDefault();

        var ext = $('#user_image').val().split('.').pop().toLowerCase();
        if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
            error_toast('The profile pic must be a file of type: jpeg, jpg, png.');
            return false;
        }

        $("#divLoading").addClass('show');
        var myform = document.getElementById("user_form");
        var fd = new FormData(myform);

        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/add-user") }}',
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                getUsers(0);
                $("#divLoading").removeClass('show');
                if(data.success){

                    $("#userModal").modal('hide');
                    success_toast(data.msg);
                    $('#user_form').trigger("reset");
                }else{
                    error_toast(data.msg);
                }

            }
        })
    });

    $('#userEditModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var admin_id = button.data('id');
        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/admin-data") }}',
            data: {"admin_id":admin_id},
            success: function (result) {
                console.log("DAta",result);
                result = result.data;
                $("#username_edit").val(result.username);                
                $("#first_name").val(result.first_name);
                $("#last_name").val(result.last_name);
                $("#email").val(result.email);
                $("#user_id").val(result.id);
                $("#admin_image").attr('src','{{ URL::to('/') }}/uploads/thumb/' + result['profile_pic'] + '');
            }
        })

    });

    $("#user_update_form").on("submit", function (e) {
        e.preventDefault();

        var myform = document.getElementById("user_update_form");
        var fd = new FormData(myform);

        if(fd["user_image"]){
            var ext = $('#image').val().split('.').pop().toLowerCase();
            if($.inArray(ext, ['png','jpg','jpeg']) == -1) {
                error_toast('The profile pic must be a file of type: jpeg, jpg, png.');
                return false;
            }
        }
        $("#divLoading").addClass('show');

        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/update-user") }}',
            data: fd,
            cache: false,
            processData: false,
            contentType: false,
            success: function (data) {
                getUsers(0);
                $("#divLoading").removeClass('show');
                if(data.success){

                    $("#userEditModal").modal('hide');
                    success_toast(data.msg);
                    $('#user_form').trigger("reset");
                }else{
                    error_toast(data.msg);
                }

            }
        })
    });

    var user_id = 0;
    $('#userdeleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        user_id = button.data('id');
        var user_name = button.data('name');
        $('.userdelete_confirm p').html('Confirm delete User <strong>' + user_name + '</strong>?');

    });

    $(document).on("click", "#delete_user", function () {

        $.ajax({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{{ url("/admin/delete-user") }}/' + user_id,
            success: function (data) {
                getUsers();
                $("#divLoading").removeClass('show');
                $('#userdeleteModal').modal('hide');
                success_toast(data.msg);

            }
        })

    })

    $("#user_check").on("change", function () {
        if ($(this).is(':checked')) {
            $(".user-checkbox").prop('checked', true);
        } else {
            $(".user-checkbox").prop('checked', false);
        }
    })

    $(".delete_users").on("click", function () {
        var selected_users = $('.user-checkbox:checked').serialize();
        if (selected_users) {
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/delete-users") }}',
                data: selected_users,
                success: function (data) {
                    getUsers();
                    $("#divLoading").removeClass('show');
                    $('#userdeleteAllModal').modal('hide');
                    success_toast(data.msg);

                }
            })
        } else {
            error_toast('No user selected, please select a user.');
        }

    });

</script>
@endsection
