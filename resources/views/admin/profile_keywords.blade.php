<div class="row mb-2">

<div class="col-sm-12 col-md-12 col-lg-12">
    <ul class="inline-list">
        {{--<li><a class="add-button" href="#">ADD PROFILE <img src="img/plus-gray.svg"></a></li>--}}
        <li><a href="{{ URL::to('/') }}/admin/configuration/0#profiles" class="text-gray" id="all_profiles">
                <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.5 4.875H2.89375L6.3875 1.38125L5.5 0.5L0.5 5.5L5.5 10.5L6.38125 9.61875L2.89375 6.125H10.5V4.875Z"
                          fill="#999999"/>
                    </path>
                </svg>
                All Profiles</a></li>
        <li><h4>{{$profile_name}} Profile</h4></li>
    </ul>
    <ul class="nav nav-tabs design-tabs-1" role="tablist" id="myTab">
        <li role="presentation" class="active">
            <a href="#profilekeyword" aria-controls="home" role="tab" data-toggle="tab">Profile Keywords</a>
        </li>
        <li role="presentation">
            <a href="#profilecompany" aria-controls="profile" role="tab" data-toggle="tab">Company</a>
        </li>
        <li role="presentation">
            <a href="#profilejobtype" aria-controls="profile" role="tab" data-toggle="tab">Job Type</a>
        </li>
        <li role="presentation">
            <a href="#profilelocation" aria-controls="profile" role="tab" data-toggle="tab">Location</a>
        </li>
{{--        <li role="presentation">--}}
{{--            <a href="#profileexperience" aria-controls="profile" role="tab" data-toggle="tab">Experience</a>--}}
{{--        </li>--}}
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="profilekeyword">
            <div class="row mb-2">
                <div class="col-sm-4 col-md-5 col-lg-5">
                    <ul class="inline-list">
                        <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_keywords">0</strong> Keywords Found</a></li>
                    </ul>
                </div>
                <div class="col-sm-8 col-md-7 col-lg-7">
                    <ul class="inline-list pull-right ui_modify">
                        <li>
                            <div class="search-box">
                                <input type="search" id="search_pkeyword" name="" placeholder="Search">
                                <button><i class="far fa-search"></i></button>
                            </div>
                        </li>
                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#pkeywordModal"> Add Keyword</a></li>
                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#pBulkKeywordModal"> Bulk Keyword</a></li>
                    </ul>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="inline-list pull-left">
                        <li>
                            <div class="checkbox-btn">
                                <label class="checkbox-design"><input type="checkbox" class="input-checkbox" name="keyword_check" id="pkeyword_check"><span class="checkmark"></span></label>
                            </div>
                        </li>
                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#pkeyworddeleteAllModal"> Remove</a></li>

                    </ul>
                    <div class="pull-right">
                        <a class="outline-btn btn sort-by-weight" id="sort-by-pkweight" data-weight="asc">Sort by name<img src="{{ URL::to('/') }}/img/dropdownarrow-gray.svg"> </a>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="listing-table table-responsive" id="pkeywordtable">
                        <table>
                            <thead>
                            <tr>
                                <th></th>
                                <th>NAME</th>
                                <th>WEIGHT</th>
                                <th>PROFILE BALANCE</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profilecompany">
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list">
                        {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                        <li><h4>Companies</h4></li>
                        <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_companies">0</strong> Companies
                                Found</a></li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list pull-right">
                        {{-- <li>
                             <div class="search-box">
                                 <input type="search" name="" placeholder="Search">
                                 <button><i class="far fa-search"></i></button>
                             </div>
                         </li>--}}

                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#companyModal"> Add Company</a></li>
                    </ul>

                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="listing-table table-responsive" id="companytable">
                        <table>
                            <thead>
                            <tr>
                                <th>SR NO</th>
                                <th>Company</th>
                                <th>RATING</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profilelocation">
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list">
                        {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                        <li><h4>Locations</h4></li>
                        <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_locations">0</strong> Locations
                                Found</a></li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list pull-right">
                        {{-- <li>
                             <div class="search-box">
                                 <input type="search" name="" placeholder="Search">
                                 <button><i class="far fa-search"></i></button>
                             </div>
                         </li>--}}

                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#locationModal"> Add Location</a></li>
                    </ul>

                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="listing-table table-responsive" id="locationtable">
                        <table>
                            <thead>
                            <tr>
                                <th>SR NO</th>
                                <th>COUNTRY</th>
                                <th>RATING</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profilejobtype">
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list">
                        {{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
                        <li><h4>Job Type</h4></li>
                        <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_jobtypes">0</strong> Job Type Ratings
                                Found</a></li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <ul class="inline-list pull-right">
                        <li><a class="button green-bg" data-toggle="modal"
                               data-target="#ajobtypeModal"> Add JobType</a></li>
                    </ul>

                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="listing-table table-responsive" id="jobtypetable">
                        <table>
                            <thead>
                            <tr>
                                <th>SR NO</th>
                                <th>TYPE</th>
                                <th>RATING</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
{{--        <div role="tabpanel" class="tab-pane" id="profileexperience">--}}
{{--            <div class="row mb-2">--}}
{{--                <div class="col-sm-12 col-md-12 col-lg-6">--}}
{{--                    <ul class="inline-list">--}}
{{--                        --}}{{--<li><a class="add-button" href="#">ADD PROFILE <img src="{{ URL::to('/') }}/img/plus-gray.svg"></a></li>--}}
{{--                        <li><h4>Experience</h4></li>--}}
{{--                        <li><a href="javascript:void(0);"><img src="{{ URL::to('/') }}/img/tags.svg"> <strong class="total_experiences">0</strong> Experience Ratings--}}
{{--                                Found</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--                <div class="col-sm-12 col-md-12 col-lg-6">--}}
{{--                    <ul class="inline-list pull-right">--}}
{{--                        --}}{{-- <li>--}}
{{--                             <div class="search-box">--}}
{{--                                 <input type="search" name="" placeholder="Search">--}}
{{--                                 <button><i class="far fa-search"></i></button>--}}
{{--                             </div>--}}
{{--                         </li>--}}

{{--                        <li><a class="button green-bg" data-toggle="modal" data-target="#experienceModal"> Add Experience</a></li>--}}
{{--                    </ul>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="row mb-2">--}}
{{--                <div class="col-sm-12 col-md-12 col-lg-12">--}}
{{--                    <div class="listing-table table-responsive" id="experiencetable">--}}
{{--                        <table>--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th>SR NO</th>--}}
{{--                                <th>EXPERIENCE</th>--}}
{{--                                <th>RATING</th>--}}
{{--                                <th>ACTION</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}

{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>

</div>
</div>

<!------------Create Generic Keyword Modal--------------------------->
<div class="modal fade" id="pkeywordModal" tabindex="-1" role="dialog" aria-labelledby="pkeywordModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="pkeywordModalLabel">Add Keyword</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="pgeneric_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="col-form-label">Title:</label>
                        <input type="text" class="form-control" id="keyword_title" name="keyword_title" required>
                    </div>
                    <div class="form-group">
                        <label for="weight" class="col-form-label">Weight:</label>
                        <div class="select-wrapper2">
                            <select class="form-control" id="sel1" name="keyword_weight" required>
                                @foreach($weights as $weight)
                                    <option value="{{$weight->id}}">{{$weight->title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="balance" class="col-form-label">Profile Balance:</label>
                        <div class="btn-group btn-toggle btn-group-design">
                            <button type="button" class="btn keyword_balance" value="1">Positive</button>
                            <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="add_keyword">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!------------Create Generic Keyword Modal End--------------------------->
<!------------Create bulk Keyword Modal--------------------------->
<div class="modal fade" id="pBulkKeywordModal" tabindex="-1" role="dialog" aria-labelledby="pBulkKeywordModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="pgeneric_bulk_form" class="bulk_keywords">
                <div class="modal-body">
                    <div class="modal_title">
                        <h4 class="modal-title" id="keywordModalLabel">Add Keyword(s)</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-form-label">1. Keyword Name</label>
                        <input type="text" data-role="tagsinput" class="keyword_title" id="keyword_title" name="keyword_title">
<!--                        <div class="keyword_title" id="keyword_title" name="keyword_title"></div>-->
                        <div class="group_tags file-upload">
                            <span class="keyword_error">Upload at least more than one keyword!</span>
                            <div class="upload-btn-wrapper">
                                <button class="file-btn">Upload Only CSV File</button>
                                <input type="file" name="upload_group_tags" id="upload_group_tags"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="name" class="col-form-label">2. Associate to profiles</label>
                        <input type="text" class="pro_title" value="{{$profile_name}}" disabled="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-form-label">3. Set balance and weight</label>
                        <div class="balanceData">
                            <button type="button" class="btn keyword_balance" value="1">Positive</button>
                            <button type="button" class="btn keyword_balance  active" value="-1">Negative</button>
                        </div>
                        <div class="select-option">
                            <select class="form-control bulk_weight" id="sel1" name="keyword_weight" required>
                                <option value="" disabled selected>Select Weight</option>
                                @foreach($weights as $weight)
                                    <option value="{{$weight->id}}">{{$weight->title}} ({{$weight->rating}}
                                        points)
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="footer_button">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                        <button type="submit" class="btn btn-primary" id="profile_bulk_keyword">Add Keywords</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!------------Create bulk Keyword Modal End--------------------------->
<!----------------------------Generic Keyword Delete Modal ------------------>
<div class="modal fade" id="pkeyworddeleteModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Keyword</h4>
            </div>
            <div class="modal-body keyworddelete_confirm">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" id="delete_keyword">Delete</button>
            </div>
        </div>

    </div>
</div>
<!--------------Generic All Keyword Delete Modal End------------------------->

<!----------------------------Generic Keyword Delete Modal ------------------>
<div class="modal fade" id="pkeyworddeleteAllModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Keyword</h4>
            </div>
            <div class="modal-body keyworddelete_confirm">
                <p>Confirm delete keywords.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete_pkeywords">Delete</button>
            </div>
        </div>

    </div>
</div>
<!--------------Generic Keyword Delete Modal End------------------------->

<script type="text/javascript">
    $(function () {

        // $('.btn-toggle').on('click',function() {
        //     $(this).find('.btn').toggleClass('active');
        // });

        $('.balanceData').on('click',function() {
            $(this).find('.keyword_balance').toggleClass('active');
        });

        /********************************************Generic Keyword***************************************************/
        var keyword_profile_id = '{{$keyword_profile_id}}';

        function getPKeywords(offset, order_by) {
            // $("#divLoading").addClass('show');
            var search_text = $('#search_pkeyword').val();

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/profile-keywords") }}',
                data: '{"keyword_profile_id":' + keyword_profile_id + ',"offset": ' + offset + ', "pagesize": "10","order_by": "' + order_by + '", "search_text": "' + search_text + '"}',
                contentType: 'application/json',
                success: function (result) {
                    if(result['total_keywords'] > 10){
                        $("#paginate_profile_keyword").show();
                    }else{
                        $("#paginate_profile_keyword").hide();
                    }
                    result = result.data;
                    var innerhtml = '';
                    if(result.length == 0){
                        $("#pkeyword_check").prop("checked",false);
                    }
                
                    for (var i = 0; i < result.length; i++) {

                        if (result[i]['generic_balance'] == 1) {
                            var positive = "active";
                            var negative = "";
                        } else {
                            positive = "";
                            negative = "active";
                        }

                        innerhtml += '<tr>'+
                            '<td>'+
                            '<div class="checkbox-btn">'+
                            '<label class="checkbox-design"><input class="input-checkbox pkeyword-checkbox" id="pkeyword_check' + i + '" type="checkbox" name="pkeyword_checkbox[]" value="' + result[i]['id'] + '"><span class="checkmark"></span></label>'+
                            '</div>'+
                            '</td>'+
                            '<td><p class="name-text">' + result[i]['generic_title'] + '</p></td>'+
                            '<td>'+
                            '<div class="select-wrapper2">'+
                            '<select class="pkeyword_weight" id="pkeyword_weight_' + result[i]['id'] + '" name="' + result[i]['id'] + '">' +
                            '@foreach($weights as $weight)' +
                            '<option value="{{$weight->id}}">{{$weight->title}}</option>' +
                            '@endforeach' +
                            '</select>'+
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<div class="btn-group btn-toggle btn-group-design">'+
                            '<button class="btn pkeyword_bal ' + positive + '" value="1">Positive</button>' +
                            '<button class="btn pkeyword_bal ' + negative + '" value="-1">Negative</button>' +
                            '</div>'+
                            '</td>'+
                            '<td>'+
                            '<ul class="inline-list">'+
                            '<li><a class="button green-bg" data-toggle="modal" data-target="#pkeyworddeleteModal" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>'+
                            '</ul>'+
                            '</td>'+
                            '</tr>';
                    }

                    if (offset > 0) {
                        $("#pkeywordtable tbody").append(innerhtml);
                    }else{
                        $("#pkeywordtable tbody").html(innerhtml);
                    }


                    for (var i = 0; i < result.length; i++) {
                        $("#pkeyword_weight_" + result[i]['id']).val(result[i]['generic_weight']);
                    }

                    $(".total_keywords").text($("#pkeywordtable tbody").find('tr').length);

                    //$("#divLoading").removeClass('show');
                }
            })
        }

        getPKeywords(0, 'asc');

        var count = 0;
        $("#paginate_profile_keyword").on("click", function () {
            count += 1;
            getPKeywords(count * 10, 'asc');
        })

        $(document).on("keyup", "#search_pkeyword", function () {
            getPKeywords(0, 'asc');
        });

        $("#pgeneric_form").on("submit", function (e) {
            e.preventDefault();

            $("#divLoading").addClass('show');

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/add-profile-keyword") }}',
                data: $("#pgeneric_form").serialize() + '&keyword_balance=' + $(".keyword_balance.active").val() + '&keyword_profile_id=' + keyword_profile_id,
                success: function (data) {
                    getPKeywords(0,'asc');
                    $("#divLoading").removeClass('show');
                    $("#pkeywordModal").modal('hide');
                    $(".modal").modal('hide');
                    success_toast(data.msg);
                    $('#pgeneric_form').trigger("reset");
                }
            })
        });

        $(document).on("change", ".pkeyword_weight", function () {
            $("#divLoading").addClass('show');
            var keyword_weight = $(this).val();
            var keyword_id = $(this).attr('name')
            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/update-keyword") }}',
                data: '{"keyword_profile_id":' + keyword_profile_id + ',"keyword_weight":' + keyword_weight + ',"keyword_id":' + keyword_id + ',"action":"update_keyword_weight"}',
                contentType: 'application/json',
                success: function (data) {
                    getPKeywords(0, 'asc');
                    $("#divLoading").removeClass('show');
                    success_toast(data.msg);

                }
            })
        });

        $(document).on("click", ".btn-toggle", function () {
            $(this).find('.pkeyword_bal').toggleClass('active');
            //$(this).find('.pkeyword_bal').toggleClass('active');
            var keyword_balance = $(this).find('.pkeyword_bal.active').val();
            var keyword_id = $(this).parents('tr').find('.pkeyword_weight').attr('name');
            if (keyword_id) {
                $("#divLoading").addClass('show');
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/update-keyword") }}',
                    data: '{"keyword_profile_id":' + keyword_profile_id + ',"keyword_balance":' + keyword_balance + ',"keyword_id":' + keyword_id + ',"action":"update_keyword_balance"}',
                    contentType: 'application/json',
                    success: function (data) {
                        //getPKeywords(0, 'asc');
                        $("#divLoading").removeClass('show');
                        success_toast(data.msg);

                    }
                })
            }
        });

        $("#pkeyword_check").on("change", function () {
            if ($(this).is(':checked')) {
                $(".pkeyword-checkbox").prop('checked', true);
            } else {
                $(".pkeyword-checkbox").prop('checked', false);
            }
        })

        $(".delete_pkeywords").on("click", function () {
            $("#divLoading").addClass('show');
            var selected_keywords = $('.pkeyword-checkbox:checked').serialize();
            console.log(selected_keywords);
            if (selected_keywords) {
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/delete-keywords") }}',
                    data: selected_keywords + '&keyword_profile_id=' + keyword_profile_id,
                    success: function (data) {
                        getPKeywords(0, 'asc');
                        $("#divLoading").removeClass('show');
                        $("#pkeyworddeleteAllModal").modal('hide');
                        success_toast(data.msg);

                    }
                })
            } else {
                error_toast('No keyword selected, please select a keyword.');
            }
        })

        var keyword_id = 0;
        $('#pkeyworddeleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            keyword_id = button.data('id');
            $('.keyworddelete_confirm p').html('Confirm delete Profile <strong>' + button.parents('tr').find('td:nth-child(2) > .name-text').text() + '</strong>?');

        })

        $(document).on("click", "#delete_keyword", function () {

            $.ajax({
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/delete-keyword") }}/' + keyword_id,
                data: '{"keyword_profile_id":' + keyword_profile_id + '}',
                contentType: 'application/json',
                success: function (data) {
                    getPKeywords(0, 'asc');
                    $("#divLoading").removeClass('show');
                    $('#pkeyworddeleteModal').modal('hide');
                    success_toast(data.msg);

                }
            })

        })

        $("#sort-by-pkweight").on("click", function () {
            if ($(this).data('weight') == 'asc') {
                $(this).data('weight', 'desc')
                $('.sort-by-weight img').css('transform', 'rotate(180deg)');
            } else {
                $(this).data('weight', 'asc')
                $('.sort-by-weight img').css('transform', 'rotate(360deg)');
            }

            getPKeywords(0, $(this).data('weight'));
        });

        $("#all_profiles").on('click', function () {
            location.reload();
        })

        function getLocations() {
            //$("#divLoading").addClass('show');
            console.log('keyword_profile_id',keyword_profile_id);
            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/location-rating") }}',
                contentType: 'application/json',
                data: {"profile_id":keyword_profile_id},
                success: function (result) {
                    $('.total_locations').text(result.total_locations);
                    var result = result.data;
                    var innerhtml = '';
                    for (var i = 0; i < result.length; i++) {
                        innerhtml += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td class="name-text">' + result[i]['country'] + '</td>' +
                            '<td>' + result[i]['rating'] + '</td>' +
                            '<td>' +
                            '<ul class="inline-list">' +
                            '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteLocationModal" data-country="' + result[i]['country'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                            '</ul>' +
                            '</td>' +
                            '</tr>';
                    }

                    $("#locationtable tbody").html(innerhtml);


                    // $("#divLoading").removeClass('show');
                }
            })
        }

        getLocations();

        function getJobtypes() {
            //$("#divLoading").addClass('show');
            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/all-jobtype-rating") }}',
                contentType: 'application/json',
                data: {"profile_id":keyword_profile_id},
                success: function (result) {
                    $('.total_jobtypes').text(result.total_jobtypes);
                    var result = result.data;
                    var innerhtml = '';
                    for (var i = 0; i < result.length; i++) {
                        var jobtype  = result[i]['jobtype'] == 1 ? "Full Time": result[i]['jobtype'] == 2 ? "Part Time":"Internship";
                        innerhtml += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td class="name-text">' + jobtype + '</td>' +
                            '<td>' + result[i]['rating'] + '</td>' +
                            '<td>' +
                            '<ul class="inline-list">' +
                            '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#jobtypeModal" data-jobtypeid="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/pencil-white.svg"> Edit</a></li>' +
                            '</ul>' +
                            '</td>' +
                            '</tr>';
                    }

                    $("#jobtypetable tbody").html(innerhtml);


                    // $("#divLoading").removeClass('show');
                }
            })
        }

        getJobtypes();

        function getExperiences() {
            //$("#divLoading").addClass('show');
            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/experience-rating") }}',
                contentType: 'application/json',
                data: {"profile_id":keyword_profile_id},
                success: function (result) {
                    $('.total_experiences').text(result.total_experiences);
                    result = result.data;
                    var innerhtml = '';
                    for (var i = 0; i < result.length; i++) {
                        innerhtml += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td class="name-text">' + result[i]['experience_from'] + '-' + result[i]['experience_to'] + ' yrs</td>' +
                            '<td>' + result[i]['rating'] + '</td>' +
                            '<td>' +
                            '<ul class="inline-list">' +
                            '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteExperienceModal" data-id="' + result[i]['id'] + '" data-from="' + result[i]['experience_from'] + '" data-to="' + result[i]['experience_to'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                            '</ul>' +
                            '</td>' +
                            '</tr>';
                    }

                    $("#experiencetable tbody").html(innerhtml);


                    // $("#divLoading").removeClass('show');
                }
            })
        }

        getExperiences();

        function getCompanies() {
            //$("#divLoading").addClass('show');
            console.log('k_profile_id',keyword_profile_id);
            $.ajax({
                method: 'GET',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url("/admin/company-rating") }}',
                contentType: 'application/json',
                data: {"profile_id":keyword_profile_id},
                success: function (result) {
                    $('.total_locations').text(result.total_companies);
                    result = result.data;
                    var innerhtml = '';
                    for (var i = 0; i < result.length; i++) {
                        innerhtml += '<tr>' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td class="name-text">' + result[i]['company'] + '</td>' +
                            '<td>' + result[i]['rating'] + '</td>' +
                            '<td>' +
                            '<ul class="inline-list">' +
                            '<li><a class="button green-bg" href="#" data-toggle="modal" data-target="#deleteCompanyModal" data-company="' + result[i]['company'] + '" data-id="' + result[i]['id'] + '"><img src="{{ URL::to('/') }}/img/trash.svg"> Delete</a></li>' +
                            '</ul>' +
                            '</td>' +
                            '</tr>';
                    }

                    $("#companytable tbody").html(innerhtml);


                    // $("#divLoading").removeClass('show');
                }
            })
        }
        getCompanies();

        /* bulk keyword */

        $('#pgeneric_bulk_form .keyword_title').tagsinput();

        $("#pgeneric_bulk_form").on("submit", function (e) {
            e.preventDefault();
            $("#divLoading").addClass('show');

            var result = $('#pgeneric_bulk_form .keyword_title').val();
            if(result.length === 0){
                error_toast("Upload at least one keyword!");
                $('.keyword_error').show();
                $("#divLoading").removeClass('show');
                $('#pgeneric_bulk_form').trigger("reset");
                $('#pgeneric_bulk_form .keyword_title').tagsinput('removeAll');
            }else{

                var balance = $("#pgeneric_bulk_form .keyword_balance.active").val();
                var profile = keyword_profile_id;
                var weight = $('#pgeneric_bulk_form .bulk_weight').val();
                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ url("/admin/profile-bulk-keyword") }}',
                    data: {
                        "keyword_balance" :balance,
                        "keyword_title" : result,
                        "profiles" :profile,
                        "keyword_weight" : weight,
                    },
                    success: function (data) {
                        getPKeywords(0,'asc');
                        $("#divLoading").removeClass('show');
                        $("#pBulkKeywordModal").modal('hide');
                        $(".modal").modal('hide');
                        success_toast(data.msg);
                        $('#pgeneric_bulk_form').trigger("reset");
                        $('#pgeneric_bulk_form .keyword_title').tagsinput('removeAll');
                    }
                });
            }

        });


        /* keyword upload file */
        $("#upload_group_tags").change(function() {
            var fd = new FormData();
            fd.append('file', this.files[0]);

            $.ajax({
                url: '{{ url("/admin/upload_keyword_csv") }}',
                type: "post",
                dataType: 'json',
                processData: false,
                contentType: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: fd,
                success: function(response) {
                    if(response.status == "error"){
                        error_toast("Upload only CSV file!");
                    }
                    $.each(response.data,function (key, value) {
                        $('#pgeneric_bulk_form .keyword_title').tagsinput('add',value);
                    });
                    $('.keyword_error').hide();
                },
                error: function() {
                    error_toast("An error occured, please try again.");
                }
            });
        });
        $('#pBulkKeywordModal').on('hidden.bs.modal', function () {
            $('.keyword_data').remove();
            $('.keyword_error').hide();
            $(this).find("select").val('').end();
            $('#pgeneric_bulk_form').trigger("reset");
            $('#pgeneric_bulk_form .keyword_title').tagsinput('removeAll');
        });
        /********************************************Generic Keyword End***************************************************/
    })
</script>
