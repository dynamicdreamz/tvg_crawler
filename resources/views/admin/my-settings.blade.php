@extends("layouts.master-admin")
@section("content")
    <div class="box box-primary admin">
        <form role="form" method="post" id="adminedituser" name="adminedituser" action="{{URL::to('admin/save-settings')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="box-header with-border">
                <h3 class="box-title">My Settings</h3>
            </div>
            <div class="box-body">
                <input type="hidden" id="user_id" name="user_id" value="{{$admin[0]["id"]}}">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Threshold</label>
                            <input type="number" min="0" max="1000" class="form-control" name="threshold" value="{{$admin[0]["threshold"]}}">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <div class="row">
                            <div class="col-md-2 col-sm-6 col-lg-2">
                                <a href="{{URL::to('admin/dashboard')}}" class="btn btn-block btn-default">Back</a>
                            </div>
                            <div class="col-md-2 col-sm-6 col-lg-2">
                                <button type="submit" class="btn btn-block btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
