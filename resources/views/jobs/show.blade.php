@extends("layouts.detail")

@section("content")

    <div class="container-fluid">
        <div class="row">
            <div class="content-wrapper full-width">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        <h2 class="main-title">{{$jobDetail->job_title}} ({{$jobDetail->reference_id}})</h2>
                    </div>
                </div>
                <div class="row mb-2 fr-jobdetail">
                    <div class="col-md-8">
                        <div class="custom-block mb-2">
                            <div class="fr-jobdetail-dec">

                                {!!html_entity_decode($jobDetail->description, ENT_QUOTES, 'UTF-8')!!}
                                {{--<a class="btn button" href="#">Read More</a>--}}
                            </div>
                        </div>
                        <div class="custom-block mb-2">
                            <div class="fr-jobloc">
                                <ul class="list-inline">
                                    <li>
                                        <h4>Country</h4>
                                        <h5 class="mb-0">{{$jobDetail->country}}</h5>
                                    </li>
                                    <li>
                                        <h4>Language</h4>
                                        <h5 class="mb-0">{{$jobDetail->language}}</h5>
                                    </li>
                                    <li>
                                        <h4>Source</h4>
                                        <h5 class="mb-0"><a href="{{$jobDetail->source_url}}" target="_blank">{{$jobDetail->source_url}}</a></h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @if($jobDetail->contact_name || $jobDetail->contact_email || $jobDetail->contact_phone)
                        <div class="custom-block mb-2">
                            <div class="fr-jobcontact">
                                <h4>Contact Person</h4>
                                <ul class="list-inline">
                                    <li><h4><img class="img-responsive" src="{{ url('/') }}/img/user.png">{{$jobDetail->contact_name}}</h4></li>
                                    <li><h4><a href="tel:801-5623-7546"><img class="img-responsive" src="{{ url('/') }}/img/call.png">{{$jobDetail->contact_phone}}</a></h4></li>
                                    <li><h4><a href="mailto:801-5623-7546"><img class="img-responsive" src="{{ url('/') }}/img/envelope.svg">{{$jobDetail->contact_email}}</a></h4></li>
                                </ul>
                            </div>
                        </div>
                            @endif
                    </div>
                    <div class="col-md-4">
                        <div class="custom-block mb-2">
                            <div class="text-center">
                                <img class="img-responsive center-block" src="{{ url('/') }}/img/talentpool-logo.png" alt="">
                            </div>
                        </div>
                        {{--<div class="custom-block mb-2">
                            <div class="fr-jobdetail-btngroup text-center">
                                <ul class="list-inline">
                                    <li><a class="button green-bg" href="#"><img src="{{ url('/') }}/img/edit.svg">Edit</a></li>
                                    <li><a class="button green-bg" href="#"><img src="{{ url('/') }}/img/eye.svg">Preview</a></li>
                                    <li><a class="button green-bg" href="#"><img src="{{ url('/') }}/img/hide.svg">Hide</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="fr-jobdetail-tag">
                            <input type="text" name="tags" value="{{$jobDetail->keywords}}" data-role="tagsinput" />
                        </div>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>

{{--<div class="main-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <h1>{{$jobDetail->job_title}} ({{$jobDetail->reference_id}})</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 col-md-9 col-lg-9">
                <div class="job-information-section">
                    <h3>Job information</h3>
                    <div class="static-block">
                        <label>Description</label>
                        <p class="input-text">{!!html_entity_decode($jobDetail->description, ENT_QUOTES, 'UTF-8')!!}</p>
                        <div class="row">

                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <label>country</label>
                                <p class="input-text">{{$jobDetail->country}}</p>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <label>language</label>
                                <p class="input-text">{{$jobDetail->language}}</p>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-6">

                                <label>source</label>
                                <p class="input-text">{{$jobDetail->source_url}}</p>
                            </div>
                        </div>
                    </div>
                    <h3>Contact person</h3>
                    <div class="static-block">
                        <div class="input-text">{{$jobDetail->contact}}</div>
                    </div>
                    <div class="static-block sb-h1">
                        <h3 class="text-center">Feeds (Nova, PulsR, Shortcut)</h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3 col-lg-3">
                <div class="right-block gray-block">
                    <div class="company-logo-block">
                        <img src="{{ asset('img/company-logo.png') }}" > <h3>{{$jobDetail->company}}</h3>
                    </div>
                    <div class="right-block-checkbtn">
                        <label><div class="checkbox-btn">
                                <input type="checkbox" class="input-checkbox" name="rememberme" id="rememberme">
                                <label for="rememberme" class="acccke"></label>
                            </div>Connect to talent pool</label>
                    </div>
                </div>
                <div class="job-selection">
                    <div class="job-percentage  d-i">
                        <h4 class="mb-0">{{$jobDetail->relevant_match}}%</h4>
                        <p class="mb-0">MATCH</p>
                    </div>
                    <button type="button" class="dark-btn btn">Ignore</button>
                    <button type="button" class="dark-btn btn">Preview</button>
                </div>
                <button type="button" class="dark-btn btn btn-block approve-btn">Approve</button>
                <hr class="border-line">
                <div class="sub2-titile">
                    <h5>Related Keywords</h5>
                    <ul class="nav nav-pills related-keyword-tab common-line-tab">
                        <li class="active"><a data-toggle="pill" href="#positive">positive  ({{$jobDetail->positive_keywords_count}})</a></li>
                        <li><a data-toggle="pill" href="#negative">negative  ({{$jobDetail->negative_keywords_count}})</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="positive" class="tab-pane fade in active">
                            <input type="text" name="tags" value="{{$jobDetail->positive_keywords}}" data-role="tagsinput" />
                            <button type="button" class="outline-btn btn btn-block addkeyword">Add keyword</button>
                            <hr class="border-line">
                        </div>
                        <div id="negative" class="tab-pane fade">
                            <input type="text" name="tags" value="{{$jobDetail->negative_keywords}}" data-role="tagsinput" />
                            <button type="button" class="outline-btn btn btn-block addkeyword">Add keyword</button>
                            <hr class="border-line">
                        </div>
                    </div>
                </div>
                <div class="right-block gray-block text-center">
                    <a class="button button-2" href="#">Edit</a>
                    <a class="button button-2" href="#">Preview</a>
                    <a class="button button-2" href="#">Action</a>
                </div>
                <div class="right-block gray-block text-center sb-h2">
                    <a class="tag" href="#">Tag</a><a class="tag" href="#">Tag</a>
                </div>
            </div>
        </div>
    </div>
</div>--}}

    <script type="text/javascript">
        $(function () {
            $("#search_btn").on("click", function(){
                var search = $("#search_job").val();
                console.log('{{ url('/') }}/'+search);
                window.location.href = '{{ url('/') }}/'+search;
            })

            $(".job-list").text("{{$totalJobs}}");
        })
    </script>

@endsection