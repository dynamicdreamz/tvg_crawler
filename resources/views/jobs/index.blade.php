@extends("layouts.master")

@section("content")
    <div class="row mb-2 content-btngroup">
        <div class="col-sm-12 col-md-12 col-lg-12">
           {{-- <ul class="inline-list pull-left">
                <li><div class="checkbox-btn">
                        <label class="checkbox-design"><input type="checkbox" ><span class="checkmark"></span></label>
                    </div></li>
                <li><a class="button green-btn btn" href="#" disabled>Hide</a></li>
                <li><a class="button green-btn btn" href="#"> Bulk Action</a></li>
                <li><a class="button green-btn btn" href="#"> Bulk Action</a></li>
                <li><a class="button green-btn btn" href="#"> Clean Out</a></li>
            </ul>--}}
        </div>
    </div>
    <div class="row mb-2">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="job-list-saection">
            </div>
            <div class="text-center">
                <a href="javascript:void(0);" class="button paginate" id="paginate">Show More</a>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#search_job').val('{{$search}}');
            function get_jobs(offset) {

                var search_text = $('#search_job').val();
                var date = $("#date_filter").val();
                var country = '';
                var relevance = $("#relevance").val();
                if(typeof $('input.typeahead.tt-input').val() !='undefined'){
                    country = $('input.typeahead.tt-input').val();
                }

                if(!search_text){
                    $("div#divLoading").addClass('show');
                }

                var job_type = '';
                if ($('.job_type_filter:checked').map(function () {
                        return this.value;
                    }).get().join(',')) {
                    job_type = $('.job_type_filter:checked').map(function () {
                        return this.value;
                    }).get().join(',');
                }


                $.ajax({
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: '{"offset": ' + offset + ', "pagesize": "50", "search_text": "'+search_text+'", "post_date": "'+date+'", "country": "'+country+'", "relevance": "'+relevance+'", "job_type": "'+job_type+'"}',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: '{{ url("/ajax/jobs/index") }}',
                    success: function (data) {
                        $("#divLoading").addClass('show');
                        var total_jobs = data.total_jobs;
                        var result = data.jobs;

                        var innerhtml = $(".job-list-saection").html();
                        if(search_text !='' && offset <= 0 || date !='' && offset <= 0 || country !='' && offset <= 0 || relevance !=''|| job_type !='' && offset <= 0){
                            innerhtml = '';
                        }
                        for (var i = 0; i < result.length; i++) {

                            innerhtml += '<div class="job-list-block dm-block"><div class="dm-div">'+
                                '<div class="dm-left">'+
                                '<div class="dm-date">'+result[i]['posted_on']+'</div>'+
                                '<h2> <a href="{{ url('/job-detail/') }}/'+result[i]['id']+'" target="_blank">'+result[i]['job_title']+'</h2>'+
                                '</div>'+
                                '<div class="dm-center">'+
                                '<div class="job-match">'+
                                '<h3>'+result[i]['relevant_match']+' <span>Relevence</span></h3>'+
                                '<div class="job-progress">'+
                                '<div class="progress">'+
                                '<div class="progress-bar" role="progressbar" aria-valuenow="'+result[i]['relevant_match']+'"'+
                                ' aria-valuemin="0" aria-valuemax="100"'+
                                ' style="width: '+result[i]['relevant_match']+'%;"></div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>'+
                              /*  '<div class="dm-right">'+
                                '<a class="button" href="#"><img src="images/setting.png">MANAGE</a>'+
                                '</div>'+*/
                                '</div>'+
                                '<div class="dm-bottom">'+
                                '<p>'+result[i]['description']+'</p>'+
                                '</div></div>';

                        }

                        $(".job-list-saection").html(innerhtml);

                        /*function sortUsingNestedText(parent, childSelector) {
                            var items = parent.children(childSelector).sort(function(a, b) {
                                var vA = parseInt($(b).find('.jlb-right h3').text());
                                var vB = parseInt($(a).find('.jlb-right h3').text());

                                return (vA > vB) ? 1 : (vA < vB) ? -1 : 0;

                            });
                            parent.append(items);
                        }
                        sortUsingNestedText($('.job-list-saection'), "div")*/;

                        if(data.total_jobs > 50){
                            $("#paginate").show();
                        }else{
                            $("#paginate").hide();
                        }

                        $("#divLoading").removeClass('show');

                        $(".job-list").text(total_jobs);

                    }
                });
            }

            get_jobs(0);
            var count = 0;
            $("#paginate").on("click", function () {
                count += 1;
                get_jobs(count * 50);
            })

            $(".search-box").on("keyup", function(){
                get_jobs(0);
            })

            $('#date_filter').datepicker();

            var substringMatcher = function(strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;

                    // an array that will be populated with substring matches
                    matches = [];

                    // regex used to determine if a string contains the substring `q`
                    substrRegex = new RegExp(q, 'i');

                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function(i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var countries = ['{!!html_entity_decode($countries, ENT_QUOTES, 'UTF-8')!!}'];

            $('.typeahead').typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 1

                },
                {
                    name: 'countries',
                    source: substringMatcher(countries)
                });

            $('.apply-btn').on('click', function() {
                get_jobs(0);
            })
        })
    </script>
@endsection


