<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'company', 'job_title', 'posted_on', 'category', 'country', 'description', 'reference_id', 'source_url', 'close_on', 'status'
    ];

    public function getJobDataByID($id, $status)
    {
        return $this->where('user_id',$id)->where('status',$status)->get()->sortByDesc('points');
    }
}
