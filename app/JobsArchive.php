<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class JobsArchive extends Model
{
    protected $table = "jobs_archive";
    protected $fillable = [
        'id', 'company', 'job_title', 'posted_on', 'category', 'country', 'description', 'reference_id', 'source_url', 'close_on', 'status'
    ];

}