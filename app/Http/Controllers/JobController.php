<?php

namespace App\Http\Controllers;

use App\JobMatchedProfile;
use Illuminate\Http\Request;
use App\Job;
use App\JobMatchedKeyword;
use App\Traits\JobQuery;

class JobController extends Controller
{
    use JobQuery;

    /**
     * @var string
     */
    protected $item;

    /**
     * JobController constructor.
     */
    public function __construct()
    {
        $this->item = "New Product";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($search="")
    {
        return \Redirect::to('admin/login');
        $jobs = Job::paginate(20);

        $countries = array_filter(Job::groupBy('country')->pluck('country')->toArray());

        $countries = array_map('trim',$countries);
//dd("'".implode($countries, "','")."'");
        return view("jobs.index")->with(['countries'=>implode(array_unique($countries), "', '"),'search'=>$search]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id = "")
    {
        try {
            if ($id == "") {
                throw new ModelNotFoundException();
            }

            $jobDetail = Job::where("id", "=", $id)
                ->firstOrFail();

            $country_detail = json_decode(file_get_contents('https://restcountries.eu/rest/v2/name/' . $jobDetail->country));

            $jobDetail->language = $country_detail[0]->languages[0]->name;

            $matches = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
            })->leftJoin('weights', function ($join) {
                $join->on('keywords.generic_weight', '=', 'weights.id');
            })->where('job_matched_keywords.job_id', $jobDetail->id)->select('keywords.*', 'weights.rating','job_matched_keywords.points')->get();

            $rel_match = 0;
            $keyowrds_pos = [];
            $keyowrds_neg = [];
            foreach ($matches as $k => $match) {
                if($match->generic_balance == 1){
                    $keyowrds_pos[] = $match->generic_title;
                }else{
                    $keyowrds_neg[]= $match->generic_title;
                }

                $rel_match += $match->points;
            }
            $jobDetail->relevant_match = $rel_match;
            $jobDetail->description = str_replace("â€™",'’',$jobDetail->description);
            $jobDetail->description = str_replace("â€¢","",$jobDetail->description);
            $jobDetail->description = str_replace("â€“","-",$jobDetail->description);
            $jobDetail->description = preg_replace('/\\\\/', '',$jobDetail->description);
            $jobDetail->positive_keywords_count = count($keyowrds_pos);
            $jobDetail->negative_keywords_count = count($keyowrds_neg);
            $jobDetail->positive_keywords = implode(',',$keyowrds_pos);
            $jobDetail->negative_keywords = implode(',',$keyowrds_neg);
            $jobDetail->keywords = implode(',',array_merge($keyowrds_pos,$keyowrds_neg));

            $totalJobs = Job::where('status','=',1)->count();

            return view("jobs.show", compact("jobDetail", "totalJobs"));

        } catch (ModelNotFoundException $mnfe) {
            Flash::error(trans("flash.error.not_found"));

        } catch (Exception $e) {
            Flash::error("ERROR: {$e->getCode()}");
        }

        return redirect()->route("jobs.index");
    }

    public function postJobBasedProfile(Request $request)
    {
        try{
            $data = $request->except("_method", "_token");
            Job::where('id',$data["job_id"])->update(["status"=>1]);
            $exist = JobMatchedProfile::where('job_id',$data["job_id"])->where('profile_id',$data["profile_id"])->count();
            if($exist <= 0){
                $jobmatchedprofile = new JobMatchedProfile();
                $jobmatchedprofile->job_id = $data["job_id"];
                $jobmatchedprofile->profile_id = $data["profile_id"];
                $jobmatchedprofile->save();
                return response()->json(['success' => true, 'msg' => "Job assigned to profile successfully."]);
            }else{
                return response()->json(['success' => false, 'msg' => "Job already exist in selected profile."]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }
}
