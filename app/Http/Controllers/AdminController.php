<?php

namespace App\Http\Controllers;

use App\ExperienceRating;
use App\JobMatchedKeyword;
use App\JobtypeRating;
use App\LocationRating;
use App\CompanyRating;
use App\User;
use Illuminate\Http\Request;
use App\Lib\Validation;
use App\Weight;
use App\Profile;
use App\Keyword;
use App\Job;
use App\Admin;
use App\JobMatchedProfile;
use App\Traits\AdminJobQuery;
use App\Setting;
use DB;
use Illuminate\Support\Facades\Artisan;
use Image;
use Mail;
use Auth;
use AuraIsHere\FireAndForget\FireAndForget;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Validator;

class AdminController extends Controller
{
    use AdminJobQuery;
    /**
     * @var string
     */
    protected $item;

    protected $layout = 'layouts.master';

    /**
     * JobController constructor.
     */
    public function __construct()
    {
        $this->item = "AI Configuration";
    }

    public function index()
    {
        if (Auth::guard('admin')->check()){
            return \Redirect::to('admin/dashboard');
        }else{
            return view("admin.auth.login");
        }
    }

    public function getLogin()
    {
        if (Auth::guard('admin')->check()){
            return \Redirect::to('admin/dashboard');
        }else{
            return view("admin.auth.login");
        }
    }


    public function getUserId()
    {

        $user = \Auth::guard('admin')->user();
        $userId = $user->id;
        return $userId;
    }

    public function getThreshold()
    {

        $user = \Auth::guard('admin')->user();
        $threshold = $user->threshold;
        return $threshold;
    }

    public function getUserRole()
    {

        $user = \Auth::guard('admin')->user();
        $role = $user->type;
        return $role;
    }

    public function postCloneJobs(Request $request)
    {
        $jobs = Job::where("user_id", 1)->select('id')->get();
        foreach ($jobs as $value) {
            $job = Job::find($value->id);

            $newJob = $job->replicate();
            $newJob->user_id = $request->post('user_id');
            $newJob->points = 0;
            $newJob->keywords = '';
            $newJob->keyword_ids = '';
            $newJob->keyword_points = '';
            $newJob->rating_types = '';
            $newJob->rating_points = '';
            $newJob->status = 0;
            $newJob->save();
        }
    }

    // Update or change Account profile-----
    public function my_account()
    {
        $user_id = $this->getUserId();

        $data['admin'] = Admin::where("id", "=", $user_id)->get()->toArray();
        return view("admin.my-account", $data);
    }


    public function my_account_update(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), Validation::get_rules("admin", "update_account"));

            if ($validator->fails()) {
                \Session::flash('error', $validator->errors()->first());

                // return response()->json(['success' => true, 'msg' => "User updated successfully."]);
                return \Redirect::to('admin/my-account');
            }

            $data = $request->except("_method", "_token");

            $user_id = $data["user_id"];
            unset($data["user_id"]);

            if ($data["password"] == null) {
                unset($data["password"]);
            } else {
                $data["password"] = bcrypt($data['password']);
            }

            if (isset($data["profile_pic"]) && $data["profile_pic"]) {
                $file = $data["profile_pic"];
                $imagename = $user_id . '_' . trim($file->getClientOriginalName());

                $destinationPath = public_path('uploads/thumb');
                $thumb_img = Image::make($file->getRealPath())->resize(100, 100);
                $thumb_img->save($destinationPath . '/' . $imagename, 80);


                $destinationPath = public_path('uploads/original');
                $file->move($destinationPath, $imagename);
                Admin::where("id", $user_id)->update(["profile_pic" => $imagename]);

                unset($data["profile_pic"]);

            }

            Admin::where("id", $user_id)->update($data);

            \Session::flash('success', 'Profile Updated Successfully.!!');

            // return response()->json(['success' => true, 'msg' => "User updated successfully."]);
            return \Redirect::to('admin/my-account');
        } catch (Exception $e) {
            \Session::flash('error', $e->getMessage());
            return \Redirect::to('admin/my-account');
            //  return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    //end my_account ----------

    public function postLogin(Request $request)
    {

        $validator = \Validator::make($request->all(), Validation::get_rules("admin", "login"));

        if ($validator->fails()) {
            return \Redirect::to('admin/login')->WithErrors($validator)->withInput();
        }
        $params = $request->all();
        $remember_me = isset($params["remember"]) ? true : false;
        //dd($remember_me);
        if($remember_me){
            \Cookie::queue(\Cookie::make("username", $params['username']));
            \Cookie::queue(\Cookie::make("password", $params['password']));
            \Cookie::queue(\Cookie::make("remember", $params['remember']));
        }else{
            \Cookie::queue(\Cookie::forget('username'));
            \Cookie::queue(\Cookie::forget('password'));
            \Cookie::queue(\Cookie::forget('remember'));
        }

        $doLogin = Admin::doLogin($params);

        if ($doLogin) {
            \Session::flash('message', 'Login Successful');
            \Session::flash('alert-class', 'alert-success');
            return \Redirect::to('admin/dashboard');
        } else {
            return \Redirect::to('admin/login')->WithErrors(array('message' => 'You entered wrong credentials,please try with correct credentials.'))->withInput();
        }
    }

    public function getLogout(Request $request)
    {
        \Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user_id = $this->getUserId();
        $setting = Admin::where('id', $user_id)->first();
        $crawl_date = date('l, F j H:i A', strtotime($setting->updated_at));

        $countries = array_filter(Job::groupBy('country')->pluck('country')->toArray());

        $countries = array_map('trim', $countries);

        $companies = Job::groupBy('company')->orderBy('company', 'asc')->get();

        return view("admin.dashboard")->with(['profile_id' => 0, "user_id" => $user_id, 'profile_name' => 'All', 'crawl_date' => $crawl_date, 'companies' => $companies, 'countries' => implode(array_unique($countries), "', '")]);
    }

    public function getCompanies()
    {
        $companies = Job::groupBy('company')->orderBy('company', 'asc')->get();

        return response()->json(['companies' => $companies]);
    }

    public function configuration($id = "")
    {

        $user_id = $this->getUserId();

        $weights = Weight::where("user_id", $user_id)->orderby('sort', 'asc')->get();

        $countries = array_unique(array_filter(Job::groupBy('country')->pluck('country')->toArray()));

        $companies = Job::groupBy('company')->orderBy('company', 'asc')->select("company")->get();
        $profile = Profile::where("user_id", $user_id)->orderby('name', 'asc')->get();
        return view("admin.aiconfiguration")->with(['weights' => $weights, "profile_id" => $id, "countries" => $countries, "companies" => $companies, 'profile' => $profile]);
    }

    public function profile_keywords($id)
    {
        $user_id = $this->getUserId();

        $keyword_profile_id = $id;
        // $weights = Weight::all();

        $weights = Weight::where("user_id", $user_id)->orderby('sort', 'asc')->get();

        $profile = Profile::where('id', $keyword_profile_id)->first();
        $profile_name = $profile['name'];
        return view("admin.profile_keywords", compact('keyword_profile_id', 'weights', 'profile_name'));
    }

    public function postAddWeight(Request $request)
    {
        try {
            $user_id = $this->getUserId();

            $data = $request->input();
            $weight = new Weight();
            $weight->title = $data['title'];
            $weight->rating = $data['rating'];
            $weight->sort = $data['sort'];
            $weight->user_id = $user_id;
            $weight->save();
            return response()->json(['success' => true, 'msg' => 'Weight added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function getWeights()
    {

        $user_id = $this->getUserId();
        $weights = Weight::where('user_id', $user_id)->get();

        return response()->json(['total_weight' => count($weights), 'data' => $weights]);
    }

    public function postDeleteWeight($id)
    {
        try {
            Weight::where('id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'Weight delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postCreateProfile(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $data = $request->input();
            $profile = new Profile();
            $profile->name = $data['profile_title'];
            $profile->user_id = $user_id;
            $profile->save();
            return response()->json(['success' => true, 'msg' => 'Profile created successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getProfiles(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $user_id = $this->getUserId();
            $search_text = trim($data["search_text"]);
            // Start a new query

            $Profile = new Profile();
            $Profiles = $Profile->newQuery();

            $Profiles = $Profiles->where('user_id', $user_id);


            // Filter by Profile title
            $data["order_by"] = $data["order_by"] == "" ? "asc" : $data["order_by"];
            if ($search_text != '') {

                $Profiles = $Profiles->where('name', 'LIKE', '%' . $search_text . '%')
                    ->orderBy('name', $data["order_by"])
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            } else {
                // Return records
                $Profiles = $Profiles
                    ->orderBy('name', $data["order_by"])
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            }

            foreach ($Profiles as $k => $v) {

                $Profiles[$k]['keywords'] = count(Keyword::where('profile_id', $v['id'])->where("user_id", $user_id)->get());
            }

            return response()->json(["total_profiles" => count($Profiles), "data" => $Profiles]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postUpdateProfile(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if (isset($data["action"]) && $data["action"] == "update_profile_weight") {
                Profile::find($data["profile_id"])->update(['weight' => $data["profile_weight"]]);
                return response()->json(['success' => true, 'msg' => 'Profile weight updated successfully.']);
            }
            if (isset($data["action"]) && $data["action"] == "update_profile_balance") {
                Profile::find($data["profile_id"])->update(['balance' => $data["profile_balance"]]);
                return response()->json(['success' => true, 'msg' => 'Profile balance updated successfully.']);
            }
            if (isset($data["action"]) && $data["action"] == "update_profile_name") {
                Profile::find($data["profile_id"])->update(['name' => trim($data["profile_name"])]);
                return response()->json(['success' => true, 'msg' => 'Profile name updated successfully.']);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postDeleteProfiles(Request $request)
    {
        try {
            $data = $request->input();
            Profile::whereIn('id', $data['profile_checkbox'])->delete();
            return response()->json(['success' => true, 'msg' => 'Profiles delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postDeleteProfile($id)
    {
        try {
            Profile::where('id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'Profile delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postAddKeyword(Request $request)
    {
        try {
            $user_id = $this->getUserId();


            $data = $request->input();
            $keyword = new Keyword();
            $keyword->generic_title = $data['keyword_title'];
            $keyword->generic_weight = $data['keyword_weight'];
            $keyword->generic_balance = $data['keyword_balance'];
            if (isset($data['keyword_profile'])) {
                $keyword->profile_id = $data['keyword_profile'];
            }
            $keyword->user_id = $user_id;

            $keyword->save();
            return response()->json(['success' => true, 'msg' => 'Keyword added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getKeywords(Request $request)
    {

        try {

            $user_id = $this->getUserId();

            $data = $request->except("_method", "_token");

            $search_text = trim($data["search_text"]);
            // Start a new query

            $Keyword = new Keyword();
            $Keywords = $Keyword->newQuery();

            $Keywords = $Keywords->where('keywords.user_id', $user_id);

            // Filter by Keyword title
            $total_keywords = 0;
            if ($search_text != '') {


                $total_keywords = Keyword::where('generic_title', 'LIKE', '%' . $search_text . '%')
                    ->where('profile_id', 0)->where('user_id', $user_id)->count();


                $Keywords = $Keywords->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.generic_title', 'LIKE', '%' . $search_text . '%')
                    ->where('keywords.profile_id', 0)->select('keywords.*', 'weights.rating')
                    ->orderBy('keywords.generic_title', $data["order_by"])
                    ->get();
            } else {

                $total_keywords = Keyword::where('profile_id', 0)->where('user_id', $user_id)->count();

                // Return records
                $Keywords = $Keywords->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.profile_id', 0)->select('keywords.*', 'weights.rating')
                    ->orderBy('keywords.generic_title', $data["order_by"])
                    /*->offset($data["offset"])
                    ->limit($data["pagesize"])*/
                    ->get();
            }


            return response()->json(["total_keywords" => $total_keywords, "data" => $Keywords]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postUpdateKeyword(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if (isset($data["action"]) && $data["action"] == "update_keyword_weight") {
                Keyword::find($data["keyword_id"])->update(['generic_weight' => $data["keyword_weight"]]);
                if (isset($data["keyword_profile_id"]) && $data["keyword_profile_id"] > 0) {
                    Profile::where("id", $data["keyword_profile_id"])->update(["updated_at" => date("Y-m-d H:i:s")]);
                }

                return response()->json(['success' => true, 'msg' => 'Keyword weight updated successfully.']);
            }
            if (isset($data["action"]) && $data["action"] == "update_keyword_balance") {
                Keyword::find($data["keyword_id"])->update(['generic_balance' => $data["keyword_balance"]]);
                if (isset($data["keyword_profile_id"]) && $data["keyword_profile_id"]) {
                    Profile::where("id", $data["keyword_profile_id"])->update(["updated_at" => date("Y-m-d H:i:s")]);
                }
                return response()->json(['success' => true, 'msg' => 'Keyword balance updated successfully.']);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postDeleteKeywords(Request $request)
    {
        try {
            $data = $request->input();
            if (isset($data['pkeyword_checkbox'])) {
                $keyword_ids = $data['pkeyword_checkbox'];
            } else {
                $keyword_ids = $data['keyword_checkbox'];
            }

            Keyword::whereIn('id', $keyword_ids)->delete();
            return response()->json(['success' => true, 'msg' => 'Keywords delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postDeleteKeyword($id)
    {
        try {
            Keyword::where('id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'Keyword delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

    public function postAddProfileKeyword(Request $request)
    {
        try {
            $user_id = $this->getUserId();

            $data = $request->input();
            $keyword = new Keyword();
            $keyword->profile_id = $data['keyword_profile_id'];
            $keyword->generic_title = $data['keyword_title'];
            $keyword->generic_weight = $data['keyword_weight'];
            $keyword->generic_balance = $data['keyword_balance'];
            $keyword->user_id = $user_id;


            $keyword->save();
            Profile::where('id', $data['keyword_profile_id'])->update(["updated_at" => date('Y-m-d H:i:s')]);
            return response()->json(['success' => true, 'msg' => 'Keyword added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getProfileKeywords(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $user_id = $this->getUserId();

            $search_text = trim($data["search_text"]);
            // Start a new query

            $Keyword = new Keyword();
            $Keywords = $Keyword->newQuery();

            $Keywords = $Keywords->where('keywords.user_id', $user_id);


            // Filter by Keyword title
            if ($search_text != '') {
                $Keywords = $Keywords->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.generic_title', 'LIKE', '%' . $search_text . '%')->where('keywords.profile_id', $data['keyword_profile_id'])->select('keywords.*', 'weights.rating')
                    ->orderBy('keywords.generic_title', $data["order_by"])
                    /*->offset($data["offset"])
                    ->limit($data["pagesize"])*/
                    ->get();
                $total_profile_keywords = Keyword::where('generic_title', 'LIKE', '%' . $search_text . '%')
                    ->where('profile_id', $data['keyword_profile_id'])->count();

            } else {
                // Return records
                $Keywords = $Keywords->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('profile_id', $data['keyword_profile_id'])->select('keywords.*', 'weights.rating')
                    ->orderBy('keywords.generic_title', $data["order_by"])
                    /*  ->offset($data["offset"])
                      ->limit($data["pagesize"])*/
                    ->get();

                $total_profile_keywords = Keyword::where('profile_id', $data['keyword_profile_id'])->count();
            }
            return response()->json(["total_keywords" => $total_profile_keywords, "data" => $Keywords]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getJobMatched()
    {
        try {

            $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                $join->on('keywords.generic_weight', '=', 'weights.id');
            })->where('keywords.profile_id', 0)->select('keywords.*', 'weights.rating')->get();
            //dd($generic_keywords);
            $jobs = Job::all();

            foreach ($jobs as $job) {
                foreach ($generic_keywords as $keyword) {
                    echo $keyword->generic_title . '-' . $job->job_title . '<br>';
                    if (strpos($keyword->generic_title, $job->job_title) !== false) {
                        echo 'inside-' . $keyword->generic_title . '-' . $job->job_title . '<br>';
                    }
                }

            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getJobProfiles(Request $request)
    {
        try {
            $Profile = new Profile();
            $Profiles = $Profile->newQuery();
            // Return records
            $user_id = $this->getUserId();
            $Profiles = $Profiles->where('user_id', $user_id)->orderBy('name', 'asc')->get();


            //   $result = JobMatchedProfile::where('profile_id', 20)->get();

            //  print_r(count($result));


            foreach ($Profiles as $k => $v) {
                // $Profiles[$k]['jobs'] = count(JobMatchedProfile::where('profile_id', $v['id'])->get());
                //$result = JobMatchedProfile::where('profile_id', $v['id'])->get();

                $point = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    /*->leftJoin('job_matched_profiles', function ($join) {
                        $join->on('jobs.id', '=', 'job_matched_profiles.job_id');
                    })*/
                    ->where('jobs.user_id', $user_id)
                    ->where('job_matched_profiles.profile_id', $v['id'])
                    ->count();

                $Profiles[$k]['jobs'] = $point;

            }


            return response()->json(["total_profiles" => Job::where('user_id', $user_id)->count(), "data" => $Profiles]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postJobStatus(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if (isset($data["action"]) && $data["action"] == "ignore") {
                Job::where('id', $data["job_id"])->update(['status' => 2]);
                return response()->json(['success' => true, 'msg' => 'Job status updated successfully.']);
            }
            if (isset($data["action"]) && $data["action"] == "approve") {
                Job::where('id', $data["job_id"])->update(['status' => 1]);
                return response()->json(['success' => true, 'msg' => 'Job status updated successfully.']);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function getProfileJobs($profile_id = "")
    {
        $user_id = $this->getUserId();
        if ($profile_id == "") {
            throw new ModelNotFoundException();
        }

        $profile_name = Profile::where('id', $profile_id)->where('user_id', $user_id)->select('name')->first();
        $profile_name = $profile_name->name;

        $setting = Setting::where('id', 1)->first();
        $crawl_date = date('l, F j H:i A', strtotime($setting->executed_at));
        $companies = Job::where('user_id', $user_id)->groupBy('company')->orderBy('company', 'asc')->get();

        $countries = array_filter(Job::where('user_id', $user_id)->groupBy('country')->pluck('country')->toArray());

        $countries = array_map('trim', $countries);

        return view("admin.dashboard")->with(['profile_id' => $profile_id, 'profile_name' => $profile_name, "crawl_date" => $crawl_date, "companies" => $companies, 'countries' => implode(array_unique($countries), "', '")]);
    }

    public function cmp($a, $b)
    {
        return $a->relevant_match < $b->relevant_match;
    }

    public function postProfileJobs(Request $request)
    {
        try {

            $user_id = $this->getUserId();

            $threshold = $this->getThreshold();
            $data = $request->except("_method", "_token");

            $data['sort_by'] = $data['sort_by'] != "" ? $data['sort_by'] : "desc";
            if ($data['profile_id'] > 0) {

                // $profile_jobs = Job::leftJoin('job_matched_profiles', function ($join) {
                //     $join->on('jobs.id', '=', 'job_matched_profiles.job_id');
                // })->leftJoin('keywords', function ($join) {
                //     $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                // });

                // $profile_jobs->where('job_matched_profiles.profile_id', $data['profile_id'])->where('jobs.user_id', $user_id);


                $profile_jobs = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->join('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('job_matched_profiles.total_points', '>', $threshold)
                    ->where('jobs.status', 0);


                if ($data['search_text'] != "") {
                    $profile_jobs->where('jobs.job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('jobs.company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }


                $total_profile_jobs = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->join('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('job_matched_profiles.total_points', '>', $threshold)
                    ->where('jobs.status', 0)
                    ->count();

                // $total_profile_jobs = $profile_jobs->where('jobs.status', 0)->where('job_matched_profiles.total_points', '>', $threshold)->groupBy('jobs.id')->select('jobs.id')->get();

                $profile_jobs->groupBy('jobs.id')->orderBy('job_matched_profiles.total_points', $data['sort_by']);

                $profile_jobs = $profile_jobs
                    //->where('job_matched_profiles.total_points', '>', $threshold)
                    //->select('jobs.job_title', 'jobs.company', 'jobs.website', 'jobs.posted_on', 'keywords.*', 'job_matched_profiles.*')
                    ->offset($data["offset"])->limit($data["pagesize"])->get();


                foreach ($profile_jobs as $key => $job) {

                    $profile_jobs[$key]->id = $profile_jobs[$key]->job_id;
                    $profile_jobs[$key]->job_title = str_replace("â€“", "-", $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€™", '’', $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€¢", "", $profile_jobs[$key]->job_title);

                    $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->total_points;
                    $keywords = unserialize($profile_jobs[$key]->profile_keywords);
                    $profile_jobs[$key]->keyword_ids = unserialize($profile_jobs[$key]->profile_keyword_ids);
//                    $profile_jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->posted_on = date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->match_points = unserialize($profile_jobs[$key]->profile_keyword_points);
                    $profile_jobs[$key]->rating_types = unserialize($profile_jobs[$key]->profile_rating_types);
                    $profile_jobs[$key]->rating_points = unserialize($profile_jobs[$key]->profile_rating_points);

                    if ($profile_jobs[$key]->keyword_ids) {
                        foreach ($profile_jobs[$key]->keyword_ids as $k => $id) {

                            $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                            if ($balance) {
                                $balance = $balance->generic_balance == 1 ? "+" : "-";
                            } else {
                                $balance = "+";
                            }
                            $keywords[$k] = $balance . $keywords[$k];
                        }
                    }
                    $profile_jobs[$key]->keywords = $keywords;
                }

                return response()->json(["total_jobs" => $total_profile_jobs, "data" => $profile_jobs]);
            }

            $job = new Job();
            $jobs = $job->newQuery();

            $jobs->where('user_id', $user_id);
            // Return records
            if ($data['search_text'] != "") {
                $jobs->where('job_title', 'LIKE', '%' . $data['search_text'] . '%');
            }

            if ($data['company'] != "") {
                $jobs->where('company', $data['company']);
            }

            if ($data['country'] != "") {
                $jobs->where('jobs.country', $data['country']);
            }

            if ($data['job_type'] != "") {
                $jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
            }

            $total_jobs = $jobs->where('points', '>', $threshold)->where('status', 0)->count();
            $jobs = $jobs->where('points', '>', $threshold)->where('status', 0)->offset($data["offset"])->limit($data["pagesize"])->orderBy('points', $data['sort_by'])->select('jobs.*', 'jobs.points as relevant_match')->get();

            foreach ($jobs as $key => $job) {

                $jobs[$key]->job_title = str_replace("â€“", "-", $jobs[$key]->job_title);
                $jobs[$key]->job_title = str_replace("â€™", '’', $jobs[$key]->job_title);
                $jobs[$key]->job_title = str_replace("â€¢", "", $jobs[$key]->job_title);

                $keywords = unserialize($jobs[$key]->keywords);
                $jobs[$key]->keyword_ids = unserialize($jobs[$key]->keyword_ids);
//                $jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                $jobs[$key]->posted_on = date('M d, Y', strtotime($job->created_at));
                $jobs[$key]->profile_id = 0;
                $jobs[$key]->match_points = unserialize($jobs[$key]->keyword_points);
                $jobs[$key]->rating_types = unserialize($jobs[$key]->rating_types);
                $jobs[$key]->rating_points = unserialize($jobs[$key]->rating_points);

                if ($jobs[$key]->keyword_ids && count($jobs[$key]->keyword_ids) > 0) {
                    foreach ($jobs[$key]->keyword_ids as $k => $id) {

                        $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                        if ($balance) {
                            $balance = $balance->generic_balance == 1 ? "+" : "-";
                        } else {
                            $balance = "+";
                        }

                        $keywords[$k] = $balance . $keywords[$k];
                    }
                }

                $jobs[$key]->keywords = $keywords;

            }

            return response()->json(["total_jobs" => $total_jobs, "data" => $jobs]);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postProfileapprovedJobs(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $jobs = [];

            $data['sort_by'] = $data['sort_by'] != "" ? $data['sort_by'] : "desc";
            if ($data['profile_id'] > 0) {

                // $profile_jobs = Job::leftJoin('job_matched_profiles', function ($join) {
                //     $join->on('jobs.id', '=', 'job_matched_profiles.job_id');
                // })->leftJoin('keywords', function ($join) {
                //     $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                // });

                // $profile_jobs->where('job_matched_profiles.profile_id', $data['profile_id'])->where('jobs.user_id', $user_id);


                $profile_jobs = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->leftJoin('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('jobs.user_id', $user_id)
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('jobs.status', 1);


                if ($data['search_text'] != "") {
                    $profile_jobs->where('jobs.job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('jobs.company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }

                // $total_profile_jobs = $profile_jobs->where('jobs.status', 1)->groupBy('jobs.id')->get();


                $total_profile_jobs = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->leftJoin('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('jobs.user_id', $user_id)
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('jobs.status', 1)
                    ->get();

                $profile_jobs->orderBy('job_matched_profiles.total_points', $data['sort_by']);

                $profile_jobs = $profile_jobs
                    //   ->select('jobs.job_title', 'jobs.company', 'jobs.website', 'jobs.posted_on', 'keywords.*', 'job_matched_profiles.*')
                    ->offset($data["offset"])->limit($data["pagesize"])->get();

                foreach ($profile_jobs as $key => $job) {

                    $point1 = 0;
                    $point2 = 0;
                    $profile_jobs[$key]->id = $profile_jobs[$key]->job_id;
                    //  $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->total_points;
                    $keywords = unserialize($profile_jobs[$key]->profile_keywords);
                    $profile_jobs[$key]->keyword_ids = unserialize($profile_jobs[$key]->profile_keyword_ids);
                    $profile_jobs[$key]->match_points = unserialize($profile_jobs[$key]->profile_keyword_points);
                    $profile_jobs[$key]->rating_types = unserialize($profile_jobs[$key]->profile_rating_types);
                    $profile_jobs[$key]->rating_points = unserialize($profile_jobs[$key]->profile_rating_points);

                    $profile_jobs[$key]->job_title = str_replace("â€“", "-", $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€™", '’', $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€¢", "", $profile_jobs[$key]->job_title);


                    $profile_jobs[$key]->relevant_match = 0;
                    if ($profile_jobs[$key]->profile_keyword_points != "") {
                        // $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;
                        $point1 = unserialize($profile_jobs[$key]->profile_keyword_points);

                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }

                    }


                    if ($profile_jobs[$key]->rating_points != "") {

                        //  print_r($profile_jobs[$key]->rating_points);

                        $point2 = $profile_jobs[$key]->rating_points;
                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }


                    $profile_jobs[$key]->relevant_match = (int)$point1 + (int)$point2;


//                    $profile_jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->posted_on = date('M d, Y', strtotime($job->created_at));

                    if ($profile_jobs[$key]->keyword_ids) {
                        foreach ($profile_jobs[$key]->keyword_ids as $k => $id) {

                            $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                            if ($balance) {
                                $balance = $balance->generic_balance == 1 ? "+" : "-";
                            } else {
                                $balance = "+";
                            }

                            $keywords[$k] = $balance . $keywords[$k];
                        }
                    }

                    $profile_jobs[$key]->keywords = $keywords;

                    $jobs[] = $profile_jobs[$key];
                }

            } else {
                $job = new Job();
                $profile_jobs = $job->newQuery();
                $profile_jobs->where('jobs.user_id', $user_id);
                // Return records
                if ($data['search_text'] != "") {
                    $profile_jobs->where('job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('jobs.company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }

                $total_profile_jobs = $profile_jobs->where('status', 1)->get();

                $profile_jobs = $profile_jobs->where('status', 1)->orderBy('points', $data['sort_by'])->offset($data["offset"])->limit($data["pagesize"])->get();

                foreach ($profile_jobs as $key => $job) {

                    $point1 = 0;
                    $point2 = 0;

                    $profile_jobs[$key]->relevant_match = 0;
                    if ($profile_jobs[$key]->keyword_points != "") {
                        //  $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;

                        //      $point1 = $profile_jobs[$key]->points;


                        $point1 = unserialize($profile_jobs[$key]->keyword_points);

                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }
                    }

                    $keywords = unserialize($profile_jobs[$key]->keywords);
                    $profile_jobs[$key]->keyword_ids = unserialize($profile_jobs[$key]->keyword_ids);
                    $profile_jobs[$key]->match_points = unserialize($profile_jobs[$key]->keyword_points);
                    $profile_jobs[$key]->rating_points = unserialize($profile_jobs[$key]->rating_points);
                    $profile_jobs[$key]->rating_types = unserialize($profile_jobs[$key]->rating_types);
                    $profile_jobs[$key]->profile_id = 0;


                    $profile_jobs[$key]->job_title = str_replace("â€“", "-", $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€™", '’', $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€¢", "", $profile_jobs[$key]->job_title);


                    if ($profile_jobs[$key]->rating_points != "") {
                        $point2 = $profile_jobs[$key]->rating_points;

                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }
                    $profile_jobs[$key]->relevant_match = $point1 + $point2;

//                    $profile_jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->posted_on = date('M d, Y', strtotime($job->created_at));

                    if ($profile_jobs[$key]->keyword_ids) {
                        foreach ($profile_jobs[$key]->keyword_ids as $k => $id) {

                            $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                            if ($balance) {
                                $balance = $balance->generic_balance == 1 ? "+" : "-";
                            } else {
                                $balance = "+";
                            }

                            $keywords[$k] = $balance . $keywords[$k];
                        }
                    }

                    $profile_jobs[$key]->keywords = $keywords;

                    $jobs[] = $profile_jobs[$key];
                }
            }


            $total_jobs = count($total_profile_jobs);

            return response()->json(["total_jobs" => $total_jobs, "data" => $jobs]);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postProfileignoredJobs(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $threshold = $this->getThreshold();
            $data = $request->except("_method", "_token");

            $jobs = [];
            $data['sort_by'] = $data['sort_by'] != "" ? $data['sort_by'] : "desc";
            if ($data['profile_id'] > 0) {


                // $profile_jobs = Job::leftJoin('job_matched_profiles', function ($join) {
                //     $join->on('jobs.id', '=', 'job_matched_profiles.job_id');
                // })->leftJoin('keywords', function ($join) {
                //     $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                // });

                // $profile_jobs->where('job_matched_profiles.profile_id', $data['profile_id'])->where('jobs.user_id', $user_id)->where('job_matched_profiles.total_points', '<=', $threshold);

                // if ($data['search_text'] != "") {
                //     $profile_jobs->where('jobs.job_title', 'LIKE', '%' . $data['search_text'] . '%');
                // }

                // if ($data['company'] != "") {
                //     $profile_jobs->where('jobs.company', $data['company']);
                // }

                // if ($data['country'] != "") {
                //     $profile_jobs->where('jobs.country', $data['country']);
                // }

                // if ($data['job_type'] != "") {
                //     $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                // }

                // $profile_jobs->where('jobs.status', "!=", 1);

                // if ($data['search_text'] != "") {
                //     $profile_jobs->where('jobs.job_title', 'LIKE', '%' . $data['search_text'] . '%');
                // }

                // if ($data['company'] != "") {
                //     $profile_jobs->where('jobs.company', $data['company']);
                // }

                // if ($data['country'] != "") {
                //     $profile_jobs->where('jobs.country', $data['country']);
                // }

                // if ($data['job_type'] != "") {
                //     $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                // }

                // $total_profile_jobs = $profile_jobs->groupBy('jobs.id')->select('jobs.id')->get();

                // $profile_jobs = $profile_jobs->select('jobs.job_title', 'jobs.company', 'jobs.website', 'jobs.posted_on', 'keywords.*', 'job_matched_profiles.*')->offset($data["offset"])->limit($data["pagesize"])
                //     ->groupBy('jobs.id')->orderBy('job_matched_profiles.total_points', $data['sort_by'])->get();


                // $profile_jobs = Job::leftJoin('job_matched_profiles', function ($join) {
                //                  $join->on('jobs.id', '=', 'job_matched_profiles.job_id');
                //              })->leftJoin('keywords', function ($join) {
                //                  $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                //              });

                //              $profile_jobs->where('job_matched_profiles.profile_id', $data['profile_id'])->where('jobs.user_id', $user_id);

                $profile_jobs = DB::table('jobs')
                    ->selectRaw('jobs.job_title, jobs.company, jobs.website, jobs.posted_on, keywords.*, job_matched_profiles.*')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->join('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('job_matched_profiles.total_points', '<=', $threshold)
                    ->where('jobs.status','!=',1)
                    ->where('jobs.user_id', $user_id);


                if ($data['search_text'] != "") {
                    $profile_jobs->where('jobs.job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('jobs.company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }

                //    $total_profile_jobs = $profile_jobs->where('jobs.status', 2)->groupBy('jobs.id')->get();


                $total_profile_jobs = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->join('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('job_matched_profiles.total_points', '<=', $threshold)
                    ->where('jobs.status','!=',1)
                    ->where('jobs.user_id', $user_id)
                    ->count();

                $profile_jobs->orderBy('job_matched_profiles.total_points', $data['sort_by']);

                $profile_jobs = $profile_jobs
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();

                foreach ($profile_jobs as $key => $job) {
                    $point1 = 0;
                    $point2 = 0;

                    $profile_jobs[$key]->relevant_match = 0;
                    if ($profile_jobs[$key]->profile_keyword_points != "") {
                        // $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;
                        $point1 = unserialize($profile_jobs[$key]->profile_keyword_points);
                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }
                    }
                    $profile_jobs[$key]->id = $profile_jobs[$key]->job_id;
                    //  $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->total_points;
                    $keywords = unserialize($profile_jobs[$key]->profile_keywords);
                    $profile_jobs[$key]->keyword_ids = unserialize($profile_jobs[$key]->profile_keyword_ids);
                    $profile_jobs[$key]->match_points = unserialize($profile_jobs[$key]->profile_keyword_points);
                    $profile_jobs[$key]->rating_types = unserialize($profile_jobs[$key]->profile_rating_types);
                    $profile_jobs[$key]->rating_points = unserialize($profile_jobs[$key]->profile_rating_points);
                    $profile_jobs[$key]->job_title = str_replace("â€“", "-", $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€™", '’', $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€¢", "", $profile_jobs[$key]->job_title);

                    if ($profile_jobs[$key]->rating_points != "") {
                        $point2 = $profile_jobs[$key]->rating_points;
                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }
                    $profile_jobs[$key]->relevant_match = (int)$point1 + (int)$point2;
//                    $profile_jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->posted_on = date('M d, Y', strtotime($job->created_at));
                    if ($profile_jobs[$key]->keyword_ids) {
                        foreach ($profile_jobs[$key]->keyword_ids as $k => $id) {

                            $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                            if ($balance) {
                                $balance = $balance->generic_balance == 1 ? "+" : "-";
                            } else {
                                $balance = "+";
                            }

                            $keywords[$k] = $balance . $keywords[$k];
                        }
                    }

                    $profile_jobs[$key]->keywords = $keywords;

                    $jobs[] = $profile_jobs[$key];
                }

            } else {
                $job = new Job();
                $profile_jobs = $job->newQuery();

                $profile_jobs->where('points', '<=', $threshold)->where('user_id', $user_id);

                // Return records
                if ($data['search_text'] != "") {
                    $profile_jobs->where('job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }

                $profile_jobs->where('status', "!=", 1);

                if ($data['search_text'] != "") {
                    $profile_jobs->where('job_title', 'LIKE', '%' . $data['search_text'] . '%');
                }

                if ($data['company'] != "") {
                    $profile_jobs->where('company', $data['company']);
                }

                if ($data['country'] != "") {
                    $profile_jobs->where('jobs.country', $data['country']);
                }

                if ($data['job_type'] != "") {
                    $profile_jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
                }

                $total_profile_jobs = $profile_jobs->count();

                $profile_jobs = $profile_jobs->orderBy('points', $data['sort_by'])->offset($data["offset"])->limit($data["pagesize"])->get();

                $point1 = 0;
                $point2 = 0;
                foreach ($profile_jobs as $key => $job) {
                    $profile_jobs[$key]->relevant_match = 0;
                    if ($profile_jobs[$key]->keyword_points != "") {
                        // $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;
                        $point1 = unserialize($profile_jobs[$key]->keyword_points);

                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }
                    }


                    $keywords = unserialize($profile_jobs[$key]->keywords);
                    $profile_jobs[$key]->keyword_ids = unserialize($profile_jobs[$key]->keyword_ids);
                    $profile_jobs[$key]->match_points = unserialize($profile_jobs[$key]->keyword_points);
                    $profile_jobs[$key]->rating_types = unserialize($profile_jobs[$key]->rating_types);
                    $profile_jobs[$key]->rating_points = unserialize($profile_jobs[$key]->rating_points);
                    $profile_jobs[$key]->profile_id = 0;
                    $profile_jobs[$key]->job_title = str_replace("â€“", "-", $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€™", '’', $profile_jobs[$key]->job_title);
                    $profile_jobs[$key]->job_title = str_replace("â€¢", "", $profile_jobs[$key]->job_title);
//                    $profile_jobs[$key]->posted_on = $job->posted_on != NULL ? date('M d, Y', strtotime($job->posted_on)): date('M d, Y', strtotime($job->created_at));
                    $profile_jobs[$key]->posted_on =  date('M d, Y', strtotime($job->created_at));

                    if ($profile_jobs[$key]->rating_points != "") {
                        $point2 = $profile_jobs[$key]->rating_points;

                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }

                    $profile_jobs[$key]->relevant_match = $point1 + $point2;


                    if ($profile_jobs[$key]->keyword_ids) {
                        foreach ($profile_jobs[$key]->keyword_ids as $k => $id) {

                            $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                            if ($balance) {
                                $balance = $balance->generic_balance == 1 ? "+" : "-";
                            } else {
                                $balance = "+";
                            }

                            $keywords[$k] = $balance . $keywords[$k];
                        }
                    }
                    $profile_jobs[$key]->keywords = $keywords;

                    $jobs[] = $profile_jobs[$key];
                }
            }

            $total_jobs = $total_profile_jobs;

            return response()->json(["total_jobs" => $total_jobs, "data" => $profile_jobs]);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getMatchingProfiles($jobId)
    {
        try {
            $user_id = $this->getUserId();
            $matches = JobMatchedProfile::leftJoin('profiles', function ($join) {
                $join->on('job_matched_profiles.profile_id', '=', 'profiles.id');
            })->leftJoin('keywords', function ($join) {
                $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
            })->where('keywords.profile_id', '>', 0)->where('profiles.user_id', $user_id)->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.remove', 0)->select('profiles.id as profile_id', 'profiles.name', 'keywords.generic_title', 'keywords.generic_balance', 'job_matched_profiles.*')->get();

            $relevant_matches = [];

            foreach ($matches as $key => $match) {

                if ($match->profile_keyword_points != "") {
                    $keyword_points = unserialize($match->profile_keyword_points);
                    if (!empty($keyword_points)) {
                        $keyword_points = array_sum($keyword_points);
                    } else {
                        $keyword_points = 0;
                    }

                    $rating_points = unserialize($match->profile_rating_points);

                    if (!empty($rating_points)) {
                        $rating_points = array_sum($rating_points);
                    } else {
                        $rating_points = 0;
                    }

                    $relevant_matches[$match->profile_id]['points'] = $keyword_points + $rating_points;

                    $relevant_matches[$match->profile_id]['name'] = $match->name;
                    $relevant_matches[$match->profile_id]['keywords'] = unserialize($match->profile_keywords);
                    $relevant_matches[$match->profile_id]['keyword_ids'] = unserialize($match->profile_keyword_ids);

                    $relevant_matches[$match->profile_id]['keyword_points'] = unserialize($match->profile_keyword_points);
                    $relevant_matches[$match->profile_id]['rating_types'] = unserialize($match->profile_rating_types);
                    $relevant_matches[$match->profile_id]['rating_points'] = unserialize($match->profile_rating_points);
                    $keywords = unserialize($match->profile_keywords);
                    $relevant_matches[$match->profile_id]['profile_id'] = $match->profile_id;
                    $relevant_matches[$match->profile_id]['deleted_keywords'] = JobMatchedProfile::leftJoin('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.profile_id', $match->profile_id)
                        ->where('job_matched_profiles.remove', 1)->select('keywords.*', 'job_matched_profiles.*')->get();

                    $keyowrds_pos = [];
                    $keyowrds_neg = [];
                    foreach ($relevant_matches[$match->profile_id]['keyword_ids'] as $k => $id) {

                        $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                        if ($balance) {

                            $balance_sign = $balance->generic_balance == 1 ? "+" : "-";

                            $keywords[$k] = $balance_sign . $keywords[$k];

                            if ($balance->generic_balance == 1) {
                                $keyowrds_pos[$id] = $keywords[$k];
                            } else {
                                $keyowrds_neg[$id] = $keywords[$k];
                            }
                        }
                    }

                    $relevant_matches[$match->profile_id]['keywords'] = $keywords;
                    $relevant_matches[$match->profile_id]['positive_keywords_count'] = count($keyowrds_pos);
                    $relevant_matches[$match->profile_id]['negative_keywords_count'] = count($keyowrds_neg);
                    $relevant_matches[$match->profile_id]['positive_keywords'] = $keyowrds_pos;
                    $relevant_matches[$match->profile_id]['negative_keywords'] = $keyowrds_neg;
                }
            }
            return array_values($relevant_matches);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getJobDetails($profileId = "", $jobId = "")
    {
        try {
            $user_id = $this->getUserId();
            $threshold = $this->getThreshold();
            $job = new Job();
            $jobs = $job->newQuery();
            // Return records

            $jobs = $jobs->where('id', $jobId)->first();


            if ($profileId > 0) {

                $matches = JobMatchedProfile::leftJoin('keywords', function ($join) {
                    $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                })->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.profile_id', $profileId)
                    ->select('keywords.*', 'job_matched_profiles.*')->first();

                if ($matches) {
                    // $jobs->relevant_match = array_sum(unserialize($matches->profile_keyword_points));


                    $point1 = 0;
                    $point2 = 0;

                    $jobs->relevant_match = 0;
                    if ($jobs->profile_keyword_points != "") {
                        // $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;
                        $point1 = unserialize($jobs->profile_keyword_points);

                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }
                    }


                    if ($jobs->rating_points != "") {
                        $point2 = unserialize($jobs->rating_points);
                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }

                    $jobs->relevant_match = (int)$point1 + (int)$point2;


                    $keywords = unserialize($matches->profile_keywords);
                    $jobs->keyword_ids = unserialize($matches->profile_keyword_ids);
                    $jobs->match_points = unserialize($matches->profile_keyword_points);
                    $jobs->deleted_keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.profile_id', $profileId)
                        ->where('job_matched_profiles.remove', 1)->select('keywords.*', 'job_matched_profiles.*')->get();
                } else {
                    $keywords = [];
                    $jobs->relevant_match = 0;
                    $jobs->keyword_ids = [];
                    $jobs->keyword_ids = [];
                    $jobs->match_points = [];
                    $jobs->deleted_keywords = [];
                }

            } else {

                if (unserialize($jobs->keyword_ids) && count(unserialize($jobs->keyword_ids)) > 0 || unserialize($jobs->rating_points) && count(unserialize($jobs->rating_points)) > 0) {

                    //   $jobs->relevant_match = array_sum(unserialize($jobs->keyword_points));

                    $point1 = 0;
                    $point2 = 0;

                    $jobs->relevant_match = 0;
                    if ($jobs->keyword_points != "") {
                        // $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->relevant_match = $profile_jobs[$key]->points;
                        $point1 = unserialize($jobs->keyword_points);

                        if (count($point1)) {
                            $point1 = array_sum($point1);
                        }
                    }


                    if ($jobs->rating_points != "") {
                        $point2 = unserialize($jobs->rating_points);
                        if (count($point2)) {
                            $point2 = array_sum($point2);
                        }
                    }
                    $jobs->relevant_match = (int)$point1 + (int)$point2;


                    $keywords = unserialize($jobs->keywords);
                    $jobs->keyword_ids = unserialize($jobs->keyword_ids);
                    $jobs->match_points = unserialize($jobs->keyword_points);
                    $jobs->rating_types = unserialize($jobs->rating_types);
                    $jobs->rating_points = unserialize($jobs->rating_points);
                    $jobs->deleted_keywords = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                        $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
                    })->where('job_matched_keywords.job_id', $jobId)
                        ->where('job_matched_keywords.remove', 1)->select('keywords.*', 'job_matched_keywords.*')->get();
                } else {
                    $keywords = [];
                    $jobs->relevant_match = 0;
                    $jobs->keyword_ids = [];
                    $jobs->keyword_ids = [];
                    $jobs->match_points = [];
                    $jobs->deleted_keywords = [];
                }

            }

            //dd($jobs);
            $keyowrds_pos = [];
            $keyowrds_neg = [];

            //dd($jobs->keyword_ids);
            if ($jobs->keyword_ids) {
                foreach ($jobs->keyword_ids as $k => $id) {

                    $balance = Keyword::where('id', $id)->select('generic_balance')->first();

                    if ($balance) {

                        $balance_sign = $balance->generic_balance == 1 ? "+" : "-";

                        $keywords[$k] = $balance_sign . $keywords[$k];

                        if ($balance->generic_balance == 1) {
                            $keyowrds_pos[$id] = $keywords[$k];
                        } else {
                            $keyowrds_neg[$id] = $keywords[$k];
                        }

                    }
                }
            }

            $jobs->keywords = $keywords;

            $jobs->description = str_replace("â€™", '’', $jobs->description);
            $jobs->description = str_replace("â€¢", "", $jobs->description);
            $jobs->description = stripslashes(str_replace("â€“", "-", $jobs->description));
            $jobs->positive_keywords_count = count($keyowrds_pos);
            $jobs->negative_keywords_count = count($keyowrds_neg);
            $jobs->positive_keywords = $keyowrds_pos;
            $jobs->negative_keywords = $keyowrds_neg;

//            $jobs->posted_on = $jobs->posted_on != NULL ? date('M d, Y', strtotime($jobs->posted_on)): date('M d, Y', strtotime($jobs->created_at));
            $jobs->posted_on = date('M d, Y', strtotime($jobs->created_at));
            $jobs->close_on = date('M d, Y', strtotime($jobs->close_on));
            $jobs->profile_id = $profileId;
            $jobs->matching_profiles = self::getMatchingProfiles($jobs->id);

            $jobDetail = $jobs;


            $weights = Weight::where('user_id', $user_id)->orderby('sort', 'asc')->get();
            $profiles = Profile::where('user_id', $user_id)->orderby('name', 'asc')->get();

            return view("admin.details", compact("jobDetail", "weights", "profiles", "threshold"));

        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function postJobStatusAll(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            //  dd($data);
            if (isset($data["action"]) && $data["action"] == "ignore") {
                Job::whereIn('id', $data["relevant_checkbox"])->update(['status' => 2]);
                return response()->json(['success' => true, 'msg' => 'Jobs status updated successfully.']);
            }
            if (isset($data["action"]) && $data["action"] == "approve") {
                Job::whereIn('id', $data["relevant_checkbox"])->update(['status' => 1]);
                return response()->json(['success' => true, 'msg' => 'Jobs status updated successfully.']);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function postJobRemove(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getEditjob($profileId, $jobId)
    {
        try {
            $user_id_is = $this->getUserId();
            $threshold = $this->getThreshold();
            $job = new Job();
            $jobs = $job->newQuery();
            // Return records

            $jobs = $jobs->where('id', $jobId)->first();

            if ($profileId > 0) {
                $matches = JobMatchedProfile::leftJoin('keywords', function ($join) {
                    $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                })->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.profile_id', $profileId)
                    ->select('keywords.*', 'job_matched_profiles.*')->first();

                if ($matches) {
                    $jobs->relevant_match = array_sum(unserialize($matches->profile_keyword_points));
                    $keywords = unserialize($matches->profile_keywords);
                    $jobs->keyword_ids = unserialize($matches->profile_keyword_ids);
                    $jobs->match_points = unserialize($matches->profile_keyword_points);
                    $jobs->deleted_keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                        $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                    })->where('job_matched_profiles.job_id', $jobId)->where('job_matched_profiles.profile_id', $profileId)
                        ->where('job_matched_profiles.remove', 1)->select('keywords.*', 'job_matched_profiles.*')->get();
                } else {
                    $keywords = [];
                    $jobs->relevant_match = 0;
                    $jobs->keyword_ids = [];
                    $jobs->keyword_ids = [];
                    $jobs->match_points = [];
                    $jobs->deleted_keywords = [];
                }

            } else {

                if (unserialize($jobs->keyword_ids) && count(unserialize($jobs->keyword_ids)) > 0) {
                    $jobs->relevant_match = array_sum(unserialize($jobs->keyword_points));
                    $keywords = unserialize($jobs->keywords);
                    $jobs->keyword_ids = unserialize($jobs->keyword_ids);
                    $jobs->match_points = unserialize($jobs->keyword_points);
                    $jobs->deleted_keywords = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                        $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
                    })->where('job_matched_keywords.job_id', $jobId)
                        ->where('job_matched_keywords.remove', 1)->select('keywords.*', 'job_matched_keywords.*')->get();
                } else {
                    $keywords = [];
                    $jobs->relevant_match = 0;
                    $jobs->keyword_ids = [];
                    $jobs->keyword_ids = [];
                    $jobs->match_points = [];
                    $jobs->deleted_keywords = [];
                }

            }

            //dd($jobs);
            $keyowrds_pos = [];
            $keyowrds_neg = [];


            if ($jobs->keyword_ids) {
                foreach ($jobs->keyword_ids as $k => $id) {
                    $balance = Keyword::where('id', $id)->select('generic_balance')->first();
                    if ($balance) {
                        $balance_sign = $balance->generic_balance == 1 ? "+" : "-";
                        $keywords[$k] = $balance_sign . $keywords[$k];
                        if ($balance->generic_balance == 1) {
                            $keyowrds_pos[$id] = $keywords[$k];
                        } else {
                            $keyowrds_neg[$id] = $keywords[$k];
                        }

                    }
                }
            }
            $jobs->keywords = $keywords;

            $jobs->description = str_replace("â€™", '’', $jobs->description);
            $jobs->description = str_replace("â€¢", "", $jobs->description);
            $jobs->description = stripslashes(str_replace("â€“", "-", $jobs->description));
            $jobs->positive_keywords_count = count($keyowrds_pos);
            $jobs->negative_keywords_count = count($keyowrds_neg);
            $jobs->positive_keywords = $keyowrds_pos;
            $jobs->negative_keywords = $keyowrds_neg;
            $jobs->posted_on = $jobs->posted_on != NULL ? date('M d, Y', strtotime($jobs->posted_on)): date('M d, Y', strtotime($jobs->created_at));
            $jobs->close_on = date('M d, Y', strtotime($jobs->close_on));
            $jobs->profile_id = $profileId;
            $jobs->matching_profiles = self::getMatchingProfiles($jobs->id);


            //dd($jobs);
            $jobDetail = $jobs;

            $weights = Weight::where('user_id', $user_id_is)->orderby('sort', 'asc')->get();
            $profiles = Profile::where('user_id', $user_id_is)->orderby('name', 'asc')->get();

            return view("admin.editjob", compact("jobDetail", "weights", "profiles", "threshold"));

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postUpdateJob(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            \DB::enableQueryLog();
            Job::where('id', $data["job_id"])->update([
                'job_title' => $data["title"],
                'website' => $data["website"],
                'category' => $data["category"],
                'country' => $data["location"],
                'job_type' => $data["type"],
                'company' => $data["company"],
                'contact_name' => addslashes($data["contact_name"]),
                'contact_email' => $data["contact_email"],
                'contact_phone' => $data["contact_phone"],
                'reference_id' => $data["reference_id"],
                'source_url' => $data["source_url"],
                'posted_on' => date('Y-m-d H:i:s', strtotime($data["start"])),
                'close_on' => date('Y-m-d H:i:s', strtotime($data["deadline"])),
                'experience_from' => $data["experience_from"],
                'experience_to' => $data["experience_to"],
                'description' => addslashes($data["description"])
            ]);

            return response()->json(['success' => true, 'msg' => 'Job updated successfully.']);

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function getAddJob()
    {
        $user_id = $this->getUserId();
        $weights = Weight::where('user_id', $user_id)->orderby('sort', 'asc')->get();
        $profiles = Profile::where('user_id', $user_id)->orderby('name', 'asc')->get();
        return view("admin.addjob", compact("weights", "profiles"));
    }

    public function postCreateJob(Request $request)
    {
        $user_id = $this->getUserId();
        try {
            $data = $request->except("_method", "_token");

            $job = new Job();
            $job->user_id = $user_id;
            $job->job_title = addslashes($data['title']);
            $job->company = $data['company'];
            $job->website = $data['website'];
            $job->category = $data['category'];
            $job->country = $data['location'];
            $job->description = addslashes($data['description']);
            $job->reference_id = $data['reference_id'];
            $job->contact_name = addslashes($data['contact_name']);
            $job->contact_email = $data['contact_email'];
            $job->contact_phone = $data['contact_phone'];
            $job->source_url = $data['source_url'];
            $job->job_type = $data['type'];
            $job->posted_on = date('Y-m-d', strtotime($data['start']));
            $job->close_on = date('Y-m-d', strtotime($data['deadline']));
            $job->experience_from = $data["experience_from"];
            $job->experience_to = $data["experience_to"];
            $job->save();


            $data['job_id'] = $job->id;
            if (isset($data['keyword_title']) && $data['keyword_title'] != "") {

                if ($data['keyword_exist'] == "false") {

                    $keyword = new Keyword();
                    $keyword->generic_title = $data['keyword_title'];
                    $keyword->generic_weight = $data['keyword_weight'];
                    $keyword->generic_balance = $data['keyword_balance'];
                    $keyword->profile_id = $data['profile_id'];
                    $keyword->user_id = $user_id;

                    return response()->json($keyword);

                    $keyword->save();
                    $data['keyword_id'] = $keyword->id;
                }

                if ($data['profile_id'] > 0) {

                    $generic_keywords = $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                        $join->on('keywords.generic_weight', '=', 'weights.id');
                    })->where('keywords.profile_id', $data['profile_id'])
                        ->where('keywords.id', $data['keyword_id'])->where('keywords.user_id', $user_id)->select('keywords.*', 'weights.rating')->first();

//             dd($generic_keywords);
                    $jmprofile = new JobMatchedProfile();

                    $keyword_exist = $jmprofile->where("job_id", $data['job_id'])->where("keyword_id", $data['keyword_id'])->where("profile_id", $data['profile_id'])->first();
                    if ($keyword_exist > 0) {
                        return response()->json(['error' => true, 'msg' => 'Keyword already exists for this job.']);
                    } else {
                        $jmprofile->job_id = $data['job_id'];
                        $jmprofile->keyword_id = $data['keyword_id'];
                        $jmprofile->profile_id = $data['profile_id'];
                        $jmprofile->points = $generic_keywords->rating * $generic_keywords->generic_balance;
                        $jmprofile->save();
                    }
                } else {

                    $generic_keywords = $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                        $join->on('keywords.generic_weight', '=', 'weights.id');
                    })->where('keywords.profile_id', $data['profile_id'])->where('keywords.id', $data['keyword_id'])->where('keywords.user_id', $user_id)->select('keywords.*', 'weights.rating')->first();

                    $jmkeyword = new JobMatchedKeyword;

                    $keyword_exist = $jmkeyword->where("job_id", $data['job_id'])->where("keyword_id", $data['keyword_id'])->count();
                    if ($keyword_exist > 0) {
                        return response()->json(['error' => true, 'msg' => 'Keyword already exists for this job.']);
                    } else {
                        $jmkeyword->job_id = $data['job_id'];
                        $jmkeyword->user_id = $user_id;
                        $jmkeyword->keyword_id = $data['keyword_id'];
                        $jmkeyword->points = $generic_keywords->rating * $generic_keywords->generic_balance;
                        $jmkeyword->save();
                    }
                }
            }

            return response()->json(['success' => true, 'msg' => 'Job created successfully.']);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function postFindKeyword(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");

            $query = new Keyword();
            $keyword = $query->whereRaw('`profile_id` = ? AND LOWER(`generic_title`) = ?', [$data["profile_id"], trim(strtolower($data['keyword_title']))])->first();

            if ($keyword) {
                return response()->json(['success' => true, 'exist' => true, 'keyword_id' => $keyword->id]);
            } else {
                return response()->json(['success' => true, 'exist' => false, 'keyword_id' => 0]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postAddJobkeyword(Request $request)
    {

        $user_id = $this->getUserId();

        try {
            $data = $request->input();

            if ($data['keyword_exist'] == "false") {

                $keyword = new Keyword();
                $keyword->generic_title = $data['keyword_title'];
                $keyword->generic_weight = $data['keyword_weight'];
                $keyword->generic_balance = $data['keyword_balance'];
                $keyword->profile_id = $data['keyword_profile'];
                $keyword->user_id = $user_id;

                $keyword->save();
                $data['keyword_id'] = $keyword->id;
            }

            if ($data['keyword_profile'] > 0) {

                $generic_keywords = $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.profile_id', $data['keyword_profile'])
                    ->where('keywords.id', $data['keyword_id'])->select('keywords.*', 'weights.rating')->first();


                $jmprofile = new JobMatchedProfile();

                $keyword_exist = $jmprofile->where("job_id", $data['job_id'])->where("keyword_id", $data['keyword_id'])->where("profile_id", $data['keyword_profile'])->first();
                if ($keyword_exist > 0) {
                    return response()->json(['error' => true, 'msg' => 'Keyword already exists for this job.']);
                } else {
                    $jmprofile->job_id = $data['job_id'];
                    $jmprofile->keyword_id = $data['keyword_id'];
                    $jmprofile->profile_id = $data['keyword_profile'];
                    $jmprofile->points = $generic_keywords->rating * $generic_keywords->generic_balance;
                    $jmprofile->save();
                }
            } else {

                $generic_keywords = $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.profile_id', $data['keyword_profile'])->where('keywords.id', $data['keyword_id'])->select('keywords.*', 'weights.rating')->first();

                $jmkeyword = new JobMatchedKeyword;

                $keyword_exist = $jmkeyword->where("job_id", $data['job_id'])->where("keyword_id", $data['keyword_id'])->count();
                if ($keyword_exist > 0) {
                    return response()->json(['error' => true, 'msg' => 'Keyword already exists for this job.']);
                } else {

                    $jmkeyword->job_id = $data['job_id'];
                    $jmkeyword->user_id = $user_id;
                    $jmkeyword->keyword_id = $data['keyword_id'];
                    $jmkeyword->points = $generic_keywords->rating * $generic_keywords->generic_balance;
                    $jmkeyword->save();
                }
            }

            return response()->json(['success' => true, 'msg' => 'Keyword added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postDeleteJobkeyword(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if ($data["profile_id"] > 0) {
                JobMatchedProfile::where('job_id', $data["job_id"])->where('keyword_id', $data["keyword_id"])->where('profile_id', $data["profile_id"])->update(["remove" => $data["remove"]]);
            } else {
                JobMatchedKeyword::where('job_id', $data["job_id"])->where('keyword_id', $data["keyword_id"])->update(["remove" => $data["remove"]]);
            }

            return response()->json(['success' => true, 'msg' => 'Keyword removed successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postUpdateCron(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");

            $job = Job::where("id", $data["job_id"])->first();

            if ($data["profile_id"] > 0) {
                $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.profile_id', $data["profile_id"])->select('keywords.*', 'weights.rating')->get();

                foreach ($generic_keywords as $keyword) {
                    $all_generic_keywords = Keyword::where("profile_id", $data["profile_id"])->pluck('id')->all();

                    $matched_keywords = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->get();

                    foreach ($matched_keywords as $v) {
                        if (!in_array($v->keyword_id, $all_generic_keywords)) {
                            JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('keyword_id', $v->keyword_id)->delete();
                        }
                    }

                    $update = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('keyword_id', $keyword->id)->update(['points' => $keyword->rating * $keyword->generic_balance]);

                    if ($update) {
                        $profile_match_rating = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->first();
                        $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->orderBy('keyword_id', 'asc')->pluck('points')->toArray();
                        $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                            $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                        })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);


                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->update(['total_points' => array_sum($points), 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()),
                            'profile_keyword_points' => serialize($points), 'profile_rating_types' => $profile_match_rating->profile_rating_types, 'profile_rating_points' => $profile_match_rating->profile_rating_points]);
                    }
                }

            } else {
                $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('keywords.profile_id', 0)->select('keywords.*', 'weights.rating')->get();

                foreach ($generic_keywords as $keyword) {

                    $all_generic_keywords = Keyword::where("profile_id", 0)->pluck('id')->all();

                    $matched_keywords = JobMatchedKeyword::where('job_id', $job->id)->get();

                    foreach ($matched_keywords as $v) {
                        if (!in_array($v->keyword_id, $all_generic_keywords)) {
                            JobMatchedKeyword::where('job_id', $job->id)->where('keyword_id', $v->keyword_id)->delete();
                        }
                    }

                    $update = JobMatchedKeyword::where('job_id', $job->id)->where('keyword_id', $keyword->id)->update(['points' => $keyword->rating * $keyword->generic_balance]);

                    if ($update) {
                        $points = JobMatchedKeyword::where('job_id', $job->id)->where('job_matched_keywords.remove', 0)->pluck('points')->toArray();
                        $keywords = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                            $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
                        })->where('job_matched_keywords.job_id', $job->id)->where('job_matched_keywords.remove', 0);


                        Job::where('id', $job->id)->update(['points' => array_sum($points), 'keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'keyword_points' => serialize($points)]);
                    }

                }
            }

            return response()->json(['success' => true, 'msg' => 'Cron updated successfully.']);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postExecutegenericCron()
    {
        try {
            $user_id = $this->getUserId();
            $cron = Admin::where('id', $user_id)->first();
            //  dd($setting);
            if ($cron->generic_cron == 2) {
                Admin::where('id', $user_id)->update(["generic_cron" => 0]);
                //Artisan::call('algorithm:generickeywords');
                return response()->json(['success' => true, 'msg' => "Cron execution started."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Cron execution is in progress."]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postExecuteprofileCron()
    {
        try {
            $user_id = $this->getUserId();
            $cron = Admin::where('id', $user_id)->first();
            //Artisan::call('algorithm:profilekeywords');

            if ($cron->profile_cron == 2) {

                Admin::where('id', $user_id)->update(["profile_cron" => 0]);
                Artisan::call('algorithm:profilekeywords');
                return response()->json(['success' => true, 'msg' => "Cron execution started."]);

            } else {
                return response()->json(['success' => false, 'msg' => "Cron execution is in progress."]);
            }


        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }


    public function getCronStatus()
    {
        try {
            $setting = Setting::where('id', 1)->first();
            return response()->json(['success' => false, 'cron_status' => $setting->status]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postCompanyRating(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $user_id = $this->getUserId();
            $location_rating = new CompanyRating();
            $location_rating->profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
            $location_rating->company = $data['company'];
            $location_rating->rating = $data['rating'];
            $location_rating->user_id = $user_id;
            $location_rating->save();

            return response()->json(['success' => true, 'msg' => "Company Rating added successfully."]);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getCompanyRating(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;

            $companies = CompanyRating::where("profile_id", $profile_id)->where("user_id", $user_id)->get();


            return response()->json(['total_companies' => count($companies), 'data' => $companies]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postCompanyDelete($id)
    {
        try {
            CompanyRating::where('id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'Company delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postLocationRating(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $user_id = $this->getUserId();

            $location_rating = new LocationRating();
            $location_rating->profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
            $location_rating->country = $data['country'];
            $location_rating->rating = $data['rating'];
            $location_rating->user_id = $user_id;

            $location_rating->save();

            return response()->json(['success' => true, 'msg' => "Location Rating added successfully."]);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getLocationRating(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;


            $locations = LocationRating::where("profile_id", $profile_id)->where("user_id", $user_id)->get();


            return response()->json(['total_locations' => count($locations), 'data' => $locations]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postLocationDelete($country, Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
            LocationRating::where('profile_id', $profile_id)->where('country', $country)->delete();
            return response()->json(['success' => true, 'msg' => 'Location delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postJobtypeRating(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $user_id = $this->getUserId();
            $jobtype = JobtypeRating::where('profile_id', $data['profile_id'])->where('user_id', $user_id)->where('jobtype', $data['job_type'])->count();

            if ($jobtype == 0) {
                $job_rating = new JobtypeRating();
                $job_rating->profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
                $job_rating->jobtype = $data['job_type'];
                $job_rating->rating = $data['rating'];
                $job_rating->user_id = $user_id;

                $job_rating->save();

                return response()->json(['success' => true, 'msg' => "Job type Rating added successfully."]);
            } else {
                return response()->json(['success' => false, 'msg' => "Job type Rating already exists."]);
            }

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postJobtypeRatingUpdate(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
            JobtypeRating::where('profile_id', $profile_id)->where('id', $data['id'])->update(['rating' => $data['rating'], 'jobtype' => $data['job_type']]);

            return response()->json(['success' => true, 'msg' => "Job type Rating added successfully."]);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getAllJobtypeRating(Request $request)
    {
        try {
            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;

            $jobtype = JobtypeRating::where("profile_id", $profile_id)->where("user_id", $user_id)->get();


            return response()->json(['total_jobtypes' => count($jobtype), 'data' => $jobtype]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getJobtypeRating($id, Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;

            $jobtype = JobtypeRating::where("profile_id", $profile_id)->where("id", $id)->first();

            return response()->json(['success' => true, 'data' => $jobtype]);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getExperienceRating(Request $request)
    {
        try {
            $user_id = $this->getUserId();

            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;

            $experiences = ExperienceRating::where("profile_id", $profile_id)->where("user_id", $user_id)->get();


            return response()->json(['total_experiences' => count($experiences), 'data' => $experiences]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postExperienceRating(Request $request)
    {
        try {

            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $profile_id = isset($data['profile_id']) ? $data['profile_id'] : 0;
            $experience_rating = new ExperienceRating();
            $experience_rating->profile_id = $profile_id;
            $experience_rating->experience_from = $data['experience_from'];
            $experience_rating->experience_to = $data['experience_to'];
            $experience_rating->rating = $data['rating'];
            $experience_rating->user_id = $user_id;

            $experience_rating->save();

            return response()->json(['success' => true, 'msg' => "Experience Rating added successfully."]);

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postExperienceDelete($id)
    {
        try {
            ExperienceRating::where('id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'Experience deleted successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getUsers()
    {
        if (\Auth::guard('admin')->user()->type == 'admin') {
            return redirect('/admin/dashboard');
        }
        return view("admin.users");
    }

    public function postAddUser(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), Validation::get_rules("admin", "create"));

            if ($validator->fails()) {
                return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
            }

            $data = $request->except("_method", "_token");
            $admin = new Admin();
            $admin->username = $data["username"];
            $admin->first_name = $data["user_first_name"];
            $admin->last_name = $data["user_last_name"];
            $admin->email = $data["user_email"];
            $admin->password = bcrypt($data["user_password"]);
            $admin->type = "admin";
            $admin->generic_cron = 2;
            $admin->profile_cron = 2;
            $insert = $admin->save();
            $faf = new FireAndForget();
            $faf->post(route('clone-jobs'), ["user_id" => $admin->id]);
            if ($insert) {
                $file = $data["user_image"];
                $imagename = $admin->id . '_' . trim($file->getClientOriginalName());

                $destinationPath = public_path('uploads/thumb');
                $thumb_img = Image::make($file->getRealPath())->resize(100, 100);
                $thumb_img->save($destinationPath . '/' . $imagename, 80);


                $destinationPath = public_path('uploads/original');
                $file->move($destinationPath, $imagename);
                Admin::where("id", $admin->id)->update(["profile_pic" => $imagename]);
                return response()->json(['success' => true, 'msg' => "User added successfully."]);
            } else {
                return response()->json(['success' => false, 'msg' => "User failed to add."]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getAllUsers(Request $request)
    {
        $data = $request->except("_method", "_token");

        $admins = new Admin();
        $admins = $admins->newQuery();

        // Filter by job title
        $search_text = trim($data["search_text"]);
        if ($search_text != '') {

            $admins->where('first_name', 'LIKE', '%' . $search_text . '%')->orWhere('last_name', 'LIKE', '%' . $search_text . '%')->orWhere('email', $search_text);
        }

        $admin_users = $admins->where("type", "!=", "superadmin")->offset($data["offset"])->limit($data["pagesize"])
            ->get();

        return response()->json(['success' => true, 'total_users' => Admin::where("type", "!=", "superadmin")->count(), 'data' => $admin_users]);
    }

    public function getAdminData(Request $request)
    {
        $data = $request->except("_method", "_token");
        $admin = Admin::where("id", $data["admin_id"])->first();
        return response()->json(['success' => true, 'data' => $admin]);
    }

    public function postUpdateUser(Request $request)
    {
        try {
            $validator = \Validator::make($request->all(), Validation::get_rules("admin", "update_user"));

            if ($validator->fails()) {
                return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
            }

            $data = $request->except("_method", "_token");
            $user_id = $data["user_id"];
            unset($data["user_id"]);
            if ($data["password"] == null) {
                unset($data["password"]);
            } else {
                $data["password"] = bcrypt($data["password"]);
            }

            if (isset($data["user_image"]) && $data["user_image"]) {
                $file = $data["user_image"];
                $imagename = $user_id . '_' . trim($file->getClientOriginalName());

                $destinationPath = public_path('uploads/thumb');
                $thumb_img = Image::make($file->getRealPath())->resize(100, 100);
                $thumb_img->save($destinationPath . '/' . $imagename, 80);


                $destinationPath = public_path('uploads/original');
                $file->move($destinationPath, $imagename);
                Admin::where("id", $user_id)->update(["profile_pic" => $imagename]);

            } else {
                unset($data["user_image"]);
                Admin::where("id", $user_id)->update($data);
            }

            return response()->json(['success' => true, 'msg' => "User updated successfully."]);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postDeleteUser($id)
    {
        try {
            Admin::where('id', $id)->delete();
            Job::where('user_id', $id)->delete();
            return response()->json(['success' => true, 'msg' => 'User delete successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postDeleteUsers(Request $request)
    {
        try {
            $data = $request->input();
            if (isset($data['user_checkbox'])) {
                $user_ids = $data['user_checkbox'];
            } else {
                $user_ids = $data['user_checkbox'];
            }
            Admin::whereIn('id', $user_ids)->delete();
            Job::whereIn('user_id', $user_ids)->delete();
            return response()->json(['success' => true, 'msg' => 'Users deleted successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getSelectedJobs(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if (isset($data["relevant_checkbox"])) {
                $job_ids = $data["relevant_checkbox"];
                $jobs = Job::whereIn("id", $job_ids)->orderBy('points', 'desc')->get();

                foreach ($jobs as $key => $job) {
                    $jobs[$key]->description = substr(stripslashes(strip_tags($jobs[$key]->description)), 0, 300) . '...';
                }

                return response()->json(['success' => true, 'data' => $jobs]);
            } else {
                return response()->json(['success' => true, 'data' => []]);
            }

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postSendNewsletter(Request $request)
    {
        try {
            $data = $request->except("_method", "_token");
            if (isset($data["email"])) {
                if (isset($data["relevant_checkbox"])) {
                    $job_ids = $data["relevant_checkbox"];
                    $jobs = Job::whereIn("id", $job_ids)->orderBy('points', 'desc')->get();
                    foreach ($jobs as $key => $job) {
                        $jobs[$key]->description = substr(stripslashes(strip_tags($jobs[$key]->description)), 0, 300) . '...';
                    }

                    $data = ["jobs" => $jobs, "email" => $data["email"]];
                    Mail::send('admin.emails.newsletter', $data, function ($message) use ($data) {
                        $message->from('devdyna@gmail.com', 'Devdyna');

                        $message->to($data["email"])->cc('devdyna@gmail.com');
                        $message->subject("Newsletter");
                    });

                    return response()->json(['success' => true, 'msg' => "Mail sent successfully."]);
                } else {
                    return response()->json(['success' => false, 'msg' => "Jobs not selected, please select atleast one job."]);
                }
            } else {
                return response()->json(['success' => false, 'msg' => "Invalid email."]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getSettings()
    {
        $user_id = $this->getUserId();
        $data['admin'] = Admin::where("id", "=", $user_id)->get()->toArray();
        return view("admin.my-settings", $data);
    }

    public function postSettings(Request $request)
    {
        try {

            $data = $request->except("_method", "_token");
            $user_id = $data["user_id"];
            $threshold = $data["threshold"];
            $data['admin'] = Admin::where("id", $user_id)->update(["threshold" => $threshold]);
            \Session::flash('success', 'Threshold value updated successfully.');

            // return response()->json(['success' => true, 'msg' => "User updated successfully."]);
            return \Redirect::to('admin/my-settings');

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }


    public function getResetAdminPassword()
    {
        return view('admin.reset_password');
    }

    public function postResetAdminPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if (!$validator->fails()) {

            $email = request()->input('email');
            $user = Admin::where('email', $email)->first();

            if ($user) {
                $token = str_random(64);
                $user_name = $user->first_name . " " . $user->last_name;


                $mail = Mail::send('admin.emails.SendReserPasswordLink', ['actionUrl' => $token, 'email' => $email, "user_name" => $user_name], function (Message $message) use ($user) {
                    $message->subject(config('app.name') . ' Password Reset Link');
                    $message->to($user->email);
                });

                Admin::where('email', $email)->update([
                    "password_reset_token" => $token
                ]);
                return back()->with('success', 'Mail sent successfully.');

            } else {
                return back()->with('error', 'User not Found for this Email Address.');
            }
        }
        return redirect()->back()->withInput()->withErrors($validator);
    }


    public function resetPassword(Request $request, $email, $verificationLink)
    {
        $user = Admin::where('email', $email)->where('password_reset_token', "=", $verificationLink)->first();
        $userid = $user->id;
        $data["userid"] = $userid;
        return view('admin.new_password', $data);
    }


    public function newPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        ]);

        if (!$validator->fails()) {
            $param = $request->all();
            $userid = $param['userid'];
            $new_password = $param['new_password'];
            $data["password"] = bcrypt($new_password);
            $updated_status = Admin::where("id", $userid)->update($data);

            if ($updated_status) {
                return redirect('/admin/login')->with("success", 'Password Reset successfully.');
            }
        } else {
            return redirect()->back()->withInput()->withErrors($validator);
        }
    }
    public function excelExportApprovedData(Request $request)
    {
        try {
            $csvExporter = new \Laracsv\Export();
            $user_id = $this->getUserId();
            $data = $request->except("_method", "_token");
            $newJob = new Job();
            if (!empty($data['profile_id']) && $data['profile_id'] > 0) {
                $getJobData = DB::table('jobs')
                    ->join('job_matched_profiles', 'jobs.id', '=', 'job_matched_profiles.job_id')
                    ->where('job_matched_profiles.profile_id', $data['profile_id'])
                    ->where('jobs.status', 1)->get()->sortByDesc('points');

            } else {
                $getJobData = $newJob->getJobDataByID($user_id, 1);
            }
            $csvExporter->beforeEach(function ($getJobData) {

                $job_type = "";
                if(isset($getJobData->source_url) && !empty($getJobData->source_url)){
                    $getJobData->source_url = htmlspecialchars_decode(urldecode($getJobData->source_url));
                }
                if (isset($getJobData->job_type) && !empty($getJobData->job_type)) {
                    if ($getJobData->job_type == 1) {
                        $job_type = "Full Time";
                    } elseif ($getJobData->job_type == 2) {
                        $job_type = "Part Time";
                    } elseif ($getJobData->job_type == 3) {
                        $job_type = "Internship";
                    } else {
                        $job_type = "";
                    }
                }
                $getJobData->job_type = (isset($job_type) != "" ? $job_type : "");
            });

            $csvExporter->build($getJobData, ['id', 'company', 'website', 'job_title', 'posted_on', 'country','source_url', 'job_type'])->download('Approved_Jobs.csv');

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postJobToProfle(Request $request){
        try {
            $data = $request->except("_method", "_token");
            Job::where('id',$data["job_id"])->update(["status"=>1]);
            $exist = JobMatchedProfile::where('job_id',$data["job_id"])->where('profile_id',$data["profile_id"])->count();
            if($exist <= 0){
                $jobmatchedprofile = new JobMatchedProfile();
                $jobmatchedprofile->job_id = $data["job_id"];
                $jobmatchedprofile->profile_id = $data["profile_id"];
                $jobmatchedprofile->save();
                return response()->json(['success' => true, 'msg' => "Job assigned to profile successfully."]);
            }else{
                return response()->json(['success' => false, 'msg' => "Job already exist in selected profile."]);
            }

        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }

    public function upload_bulk_csv_keywords(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'file' => 'required|mimes:csv,txt',
            ]);

            if (!$validator->fails()) {
                $file = $request->file;

                $customerArr = self::csvToArray($file);
                foreach ($customerArr as $key => $value) {
                    $data[] = $value[0];
                }

                return response()->json(['status' => 'success', "message" => "Keyword listed successfully.", "data" => $data]);

            } else {
                return response()->json(['status' => 'error', "message" => $validator->errors()->first()]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postBulkKeyword(Request $request)
    {
        try {
            $params = $request->all();
            $title = explode(',', $params['keyword_title']);
            $finalArray = [];
            $user_id = $this->getUserId();
            $profile = 0;
            if (isset($params['profiles'])) {
                $profile = $params['profiles'];
            }
            $keyword_weight = (isset($params['keyword_weight']) != "" ? $params['keyword_weight'] : "");
            $keyword_balance = (isset($params['keyword_balance']) != "" ? $params['keyword_balance'] : "");
            foreach ($title as $key => $value) {
                $finalArray [] = [
                    'generic_title' => (isset($value) != "" ? $value : ""),
                    'generic_weight' => $keyword_weight,
                    'generic_balance' => $keyword_balance,
                    'profile_id' => $profile,
                    'user_id' => $user_id,
                ];
            };
            Keyword::insert($finalArray);
            return response()->json(['success' => true, 'msg' => 'Keyword added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function postBulkProfileKeyword(Request $request)
    {
        try {
            $params = $request->all();
            $title = explode(',', $params['keyword_title']);
            $user_id = $this->getUserId();
            $profile = 0;
            if (isset($params['profiles'])) {
                $profile = $params['profiles'];
            }
            $keyword_weight = (isset($params['keyword_weight']) != "" ? $params['keyword_weight'] : "");
            $keyword_balance = (isset($params['keyword_balance']) != "" ? $params['keyword_balance'] : "");
            foreach ($title as $key => $value) {
                $finalArray [] = [
                    'profile_id' => $profile,
                    'generic_title' => (isset($value) != "" ? $value : ""),
                    'generic_weight' => $keyword_weight,
                    'generic_balance' => $keyword_balance,
                    'user_id' => $user_id,
                ];
            };
            Keyword::insert($finalArray);
            Profile::where('id', $params['profiles'])->update(["updated_at" => date('Y-m-d H:i:s')]);
            return response()->json(['success' => true, 'msg' => 'Keyword added successfully.']);
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getExportRelevantJobs(Request $request)
    {
        try {

            $data = $request->except("_method", "_token");

            if (isset($data["relevant_checkbox"])) {
                foreach ($data["relevant_checkbox"] as $key=>$value){
                    $get_record = Job::select('id', 'company', 'website', 'job_title', 'posted_on', 'country', 'source_url', 'job_type')->where('id',$value)->first();
                    $data ['id'] = $get_record['id'];
                    $data ['company'] = $get_record['company'];
                    $data ['website'] = $get_record['website'];
                    $data ['job_title'] = $get_record['job_title'];
                    $data ['posted_on'] = $get_record['posted_on'];
                    $data ['country'] = $get_record['country'];

                    $job_type = "";
                    $source_url = "";
                    if (isset($get_record['source_url']) && !empty($get_record['source_url'])) {
                        $source_url = htmlspecialchars_decode(urldecode($get_record['source_url']));
                    }
                    if (isset( $get_record['job_type']) && !empty( $get_record['job_type'])) {
                        if ( $get_record['job_type'] == 1) {
                            $job_type = "Full Time";
                        } elseif ( $get_record['job_type'] == 2) {
                            $job_type = "Part Time";
                        } elseif ( $get_record['job_type'] == 3) {
                            $job_type = "Internship";
                        } else {
                            $job_type = "";
                        }
                    }
                    $data ['source_url'] = $source_url;
                    $data ['job_type'] = $job_type;
                    $getJobData[] = $data;
                }
                $jobs_data = collect($getJobData);

                return response()->json(['success' => true, 'msg' => "Selected relevant jobs download successfully!", "data"=>$jobs_data]);
            } else {
                return response()->json(['success' => false, 'msg' => "Select at least one job!"]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }

    }

}
