<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;
use App\Job;
use App\Admin;

class UserJobsController extends Controller
{
    public function postAuthenticate(Request $request)
    {
        try {
            $params = $request->all();
            $validator = Validator::make($params, [
                'username' => 'required',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
            }
            $doLogin = Admin::doLogin($params);
            if($doLogin) {
                $token = str_random(16);
                $update = Admin::where('username', $params["username"])->update(['access_token' => $token]);
                if ($update) {
                    return response()->json(['success' => true, 'token' => $token]);
                } else {
                    return response()->json(['success' => false, 'msg' => 'Unable to generate token.']);
                }
            }else{
                return response()->json(['success' => false, 'msg' => 'You entered wrong credentials,please try with correct credentials.']);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

    public function getJobs(Request $request){
        try {
            if($request->headers->has('access-token') && $request->header('access-token')!= '') {
                $validator = Validator::make($request->all(), [
                    'date_from' => 'required|date|date_format:Y-m-d',
                    'date_to' => 'required|date|date_format:Y-m-d',
                ]);
                if ($validator->fails()) {
                    return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
                }
                $admin_user = Admin::where('access_token', $request->header('access-token'))->first();
                if ($admin_user) {
                    $jobs = Job::where('user_id', $admin_user->id)
                        ->where('created_at', '>=', $request->input('date_from'))
                        ->where('created_at', '<=', $request->input('date_to'))
                        ->get();
                    if ($validator->fails()) {
                        return response()->json(['success' => false, 'msg' => $validator->errors()->first()]);
                    } else {
                        return response()->json(['success' => true, 'data' => $jobs]);
                    }
                } else {
                    return response()->json(['success' => false, 'msg' => "Invalid access token."]);
                }
            }else{
                return response()->json(['success' => false, 'msg' => "Access token is not found in headers."]);
            }
        } catch (Exception $e) {
            return response()->json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }
}
