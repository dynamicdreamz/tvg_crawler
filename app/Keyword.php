<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'profile_id','user_id', 'generic_title', 'generic_weight', 'generic_balance', 'rating'
    ];
}
