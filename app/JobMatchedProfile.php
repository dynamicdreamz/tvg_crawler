<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobMatchedProfile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'job_id', 'profile_id', 'keyword_id'
    ];
}
