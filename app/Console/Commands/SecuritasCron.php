<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SecuritasCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'securitas:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();
            $pagination = ceil(4025 / 50);
            for ($i = 1; $i < $pagination; $i++) {
                //print_r($i.",");
                $html_data = HtmlDomParser::file_get_html("https://www.securitasjobs.com/en-US/search?pagenumber=" . $i);
                $job_data = $html_data->find('table[id=job-result-table]', 0)->find('tr[class=job-result]');
                for ($j = 0; $j < count($job_data); $j++) {
                    $postdate = $job_data[$j]->find('td', 3)->innertext;
                    $posted_date = date('Y-m-d', strtotime(trim($postdate)));
                    if (strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date)) {
                        $url = $job_data[$j]->find('td', 0)->find('a', 0)->href;
                        $source_url = "https://www.securitasjobs.com" . $url;
                        $job_id = explode("/", $url);
                        $job_id = end($job_id);
                        //print_r($job_id.",");
                        $job_title = $job_data[$j]->find('td', 0)->find('a', 0)->innertext;
                        $job_title = trim($job_title);
                        $country = "United States";
                        $jobdesc = HtmlDomParser::file_get_html($source_url);
                        if(isset($jobdesc)){
                            $desc = $jobdesc->find('div[class=job-details-content]', 0)->find('div[class=container]', 0)->find('div[class=row]', 0)->find('div[class=jdp-left-column]', 0)->innertext;
                            $job_desc = preg_replace('/\s+/', ' ', $desc);
                            $job_desc = addslashes($job_desc);
                        }
                        else{
                            $job_desc = "";
                        }

                        $category = $jobdesc->find('div[class=job-details-content]', 0)->find('div[class=container]', 0)->find('div[class=row]', 0)->find('div[class=jdp-right-column]', 0)->find('div[id=jdp-job-snapshot-section]', 0)->find('div[class=jdp-job-snapshot-card content-card]', 0)->find('ul[class=snapshot-padding]', 0)->find('li[class=job-categories]', 0)->find('div[class=snapshot-text]', 0)->find('div a', 0)->innertext;
                        $category = trim($category);

                        $row = Job::where('job_id', $job_id)->count();
                        if ($row == 0) {
                            $insert_data = [
                                "company" => "Securitas",
                                "website" => "https://www.securitasjobs.com/",
                                "job_title" => $job_title,
                                "posted_on" => $posted_date,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name" => '',
                                "contact_email" => '',
                                "contact_phone" => '',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type" => 1,
                                "points" => 0,
                                "keywords" => '',
                                "keyword_ids" => '',
                                "keyword_points" => '',
                                "rating_types" => '',
                                "rating_points" => '',
                                "status" => 0,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ];

                            Job::insert($insert_data);
//                            print_r($insert_data);
                        }
                    }
                }
            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
