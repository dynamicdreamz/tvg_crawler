<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AtlascopcogroupCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'atlascopcogroup:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'USA' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(461 / 20);
             for ($i=0; $i <= 2; $i++) 
             { 
                //print_r($i.",");
                $json_data = HtmlDomParser::file_get_html("https://www.atlascopcogroup.com/content/corporate/en/careers/job-overview/jcr:content/par/jobs_overview.jobs.json?index=".$i);
                $jobs_content = json_decode($json_data, true);

                foreach ($jobs_content['jobs'] as $key => $jobs) 
                {
                   $posted_date = date('Y-m-d', strtotime($jobs['PostStartDate']));
                   if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                   {
                       $job_id = trim($jobs['JobReqId']);
                      // print_r($job_id.",");
                       $job_title = trim($jobs['Title']);
                       $category = trim($jobs['JobFunc']);
                       if($job_id == '70F292D0279AC5BB442583990051479D')
                       {
                           $country = "Italy";
                       }
                       else
                       {
                          if($job_id == '6167-atlascopco')
                           {
                               $country = "Czech Republic";
                           }
                           else
                           {
                               if($job_id == 'E1D2591F5863F46B442583CC0059E2BB')
                               {
                                   $country = "Italy";
                               }
                               else
                               {
                                   if($job_id == '2DE8CE6C581B754F432580CF002511E5')
                                   {
                                       $country = "Russia";
                                   }
                                   else
                                   {
                                      if($job_id == '4B130BF0637CA325C1258397003FA65D')
                                       {
                                           $country = "Russia";
                                       }
                                       else
                                       {
                                           if($job_id == 'EE5F16A66786E552C125840A006ABAB8')
                                           {
                                               $country = "Germany";
                                           }
                                           else
                                           {
                                              
                                              if($job_id == 'FF728A017DD733AFC1258409004F0B49')
                                               {
                                                   $country = "Germany";
                                               }
                                               else
                                               {
                                                  if($job_id == '8CC7E2ADC7A2FB30C1258328003A1B87')
                                                   {
                                                       $country = "United Kingdom";
                                                   }
                                                   else
                                                   {
                                                      
                                                       if($job_id == '911AFD7DF2E9A0F7C12583AF003086B9')
                                                       {
                                                           $country = "Spain";
                                                       }
                                                       else
                                                       {
                                                           if($job_id == '7C035D136769ABF2C12584040031FB84')
                                                           {
                                                               $country = "Germany";
                                                           }
                                                           else
                                                           {
                                                              if($job_id == '5CD00A0D01AD5A6EC1258404003BEDE3')
                                                               {
                                                                   $country = "Germany";
                                                               }
                                                               else
                                                               {
                                                                  
                                                                  $country = trim($jobs['LegEntCountry']);
                                                               }
                                                           }
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                         
                       }
                       //$country = trim($jobs['LegEntCountry']);
                       if(count($jobs) == 17)
                       {
                          $enddate = date('Y-m-d', strtotime($jobs['PostEndDate']));
                       }
                       else
                       {
                          $enddate = Null;
                       }
                                                    
                       $url = trim($jobs['path']);
                       $source_url = "https://www.atlascopcogroup.com".$url;
                       if($job_id == '70F292D0279AC5BB442583990051479D')
                       {
                            $job_desc = "";
                       }
                       else
                       {
                           if($job_id == 'E1D2591F5863F46B442583CC0059E2BB')
                           {
                                $job_desc = "";
                           }
                           else
                           {
                               if($job_id == '2DE8CE6C581B754F432580CF002511E5')
                               {
                                    $job_desc = "";
                               }
                               else
                               {
                                   if($job_id == '4B130BF0637CA325C1258397003FA65D')
                                   {
                                        $job_desc = "";
                                   }
                                   else
                                   {
                                       if($job_id == 'EE5F16A66786E552C125840A006ABAB8')
                                       {
                                            $job_desc = "";
                                       }
                                       else
                                       {
                                           if($job_id == 'FF728A017DD733AFC1258409004F0B49')
                                           {
                                                $job_desc = "";
                                           }
                                           else
                                           {
                                               if($job_id == '8CC7E2ADC7A2FB30C1258328003A1B87')
                                               {
                                                    $job_desc = "";
                                               }
                                               else
                                               {
                                                   if($job_id == '911AFD7DF2E9A0F7C12583AF003086B9')
                                                   {
                                                        $job_desc = "";
                                                   }
                                                   else
                                                   {
                                                       if($job_id == '7C035D136769ABF2C12584040031FB84')
                                                       {
                                                            $job_desc = "";
                                                       }
                                                       else
                                                       {
                                                           if($job_id == '5CD00A0D01AD5A6EC1258404003BEDE3')
                                                           {
                                                                $job_desc = "";
                                                           }
                                                           else
                                                           {
                                                               $jobdesc_data = $jobs['Body'];
                                                               $job_desc = preg_replace('/\s+/', ' ', $jobdesc_data);
                                                               $job_desc = addslashes($job_desc);
                                                           }
                                                       }
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                       }
                       $posted_on = $posted_date; 
                       //print_r($posted_on.",");

                       $row = Job::where('job_id', $job_id)->count();  
                       if($row == 0)
                       {
                              $insert_data = [
                                "company" => "Atlascopcogroup",
                                "website" => "https://www.atlascopcogroup.com/en/careers",
                                "job_title" => $job_title,
                                "posted_on"=> $posted_on,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "close_on"=>$enddate,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
                   }
                  
                  
                }
                                   
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
