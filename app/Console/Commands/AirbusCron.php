<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AirbusCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'airbus:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $dom = new HtmlDomParser();
              $brk = '';
              $current_date = date("Y-m-d");
              $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d")))); 
              $dom = new HtmlDomParser();
              $total_jobs = 1789;
              $pagination = ceil($total_jobs / 20);
              //print_r($pagination);

              for ($i = 0; $i <= $pagination; $i++) 
              {
              	 //--print_r($i.",");
                 $dom = HtmlDomParser::file_get_html('https://www.airbus.com/careers/search-and-apply/search-for-vacancies.html?filters=&textsearch=&page='.$i);
                 $jobs_data = $dom->find('div[class=row c-jobsearchpage__items]',0)->find('div[class=c-jobcarousel__item col-md-3 col-sm-6]');
                 foreach ($jobs_data as $key => $job) 
                 {
                    $job_url = $job->find('div[class=c-jobcarousel__slider--title] a',0)->href;
                    $job_spes = explode("=", $job_url);
                    $job_id = $job_spes[1];

                    $job_title = $job->find('div[class=c-jobcarousel__slider--title] a',0)->innertext;
                    $category = $job->find('span[class=c-jobcarousel__slider--subtitle]',0)->innertext;
                    $location = $job->find('span[class=c-jobcarousel__slider--location]',0)->find('span',0)->innertext;
                    $country = explode(",", $location);

                    $jobs = HtmlDomParser::file_get_html($job_url);
                    $date_spec = $jobs->find('div[class=col-sm-9]',0)->find('div[class=c-banner__jobinfo-ligne]',0)->find('span',1)->innertext;
                    $posted_date = date('Y-m-d', strtotime($date_spec));
                    if($posted_date != '1970-01-01')
                    {
                       if(strtotime($current_date) == strtotime($posted_date))
                       {
                            $jobid = $job_id;
                           //-- print_r($jobid.",");
                            $joburl = $job_url;
                            $jobtitle = $job_title;
                            $jobcate = $category;
                            $jobcountry = trim($country[1]);
                            $posted_on = $posted_date;
                            //print_r( $posted_on .",");

                            $reference_id = $jobs->find('div[class=c-banner__jobinfo-botton-ligne]',0)->find('span',1)->innertext;

                            $description = $jobs->find('div[class=col-md-8 col-sm-12 col-xs-12]',1);
                            $description = preg_replace('/\s+/', ' ', $description);
                            $jobdesc = addslashes($description);
                           
                            $row = Job::where('job_id', $jobid)->count();  
                            if($row == 0)
                            {
                                  $insert_data = [
                                    "company" => "Airbus",
                                    "website" => "https://www.airbus.com/",
                                    "job_title" => $jobtitle,
                                    "posted_on"=> $posted_on,
                                    "category" => $jobcate,
                                    "country" => $jobcountry,
                                    "description" => $jobdesc,
                                    "job_id" => $jobid,
                                    "reference_id" => $reference_id,
                                    "contact_name"=>'',
                                    "contact_email"=>'',
                                    "contact_phone"=>'',
                                    "source_url" => $joburl,
                                    "experience_from" => 0,
                                    "experience_to" => 0,
                                    "job_type"=>1,
                                    "points"=>0,
                                    "keywords"=>'',
                                    "keyword_ids"=>'',
                                    "keyword_points"=>'',
                                    "rating_types"=>'',
                                    "rating_points"=>'',
                                    "status"=>0,
                                    "created_at"=>date("Y-m-d H:i:s"),
                                    "updated_at"=>date("Y-m-d H:i:s")     

                                  ]; 
                                //print_r($insert_data);
                                Job::insert($insert_data);                
                            }

                       }
                       else
                       {
                           $brk = "error";
                       }
                    }
                    // else
                    // {
                         
                    // }
                  
                }
                if($brk == 'error')
                {
                    break;
                }
                 	           		
              }
 
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
