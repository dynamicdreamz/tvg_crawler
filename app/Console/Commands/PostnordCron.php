<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class PostnordCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'postnord:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $html_data = HtmlDomParser::file_get_html("https://www.postnord.com/en/career/vacancies/");
                $job_data = $html_data->find('article[id=main-article]',0)->find('table',0)->find('tr');
                for ($i=1; $i <count($job_data) ; $i++) 
                { 
                    $postdate = $job_data[$i]->find('td',1)->innertext;
                    $postdate = trim($postdate);
                    $posted_date = date('Y-m-d', strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = "54028".$i;
                        //print_r($job_id.",");
                        $url = $job_data[$i]->find('a',0)->href;
                        $source_url = "https://www.postnord.com".$url;
                        $jobtitle = $job_data[$i]->find('a',0)->innertext; 
                        $jobtitle = explode(",", trim($jobtitle));
                        $job_title = trim($jobtitle[0]);
                        $country = trim(end($jobtitle));
                        $enddate = $job_data[$i]->find('td',2)->innertext;
                        $enddate = date("Y-m-d", strtotime($enddate));
                        $description = HtmlDomParser::file_get_html($source_url);
                        $desc = $description->find('article[id=main-article]',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Postnord",
                                        "website" => "https://www.postnord.com/en/career/vacancies/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => '',
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'HR Direct Staff',
                                        "contact_email"=>'hrdirekt.personal@postnord.com',
                                        "contact_phone"=>'+46 (0) 10 436 66 00',
                                        "source_url" => $source_url,
                                        "close_on"=> $enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                 

                    }
                    
                }
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
