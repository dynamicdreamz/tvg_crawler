<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AstrazenecaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'astrazeneca:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $categories = [
                  'Administration'=>"Administration",
                  'BioPharmaceutical Physician'=>"BioPharmaceutical+Physician",
                  'BioPharmaceuticals Commercial'=>'BioPharmaceuticals+Commercial',
                  'BioPharmaceuticals R&D'=>'BioPharmaceuticals+R%26D',
                  'BPD'=>'BPD',
                  'Business Development'=>'Business+Development',
                  'Clinical Development'=>'Clinical+Development',
                  'Commercial Non-Sales'=>'Commercial+Non-Sales',
                  'Compliance'=>'Compliance',
                  'Corporate Affairs'=>'Corporate+Affairs',
                  'Data and AI'=>'Data+and+AI',
                  'Engineering'=>'Engineering',
                  'Facilities Management'=>'Facilities+Management',
                  'Finance'=>'Finance',
                  'Global Biologics'=>'Global+Biologics',
                  'Global Operations'=>'Global+Operations',
                  'Global Operations Lean'=>'Global+Operations+Lean',
                  'Human Resources'=>'Human+Resources',
                  'Information Management'=>'Information+Management',
                  'Information Technology'=>'Information+Technology',
                  'Legal'=>'Legal',
                  'Manufacturing'=>'Manufacturing',
                  'Medical Affairs'=>'Medical+Affairs',
                  'Post Doc'=>'Post+Doc',
                  'Procurement'=>'Procurement',
                  'Project, Change, Business Improvement and Strategy Management'=>'Project%2C+Change%2C+Business+Improvement+and+Strategy+Management',
                  'QA'=>'QA',
                  'Quality'=>'Quality',
                  'Regulatory'=>'Regulatory',
                  'Respiratory'=>'Respiratory',
                  'Safety, Health and Environmental Science'=>'Safety%2C+Health+and+Environmental+Science',
                  'Sales'=>'Sales',
                  'Sales Force Effectiveness'=>'Sales+Force+Effectiveness',
                  'Scientific'=>'Scientific',
                  'SEUIT'=>'SEUIT',
                  'Supply'=>'Supply'
            ];

            $cat_ActiveFacetID = [
                  'Administration'=>"34693",
                  'BioPharmaceutical Physician'=>"66789",
                  'BioPharmaceuticals Commercial'=>'61589',
                  'BioPharmaceuticals R&D'=>'36696',
                  'BPD'=>'62494',
                  'Business Development'=>'38387',
                  'Clinical Development'=>'34828',
                  'Commercial Non-Sales'=>'40085',
                  'Compliance'=>'33542',
                  'Corporate Affairs'=>'35260',
                  'Data and AI'=>'69956',
                  'Engineering'=>'28256',
                  'Facilities Management'=>'40086',
                  'Finance'=>'21707',
                  'Global Biologics'=>'57771',
                  'Global Operations'=>'60495',
                  'Global Operations Lean'=>'61950',
                  'Human Resources'=>'40087',
                  'Information Management'=>'38384',
                  'Information Technology'=>'21713',
                  'Legal'=>'21716',
                  'Manufacturing'=>'36563',
                  'Medical Affairs'=>'34829',
                  'Post Doc'=>'48203',
                  'Procurement'=>'25094',
                  'Project, Change, Business Improvement and Strategy Management'=>'40088',
                  'QA'=>'34906',
                  'Quality'=>'40082',
                  'Regulatory'=>'49462',
                  'Respiratory'=>'61561',
                  'Safety, Health and Environmental Science'=>'38385',
                  'Sales'=>'40089',
                  'Sales Force Effectiveness'=>'40090',
                  'Scientific'=>'40091',
                  'SEUIT'=>'60187',
                  'Supply'=>'40084'
            ];

            $countries = array("Algeria"=>"Algeria","Argentina"=>"Argentina","Australia"=>"Australia","Austria"=>"Austria","Brazil"=>"Brazil","Canada"=>"Canada","Chile"=>"Chile","China"=>"China","Colombia"=>"Colombia","Costa Rica"=>"Costa+Rica","Czechia"=>"Czechia","Denmark"=>"Denmark","Egypt"=>"Egypt","France"=>"France","Germany"=>"Germany","Hungary"=>"Hungary","India"=>"India","Indonesia"=>"Indonesia","Israel"=>"Israel","Italy"=>"Italy","Japan"=>"Japan","Latvia"=>"Latvia","Malaysia"=>"Malaysia","Mexico"=>"Mexico","Netherlands"=>"Netherlands","New Zealand"=>"New+Zealand","Norway"=>"Norway","Peru"=>"Peru","Poland"=>"Poland","Puerto Rico"=>"Puerto+Rico","Romania"=>"Romania","Russia"=>"Russia","Singapore"=>"Singapore","South Africa"=>"South+Africa","South Korea"=>"South+Korea","Spain"=>"Spain","Sweden"=>"Sweden","Switzerland"=>"Switzerland","Taiwan"=>"Taiwan","Thailand"=>"Thailand","Turkey"=>"Turkey","Ukraine"=>"Ukraine","United Arab Emirates"=>"United+Arab+Emirates","United Kingdom"=>"United+Kingdom","United States"=>"United+States","Uruguay"=>"Uruguay","Vietnam"=>"Vietnam");

            $cuntry_ActiveFacetID = array("Algeria"=>"2589581","Argentina"=>"3865483","Australia"=>"2077456","Austria"=>"2782113","Brazil"=>"3469034","Canada"=>"6251999","Chile"=>"3895114","China"=>"1814991","Colombia"=>"3686110","Costa Rica"=>"3624060","Czechia"=>"3077311","Denmark"=>"2623032","Egypt"=>"357994","France"=>"3017382","Germany"=>"2921044","Hungary"=>"719819","India"=>"1269750","Indonesia"=>"1643084","Israel"=>"294640","Italy"=>"3175395","Japan"=>"1861060","Latvia"=>"458258","Malaysia"=>"1733045","Mexico"=>"3996063","Netherlands"=>"2750405","New Zealand"=>"2186224","Norway"=>"3144096","Peru"=>"3932488","Poland"=>"798544","Puerto Rico"=>"4566966","Romania"=>"798549","Russia"=>"2017370","Singapore"=>"1880251","South Africa"=>"953987","South Korea"=>"1835841","Spain"=>"2510769","Sweden"=>"2661886","Switzerland"=>"2658434","Taiwan"=>"1668284","Thailand"=>"1605651","Turkey"=>"298795","Ukraine"=>"690791","United Arab Emirates"=>"290557","United Kingdom"=>"2635167","United States"=>"6252001","Uruguay"=>"3439705","Vietnam"=>"1562822");


            $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($categories as $cat_key => $cate) 
             {
                 //print_r($cat_key.",");
                 $ActiveFacetIDs = $cat_ActiveFacetID[$cat_key];
                 // /dd($ActiveFacetIDs);
                 foreach ($countries as $cuntry_key => $cuntry) 
                 {
                    //print_r($cuntry_key.","); 
                    $countries_a_ids = $cuntry_ActiveFacetID[$cuntry_key];
                    $html_data = HtmlDomParser::file_get_html("https://job-search.astrazeneca.com/search-jobs/results?ActiveFacetID=".$ActiveFacetIDs."&CurrentPage=1&RecordsPerPage=15&Distance=50&RadiusUnitType=0&Keywords=&Location=&Latitude=&Longitude=&ShowRadius=False&CustomFacetName=&FacetTerm=&FacetType=0&FacetFilters%5B0%5D.ID=".$ActiveFacetIDs."&FacetFilters%5B0%5D.FacetType=1&FacetFilters%5B0%5D.Count=33&FacetFilters%5B0%5D.Display=".$cate."&FacetFilters%5B0%5D.IsApplied=true&FacetFilters%5B0%5D.FieldName=&FacetFilters%5B1%5D.ID=".$countries_a_ids."&FacetFilters%5B1%5D.FacetType=2&FacetFilters%5B1%5D.Count=2&FacetFilters%5B1%5D.Display=".$cuntry."&FacetFilters%5B1%5D.IsApplied=true&FacetFilters%5B1%5D.FieldName=&SearchResultsModuleName=Search+Results&SearchFiltersModuleName=Search+Filters&SortCriteria=0&SortDirection=0&SearchType=5&CategoryFacetTerm=&CategoryFacetType=&LocationFacetTerm=&LocationFacetType=&KeywordType=&LocationType=&LocationPath=&OrganizationIds=&PostalCode=&fc=&fl=&fcf=&afc=&afl=&afcf=");
                    $job_data = json_decode($html_data,true);

                    if($job_data['hasJobs'] != false)
                    {

                        $fh = fopen( '/var/www/html/cronhtmlfile/AstrazenecahtmlData.html', 'w' );
                        fclose($fh);

                        $myfile = fopen("/var/www/html/cronhtmlfile/AstrazenecahtmlData.html", "w") or die("Unable to open file!");
                        $txt = $job_data['results'];
                        fwrite($myfile, $txt);
                         
                        $html_data = HtmlDomParser::file_get_html("/var/www/html/cronhtmlfile/AstrazenecahtmlData.html");
                        
                        $job_data = $html_data->find('section[id=search-results]',0)->find('section[id=search-results-list]',0)->find('ul',1)->find('li');

                        foreach ($job_data as $key => $jobs) 
                        {
                            $url = $jobs->find('a',0)->href;
                            $source_url = "https://job-search.astrazeneca.com".$url;
                            $job_id = explode("/", $url);
                            $job_id = end($job_id);
                            //print_r($job_id.",");
                            $job_title = trim($jobs->find('a',0)->find('h2',0)->innertext);
                            $job_title = html_entity_decode($job_title);
                            $category = $cat_key;
                            $country = $cuntry_key;
                            $jobdesc = HtmlDomParser::file_get_html($source_url);
                            if($jobdesc != false)
                            {
                                $desc = $jobdesc->find('section[class=job-description]',0)->find('div[class=ats-description]',0)->innertext;
                                $job_desc = preg_replace('/\s+/', ' ', $desc);
                                $job_desc = addslashes($job_desc);                          
                                $ref_id = $jobdesc->find('section[class=job-description]',0)->find('div[class=overview-left]',0)->find('span[class=job-id job-info]',0)->innertext;
                                $ref_id = explode("</b>", $ref_id);
                                $ref_id = trim($ref_id[1]);
                            }
                            else
                            {
                                $job_desc = "";
                                $ref_id = "";
                            }
                           
                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Astrazeneca",
                                        "website" => "https://job-search.astrazeneca.com",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => $ref_id,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            } 

                        }
                    }
                    $fh = fopen( '/var/www/html/cronhtmlfile/AstrazenecahtmlData.html', 'w' );
                    fclose($fh);

                 }
             }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
