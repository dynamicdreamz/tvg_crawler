<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class TrafikverketCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'trafikverket:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $ch = curl_init('https://www.trafikverket.se/Trafikverket/Library/webServices/GetAllJobs.asmx/GetJobs');
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                      'Content-Type: application/json; charset=utf-8',
                      'X-Requested-With: XMLHttpRequest'
                  ));
             curl_setopt($ch, CURLOPT_POSTFIELDS, "{'selectedCities': '' , 'selectedOrgs': '' , 'sortOrder': 'PublishedDesc'}");
             $response = curl_exec($ch);
             $result = json_decode($response,true);
             foreach ($result['d'] as $key => $jobs) 
             {
                $postdate = $jobs['PublishDate'];
                $posted_date = date("Y-m-d", strtotime(trim($postdate)));
                if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                {
                    $job_id = $jobs['PageId'];
                    //print_r($job_id.",");
                    $job_title = trim($jobs['JobPost']);
                    $source_url = trim($jobs['Link']);
                    $source_url = "https://www.trafikverket.se".$source_url;
                    $country = "Sweden";
                    $category = trim($jobs['JobUnit']);
                    $enddate = trim($jobs['LastApplyDate']);
                    $enddate = date("Y-m-d", strtotime(trim($enddate)));
                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[class=content-first]',0)->find('article[class=main-article]',0)->find('div[class=col-sm-12]',0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Trafikverket",
                                        "website" => "https://www.trafikverket.se/om-oss/jobb-och-framtid/Lediga-jobb/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        
                }

             }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
