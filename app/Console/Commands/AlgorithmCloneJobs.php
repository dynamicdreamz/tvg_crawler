<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 7/2/19
 * Time: 3:05 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Admin;

class AlgorithmCloneJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algorithm:clonejobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clone newly added jobs for each user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $users = Admin::select('id')->get();
            //dd($users);
            foreach ($users as $user) {
                //fetch the newly added jobs
                $jobs = Job::where('user_id', 0)->select('id')->get();
                foreach ($jobs as $value) {
                    $job = Job::find($value->id);

                    $newJob = $job->replicate();
                    $newJob->user_id = $user->id;
                    $newJob->points = 0;
                    $newJob->keywords = '';
                    $newJob->keyword_ids = '';
                    $newJob->keyword_points = '';
                    $newJob->rating_types = '';
                    $newJob->rating_points = '';
                    $newJob->status = 0;
                    $newJob->save();
                }

                //update cron status to apply configurations to newly added jobs for each users
                Admin::where('id',$user->id)->update(['generic_cron'=>0,'profile_cron'=>0]);
            }
            Job::where('user_id', 0)->delete();
            echo "Jobs archive cron executed successfully";exit;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
