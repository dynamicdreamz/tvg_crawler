<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class FingerprintsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'fingerprints:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://fingerprints.workbuster.com/?_=0.5588900281881992&ref=&language=");
             $job_data = $html_data->find('div[class=wb-block-inner table-default]',0)->find('div[id=items]',0)->find('div[class=position-container filter-item]');
            
             for ($i=0; $i <count( $job_data ) ; $i++)
             { 
                $url = $job_data[$i]->find('div[class=row item-row filter-item]',0)->find('div[class=item col-md-12]',0)->find('div[class=row inner-row]',0)->find('div[class=position-name]',0)->find('a',0)->href;
                $jobid_exp = explode("/", $url);
                $jobid_exp = explode("-", $jobid_exp[2]);
                $job_id = trim($jobid_exp[0]);
                //print_r($job_id.",");
                $source_url = "https://fingerprints.workbuster.com".$url;
                $job_title = $job_data[$i]->find('div[class=row item-row filter-item]',0)->find('div[class=item col-md-12]',0)->find('div[class=row inner-row]',0)->find('div[class=position-name]',0)->find('a',0)->innertext;
                $job_title = html_entity_decode(trim($job_title));
                $category = $job_data[$i]->find('div[class=row item-row filter-item]',0)->find('div[class=item col-md-12]',0)->find('div[class=row inner-row]',0)->find('div[class=meta-container]',0)->find('div[class=department]',0)->find('span',1)->innertext;
                $category = html_entity_decode(trim($category));
                //print_r($category.",");
                $country = "Sweden";
                $enddate = $job_data[$i]->find('div[class=row item-row filter-item]',0)->find('div[class=item col-md-12]',0)->find('div[class=row inner-row]',0)->find('div[class=meta-container]',0)->find('div[class=date]',0)->find('span',1)->innertext;
                $enddate = date("Y-m-d", strtotime($enddate));
                $jd_url = "https://fingerprints.workbuster.com/jobs/81166?_=0.028012849140509166&ref=https:/fingerprints.workbuster.com/?_=0.5588900281881992&language=&ga_referrer=".$source_url;
                $jobdesc = HtmlDomParser::file_get_html($jd_url);
                $desc = $jobdesc->find('body[id=careerpage]',0)->find('div[class=page-3608]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Fingerprints",
                                        "website" => "https://www.fingerprints.com/open-positions/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => 'https://www.thunderkick.com/jobs/',
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                      //print_r($insert_data);
                                      Job::insert($insert_data);                
                            }    
               
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
