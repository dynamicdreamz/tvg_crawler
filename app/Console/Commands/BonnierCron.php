<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BonnierCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'bonnier:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "California, Irvine"=>"United States",
                      "California, LA"=>"United States",
                      "Ft Lauderdale"=>"United States",
                      "Indianapolis"=>"United States",
                      "New York/32nd"=>"United States",
                      "New York 2 Park"=>"United States",
                      "NY Remote"=>"United States",
                      "Pewaukee, WI"=>"United States",
                      "Rhode Island"=>"United States",
                      "South Florida"=>"United States",
                      "Winter Park, FL"=>"United States",
                      "WP Remote"=>"United States"
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://chu.tbe.taleo.net/chu03/ats/careers/searchResults.jsp?org=WORLDPUB&cws=1");
             $job_data = $html_data->find('table[role=presentation]',0)->find('table[id=cws-search-results]',0)->find('tr');
             for ($i=1; $i < count($job_data) ; $i++) 
             { 
             	 $postdate = $job_data[$i]->find('td b',3)->innertext;
             	 $postdate_exp = explode(" ", $postdate);
                 $posted_date = date("Y-m-d", strtotime($postdate_exp[0]));
                 if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                 {
                 	 $source_url = $job_data[$i]->find('td',0)->find('a',0)->href;
	             	 $jobsid = explode("&rid=", $source_url);
	             	 $job_id = trim($jobsid[1]);
	             	 //print_r($job_id.",");
	             	 $job_title = $job_data[$i]->find('td',0)->find('a',0)->innertext;
	             	 $job_title = trim($job_title);
	             	 $country = $job_data[$i]->find('td b',1)->innertext;
	             	 $country = $countries[trim($country)];
	             	 $jobdesc = HtmlDomParser::file_get_html($source_url);
	             	 $desc = $jobdesc->find('table',0)->innertext;
	             	 $job_desc = preg_replace('/\s+/', ' ', $desc);
                     $job_desc = addslashes($job_desc);
	             	 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Bonniercorp",
                                        "website" => "https://www.bonniercorp.com/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => "",
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                

                 }
             	 
             }

             
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
