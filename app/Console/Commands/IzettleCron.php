<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class IzettleCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'izettle:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $countries = [
                "Amsterdam" => "Netherlands",
                "Berlin" => "Germany",
                "Birmingham" => "England",
                "Bristol" => "England",
                "Edinburgh" => "Scotland",
                "London" => "England",
                "Manchester" => "England",
                "Mexico DF" => "Mexico",
                "Paris" => "France",
                "São Paulo" => "Brazil",
                "Stockholm" => "Sweden"
            ];

            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();

            $html_data = HtmlDomParser::file_get_html("https://jobs.izettle.com/jobs");
            $jod_data = $html_data->find('section[class=jobs-section]', 0)->find('div[class=wrapper]', 0)->find('div[class=jobs-section-inner]', 0)->find('ul[class=jobs]', 0)->find('li');

            echo '<pre>';

            foreach ($jod_data as $key => $jobs) {
                if ($key == 5) {

                    print_r('--------------------------------' . $key . '--start---------------------------------------------------');
                    print_r('</br>');
                    $url = $jobs->find('a', 0)->href;
                    print_r($url);
                    print_r('</br>');
                    $source_url = "https://jobs.izettle.com" . $url;

                    $job_id = explode("/", $url);
                    $job_id = explode("-", $job_id[2]);
                    $job_id = trim($job_id[0]);

                    print_r($job_id);
                    print_r('</br>');
                    //print_r($job_id.",");
                    $job_title = $jobs->find('a', 0)->find('div', 0)->find('span[class=title u-link-color u-no-hover]', 0)->innertext;
                    $job_title = trim($job_title);

                    print_r($job_title);
                    print_r('</br>');
                    $categorys = $jobs->find('a', 0)->find('div', 0)->find('span[class=meta]', 0)->innertext;
                    $categorys = explode("-", trim($categorys));
                    print_r($categorys);
                    print_r('</br>');

                    if ($job_id != '146353') {
                        $category = html_entity_decode($categorys[0]);
                        $country = $countries[trim($categorys[1])];
                    } else {
                        $category = '';
                        $country = $countries[trim($categorys[0])];
                    }


                    $jobdesc = HtmlDomParser::file_get_html($source_url);

                    $desc = $jobdesc->find('div[class=outer-wrapper]', 0)->find('div[class=job]', 0)->find('div[class=wrapper]', 0)->find('div[class=body]', 0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    print_r($job_desc);
                    print_r('</br>');
                    $row = Job::where('job_id', $job_id)->count();
                    if ($row == 0) {
                        $insert_data = [
                            "company" => "Izettle",
                            "website" => "https://jobs.izettle.com/jobs",
                            "job_title" => $job_title,
                            "posted_on" => null,
                            "category" => $category,
                            "country" => $country,
                            "description" => $job_desc,
                            "job_id" => $job_id,
                            "reference_id" => '',
                            "contact_name" => '',
                            "contact_email" => '',
                            "contact_phone" => '',
                            "source_url" => $source_url,
                            "experience_from" => 0,
                            "experience_to" => 0,
                            "job_type" => 1,
                            "points" => 0,
                            "keywords" => '',
                            "keyword_ids" => '',
                            "keyword_points" => '',
                            "rating_types" => '',
                            "rating_points" => '',
                            "status" => 0,
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ];
                        //print_r($insert_data);
//                                    Job::insert($insert_data);
                    }
                }
            }

            die();


        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
