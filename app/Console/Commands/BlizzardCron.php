<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BlizzardCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'blizzard:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Austin"=>"United States",
                      "Burbank"=>"United States",
                      "Cork"=>"Ireland",
                      "Irvine"=>"United States",
                      "Santa Monica"=>"United States",
                      "Shanghai"=>"China",
                      "Sydney"=>"Australia",
                      "The Hague"=>"Netherlands",
                      "Versailles"=>"France",
                      "Seoul"=>"South Korea"
                   ];
          
                $html_data = HtmlDomParser::file_get_html("https://careers.blizzard.com/en-us/openings");
                $job_data = $html_data->find('div[class=AjaxBlock-content]',0)->find('div[class=ExpandableTableGroup-group]');
                foreach ($job_data as $key => $jobs) 
                {
                    $category = $jobs->find('div[class=Expandable-toggle is-active]',0)->find('div[class=Expandable-label]',0)->innertext;
                    $category = trim($category);
                    $jobs_data = $jobs->find('div[class=Expandable-container]',0)->find('div[class=Table-group]',0)->find('div[class=Table-container]',0)->find('a');
                    foreach ($jobs_data as $key => $job) 
                    {
                       $url = $job->href;
                       $source_url = "https://careers.blizzard.com".$url;
                       $jobid = explode("/",$url);
                       $job_id = $jobid[3];
                       //print_r($job_id.",");
                       $job_title = $job->find('div[class=Table-column]',0)->innertext;
                       $job_title = trim($job_title);
                       $country = $job->find('div[class=Table-column]',2)->innertext;
                       $country = $countries[trim($country)];
                       $jobdesc = HtmlDomParser::file_get_html($source_url);
                       $desc = $jobdesc->find('div[class=Pane-content]',0)->find('div[class=max-sm max-left align-left]',0)->find('div[class=Markup Markup--html]',0)->innertext;
                       $job_desc = preg_replace('/\s+/', ' ', $desc);
                       $job_desc = addslashes($job_desc);
                       
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Blizzard",
                                        "website" => "https://careers.blizzard.com",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        
                    }
                   
                }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
