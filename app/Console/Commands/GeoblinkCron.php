<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class GeoblinkCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'geoblink:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'USA' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $html_data = HtmlDomParser::file_get_html('https://geoblink.workable.com/');

             $job_details = $html_data->find('section[id=jobs]',0)->find('ul',0)->find('li');
             $ff = count($job_details);
            
             // $ff = count($job_details);
             foreach ($job_details as $key => $job) 
             {
                $jobid = $job->id;
                $jobid_exp = explode("_", $jobid);
                $job_id = $jobid_exp[1];
                //print_r($job_id.",");
                $url = $job->find('a',0)->href;
                $source_url = "https://geoblink.workable.com".$url;

                $job_title = $job->find('a',0)->innertext;
                $categry = $job->find('p',0)->innertext;
                $categry_exp = explode("&middot;", $categry);
                $category = trim($categry_exp[1]);
                //dd($category);                
                if($job_id == 989501)
                {
                    $country = 'Spain';
                }
                else
                {
                    $countries = explode(",", $categry_exp[0]);
                    $country = trim($countries[2]);
                }
                
                $job_desc = HtmlDomParser::file_get_html($source_url);
                $desc = $job_desc->find('section[class=section section--text]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                if($job_id == 989452)
                {
                   $job_desc = '<h2 class=\"section__header\">Description</h2><p><strong>About Geoblink</strong></p><p>We\'re a fast growing startup that have already raised close to $8 million in investment from leading venture capital firms, and have been named by Bloomberg as one of 50 most promising startups in the world to look out for.</p><p>Our goal is to revolutionise the world of Location Intelligence and the way businesses think about, and act upon location intelligence data. Getting to that goal means solving some of the most difficult problems for businesses at present. It takes us a lot of hard work, determination, team spirit and support from and for our colleagues.</p><p>At Geoblink we are proud to say that we use the latest technologies to find awesome solutions to real world problems businesses face when trying to expand. We leverage GIS technologies and Big Data to create a beautiful map-based user interface that not only provides lots of awesome statistics but also a great user experience.</p><p><strong>About the CORE team</strong></p><p>Currently we are a team of 4 who have worked super hard to build robust systems using an SOA approach that allows us to perform multiple deployments per day. We monitoring, pull requests, continuous deployment, automated testing and Cachopo (except for our vegetarian dev, which is still looking for the perfect vegan cachopo).</p><p>We’ve built everything using Node.js, Angular, Vue.js and PostgreSQL, although our architecture is still very much language-agnostic. We want to maintain the philosophy of using the right tool not sticking to the status quo. In general we like to move fast but with emphasis on the design of our architecture so that it’s simple and scalable. Our mothers would say that we write clean, high-quality, modular code to produce great software that solves the needs of our clients. You can visit our <a href=\"https://workable.com/nr989452l=https%3A%2F%2Ftech.geoblink.com%2F\" rel=\"nofollow noreferrer noopener\" class=\"external\">Tech blog</a> to learn more about the projects and technologies we use at Geoblink.</p><p><strong>About the internship</strong></p><p>We love deploying code knowing that it won\'t break anything in production and to do so, we invest in testing our code even though we’re always carefully reviewing it. However sometimes it\'s hard to move fast and keep the tests updated so some of our tests become outdated and it takes us a while to update them properly.</p><p>During your internship with us you will learn about the different kinds of testing, from unit tests to complete end to end suites. This end to end suite will be your main focus during the internship: we want you to help us increase our test coverage as well as improve existings tests that might require so.</p><p>Our end to end tests are built on top of Cypress, a cutting edge tool which uses battle tested libraries underneath, for example Chrome Headless, Mocha and Chai to run efficiently in the cloud and ensure we don\'t deploy anything that hasn\'t been tested as if a customer would use it.</p><p><strong>Who we are looking for</strong></p><p>Someone smart, humble and motivated to build an awesome product with us that our users will absolutely love. It would also be great to have someone with a love for testing and some Javascript experience, someone who can handle async patterns without race conditions and understands why solving a problem with a setTimeout is a really bad idea. If you have worked in a Single Page Application before then you get even more kudos points from us!</p><p>If you can, without a doubt say that care about edge cases, worry about finding errors without they happen, and like replacing manual error-prone work with automation then we would love to hear from you. Some other things what would be nice to see:</p><ul> <li>You understand and like CSS.</li> <li>You can participate in a float vs Flexbox discussion and you know how to handle drag &amp; drop in a form.</li> <li>You are a curious and driven person. You might not know how a == 1 &amp;&amp; a == 2 can be true but you are keen on learning why.</li> <li>You are fluent in English.</li> <li>Javascript programming experience</li> <li>Experience writing automated tests</li> </ul><p><strong>Why work for Geoblink</strong></p><p>We operate a “zero-policy” which means there are no restrictions on vacation days, office hours, working from home days, etc. We believe everyone here is a “mini-CEO”, and should have the opportunity to make their own decisions about their work schedule.</p><p>Everyone at Geoblink is passionate about their job, whether it be growing businesses ROI or building complex data systems. People join Geoblink not just for the flexibility that we offer but because we have worked hard to foster a collaborative environment filled with plenty of opportunity to have a real impact in the business and collaborate with some of the best minds in the industry.</p><ul> <li>Plenty of training initiatives to help your career progression</li> <li>Unlimited coffee, team, soft drinks, fruit etc</li> <li>Plenty of chill-out space with the typical start-up ping-pong table and office yoga</li> <li>Plenty of quiet space for you to work in peace to produce your best work</li> <li>The greatest start-up culture with fun initiatives and company events for all to enjoy</li> </ul><p><br></p><p>Location: Glorieta de Quevedo 6, Madrid<br></p><p>Duration: Minimum 3 months</p><p>Internship Type: Full-time<br></p><p>Start date: September 2019</p>';  
                }
                //dd($job_desc);
                
                $row = Job::where('job_id', $job_id)->count();  
                       if($row == 0)
                       {
                              $insert_data = [
                                "company" => "Geoblink",
                                "website" => "https://geoblink.workable.com/",
                                "job_title" => $job_title,
                                "posted_on"=> Null,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
