<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 7/2/19
 * Time: 3:05 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\JobsArchive;
use App\JobMatchedProfile;
use App\JobMatchedKeyword;
Use Carbon\Carbon;


class AlgorithmCopyJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algorithm:copyjobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Archive jobs older than 30 days from current day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $date = Carbon::today()->subDays(14);
            $jobs = Job::where('posted_on', '<=', $date)->get();

            foreach ($jobs as $job){
                // find post with given ID
                $job_row = Job::findOrFail($job->id);
                // get all Job attributes
                $data = $job_row->attributesToArray();
                // remove id attribute as
                $data = array_except($data, ['id']);
                // copy job data to jobs archive
                $insert = JobsArchive::create($data);

                //delete job after copy to archive table
                if($insert){
                    Job::where("id",$job->id)->delete();
                    JobMatchedKeyword::where("job_id",$job->id)->delete();
                    JobMatchedProfile::where("job_id",$job->id)->delete();
                }
            }
            echo "Jobs archive cron executed successfully";exit;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}