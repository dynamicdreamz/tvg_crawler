<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class NentgroupCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'nentgroup:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://www.nentgroup.com/careers/open-positions");
             $job_data = $html_data->find('div[class=view-content]',0)->find('div[class=job__container]',0)->find('div[class=row]',0)->find('div[class=job__wrapper]',0)->find('div[class=col-md-3]');

             for ($i=0; $i <count($job_data); $i++) 
             { 
                $url = $job_data[$i]->find('div[class=job__item__content]',0)->find('a',0)->href;
                $source_url = "https://www.nentgroup.com".$url;
                $job_id = "15986".$i;
                //print_r($job_id.",");
                $job_title = $job_data[$i]->find('div[class=job__item__content]',0)->find('a',0)->find('h4[class=job__item__heading]',0)->innertext;
                $job_title = trim($job_title);
                $country = trim($job_data[$i]->find('div[class=job__item__location]',0)->innertext);
                $enddate = str_replace("/", "-", trim($job_data[$i]->find('div[class=job__item__date]',0)->innertext));
                $enddate = date("Y-m-d", strtotime($enddate));
                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('article[class=jobs full clearfix]',0)->find('div[class=field field--name-body field--type-text-with-summary field--label-hidden field--item]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                
                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Nentgroup",
                                        "website" => "https://www.nentgroup.com/careers",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => "",
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }              
             }     
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
