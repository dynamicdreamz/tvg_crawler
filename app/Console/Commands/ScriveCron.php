<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class ScriveCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'scrive:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {      
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://career.scrive.com/jobs");
             $job_data = $html_data->find('section[class=at-jobs-block]',0)->find('div[class=at-content-block-wrapper]',0)->find('ul[class=at-jobs-list at-not-critical]',0)->find('li');
             for ($i=1; $i <count($job_data) ; $i++) 
             { 
                $job_id = "62489".$i;
                // print_r($job_id.",");
                $source_url = $job_data[$i]->find('div[class=at-jobs-list-cell-40]',1)->find('h6',0)->find('a',0)->href;
                $job_title = $job_data[$i]->find('div[class=at-jobs-list-cell-40]',1)->find('h6',0)->find('a',0)->innertext;
                $job_title = html_entity_decode(trim($job_title));
                // print_r($job_title.",");
                $enddate = $job_data[$i]->find('div[class=at-jobs-list-cell-40]',2)->innertext;
                $enddate = date("Y-m-d", strtotime(trim($enddate)));
                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('section[class=at-jobs-block]',0)->find('div[class=at-content-block-wrapper at-not-critical  at-compact]',0)->find('div[class=at-job-body]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Scrive",
                                        "website" => "https://career.scrive.com/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => '',
                                        "country" => '',
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
