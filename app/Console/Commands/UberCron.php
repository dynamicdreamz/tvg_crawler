<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class UberCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'uber:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(1565 / 10);
             for ($i=0; $i < 3; $i++) 
             {  
                //print_r($i.",");
                $postdata = '{"params":{"location":[],"department":[],"team":[],"programAndPlatform":[],"lineOfBusinessName":[]},"limit":10,"page":'.$i.'}';
                //dd($postdata);
                $ch = curl_init('https://www.uber.com/api/loadSearchJobsResults');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'x-csrf-token: x',
                    'Content-Type: application/json'
                ));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

                $response = curl_exec($ch);

                curl_close($ch);
                $result = json_decode($response, true);
                foreach ($result['data']['results'] as $key=>$job)
                {
                  $posted_date = date('Y-m-d', strtotime($job['updatedDate']));
                  if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                  {
                    $job_id = $job['id'];
                    //print_r($job_id.",");
                    $job_title = trim($job['title']);
                    $category = trim($job['department']);
                    $country = trim($job['location']['countryName']);
                    $job_type = trim($job['timeType']);
                    if($job_type == "Full Time")
                    {
                       $job_type = 1;
                    }
                    else
                    {
                       $job_type = 2;
                    }

                    $source_url = "https://www.uber.com/global/en/careers/list/".$job_id."/";
                    
                    $desc = preg_replace('/\s+/', ' ', $job['description']);
                    $job_desc = addslashes($desc);

                    $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Uber",
                                        "website" => "https://www.uber.com/in/en/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>$job_type,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                  }
                }
              }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
