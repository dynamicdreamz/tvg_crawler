<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SaabgroupCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'saabgroup:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $countries = array("Australia" => "Australia", "Canada" => "Canada", "Finland" => "Finland", "South Africa" => "South%20Africa", "Sweden" => "Sweden", "United Arab Emirates" => "United+Arab+Emirates", "United States" => "USA");

            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();
            $p = 0;

            foreach ($countries as $key => $country) {
                $html_data = HtmlDomParser::file_get_html("https://saabgroup.com/career/vacancies/?c=".$country);

                if (isset($html_data) && $html_data != "") {
                    $job_data = $html_data->find('div[class=filter]', 0)->find('select[id=OuterRegion_ArticleMain_MainRegion_ddlPosition]', 0)->find('option');
                    if (isset($job_data) && is_array($job_data)) {
                        for ($i = 1; $i < count($job_data); $i++) {
                            $category_name = $job_data[$i]->innertext;
                            $category_name_exp = explode("(", $category_name);
                            $category = trim($category_name_exp[0]);
                            //print_r($category.",");
                            if ($category == 'Sales &amp; Marketing') {
                                $category_u = "Sales+%26+Marketing";
                            } else {
                                $category_u = str_replace(" ", "+", trim($category_name_exp[0]));
                            }

                            $html_datas = HtmlDomParser::file_get_html("https://saabgroup.com/career/vacancies/?c=" . $country . "&p=" . $category_u);
                            if (isset($html_datas) && $html_datas != "") {
                                $job_datas = $html_datas->find('section[id=page-container]', 0)->find('div[id=primarycontent]', 0)->find('ul[class=vacancies]', 0)->find('li');
                                if (isset($job_datas) && is_array($job_datas)) {
                                    for ($j = 1; $j < count($job_datas); $j++) {
                                        $p = $p + $j;
                                        $job_id = "05892" . $p;
                                        //print_r($job_id.",");
                                        $url = $job_datas[$j]->find('span[class=title]', 0)->find('a', 0)->href;
                                        $source_url = "https://saabgroup.com" . $url;
                                        $job_title = $job_datas[$j]->find('span[class=title]', 0)->find('a', 0)->innertext;
                                        $job_title = trim($job_title);
                                        $category_n = html_entity_decode($category);
                                        $enddate = $job_datas[$j]->find('span[class=date]', 0)->innertext;
                                        $enddate = date("Y-m-d", strtotime(trim($enddate)));

                                        $description = HtmlDomParser::file_get_html($source_url);
                                        $desc = $description->find('section[id=page-container]', 0)->find('div[id=primarycontent]', 0)->find('div[class=mainbody]', 0)->innertext;

                                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                                        $job_desc = addslashes($job_desc);

                                        $row = Job::where('job_id', $job_id)->count();
                                        if ($row == 0) {
                                            $insert_data = [
                                                "company" => "Saabgroup",
                                                "website" => "https://saabgroup.com/career/",
                                                "job_title" => $job_title,
                                                "posted_on" => Null,
                                                "category" => $category_n,
                                                "country" => $key,
                                                "description" => $job_desc,
                                                "job_id" => $job_id,
                                                "reference_id" => '',
                                                "contact_name" => '',
                                                "contact_email" => '',
                                                "contact_phone" => '',
                                                "source_url" => $source_url,
                                                "close_on" => $enddate,
                                                "experience_from" => 0,
                                                "experience_to" => 0,
                                                "job_type" => 1,
                                                "points" => 0,
                                                "keywords" => '',
                                                "keyword_ids" => '',
                                                "keyword_points" => '',
                                                "rating_types" => '',
                                                "rating_points" => '',
                                                "status" => 0,
                                                "created_at" => date("Y-m-d H:i:s"),
                                                "updated_at" => date("Y-m-d H:i:s")
                                            ];

    //                                    Job::insert($insert_data);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
