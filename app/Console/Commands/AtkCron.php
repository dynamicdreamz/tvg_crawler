<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AtkCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'atk:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'USA' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];

            $job_array = array(
                
                      array(
                          "job_id"=>'002RH',
                          "job_title"=>"HRIS Functional Manager",
                          "location"=>"United States",
                          "posting_date"=>"May 16, 2019",
                          "category"=>"Human Resources",
                          "source_url"=>"https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002RH&tz=GMT%2B05%3A30",
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">HRIS Functional Manager</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002RH</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Position Summary</strong> </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">The Chicago-based Global HRIS Manager is functionally responsible for the entire suite of HR related applications, to include payroll and Knowledge systems.&nbsp; This includes bundles, change impact analysis, tools and application upgrades, new system development, and implementation and enhancements.&nbsp; Works collaboratively with the&nbsp;Global Support Team&nbsp;in the production support of all HRIS applications as requested and for advanced troubleshooting.&nbsp; Subject matter experts in all HR applications.&nbsp;&nbsp; </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">Manage a team of global functional analysts.&nbsp; Responsible for cross training with&nbsp;Support Team&nbsp;in their efforts to document, train, communicate and support new and existing developments, and end-users. </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">Under direction of the Senior Manager, Global HR Systems, the HRIS Manager provides leadership, planning, project strategy and management for the development of a cost-effective HRIS solutions while concurrently contributing towards efficient operations to meet current and future business needs within the HR/firm organizations.&nbsp; </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong></strong>&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Firm Expectation of Employee</strong> </p>
                            <ul>
                            <li>Adhere to and promote HRIS global policies and procedures</li>
                            <li>Perform other duties as workload necessitates</li>
                            <li>Maintain a positive and respectful attitude</li>
                            <li>Communicate regularly with Director regarding Department issues</li>
                            <li>Demonstrate flexible and efficient time management and ability to prioritize workload of self and team</li>
                            <li>Consistently report to work on time prepared to perform duties of position</li>
                            <li>Available to work occasional late hours/weekend&nbsp; </li>
                            <li>Meet Department productivity standards</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Experience, Education and Licensure</strong> </p>
                            <ul>
                            <li>Demonstrated strong functional and technical knowledge of the PeopleSoft HRMS software application 9.x or other ERP level HR application</li>
                            <li>Experience with Finance ERP systems and/or payroll preferred</li>
                            <li>Application management, implementation, and upgrade experience and skills </li>
                            <li>Strong interpersonal and communicative skills</li>
                            <li>Ability to prioritize and make sound effective decisions</li>
                            <li>Excellent organizational skills</li>
                            <li>Strong Project Management skills; including competence in functional specifications, flow charting,&nbsp; test strategy,&nbsp; test script development and execution,&nbsp; escalation and resolution of issues during testing and production support</li>
                            <li>Demonstrated skill managing complex multidisciplinary effort</li>
                            <li>Ability to communicate with both technical resources and business owners of all levels.</li>
                            <li>BS/BA in related field and 7-10 years’ experience or equivalent relevant experience</li>
                            <li>Excellent written and verbal communication skills</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Language Skills</strong> </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">Ability to read, analyze, and interpret related business/technical journals, financial reports, and legal documents. Ability to write and deliver presentations and training as needed.&nbsp;&nbsp; </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Equal Employment Opportunity and Non Discrimination</strong></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p>A.T. Kearney prides itself on providing a culture that allows employees to bring their best selves to work every day. Our people can feel comfortable, confident, and joyful to do great things for our firm, our colleagues, and our clients. A.T. Kearney aims to build diverse capabilities to help our clients solve their most mission critical problems.</o:p></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><o:p>&nbsp;</o:p></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">A.T. Kearney is committed to building a diverse, unbiased and inclusive workforce. A.T. Kearney is an equal opportunity employer; we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a person’s gender identity or expression, sexual orientation, race, religion, age, national origin, disability, marital status, pregnancy status, veteran status, genetic information or any other differences consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices. Members of communities historically underrepresented in consulting are encouraged to apply.</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">
                            </p><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">GLDR</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Non-Consulting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States-Chicago</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      ),
                      array(
                          "job_id"=>'002N4',
                          "job_title"=>"Learning Assistant, Global Learning & Talent Management",
                          "location"=>"United States",
                          "posting_date"=>"May 2, 2019",
                          "category"=>"Human Resources",
                          "source_url"=>"https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002N4&tz=GMT%2B05%3A30",
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">Learning Assistant, Global Learning &amp; Talent Management</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002N4</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><div>
                            <div><font size="1">A fantastic opportunity to join a critical function in a global strategy consulting firm. The Americas Learning Assistant will work on a highly-leveraged team and gain experience and knowledge in the-end-to-end delivery of learning programs and initiatives for A.T.&nbsp;Kearney employees. We’re seeking a enthusiastic self-starter and team player who is passionate about learning and development.</font></div></div>
                            <div><b><font size="1"></font></b>&nbsp;</div>
                            <div><b><font size="1">Key Responsibilities:</font></b></div>
                            <ul>
                            <li>
                            <div><font size="1">Overall team, logistics and administrative support for A.T.&nbsp;Kearney Learning and Talent Management programs and initiatives:</font></div></li>
                            <ul>
                            <li>
                            <div><font size="1">Participant administration and analysis, development and implementation of learning program logistics, systems support, and any project requirements</font></div></li></ul></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong><font size="1">Organizational Environment:</font></strong></p>
                            <ul>
                            <li><font size="1">Function Head: Head, Global Learning &amp; Talent Management</font></li>
                            <li><font size="1">Reports to: Americas Manager, Global Learning &amp; Talent Management</font></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><b><font size="1">Knowledge and skills requirements:</font></b></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><i></i><font size="1">&nbsp;</font></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><i><font size="1">Project Management</font></i></p>
                            <ul>
                            <li>
                            <div><font size="1">Support the organization of frequent and complex Americas GL&amp;TM meetings and learning programs for A.T.&nbsp;Kearney employees</font></div></li>
                            <li>
                            <div><font size="1">Provide strategic logistical support to several projects, which may include activities such as: participant scheduling, communications and tracking, faculty communications, venue logistics (including catering and audio-visual management) </font></div></li>
                            <li>
                            <div><font size="1">Prepare program materials: maintain materials library and supplies (program handouts, teambuilding materials, and audio-visual materials)</font></div></li>
                            <li>
                            <div><font size="1">Administer program participation tracking via firm’s learning management system</font></div></li>
                            <li>
                            <div><font size="1">Process and analyze level one program evaluations</font></div></li>
                            <li>
                            <div><font size="1">Provide on-site support of in-office classroom programs</font></div></li>
                            <li>
                            <div><font size="1">Support other learning-related initiatives and projects, as requested by the GL&amp;TM Head and Americas Manager</font></div></li></ul>
                            <div><i><font size="1">Relationships &amp; People</font></i></div>
                            <ul>
                            <li>
                            <div><font size="1">Build and maintain strong relationships with Global Learning and Talent Management Team, external delivery partners and internal clients</font></div></li>
                            <li>
                            <div><font size="1">Foster a collaborative, productive and creative work environment</font></div></li>
                            <li>
                            <div><font size="1">Seek feedback on overall performance—strengths and development needs and respond positively to coaching and guidance</font></div></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="1">&nbsp;<i>Analytics &amp; Financials</i></font></p>
                            <ul>
                            <li>
                            <div><font size="1">Conduct desk-based research (example: venue research, participant flight schedules, price comparison, online reviews)</font></div></li>
                            <li>
                            <div><font size="1">Perform team financials: process AMEX, invoices and manage team financial and participant expense trackers</font></div></li>
                            <li>
                            <div><font size="1">Function as a Learning Management System Super User (training to be provided), including setting up offerings, entering participant registration/completion, all with a high level of data quality </font></div></li>
                            <li><font size="1">Produce deliverables with strong attention to detail, including Excel and PowerPoint documents&nbsp;</font></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><i><font size="1">Administrative Support for Global Team Leader (~25% of time)</font></i></p>
                            <ul>
                            <li>
                            <div><font size="1">Handle calendaring/scheduling for meetings and programs as required</font></div></li>
                            <li>
                            <div><font size="1">Schedule travel, both domestic and international</font></div></li>
                            <li>
                            <div><font size="1">Process weekly expense reports and timesheets</font></div></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="1">&nbsp;<i>Communications </i></font></p>
                            <ul>
                            <li>
                            <div><font size="1">Communicate information effectively and answer questions succinctly</font></div></li>
                            <li>
                            <div><font size="1">Create visually appealing work that requires limited formatting, proofreading and edits</font></div></li></ul>
                            <div><b><font size="1">Primary Characteristics and Traits:</font></b></div>
                            <ul>
                            <li>
                            <div><font size="1">Flexibility – to adapt to the variable nature of different projects, responsibilities and conditions associated with working in a small, highly leveraged team</font></div></li>
                            <li>
                            <div><font size="1">Attention to detail</font></div></li>
                            <li>
                            <div><font size="1">Ability to work both independently and as a strong team player</font></div></li>
                            <li>
                            <div><font size="1">Dependability in seeing tasks through to completion and following up as needed</font></div></li>
                            <li>
                            <div><font size="1">Ability to interact professionally and effectively with staff at all levels in the organization</font></div></li></ul></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><b><font size="1">Knowledge and Skills Required:</font></b></p>
                            <ul>
                            <li>
                            <div><font size="1">Meeting and event planning experience a plus</font></div></li>
                            <li>
                            <div><font size="1">Intermediate PowerPoint and Excel skills </font></div></li>
                            <li>
                            <div><font size="1">Strong interpersonal and oral/written communication skills </font></div></li>
                            <li>
                            <div><font size="1">Aptitude for learning/using new software and systems</font></div></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="1">&nbsp;</font></p>
                            <div><b><font size="1">Location, Education &amp; Experience </font></b></div>
                            <ul>
                            <li>
                            <div><font size="1">Based in Chicago, IL</font></div></li>
                            <li>
                            <div><font size="1">University degree required</font></div></li>
                            <li>
                            <div><font size="1">0-2 years of relevant work experience preferred</font></div></li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="1"></font>&nbsp;</p>
                            <div><b><font color="#000000" size="1">Equal Employment Opportunity and Nondiscrimination</font></b></div>
                            <div><font color="#000000">
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><font size="1">A.T. Kearney prides itself on providing a culture that allows employees to bring their best selves to work every day. Our people can feel comfortable, confident, and joyful to do great things for our firm, our colleagues, and our clients. A.T. Kearney aims to build diverse capabilities to help our clients solve their most mission critical problems.<!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></font></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><font size="1">&nbsp;<o:p></o:p></font></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt"><font size="1">A.T. Kearney is committed to building a diverse, unbiased and inclusive workforce. A.T. Kearney is an equal opportunity employer; we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a person’s gender identity or expression, sexual orientation, race, religion, age, national origin, disability, marital status, pregnancy status, veteran status, genetic information or any other differences consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices. Members of communities historically underrepresented in consulting are encouraged to apply.</font><o:p></o:p></p></font></div>
                            <div><font color="#000000" size="1">&nbsp;</font></div>
                            <div><font color="#000000" size="1">GLDR</font></div></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Non-Consulting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States-Chicago</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      ),
                     array(
                          "job_id"=>'002LN',
                          "job_title"=>"2019 Manufacturing Associate",
                          "location"=>"United States",
                          "posting_date"=>"May 2, 2019",
                          "category"=>"Industry Recruiting",
                          "source_url"=>"https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002LN&tz=GMT%2B05%3A30",
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">2019 Manufacturing Associate</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002LN</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="2"><font color="#000000">A.T. Kearney is a global team of forward-thinking, collaborative partners that delivers immediate business impact and a long-term transformational advantage to clients and colleagues. To accommodate the exceptional growth of our firm, driven by the need for manufacturing and operations improvement services, we formed a team to serve manufacturing clients in a variety of industries (Automotive, Aerospace &amp; Defense, Equipment &amp; Components, High Tech, Consumer Products, Process Industries and Healthcare). <!--?xml:namespace prefix = "o" /--><o:p></o:p></font></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px" align="justify"><font color="#000000" size="2"><o:p>&nbsp;</o:p></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="2"><font color="#000000">As an A.T. Kearney Manufacturing Associate, your primary role will be to coach and advise a customer operations team in identifying cost reduction and efficiency improvement plans in your area of expertise.<span style="mso-spacerun: yes">&nbsp; </span>The scope will focus on manufacturing and extend to supply chain, logistics, inventories, indirect functions, R&amp;D, service and administration. You will operate as a full-time management consultant with a functional expertise in manufacturing. Associates are integrated into project teams of 3-5 consultants and are led by Managers or other senior consultants. Exemplary activities are to: <o:p></o:p></font></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#000000" size="2"><o:p>&nbsp;</o:p></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px; MARGIN-LEFT: 0.25in; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Support the development of the overall strategy for operations improvement across all client sites<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Conduct shop floor manufacturing improvement workshops for a specific site or business unit<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Employ manufacturing engineering analytical tools such as should cost modeling<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Evaluate the client’s current manufacturing strategies and develop strategic alternatives<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Work with teams to strategize how to best use manufacturing improvement methods within the context of their ongoing operations strategies<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Estimate benefits resulting from proposed manufacturing changes <o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Produce clear and persuasive documentation in support of analysis<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Drive the operations team to provide timely, high-quality data deliverables<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Recommend actions and help the client team implement them <o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><font color="#000000"><span style="mso-list: Ignore">•<span style="FONT-VARIANT: normal; FONT-WEIGHT: normal; FONT-STYLE: normal; LINE-HEIGHT: normal; font-stretch: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span>Support in developing and delivering digital manufacturing projects</font> <o:p></o:p></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font size="2"><br></font></p>
                            <p class="Pa1" style="MARGIN-BOTTOM: 5pt; MARGIN-TOP: 0px"><font size="2"><span class="A0"><b><font color="#8c0d04">What You Can Expect </font></b></span><font color="#8c0d04"><o:p></o:p></font></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px" align="justify"><font size="2"><font color="#000000">The career path for a Manufacturing Associate varies with experiences and skills. It is one that is flexible and provides cross training to suit individuals, giving them the opportunity to build skills in other areas. Our goal is to help our people be very successful throughout their career lifecycle. A variety of roles are available for successful Manufacturing associates:<o:p></o:p></font></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px; MARGIN-LEFT: 0.25in; TEXT-INDENT: -0.25in" align="justify"><font size="2"><font color="#000000">•<span style="LINE-HEIGHT: normal; font-stretch: normal; font-variant-numeric: normal; font-variant-east-asian: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Senior Manufacturing Associate — develops the operations improvement strategies and provides subject matter expertise in specific pillars of manufacturing improvement to other project teams<o:p></o:p></font></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px; MARGIN-LEFT: 0.25in; TEXT-INDENT: -0.25in" align="justify"><font size="2"><font color="#000000">•<span style="LINE-HEIGHT: normal; font-stretch: normal; font-variant-numeric: normal; font-variant-east-asian: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Manufacturing Manager — manages the overall manufacturing improvement process for a site or site group; leads project work streams and provides coaching to junior project team members<o:p></o:p></font></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px; MARGIN-LEFT: 0.25in; TEXT-INDENT: -0.25in" align="justify"><font size="2"><font color="#000000">•<span style="LINE-HEIGHT: normal; font-stretch: normal; font-variant-numeric: normal; font-variant-east-asian: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Director — responsible for leading large engagements or managing a commercially viable platform that will build the firm, grow the practice and strengthen client relationships<o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN: 3pt 0in 0pt 0.25in; LINE-HEIGHT: normal; TEXT-INDENT: -0.25in; mso-list: l0 level1 lfo1; tab-stops: list .25in"><font color="#000000"></font></p>
                            <p class="Pa0" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px; MARGIN-LEFT: 0.25in; TEXT-INDENT: -0.25in" align="justify"><font size="2"><font color="#000000">•<span style="LINE-HEIGHT: normal; font-stretch: normal; font-variant-numeric: normal; font-variant-east-asian: normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>Vice President &amp; Partner — additional steps in the career are possible, up to a viable partnership of the firm with a clear focus on client management, business development, firm building as well as fostering external awareness for the firm.</font><font color="#221e1f">&nbsp;<o:p></o:p></font></font></p><br _moz_editor_bogus_node="TRUE"></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><p class="Pa1" style="MARGIN-BOTTOM: 5pt; MARGIN-TOP: 0px"><font size="2"><span class="A0"><b><font color="#8c0d04">What We Seek </font></b></span><font color="#8c0d04"><!--?xml:namespace prefix = "o" /--><o:p></o:p></font></font></p>
                            <p class="MsoNormal" style="MARGIN-TOP: 3pt; LINE-HEIGHT: normal"><font size="2"><font color="#000000">A.T. Kearney Manufacturing Associates typically have a Master’s degree (likely an MBA and/or other post-graduate degree in Electrical, Mechanical, Industrial Engineering, etc.) with a manufacturing concentration. Most have 3 to 5 years full-time relevant experience at a Fortune 100 company in lean manufacturing, Six Sigma, maintenance, production control, and cost estimation or have expertise in specific manufacturing processes (e.g., assembly, machining, packaging, electronics). Expertise in the area of Digital Manufacturing is a plus. Ideally the described experience can be from a consulting firm or an in-house consulting type unit.<o:p></o:p></font></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#000000" size="2"><o:p>&nbsp;</o:p></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#000000" size="2">Manufacturing Associates have excellent interpersonal and communication skills. They enjoy working in a team-based environment and are committed to meeting/exceeding client objectives</font><a name="_Hlk512607443"><font color="#000000" size="2"> Advanced proficiency in PowerPoint and Excel is necessary. </font></a><font size="2"><font color="#000000">Manufacturing Associates will operate primarily in the industries listed above.<o:p></o:p></font></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#000000" size="2"><o:p>&nbsp;</o:p></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#221e1f"><font size="2"><font color="#000000">Manufacturing Associates are based out of A.T. Kearney offices in Atlanta, Chicago, Dallas, New York, or Washington DC and travel up to four days each week.</font><span style="mso-spacerun: yes">&nbsp; </span></font></font></p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#221e1f"><font size="2"><span style="mso-spacerun: yes"></span></font></font>&nbsp;</p>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font color="#221e1f"><span style="mso-spacerun: yes"></span><o:p>&nbsp;</o:p></font></p><font color="#221e1f">
                            <div><u><font color="#000000" size="2"><strong>Equal Employment Opportunity and Non-Discrimination:</strong></font></u></div>
                            <div><font color="#000000" size="2">A.T. Kearney prides itself on providing a culture that allows employees to bring their best selves to work every day. Our people can feel comfortable, confident, and joyful to do great things for our firm, our colleagues, and our clients. A.T. Kearney aims to build diverse capabilities to help our clients solve their most mission critical problems.</font></div>
                            <div><font color="#000000" size="2">&nbsp;</font></div>
                            <div><font color="#000000" size="2">A.T. Kearney is committed to building a diverse, unbiased and inclusive workforce. A.T. Kearney is an equal opportunity employer; we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a person’s gender identity or expression, sexual orientation, race, religion, age, national origin, disability, marital status, pregnancy status, veteran status, genetic information or any other differences consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices. Members of communities historically underrepresented in consulting are encouraged to apply.</font></div></font>
                            <p class="Default" style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><font size="2"><o:p>&nbsp;</o:p></font></p><br _moz_editor_bogus_node="TRUE"></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Junior Recruiting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      ),
                      array(
                          "job_id"=>'002OQ',
                          "job_title"=>"Global Business Policy Council, Business Analyst",
                          "location"=>"United States",
                          "posting_date"=>"May 1, 2019",
                          "category"=>"Industry Recruiting",
                          "source_url"=>'https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002OQ&tz=GMT%2B05%3A30',
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">Global Business Policy Council, Business Analyst</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002OQ</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><div><font size="2"><font face="Arial"><b>ABOUT A.T. KEARNEY</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2" face="Arial">A.T. Kearney is a global team of forward-thinking, collaborative partners that delivers immediate, meaningful results and a long-term transformational advantage to our clients.&nbsp; We are talented problem solvers who work in a collegial way to create and implement practical and sustainable solutions.&nbsp; Since 1926, we have been trusted advisors on CEO-agenda issues to the world’s leading organizations across all major industries and sectors.&nbsp; We seek individuals with a proven record that combines academic excellence with significant work experience to join our team.&nbsp; </font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2"><font face="Arial"><b>ABOUT THE GBPC</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2" face="Arial">The Global Business Policy Council is a specialized foresight and strategic analysis unit within A.T. Kearney. The Council is dedicated to providing immediate impact and growing advantage by helping CEOs and government leaders anticipate and plan for the future.</font></div>
                                <div><font size="2" face="Arial">Since its first CEO Retreat in 1992, A.T. Kearney’s Global Business Policy Council has been a strategic service for the world’s top executives, government officials, and business-minded thought leaders. Through exclusive global forums, public-facing thought leadership, and advisory services, the Council helps to decipher sweeping geopolitical, economic, social, and technological changes and their effects on the global business environment.&nbsp; </font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2"><font face="Arial"><b>ABOUT THE POSITION</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2" face="Arial">The role of the Business Analyst is to contribute to the Councils work in strategic foresight and macro-trends analysis on a variety of aspects of the global external operating environment through extensive research and writing white papers, articles, op-eds, and other pieces. The Business Analyst works principally with and under the supervision of the Council’s Chairman, Managing Director, and Manager for Thought Leadership. The Business Analyst will also support the Council’s global forums and advisory services work. The work environment is dynamic, collegial, and focused on continuous professional growth and new acquisition of subject-matter expertise. In addition to Council activities, the position offers exposure to the depth and breadth of the firm’s overall global management consulting operations, as well as our member companies and advisory service clients.</font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2"><font face="Arial"><b>ORGANIZATIONAL ENVIRONMENT</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2" face="Arial">Reports to GBPC Managing Director</font></div>
                                <div><font size="2" face="Arial">Located in Washington, D.C. area (Arlington, Virginia) – Candidates must be based in DC or Willing to Relocate</font></div></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><div><font size="2"><font face="Arial"><b>EXPERIENCE, KNOWLEDGE AND SKILLS REQUIREMENTS</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Graduate and undergraduate degree in international affairs, macroeconomics, political science, or other relevant field.</font></div>
                                <div><font size="2" face="Arial">•&nbsp;1-3 years relevant work experience.</font></div>
                                <div><font size="2" face="Arial">•&nbsp;US citizenship or permanent residency strongly preferred.</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Strong professional research and writing abilities including:</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Proficiency in rigorous quantitative and qualitative research methodologies (academic background minimum; previous professional experience preferred);</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Academic or professional experience in developing subject matter expertise on a particular region and/or industry/sector;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Experience and/or strong interest in identifying and analyzing how developments in geopolitical, economic, social, technological, and other changes in the external environment affect business interests; </font></div>
                                <div><font size="2" face="Arial">•&nbsp;Ability to work independently and collaboratively as part of a small team to generate original reports and briefings for a high-level business audience;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Comfort with drafting professional communications (email and letter) with minimal oversight;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Social media savvy in a professional corporate context, including familiarity with Twitter and LinkedIn;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Experience in administrative support, including strong organizational skills and attention to detail;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Strong production capabilities in PowerPoint, Keynote, Word, and Excel;</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Strong inter-personal skills and professional demeanor; and</font></div>
                                <div><font size="2" face="Arial">•&nbsp;Flexibility and enthusiasm for taking on a dynamic portfolio of responsibilities.</font></div>
                                <div><font size="2" face="Arial">&nbsp;</font></div>
                                <div><b><font size="2" face="Arial"></font></b>&nbsp;</div>
                                <div><strong><font size="2"></font></strong>&nbsp;</div>
                                <div>&nbsp;</div>
                                <div><font size="2"><font face="Arial"><b>EQUAL EMPLOYMENT OPPORTUNITY AND NON-DISCRIMINATION</b>&nbsp;</font></font></div>
                                <div><font size="2" face="Arial">A.T. Kearney has long recognized the value that diversity brings to our business and the clients we serve. Our goal is to create a climate of opportunity, innovation, and success within A.T. Kearney that capitalizes on the professional and personal diversity of our workforce. For these reasons, we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a persons race, color, religion, sex, age, national origin, sexual orientation, gender identity or expression, veteran status, marital status, or disability consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices</font></div>
                                <div><font size="2"></font>&nbsp;</div>
                                <div><font size="2">GLDR</font></div></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Junior Recruiting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States-Washington D.C.</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      ),
                      array(
                          "job_title"=>"2019 Sr. Manufacturing Analyst",
                          "location"=>"United States",
                          "posting_date"=>"Jan 24, 2019",
                          "category"=>"Industry Recruiting",
                      ),
                      array(
                          "job_id"=>'002QB',
                          "job_title"=>"Marketing Specialist - Alumni Engagement",
                          "location"=>"United States",
                          "posting_date"=>"May 10, 2019",
                          "category"=>"Marketing",
                          "source_url"=>'https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002QB&tz=GMT%2B05%3A30',
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">Marketing Specialist - Alumni Engagement</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002QB</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><font size="3" face="Arial">
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">Marketing Specialist – Alumni Engagement</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">Reporting to the Marketing Director, Global Communications &amp; Alumni, the Marketing Specialist will support alumni engagement programs. To succeed in this role, you will need to have superior writing skills, effective communication skills and an analytical mindset. You will need to be inventive, clever, open to sharing creative ideas and proactively suggest new strategies. Attention to detail and consistency is a must. This position is based in Chicago.</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><b><u>Responsibilities:</u></b></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">ALUMNI PROGRAMS</p>
                            <ul>
                            <li>Support the firm’s global alumni engagement strategy, in conjunction with local offices with a focus on the Americas region</li>
                            <li>Ensure consistent email communication to global alumni via monthly emails, enewsletters, and event invitations using the Vuture platform</li>
                            <li>Manage the alumni website, including new registrations, Jobs Board postings, and content sharing</li>
                            <li>Lead social media strategy and digital engagement to enhance awareness of alumni benefits and programs i.e. LinkedIn alumni group, alumni profiles, event features, etc.</li>
                            <ul>
                            <li>Oversee alumni profile initiative and evolve to video and audio storytelling. Curate content and track impact.</li></ul>
                            <li>Support global alumni events</li>
                            <li>Maintain alumni program KPIs, including analysis and identification of trends to drive data-driven decision making</li>
                            <li>Create materials aligned to the alumni engagement plan for the firm, including, but not limited to guidelines and resources as needed to inform and educate local alumni leads</li>
                            <li>Develop internal awareness program to establish a culture of being part of the A.T. Kearney family (on-boarding, transition out, office updates, etc.)</li>
                            <li>Together with the alumni data team located in India, maintain the integrity and accuracy of &gt;22,000 alumni records</li>
                            <li>Provide administration support to the Director of Global Internal Communications and Alumni Engagement, including team management</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">ALUMNI EVENTS</p>
                            <ul>
                            <li>Lead the launch of Worldwide A.T. Kearney Day, including pre and post outreach and communication</li>
                            <li>Support Americas alumni events, with proactive opportunities</li>
                            <li>Promote global events via internal and external channels</li>
                            <li>Track and report on impact of events</li>
                            <li>Recommend a plan to further engage alumni with in-person events/activities</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">GLOBAL LIAISON</p>
                            <ul>
                            <li>Coordinate and communicate with global alumni leads</li>
                            <li>Support global communication requests from all A.T. Kearney offices</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">COMMUNICATIONS</p>
                            <ul>
                            <li>Review internal marketing website content on A.T. Kearney’s intranet and refresh content</li>
                            <li>Draft and/or review content for emails and external website</li>
                            <li>Data analysis and reporting for communications channels </li></ul></font></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><font size="3">
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><b><u>Qualifications:</u></b></p>
                            <ul>
                            <li>Bachelor’s Degree program focused in Business/Marketing/Communications/English/Literature </li>
                            <li>Five years of experience in alumni relations, business development, or equivalent function</li>
                            <li>Experience in/understanding of business development, relationship building, event management, and program development</li>
                            <li>Ability to conduct research and synthesize the findings for distribution</li>
                            <li>Ability to think strategically and apply given strategy and/or common business knowledge to recommendations and rationale</li>
                            <li>Ability to execute with thought and attention to detail, with managerial direction (versus dictation)</li>
                            <li>Proficiency with Microsoft Office (Word, PPT, Excel), CRM tools and major social media tools</li>
                            <li>Strong interpersonal and communication (written and verbal) skills and the ability to interact effectively at all levels and collaboratively in team environments across multiple functions</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><b><u>Equal Employment Opportunity and Non Discrimination</u></b></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong><u></u></strong>&nbsp;</p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">A.T. Kearney prides itself on providing a culture that allows employees to bring their best selves to work every day. Our people can feel comfortable, confident, and joyful to do great things for our firm, our colleagues, and our clients. A.T. Kearney aims to build diverse capabilities to help our clients solve their most mission critical problems.<!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">&nbsp;<o:p></o:p></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">A.T. Kearney is committed to building a diverse, unbiased and inclusive workforce. A.T. Kearney is an equal opportunity employer; we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a person’s gender identity or expression, sexual orientation, race, religion, age, national origin, disability, marital status, pregnancy status, veteran status, genetic information or any other differences consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices. Members of communities historically underrepresented in consulting are encouraged to apply.<o:p></o:p></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">
                            </p><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">GLDR</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p></font></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Non-Consulting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States-Chicago</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      ),
                      array(
                          "job_id"=>'002Q4',
                          "job_title"=>"Social Media Lead",
                          "location"=>"United States",
                          "posting_date"=>"May 10, 2019",
                          "category"=>"Marketing",
                          "source_url"=>'https://atkcareers.taleo.net/careersection/03/jobdetail.ftl?job=002Q4&tz=GMT%2B05%3A30',
                          "description"=>'<div class="editablesection"><div id="requisitionDescriptionInterface.ID1442.row1" class="contentlinepanel" title=""><span id="requisitionDescriptionInterface.reqTitleLinkAction.row1" class="titlepage" title="">Social Media Lead</span><span id="requisitionDescriptionInterface.ID1453.row1" class="titlepage" title="">-</span><span id="requisitionDescriptionInterface.reqContestNumberValue.row1" class="titlepage" title="">002Q4</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1472.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1488.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Description</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1515.row1" class="text" title=""><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">The Social Media Lead will manage A.T. Kearney’s social media marketing efforts and will assist in content creation, strategic execution, and campaign strategy. The right candidate will be a team player who can collaborate with marketing teams and consultants to support marketing initiatives across the firm’s practices and geographies, advise on strategy for corporate communications, and help embed best practices in personal and digital branding on social media. This role will take ownership of all global social media accounts. </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">The ideal candidate is a solutions-oriented self-starter who can manage multiple projects and stakeholders at once and work autonomously to bring new ideas to our channels and adjust them as needed. The person also has excellent writing skills; a deep understanding of social media tactics, organic and paid targeting options, social and content analytics, and the latest trends in social media marketing. Lastly, the candidate must have a desire to learn, test, and grow as a team. </p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong><u>Essential Duties and Responsibilities:</u></strong></p>
                            <ul>
                            <li><strong>Lead social strategy</strong> </li>
                            <ul>
                            <li>Lead cross-functional social campaign strategy (short &amp; long-term, paid &amp; organic), working with globally distributed teams to establish campaign goals, target the right audiences, execute, and report the results</li>
                            <li>Evolve and enforce the firm’s social media brand voice to ensure we reach a relevant, engaged, executive-level B2B audience</li>
                            <li>Gather and share data-based insights on social media performance and use the data to evolve the strategy </li>
                            <li>Establish self as the go-to source for social media intelligence, coaching, and strategic support at all levels of the organization, including partner-level stakeholders. Remain up-to-date on demographic trends, best practices, and business offerings for all social media networks, including developing and maintaining relationships with representatives at each network</li>
                            <li>Proactively identify new and growing social networks, apps, and other services that could be used for business development and recruiting purposes </li></ul>
                            <li><strong>Manage social channels</strong></li>
                            <ul>
                            <li>Manage global social media accounts, including LinkedIn, Facebook, Twitter, and Instagram</li>
                            <li>Write and proof all posts for release, with strong emphasis on attention to detail to avoid errors in messaging and to ensure it is on brand</li>
                            <li>Collaborate with design to develop and optimize visual materials to promote firm content on social media</li>
                            <li>Manage inbound messages via social media, and identify relevant party or parties to respond to comments and inquiries</li>
                            <li>When necessary, provide onsite support at firm events and conferences as needed (some travel may be necessary)</li>
                            <li>Perform other digital optimization tasks upon request</li></ul>
                            <li><strong>Coordinate global content</strong> </li>
                            <ul>
                            <li>Work with the managing editor to maintain a global content calendar that aligns content distribution and promotion with firm marketing goals </li>
                            <li>Train and collaborate with colleagues globally to assist with content creation and delivery as needed</li>
                            <li>Collaborate with marketers globally to develop capabilities on social media networks at the unit, practice, and individual levels</li></ul></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1528.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><div id="requisitionDescriptionInterface.ID1544.row1" class="inlinepanel" title="" style="display: inline;"><span class="subtitle">Qualifications</span></div></h2><span class="blockpanel"><span class="">&nbsp;</span></span><span id="requisitionDescriptionInterface.ID1571.row1" class="text" title=""><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong><u>Experience, Education and Skill Requirements:</u></strong>&nbsp; </p>
                            <ul>
                            <li>Advanced interpersonal and communication (written and verbal) skills and the ability to interact effectively at all levels and collaboratively in team environments across multiple functions</li>
                            <li>Strong project management skills and ability to manage multiple projects and stakeholders, meet aggressive deadlines, and prioritize accordingly.</li>
                            <li>Bachelor’s degree, preferably in journalism, communication, marketing, or a related field</li>
                            <li>2+ years experience in digital marketing in agency, corporate communications, digital media, or a related field; B2B marketing experience a plus</li>
                            <li>Proactive, self-directed, and able to constantly act upon opportunities to improve</li>
                            <li>Ability to think and learn on the fly</li>
                            <li>Accepts a fluid structure where roles are not concrete and multitasking is essential</li></ul>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"><strong>Equal Employment Opportunity and Non-Discrimination:</strong></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">A.T. Kearney prides itself on providing a culture that allows employees to bring their best selves to work every day. Our people can feel comfortable, confident, and joyful to do great things for our firm, our colleagues, and our clients. A.T. Kearney aims to build diverse capabilities to help our clients solve their most mission critical problems.<!--?xml:namespace prefix = "o" ns = "urn:schemas-microsoft-com:office:office" /--><o:p></o:p></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">&nbsp;<o:p></o:p></p>
                            <p class="MsoNormal" style="MARGIN: 0in 0in 0pt">A.T. Kearney is committed to building a diverse, unbiased and inclusive workforce. A.T. Kearney is an equal opportunity employer; we recruit, hire, train, promote, develop, and provide other conditions of employment without regard to a person’s gender identity or expression, sexual orientation, race, religion, age, national origin, disability, marital status, pregnancy status, veteran status, genetic information or any other differences consistent with applicable laws. This includes providing reasonable accommodation for disabilities, or religious beliefs and practices. Members of communities historically underrepresented in consulting are encouraged to apply.<o:p></o:p></p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">
                            </p><p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">&nbsp;</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px">GLDR</p>
                            <p style="MARGIN-BOTTOM: 0px; MARGIN-TOP: 0px"></p></span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1584.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1600.row1" class="subtitle" title="" style="display: inline;">Job</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1621.row1" class="text" title="">Non-Consulting</span></div><div id="requisitionDescriptionInterface.ID1628.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1644.row1" class="subtitle" title="" style="display: inline;">Primary Location</span></h2><span id="requisitionDescriptionInterface.ID1661.row1" class="text" title="">Americas-United States-Chicago</span></div><div id="requisitionDescriptionInterface.ID1668" style="display: none;"></div><div id="requisitionDescriptionInterface.ID1708.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1724.row1" class="subtitle" title="" style="display: inline;">Organization</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1745.row1" class="text" title="">United States</span></div><div id="requisitionDescriptionInterface.ID1752.row1" class="contentlinepanel" title=""><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1768.row1" class="subtitle" title="" style="display: inline;">Schedule</span></h2><span class="inline">&nbsp;</span><span id="requisitionDescriptionInterface.ID1789.row1" class="text" title="">Full-time</span></div><div class="staticcontentlinepanel"><p></p></div><div id="requisitionDescriptionInterface.ID1800" style="display: none;"></div><div class="staticcontentlinepanel"><h2 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:htm="http://www.w3.org/1999/xhtml" xmlns:ftl="http://www.taleo.net/ftl" class="no-change-header-inline"><span id="requisitionDescriptionInterface.ID1860.row1" class="subtitle" title="" style="display: inline;">Unposting Date</span></h2><span class="inline">&nbsp;</span></div><div id="requisitionDescriptionInterface.ID1882" style="display: none;"></div><div class="staticcontentlinepanel"><p></p></div></div>'
                      )

                );

             //dd($job_array);

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $postdata = '{"multilineEnabled":false,"sortingSelection":{"sortBySelectionParam":"3","ascendingSortingOrder":"false"},"fieldData":{"fields":{"KEYWORD":"","LOCATION":""},"valid":true},"filterSelectionParam":{"searchFilterSelections":[{"id":"POSTING_DATE","selectedValues":[]},{"id":"LOCATION","selectedValues":[]},{"id":"JOB_TYPE","selectedValues":[]},{"id":"JOB_SCHEDULE","selectedValues":[]},{"id":"JOB_LEVEL","selectedValues":[]},{"id":"JOB_FIELD","selectedValues":[]}]},"advancedSearchFiltersSelectionParam":{"searchFilterSelections":[{"id":"LOCATION","selectedValues":[]},{"id":"JOB_FIELD","selectedValues":[]},{"id":"JOB_NUMBER","selectedValues":[]},{"id":"URGENT_JOB","selectedValues":[]},{"id":"EMPLOYEE_STATUS","selectedValues":[]},{"id":"STUDY_LEVEL","selectedValues":[]},{"id":"WILL_TRAVEL","selectedValues":[]}]},"pageNo":1}';

             $ch = curl_init('https://atkcareers.taleo.net/careersection/rest/jobboard/searchjobs?lang=en&portal=4201381138');
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'X-Requested-With: XMLHttpRequest',
                        'Content-Type: application/json',

              ));
             curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
             $response = curl_exec($ch);
             curl_close($ch);
             
             foreach ($job_array as $key => $job) {
                 $postdate = $job['posting_date'];
                 $posted_date = date('Y-m-d', strtotime($postdate));
                 if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                 {
                     $job_id = $job['job_id'];
                     //print_r($job_id.",");
                     $posted_on = $posted_date;
                     $job_title = $job['job_title'];
                     $category = $job['category'];
                     $country = $job['location'];
                     $source_url = $job['source_url'];
                     $description = $job['description'];
                     $job_desc = preg_replace('/\s+/', ' ', $description);
                     $job_desc = addslashes($job_desc);

                     $row = Job::where('job_id', $job_id)->count();  
                             if($row == 0)
                               {
                                      $insert_data = [
                                        "company" => "Atkearney",
                                        "website" => "https://www.atkearney.es/careers/apply-now",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_on,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                                }
                 }
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
