<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AxfoodCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'axfood:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $html_data = HtmlDomParser::file_get_html("https://jobb.axfood.se/search/?createNewAlert=false&q=");
                $job_data = $html_data->find('div[class=searchResultsShell]',0)->find('table[id=searchresults]',0)->find('tr[class=data-row clickable]');
                foreach ($job_data as $key => $jobs) 
                {
                    $postdate = $jobs->find('td[class=colDate hidden-phone]',0)->find('span[class=jobDate]',0)->innertext; 
                    $posted_date = date('Y-m-d', strtotime(trim($postdate)));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $url = $jobs->find('td[class=colTitle]',0)->find('span',0)->find('a',0)->href;
                        $source_url = "https://jobb.axfood.se".$url;
                        $job_id = explode("/", $url);
                        $job_id = $job_id[4];
                        //print_r($job_id.",");
                        $job_title = $jobs->find('td[class=colTitle]',0)->find('span',0)->find('a',0)->innertext;
                        $job_title = html_entity_decode(trim($job_title));
                        $country = "Sweden";
                        $category = $jobs->find('td[class=colDepartment hidden-phone]',0)->find('span[class=jobDepartment]',0)->innertext;
                        $category = html_entity_decode(trim($category));
                        $jobdesc = HtmlDomParser::file_get_html($source_url);
                        $desc = $jobdesc->find('div[class=jobDisplayShell]',0)->find('div[class=jobDisplay]',0)->find('div[class=job]',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);
                        $row = Job::where('job_id', $job_id)->count();  
                         if($row == 0)
                         {
                              $insert_data = [
                                "company" => "Axfood",
                                "website" => "https://jobb.axfood.se/",
                                "job_title" => $job_title,
                                "posted_on"=> $posted_date,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                         }
                    }

                }
           
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
