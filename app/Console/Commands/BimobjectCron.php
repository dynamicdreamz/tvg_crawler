<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BimobjectCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'bimobject:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {      
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://bimobject.bamboohr.com/jobs/embed2.php");
             $job_data = $html_data->find('div[class=BambooHR-ATS-board]',0)->find('ul[class=BambooHR-ATS-Department-List]',0)->find('li[class=BambooHR-ATS-Department-Item]');
             
             for ($i=0; $i <count($job_data) ; $i++) 
             { 
                $job_id = "95124".$i;
                //print_r($job_id.",");
                $category = $job_data[$i]->find('div',0)->innertext;
                $category = trim($category);
                $url = $job_data[$i]->find('ul[class=BambooHR-ATS-Jobs-List]',0)->find('li[class=BambooHR-ATS-Jobs-Item]',0)->find('a',0)->href;
                $source_url = "http:".$url;
                $job_title = $job_data[$i]->find('ul[class=BambooHR-ATS-Jobs-List]',0)->find('li[class=BambooHR-ATS-Jobs-Item]',0)->find('a',0)->innertext;
                $job_title = trim($job_title);
                $country = $job_data[$i]->find('ul[class=BambooHR-ATS-Jobs-List]',0)->find('li[class=BambooHR-ATS-Jobs-Item]',0)->find('span[class=BambooHR-ATS-Location]',0)->innertext;
                $country = explode(",", $country);
                $country = trim($country[1]);
                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('div[class=ResAts__Viewport js-jobs-viewport js-chosen-container]',0)->find('div[class=ResAts__page ResAts__description js-jobs-page js-jobs-description]',0)->find('div[class=ResAts__card-content]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Bimobject",
                                        "website" => "https://careers.bimobject.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
