<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class GlovoappCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'glovoapp:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                   "Barcelona" => "Spain",
                   "Peru." => "Peru",
                   "República Dominicana" => "Dominican Republic",
                   "Nairobi" => "Kenya",
                   "Kiev" => "Ukraine",
                   "EUROPE" => "Europe",
                   "Buenos Aires" => "Argentina",
                   "Bucharest" => "Romania",
                   "Barcelona HQ" => "Spain",
                   "Casablanca" => "Morocco",
                   "Barcelona (EU HQ)"=>"Spain",
                   "İstanbul"=>"Turkey",
                   "Tbilisi"=>"Georgia",
                   "Kiev (Ukraine)"=>"Ukraine",
                   "Istanbul (HQ Turkey)"=>"Turkey",
                   "La Plata"=>"Argentina",
                   "Guatemala City"=>"Guatemala",
                   "Abidjan"=>"Côte d’Ivoire",
                   "Peru Teleperformance"=>"Peru",
                   "Malaga"=>"Spain",
                   "Izmir"=>"Turkey",
                   "Zagreb"=>"Croatia",
                   "Madrid"=>"Spain",
                   "Ankara"=>"Turkey",
                   "Kiev (HQ Ukraine)"=>"Ukraine",
                   "Bucharest (HQ Romania)"=>"Romania",
                   "Casablanca (HQ Morocco)"=>"Morocco",
                   "Istanbul"=>"Turkey",
              ];

              $categories_no = [
                   "Corporate" => "0",
                   "Data" => "0",
                   "Engineering and Product" => "1",
                   "Finance" => "1",
                   "International" => "0",
                   "Internships" => "1",
                   "Marketing and Growth" => "1",
                   "New Business" => "1",
                   "Operations" => "1",
                   "People" => "1",
                   "Sales" => "1",
              ];

              $categories = [
                   "Strategy & Development" => "Corporate",
                   "Public Affairs" => "Corporate",
                   "Data Engineering" => "Data Engineering",
                   "Data Science" => "Data Science",
                   "Business Intelligence" => "Business Intelligence",
                   "Engineering and Product" => "Engineering",
                   "Product" => "Product",
                   "Finance" => "Finance",
                   "Treasury" => "Treasury",
                   "Legal" => "Legal",
                   "Fraud and Payments" => "Fraud and Payments",
                   "General Management" => "General Management",
                   "Internships" => "Internships",
                   "Marketing and Growth" => "Marketing and Growth",
                   "Brand and Communications" => "Brand and Communications",
                   "Digital" => "Digital",
                   "New Business" => "Business",
                   "Groceries" => "Business",
                   "Food New Business" => "Business",
                   "Operations" => "Operations",
                   "LiveOps" => "Operations",
                   "Supply Operations" => "Operations",
                   "Partner Operations" => "Operations",
                   "People" => "People",
                   "People Partners" => "People",
                   "People Operations" => "People",
                   "Talent Acquisition" => "Talent Acquisition",
                   "Sales" => "Sales",
                   "Sales Operations" => "Sales",
                   "Inside Sales" => "Sales",
              ];

              $cateId = [
                    "Engineering and Product"=>"4001331002",
                    "Finance"=>"4001333002",
                    "Internships"=>"4001448002",
                    "Marketing and Growth"=>"4001328002",
                    "New Business"=>"4018466002",
                    "Operations"=>"4001330002",
                    "People"=>"4001332002",
                    "Sales"=>"4001329002",
              ];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $html_data = HtmlDomParser::file_get_html('https://boards.greenhouse.io/embed/job_board?for=glovo&b=https%3A%2F%2Fglovoapp.com%2Fen%2Fjobs');
             //dd($html_data);
           
             $job_details = $html_data->find('section[class=level-0]');
             foreach ($job_details as $key => $job) 
             {
                 $categry = $job->find('h2',0)->innertext;
                 
                 $maincategory = $categories_no[trim($categry)];
                 //dd($maincategory);
                 if($maincategory == 0)
                 {
                    //print_r($maincategory.",");
                    $section = $job->find('section[class=child]');
                    foreach ($section as $key => $jobs) 
                    {
                        $category_find = $jobs->find('h2',0)->innertext;
                        $category = html_entity_decode(trim($category_find));
                        $category = $categories[$category];
                        //print_r($category.",");
                        
                        $div_count = $jobs->find('div[class=opening]');
                        foreach ($div_count as $key => $div_data) 
                        {
                            $source_url = $div_data->find('a',0)->href;
                            $source_url_exp = explode("=",$source_url);
                            $job_id = $source_url_exp[1]; 
                            $job_title = $div_data->find('a',0)->innertext;
                            $job_title = trim($job_title);
                            //print_r($job_title.",");
                            $location = $div_data->find('span[class=location]',0)->innertext;
                            $location_exp = explode(",",trim($location));
                            $count = count($location_exp);
                            if($count == 2)
                            {
                               if($location_exp[1] == 'Peru.')
                               {
                                  $country = "Peru";
                               }
                               else
                               {
                                  if($location_exp[1] == 'República Dominicana')
                                  {
                                     $country = "Dominican Republic";
                                  }
                                  else
                                  {
                                     $country = trim($location_exp[1]);
                                  }
                               }                              
                            }
                            else
                            {
                               $country = $countries[trim($location_exp[0])];
                               
                            }

                            $job_details = HtmlDomParser::file_get_html('https://boards.greenhouse.io/embed/job_app?for=glovo&token='.$job_id.'&b=https%3A%2F%2Fglovoapp.com%2Fen%2Fjobs');
                            $job_desc = $job_details->find('div[id=app_body]',0)->find('div[id=content]',0)->innertext;
                            $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                            $job_desc = addslashes($job_desc);

                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Glovoapp",
                                        "website" => "https://glovoapp.com/en/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                            

                        }                       
                        
                    }

                 }
                 else
                 { 
                    //print_r($maincategory.",");
                    $id=$cateId[trim($categry)];
                    $divs = $job->find('div[department_id='.$id.']');
                    foreach ($divs as $key => $jobs) 
                    {
                        //print_r("div".",");
                        $category = $categories[trim($categry)];
                        //print_r($category.",");
                        $source_url = $jobs->find('a',0)->href;
                        $source_url_exp = explode("=",$source_url);
                        $job_id = $source_url_exp[1];
                        $job_title = $jobs->find('a',0)->innertext;
                        $job_title = trim($job_title);
                        //print_r($job_title.",");
                        $location = $jobs->find('span[class=location]',0)->innertext;
                        $location_exp = explode(",",trim($location));
                        $count = count($location_exp);
                        if($count == 2)
                        {
                               if($location_exp[1] == 'Peru.')
                               {
                                  $country = "Peru";
                               }
                               else
                               {
                                  if($location_exp[1] == 'República Dominicana')
                                  {
                                     $country = "Dominican Republic";
                                  }
                                  else
                                  {
                                     $country = trim($location_exp[1]);
                                  }
                               }                              
                        }
                        else
                        {
                               $country = $countries[trim($location_exp[0])];
                               
                        }

                        $job_details = HtmlDomParser::file_get_html('https://boards.greenhouse.io/embed/job_app?for=glovo&token='.$job_id.'&b=https%3A%2F%2Fglovoapp.com%2Fen%2Fjobs');
                        $job_desc = $job_details->find('div[id=app_body]',0)->find('div[id=content]',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                        $job_desc = addslashes($job_desc);  

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Glovoapp",
                                        "website" => "https://glovoapp.com/en/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                            
                        
                    }

                    $section = $job->find('section[class=child]');
                    foreach ($section as $key => $job_data) 
                    {
                        //print_r("Section".",");
                        $category_find = $job_data->find('h2',0)->innertext;
                        $category = html_entity_decode(trim($category_find));
                        $category = $categories[$category];
                        //print_r($category.",");

                        $div_count = $job_data->find('div[class=opening]');
                        foreach ($div_count as $key => $div_data) 
                        {
                            $source_url = $div_data->find('a',0)->href;
                            $source_url_exp = explode("=",$source_url);
                            $job_id = $source_url_exp[1]; 
                            $job_title = $div_data->find('a',0)->innertext;
                            $job_title = trim($job_title);
                            //print_r($job_title.",");
                            $location = $div_data->find('span[class=location]',0)->innertext;
                            $location_exp = explode(",",trim($location));
                            $count = count($location_exp);
                            if($count == 2)
                            {
                               if($location_exp[1] == 'Peru.')
                               {
                                  $country = "Peru";
                               }
                               else
                               {
                                  if($location_exp[1] == 'República Dominicana')
                                  {
                                     $country = "Dominican Republic";
                                  }
                                  else
                                  {
                                     $country = trim($location_exp[1]);
                                  }
                               }                              
                            }
                            else
                            {
                               $country = $countries[trim($location_exp[0])];
                               
                            }

                            $job_details = HtmlDomParser::file_get_html('https://boards.greenhouse.io/embed/job_app?for=glovo&token='.$job_id.'&b=https%3A%2F%2Fglovoapp.com%2Fen%2Fjobs');
                            $job_desc = $job_details->find('div[id=app_body]',0)->find('div[id=content]',0)->innertext;
                            $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                            $job_desc = addslashes($job_desc); 

                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Glovoapp",
                                        "website" => "https://glovoapp.com/en/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                            
                        }

                    }

                 }
                
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
