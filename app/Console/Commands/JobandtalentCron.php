<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class JobandtalentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'Jobandtalent:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Madrid"=>"Spain",
                      "Bogotá"=>"Colombia",
                      "Bogota"=>"Colombia",
                      "HQ"=>"England",
                      "Medellin"=>"Colombia",
                      "Berlin"=>"Germany",
                      "Stockholm"=>"Sweden",
                      "London"=>"England",
                      "Paris"=>"France",
                      "Barcelona"=>"Spain",
                      "Medellin"=>"Colombia",
                      "Bilbao"=>"Spain",
                      "Colombia"=>"Colombia",
                      "Málaga"=>"Spain",
                      "Mallorca"=>"Spain",
                      "Mexico"=>"Mexico",
                      "Seville"=>"Spain",
                      "Valencia"=>"Spain",
                      "Zaragoza"=>"Spain",
                      "Madrid or remote"=>"Spain",
                      "Uppsala - Sweden"=>"Sweden",
                      "Madrid, Barcelona, Sevilla"=>"Spain",
                      "Paris, France"=>"France",
                      "Sevilla"=>"Spain"
                    ];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $html_data = HtmlDomParser::file_get_html('https://www.jobandtalent.com/join-us#open_positions');
             //dd($html_data);
             $job_details = $html_data->find('section[class=landing_tilted_section for_open_positions]',0)->find('div[class=landing_wrapper]',0)->find('h3');
             $ff = count($job_details);
             //dd($ff);
             for ($i=0; $i < $ff ; $i++)
             {
                 $category = $html_data->find('section[class=landing_tilted_section for_open_positions]',0)->find('div[class=landing_wrapper]',0)->find('h3',$i)->innertext;
                 $category = trim($category);
                 
                 //print_r( $category .",");
                 $job_data = $html_data->find('section[class=landing_tilted_section for_open_positions]',0)->find('div[class=landing_wrapper]',0)->find('ul',$i)->find('li');
                             
                 foreach ($job_data as $key => $list_data) 
                 {
                         $source_url = $list_data->find('a',0)->href;
                         $jobid = explode("/", $source_url);
                         $job_id = $jobid[5]; 
                         //print_r( $job_id .",");
                         $job_title = $list_data->find('a',0)->innertext;
                         $job_title = trim($job_title);
                         $country = $list_data->find('span[class=join_us_position_location]',0)->innertext;
                         $country = trim($country);
                         $country = $countries[$country];
                         $category = $category;
                         $job_details = HtmlDomParser::file_get_html($source_url);
                         $job_desc = $job_details->find('div[id=app_body]',0)->find('div[id=content]',0)->innertext;
                         $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                         $job_desc = addslashes($job_desc);

                         $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Jobandtalent",
                                        "website" => "https://www.jobandtalent.com/join-us#open_positions",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                         
                 }                 
             }
            
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
