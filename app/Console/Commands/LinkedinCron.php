<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class LinkedinCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'linkedin:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'USA' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $pagination = ceil(21270000 / 25);
             // dd($pagination);
             for ($i=0; $i < $pagination ; $i++) 
             { 
                 //print_r($i.",");
                 $s = $i+1;
                 $p = $i*25;

                 $opts = [
                            "http" => [
                                        'method'=>"GET",
                                        'header'=>"Accept-language: en\r\n" .
                                                  "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                                                  "User-Agent: *" // i.e. An iPad
                            ]
                        ];
                 $context  = stream_context_create($opts);

                 $html_data = HtmlDomParser::file_get_html('https://www.linkedin.com/jobs/search?location=Worldwide&pageNum='.$i.'&locationId=OTHERS.worldwide&position=1&trk=jobs_jserp_pagination_'.$s.'&start='.$p.'&count=25&sortBy=DD',false,$context);
                 $job_list = $html_data->find('div[class=jobs-search-content__grid jobs-search-content__two-pane-grid]',0)->find('div[class=jobs-search-content__results-rail]',0)->find('div[class=jobs-search-content__results-scrollable]',0)->find('ul',0)->find('li');

                 foreach ($job_list as $key => $jobs) 
                 {
                     $postdates = $jobs->find('a',0)->find('div[class=listed-job-posting__content jobs-search-result-item__content]',0)->find('div[class=listed-job-posting__flavors]',0)->find('span[class=listed-job-posting__flavor posted-time-ago__text]',0)->innertext;
                     if(trim($postdates) == 'Just now')
                     {
                        //echo "true";
                        $posted_date = date('Y-m-d'); 
                       // dd($posted_date);
                     }
                     else
                     {
                        $postdates_exp = explode(" ago", $postdates);
                        if($postdates_exp[0] == '1 day')
                        {
                             $param = '-'.$postdates_exp[0];
                             
                        }
                        else
                        {
                             $param = '+'.$postdates_exp[0];
                             
                        }
                        $posted_date = date('Y-m-d', strtotime($param));
                        //$posted_date = date('Y-m-d', strtotime($postdates_exp[0])); 
                     }
                     if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                     {
                         $source_url = $jobs->find('meta',1)->content;
                         //print_r($source_url.",");
                         $jobid_exp = explode("/", $source_url);
                         $jobid_exps = explode("-", $jobid_exp[5]);
                         $jobid = explode("?", end($jobid_exps));
                         $job_id = $jobid[0];
                         //print_r($job_id.",");
                         $job_title = $jobs->find('a',0)->find('div[class=listed-job-posting__content jobs-search-result-item__content]',0)->find('h3[class=listed-job-posting__title]',0)->innertext;
                         $job_title = trim($job_title);
                         $countrys = $jobs->find('a',0)->find('div[class=listed-job-posting__content jobs-search-result-item__content]',0)->find('p[class=listed-job-posting__location]',0)->innertext;
                         $countrys_exp = explode(",", $countrys);
                         if(count( $countrys_exp ) == '3')
                         {
                            if(strlen(trim($countrys_exp[2])) == '2')
                            {
                               $country = $countries[trim($countrys_exp[2])];
                            }
                            else
                            {
                               $country = trim($countrys_exp[2]);
                            }
                         }
                         else
                         {
                            if(count( $countrys_exp ) == '2')
                            {
                                if(strlen(trim($countrys_exp[1])) == '2')
                                {
                                   $country = $countries[trim($countrys_exp[1])];
                                }
                                else
                                {
                                   $country = trim($countrys_exp[1]);
                                }
                            }
                            else
                            {
                                $country = '';
                            }
                         }
                         $opts = [
                            "http" => [
                                        'method'=>"GET",
                                        'header'=>"Accept-language: en\r\n" .
                                                  "Cookie: foo=bar\r\n" .  // check function.stream-context-create on php.net
                                                  "User-Agent: *" // i.e. An iPad
                            ]
                         ];
                         $context  = stream_context_create($opts);
                         $jobdesc = HtmlDomParser::file_get_html($source_url,false,$context);
                         $desc = $jobdesc->find('section[class=two-column-content]',0)->find('div[class=two-column-content__left-column]',0)->find('section[class=description]',0)->innertext;
                         $job_desc = preg_replace('/\s+/', ' ', $desc);
                         $job_desc = addslashes($job_desc);

                         $category = $jobdesc->find('section[class=two-column-content]',0)->find('div[class=two-column-content__left-column]',0)->find('section[class=description]',0)->find('ul[class=job-criteria__list description__list]',0)->find('li[class=job-criteria__item description__item]',2)->find('span[class=job-criteria__text description__text job-criteria__text--criteria description__text--criteria]',0)->innertext;
                         $category = trim($category);
                         
                         $row = Job::where('job_id', $job_id)->count();  
                         if($row == 0)
                         {
                                  $insert_data = [
                                    "company" => "Linkedin",
                                    "website" => "https://www.linkedin.com/jobs",
                                    "job_title" => $job_title,
                                    "posted_on"=> $posted_date,
                                    "category" => $category,
                                    "country" => $country,
                                    "description" => $job_desc,
                                    "job_id" => $job_id,
                                    "reference_id" => '',
                                    "contact_name"=>'',
                                    "contact_email"=>'',
                                    "contact_phone"=>'',
                                    "source_url" => $source_url,
                                    "experience_from" => 0,
                                    "experience_to" => 0,
                                    "job_type"=>1,
                                    "points"=>0,
                                    "keywords"=>'',
                                    "keyword_ids"=>'',
                                    "keyword_points"=>'',
                                    "rating_types"=>'',
                                    "rating_points"=>'',
                                    "status"=>0,
                                    "created_at"=>date("Y-m-d H:i:s"),
                                    "updated_at"=>date("Y-m-d H:i:s")                                       
                                  ]; 
                                //print_r($insert_data);
                                Job::insert($insert_data);                
                         }

                     }
                     
                 }

                 
             }            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
