<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class GskCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'gsk:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $countries = [
                'Algeria'=>'DZA',
                "Cote D'Ivoire"=>'CIV',
                'Egypt'=>'EGY',
                'Ethiopia'=>'ETH',
                'Ghana'=>'GHA',
                'Kenya'=>'KEN',
                'Mauritius'=>'MUS',
                'Morocco'=>'MAR',
                'Nigeria'=>'NGA',
                'South Africa'=>'ZAF',
                'Tanzania'=>'tza',
                'Tunisia'=>'TUN',
                'Bahrain'=>'BHR',
                'Bangladesh'=>'BGD',
                'Cambodia'=>'KHM',
                'India'=>'IND',
                'Indonesia'=>'IDN',
                'Israel'=>'ISR',
                'Japan'=>'JPN',
                'Kazakhstan'=>'KAZ',
                'Kuwait'=>'KWT',
                'Lebanon'=>'LBN',
                'Malaysia'=>'MYS',
                'Myanmar'=>'MMR',
                'Oman'=>'OMN',
                'Pakistan'=>'PAK',
                'Philippines'=>'PHL',
                'Qatar'=>'QAT',
                'Saudi Arabia'=>'SAU',
                'Singapore'=>'SGP',
                'South Korea'=>'KOR',
                'Sri Lanka'=>'LKA',
                'Taiwan'=>'TWN',
                'Thailand'=>'THA',
                'Turkey'=>'TUR',
                'United Arab Emirates'=>'ARE',
                'Uzbekistan'=>'UZB',
                'Vietnam'=>'VNM',
                'Australia'=>'AUS',
                'New Zealand'=>'NZL',
                'Albania'=>'ALB',
                'Austria'=>'AUT',
                'Belarus'=>'BLR',
                'Belgium'=>'BEL',
                'Bosnia and Herzegovina'=>'BIH',
                'Bulgaria'=>'BGR',
                'Croatia'=>'HRV',
                'Cyprus'=>'CYP',
                'Czech Republic'=>'CZE',
                'Denmark'=>'DNK',
                'Estonia'=>'EST',
                'Finland'=>'FIN',
                'France'=>'FRA',
                'Georgia'=>'GEO',
                'Germany'=>'DEU',
                'Greece'=>'GRC',
                'Hungary'=>'HUN',
                'Ireland'=>'IRL',
                'Italy'=>'ITA',
                'Latvia'=>'LVA',
                'Lithuania'=>'LTU',
                'Luxembourg'=>'LUX',
                'Macedonia'=>'LUX',
                'Malta'=>'MLT',
                'Netherlands'=>'NLD',
                'Norway'=>'NOR',
                'Poland'=>'POL',
                'Portugal'=>'PRT',
                'Romania'=>'ROU',
                'Russian Federation'=>'RUS',
                'Serbia'=>'SRB',
                'Slovakia'=>'SVK',
                'Slovenia'=>'SVN',
                'Spain'=>'ESP',
                'Sweden'=>'SWE',
                'Switzerland'=>'CHE',
                'Ukraine'=>'UKR',
                'United Kingdom'=>'GBR',
                'Canada'=>'CAN',
                'Mexico'=>'MEX',
                'United States'=>'USA',
                'Argentina'=>'ARG',
                'Brazil'=>'BRA',
                'Chile'=>'CHL',
                'Colombia'=>'COL',
                'Costa Rica'=>'CRI',
                'Dominican Republic'=>'DOM',
                'Ecuador'=>'ECU',
                'El Salvador'=>'SLV',
                'Guatemala'=>'GTM',
                'Honduras'=>'HND',
                'Jamaica'=>'JAM',
                'Panama'=>'PAN',
                'Peru'=>'PER',
                'Trinidad & Tobago'=>'TTO',
                'Uruguay'=>'URY',
                'Venezuela'=>'VEN'
             ];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($countries as $keys => $cuntry) 
             {
                 $html_data = HtmlDomParser::file_get_html("https://www.gsk.com/en-gb/careers/careers-search/?q=&bu=&r=".$cuntry."&city=&et=&prog=");
                 $p_no = $html_data->find('div[class=careers-page]',0)->find('div[class=careers-filters]',0)->find('h2[class=careers-filters__count]',0)->find('span',0)->innertext;
                 $pagination = ceil($p_no / 10);
                 for ($i=0; $i <$pagination; $i++) 
                 { 
                     $p = $i + 1;
                     $html_datas = HtmlDomParser::file_get_html("https://www.gsk.com/en-gb/careers/careers-search/?q=&bu=&r=".$cuntry."&city=&et=&prog=&p=".$p);
                     $job_data = $html_datas->find('div[class=careers-page]',0)->find('div[class=careers-list]',0)->find('li[class=careers-list__item]');
                     foreach ($job_data as $key => $jobs) 
                     {
                        $postdate = $jobs->find('div[class=careers-list__details]',0)->find('div[class=careers-list__date-posted]',0)->find('p',0)->innertext;
                        $posted_date = date("Y-m-d", strtotime($postdate));
                        if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                        {                       
                            $source_url = $jobs->find('div[class=careers-list__description]',0)->find('div[class=careers-list__action]',0)->find('a',0)->href;
                            $source_url = html_entity_decode($source_url);
                            // /dd($source_url);
                            $job_title = $jobs->find('div[class=careers-list__details]',0)->find('div[class=careers-list__job-title]',0)->find('p',0)->innertext;
                            $job_title = trim($job_title);
                            $country = $keys;
                            $jobdesc = HtmlDomParser::file_get_html($source_url);
                            $desc = $jobdesc->find('div[class=col-xs-12 pf-rwd-jobdetails-body]',0)->find('div[class=row]',0)->find('div[class=col-xs-12]',0)->innertext;
                            $job_desc = preg_replace('/\s+/', ' ', $desc);
                            $job_desc = addslashes($job_desc);

                            $categry = $jobdesc->find('div[class=col-xs-12 pf-rwd-titleFields-MoreOptions]',0)->find('ul[class=pf-rwd-ul-flush]',0)->find('li',3)->find('span',1)->innertext;
                            $category = html_entity_decode(trim($categry));

                            $jobid = $jobdesc->find('div[class=col-xs-12 pf-rwd-titleFields-MoreOptions]',0)->find('ul[class=pf-rwd-ul-flush]',0)->find('li',0)->find('span',1)->innertext;
                            $job_id = trim($jobid);
                            //print_r($job_id.",");

                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Gsk",
                                        "website" => "https://www.gsk.com/en-gb/careers",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                            

                        }
                        
                     }
                 }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
