<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AvanzaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'avanza:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {  
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://avanza-career.workbuster.com/");
             $job_data = $html_data->find('body[id=careerpage]',0)->find('div[class=medium wb-section wb-section-positions wb-section-positions-list wb-block page-block-6387]',0)->find('div[class=wb-block-inner]',0)->find('div[id=items]',0)->find('a');
             foreach ($job_data as $key => $jobs) 
             {
                 $url = $jobs->href;
                 $source_url = "https://avanza-career.workbuster.com".$url;
                 $jobid = explode("/", $url);
                 $jobid_exp = explode("-", $jobid[2]);
                 $job_id = trim($jobid_exp[0]);
                 //print_r($job_id.",");
                 
                 $title = $jobs->find('div[class=item]',0)->find('div[class=head col-md-3]',0)->innertext;
                 $job_title = trim($title);
                 $category = $jobs->find('div[class=tags-container col-md-6]',0)->find('div[class=tags tags-departments col-md-6]',0)->find('span',0)->innertext;
                 $category = trim($category);
                 $enddate = $jobs->find('div[class=date col-md-3]',0)->find('span',0)->innertext;
                 $end_date = trim($end_date = date("Y-m-d", strtotime($enddate)));
                 
                 $jobdesc = HtmlDomParser::file_get_html($source_url);
                 $desc = $jobdesc->find('div[class=medium wb-section position-description page-block-6438]',0)->find('div[class=wb-block-inner]',0)->innertext;
                 $job_desc = preg_replace('/\s+/', ' ', $desc);
                 $job_desc = addslashes($job_desc);
                 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Avanza",
                                        "website" => "https://avanza-career.workbuster.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => '',
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=> $enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }


             }
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
