<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SemconCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'semcon:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             //$end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $countries = [
                 "Brazil"=>"action=teamtailor_jobs&source_lang=br",
                 "China"=>"action=teamtailor_jobs&source_lang=cn",
                 "United kingdom"=>"action=teamtailor_jobs&source_lang=en",
                 "Germany"=>"action=teamtailor_jobs&source_lang=de",
                 "Norway"=>"action=teamtailor_jobs&source_lang=no",
                 "Sweden"=>"action=teamtailor_jobs&source_lang=sv",
                 "India"=>"action=teamtailor_jobs&source_lang=hi",
                 "Hungary"=>"action=teamtailor_jobs&source_lang=hu"
             ];

             foreach ($countries as $key => $cuntry) 
             {
                  $postdata = $cuntry;
                  $ch = curl_init('https://semcon.com/wp-admin/admin-ajax.php');
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
                  ));
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

                  $response = curl_exec($ch);

                  curl_close($ch);
                  $result = json_decode($response, true);
                  
                  for ($i=0; $i < count($result) ; $i++) 
                  { 
                      //print_r($key.",");
                      $source_url = $result['data'][$i]['link'];
                      $jobid_exp = explode("/", $source_url);
                      $job_id = $jobid_exp[5];
                      //print_r($job_id.",");
                      $job_title = $result['data'][$i]['name'];
                      $job_title = trim($job_title);
                      $country = $key;
                      $category = $result['data'][$i]['role'];
                      $category = trim($category);
                      if($result['data'][$i]['end_date'] == "")
                      {
                         $enddate = Null;
                      }
                      else
                      {
                         $enddate = trim($result['data'][$i]['end_date']);
                         $enddate = date("Y-m-d", strtotime($enddate));
                      }

                      $jobdesc = HtmlDomParser::file_get_html($source_url);
                      $desc = $jobdesc->find('section[class=o-section]',0)->find('div[class=o-grid-container]',0)->innertext;
                      $job_desc = preg_replace('/\s+/', ' ', $desc);
                      $job_desc = addslashes($job_desc);
                      
                      $row = Job::where('job_id', $job_id)->count();  
                       if($row == 0)
                       {
                              $insert_data = [
                                "company" => "Semcon",
                                "website" => "https://semcon.com/join-us/jobs/",
                                "job_title" => $job_title,
                                "posted_on"=> Null,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "close_on"=>$enddate,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
                  }

             }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
