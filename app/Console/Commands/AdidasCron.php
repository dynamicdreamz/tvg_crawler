<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AdidasCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'adidas:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $current_date = date("Y-m-d");

            $pagination = 2;


            for($i=0; $i <= $pagination; $i++) {
                $ch = curl_init('https://careers.adidas-group.com/jobs?brand=&team=&type=&keywords=&location=[]&sort=&locale=en&offset=20');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'x-requested-with: XMLHttpRequest'
                ));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $response = curl_exec($ch);
                if (curl_error($ch)) {
                    $error_msg = curl_errno($ch);
                    echo $error_msg;
                }
                curl_close($ch);
                $result = json_decode($response, true);
       

                foreach ($result['jobs'] as $key=>$job) {

                    $dom = HtmlDomParser::file_get_html('https://new.abb.com/jobs/details'.$job['Url']);
                    $description = '';
                    if($dom){
                        $description = $dom->find('div[class=pf-rwd-jobdetails-body]',0);
                    }
                    $country = $job['JobLocation']['AddressCountry'];

                    $job_title = $job['Title'];
                    $category = $job['FunctionalArea']['Name'];
                    $job_description = preg_replace('/\s+/', ' ', $description);
                    $job_id = '';
                    $job_url = 'https://new.abb.com/jobs/details'.$job['Url'];
                    $reference_id = $job['ExternalCode'];
                    $exp = $job['HierarchyLevel']['Name'];
                    if (strpos($exp, 'Internship') !== false) {
                        $job_type = 3;
                    }elseif (strpos($exp, 'Part Time') !== false){
                        $job_type = 2;
                    }else{
                        $job_type = 1;
                    }


                    $row = Job::where('job_id', $job_id)->count();
                    if($row == 0)
                    {
                        $insert_data = [
                            "company" => "Abb",
                            "website" => "https://www.abb.com",
                            "job_title" => $job_title,
                            "posted_on"=> "1970-01-01",
                            "category" => $category,
                            "country" => $country,
                            "description" => $job_description,
                            "job_id" => $job_id,
                            "reference_id" => $reference_id,
                            "contact_name"=>'',
                            "contact_email"=>'',
                            "contact_phone"=>'',
                            "source_url" => $job_url,
                            "close_on" => "",
                            "experience_from" => 0,
                            "experience_to" => 0,
                            "job_type"=>$job_type,
                            "points"=>0,
                            "keywords"=>'',
                            "keyword_ids"=>'',
                            "keyword_points"=>'',
                            "rating_types"=>'',
                            "rating_points"=>'',
                            "status"=>0,
                            "created_at"=>date("Y-m-d H:i:s"),
                            "updated_at"=>date("Y-m-d H:i:s")
                        ];
                        //print_r($insert_data);
                        Job::insert($insert_data);
                    }

                }

            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
