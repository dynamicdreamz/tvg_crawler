<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class CherryCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'cherry:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    function strip_tags_content($text)
    {

        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();

            $html_data = HtmlDomParser::file_get_html("https://cherry.se/karriar/");
            $jobs_data = $html_data->find('div[class=et_pb_row_1]', 0)->find('div[class=et_pb_column]');
            $jp = count($jobs_data) - 1;
            $p = 0;
            $j = 0;

            for ($i = 0; $i < $jp; $i++) {

                $job_list = $jobs_data[$i]->find('div[class=et_pb_module]');


                foreach ($job_list as $key => $jobs) {
                    $job_id = "82761" . $p . $j;

                    $job_title = $jobs->find('div[class=et_pb_text_inner]', 0)->find('strong', 0)->innertext;
                    $job_title = explode("<br />", $job_title);
                    if (is_array($job_title)) {
                        $job_title = trim($job_title[0]);
                    } else {
                        $job_title = "";
                    }

                    $desc = $jobs->find('div[class=et_pb_text_inner]', 0)->innertext;
                    $desc = self::strip_tags_content($desc);
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);


                    $row = Job::where('job_id', $job_id)->count();
                    if ($row == 0) {
                        $insert_data = [
                            "company" => "Cherry",
                            "website" => "http://cherryspelgladje.se/karriar/",
                            "job_title" => $job_title,
                            "posted_on" => Null,
                            "category" => '',
                            "country" => '',
                            "description" => $job_desc,
                            "job_id" => $job_id,
                            "reference_id" => '',
                            "contact_name" => '',
                            "contact_email" => '',
                            "contact_phone" => '',
                            "source_url" => 'http://cherryspelgladje.se/karriar/',
                            "experience_from" => 0,
                            "experience_to" => 0,
                            "job_type" => 1,
                            "points" => 0,
                            "keywords" => '',
                            "keyword_ids" => '',
                            "keyword_points" => '',
                            "rating_types" => '',
                            "rating_points" => '',
                            "status" => 0,
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ];
                        //print_r($insert_data);
                        Job::insert($insert_data);
                    }
                    $j++;

                }
                $p++;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
