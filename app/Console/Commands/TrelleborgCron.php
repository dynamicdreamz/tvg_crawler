<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class TrelleborgCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'trelleborg:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();

            $html_data = HtmlDomParser::file_get_html("https://ext.workbuster.se/index.php?site=trelleborg&lng=2&lngForIndex=2&ref=https://www.trelleborg.com/en/career&createGeneralCookie=true&domainUrl=https://www.trelleborg.com&topurl=https://www.trelleborg.com/en/career/vacancies");
            $job_data = $html_data->find('div[id=rightOfFilter]', 0)->find('div[id=items]', 0)->find('div[class=item]');

            foreach ($job_data as $key => $jobs) {

                $postdate = $jobs->find('div[class=date]', 0)->find('span[class=published_homepage_date]', 0)->innertext;
                $posted_date = date('Y-m-d', strtotime($postdate));
                if (strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date)) {
                    $url = $jobs->find('div[class=head]', 0)->find('a', 0)->href;
                    $source_url = html_entity_decode("https://ext.workbuster.se/" . $url);

                    $jobid = explode("aguid=", $url);
                    $jobid = explode("&amp;", $jobid[1]);
                    $job_id = $jobid[0];

                    $job_title = $jobs->find('div[class=head]', 0)->find('a', 0)->innertext;
                    $job_title = html_entity_decode($job_title);

                    $enddate = $jobs->find('div[class=date]', 0)->find('span[class=last_apply_date]', 0);
                    if ($enddate) {
                        $enddate = $enddate->innertext;
                    } else {
                        $enddate = "";
                    }
                    $enddate = (isset($enddate) != "" ? date("Y-m-d", strtotime($enddate)) : "");
                    $category = $jobs->find('div[class=tagscontainer]', 0)->find('div[class=tags_job_family]', 0)->innertext;
                    $category = explode("</strong>", $category);
                    $category = trim($category[1]);

                    $country = $jobs->find('div[class=tagscontainer]', 0)->find('div[class=tags_country]', 0)->innertext;
                    $country = explode("</strong>", $country);
                    $country = trim($country[1]);

                    if ($country == 'US') {
                        $country = "United States";
                    } else {
                        if ($country == 'UK') {
                            $country = "United Kingdom";
                        } else {
                            if ($country == "UK/France") {
                                $country = "France";
                            } else {
                                $country = $country;
                            }
                        }
                    }
                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[id=global_wrapper]', 0)->find('div[class=job_desc]', 0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    $row = Job::where('job_id', $job_id)->count();
                    if ($row == 0) {
                        $insert_data = [
                            "company" => "Trelleborg",
                            "website" => "https://www.trelleborg.com/en/career/vacancies",
                            "job_title" => $job_title,
                            "posted_on" => $posted_date,
                            "category" => $category,
                            "country" => $country,
                            "description" => $job_desc,
                            "job_id" => $job_id,
                            "reference_id" => '',
                            "contact_name" => '',
                            "contact_email" => '',
                            "contact_phone" => '',
                            "source_url" => $source_url,
                            "close_on" => $enddate,
                            "experience_from" => 0,
                            "experience_to" => 0,
                            "job_type" => 1,
                            "points" => 0,
                            "keywords" => '',
                            "keyword_ids" => '',
                            "keyword_points" => '',
                            "rating_types" => '',
                            "rating_points" => '',
                            "status" => 0,
                            "created_at" => date("Y-m-d H:i:s"),
                            "updated_at" => date("Y-m-d H:i:s")
                        ];

                        Job::insert($insert_data);
                    }
                }
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
