<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class ShellCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'shell:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $ActiveFacetID = [
                "Commercial and Retail"=>"62129",
                "Contracting and Procurement"=>"62130",
                "Corporate & External Affairs"=>"62131",
                "Downstream Supply Chain"=>"62133",
                "Discipline Engineering"=>"62132",
                "Finance"=>"62134",
                "Geoscience"=>"62135",
                "Health Safety Environment & Security"=>"62136",
                "Human Resources"=>"62137",
                "Information and Data"=>"62138",
                "Information Technology"=>"62139",
                "Internship Program"=>"62662",
                "Legal"=>"62140",
                "Logistics"=>"62141",
                "Maintenance, Reliability and Turnarounds"=>"62142",
                "Maritime"=>"62143",
                "Petroleum Engineering"=>"62534",
                "Process Engineering"=>"62144",
                "Production & Manufacturing Downstream"=>"62145",
                "Production Engineering Upstream"=>"62146",
                "Project Engineering"=>"62147",
                "Real Estate"=>"62148",
                "Research & Development"=>"62149",
                "Trading"=>"62150"
             ];

             $categorirs = [
                "Commercial and Retail"=>"Commercial+and+Retail",
                "Contracting and Procurement"=>"Contracting+and+Procurement",
                "Corporate & External Affairs"=>"Corporate+%26+External+Affairs",
                "Downstream Supply Chain"=>"Downstream+Supply+Chain",
                "Discipline Engineering"=>"Discipline+Engineering",
                "Finance"=>"Finance",
                "Geoscience"=>"Geoscience",
                "Health Safety Environment & Security"=>"Health+Safety+Environment+%26+Security",
                "Human Resources"=>"Human+Resources",
                "Information and Data"=>"Information+and+Data",
                "Information Technology"=>"Information+Technology",
                "Internship Program"=>"Internship+Program",
                "Legal"=>"Legal",
                "Logistics"=>"Logistics",
                "Maintenance, Reliability and Turnarounds"=>"Maintenance%2C+Reliability+and+Turnarounds",
                "Maritime"=>"Maritime",
                "Petroleum Engineering"=>"Petroleum+Engineering",
                "Process Engineering"=>"Process+Engineering",
                "Production & Manufacturing Downstream"=>"Production+%26+Manufacturing+Downstream",
                "Production Engineering Upstream"=>"Production+Engineering+Upstream",
                "Project Engineering"=>"Project+Engineering",
                "Real Estate"=>"Real+Estate",
                "Research & Development"=>"Research+%26+Development",
                "Trading"=>"Trading"
             ];

             $countries = array("Argentina"=>"Argentina","Australia"=>"Australia","Brazil"=>"Brazil","Canada"=>"Canada","China"=>"China","Germany"=>"Germany","India"=>"India","Indonesia"=>"Indonesia","Japan"=>"Japan","Kazakhstan"=>"Kazakhstan","Kuwait"=>"Kuwait","Malaysia"=>"Malaysia","Netherlands"=>"Netherlands","Norway"=>"Norway","Oman"=>"Oman","Pakistan"=>"Pakistan","Philippines"=>"Philippines","Poland"=>"Poland","Qatar"=>"Qatar","Singapore"=>"Singapore","South Africa"=>"South+Africa","Tanzania"=>"Tanzania","Thailand"=>"Thailand","Trinidad and Tobago"=>"Trinidad+and+Tobago","Tunisia"=>"Tunisia","Turkey"=>"Turkey","United Kingdom"=>"United+Kingdom","United States"=>"United+States");

             $countries_a_id = array("Argentina"=>"3865483","Australia"=>"2077456","Brazil"=>"3469034","Canada"=>"6251999","China"=>"1814991","Germany"=>"2921044","India"=>"1269750","Indonesia"=>"1643084","Japan"=>"1861060","Kazakhstan"=>"1522867","Kuwait"=>"285570","Malaysia"=>"1733045","Netherlands"=>"2750405","Norway"=>"3144096","Oman"=>"286963","Pakistan"=>"1168579","Philippines"=>"1694008","Poland"=>"798544","Qatar"=>"289688","Singapore"=>"1880251","South Africa"=>"953987","Tanzania"=>"149590","Thailand"=>"1605651","Trinidad and Tobago"=>"3573591","Tunisia"=>"2464461","Turkey"=>"298795","United Kingdom"=>"2635167","United States"=>"6252001");
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($categorirs as $cat_key => $cate) 
             {
                 //print_r($cat_key.",");
                 $ActiveFacetIDs = $ActiveFacetID[$cat_key];
                 // /dd($ActiveFacetIDs);
                 foreach ($countries as $cuntry_key => $cuntry) 
                 {
                    //print_r($cuntry_key.","); 
                    $countries_a_ids = $countries_a_id[$cuntry_key];
                    $html_data = HtmlDomParser::file_get_html("https://jobs.shell.com/search-jobs/results?ActiveFacetID=".$ActiveFacetIDs."&CurrentPage=1&RecordsPerPage=15&Distance=50&RadiusUnitType=0&Keywords=&Location=&Latitude=&Longitude=&ShowRadius=False&CustomFacetName=&FacetTerm=&FacetType=0&FacetFilters%5B0%5D.ID=".$ActiveFacetIDs."&FacetFilters%5B0%5D.FacetType=1&FacetFilters%5B0%5D.Count=92&FacetFilters%5B0%5D.Display=".$cate."&FacetFilters%5B0%5D.IsApplied=true&FacetFilters%5B0%5D.FieldName=&FacetFilters%5B1%5D.ID=".$countries_a_ids."&FacetFilters%5B1%5D.FacetType=2&FacetFilters%5B1%5D.Count=1&FacetFilters%5B1%5D.Display=".$cuntry."&FacetFilters%5B1%5D.IsApplied=true&FacetFilters%5B1%5D.FieldName=&SearchResultsModuleName=Search+Results&SearchFiltersModuleName=Search+Filters&SortCriteria=0&SortDirection=0&SearchType=5&CategoryFacetTerm=&CategoryFacetType=&LocationFacetTerm=&LocationFacetType=&KeywordType=&LocationType=&LocationPath=&OrganizationIds=&PostalCode=&fc=&fl=&fcf=&afc=&afl=&afcf=");
                    $job_data = json_decode($html_data,true);

                    if($job_data['hasJobs'] != false)
                    {

                        $fh = fopen( '/var/www/html/cronhtmlfile/ShellContent.html', 'w' );
                        fclose($fh);

                        $myfile = fopen("/var/www/html/cronhtmlfile/ShellContent.html", "w") or die("Unable to open file!");
                        $txt = $job_data['results'];
                        fwrite($myfile, $txt);
                         
                        $html_data = HtmlDomParser::file_get_html("/var/www/html/cronhtmlfile/ShellContent.html");

                        $job_data = $html_data->find('section[id=search-results]',0)->find('section[id=search-results-list]',0)->find('ul[class=results-list]',0)->find('li');
                        foreach ($job_data as $key => $jobs) 
                        {
                            $url = $jobs->find('a',0)->href;
                            $source_url = "https://jobs.shell.com".$url;
                            $job_id = explode("/", $url);
                            $job_id = end($job_id);
                            //print_r($job_id.",");
                            $job_title = trim($jobs->find('a',0)->find('h3',0)->innertext);
                            $category = $cat_key;
                            $country = $cuntry_key;
                            $jobdesc = HtmlDomParser::file_get_html($source_url);
                            $desc = $jobdesc->find('section[class=job-description]',0)->find('div[class=ats-description]',0)->innertext;
                            $job_desc = preg_replace('/\s+/', ' ', $desc);
                            $job_desc = addslashes($job_desc);

                            $ref_id = $jobdesc->find('section[class=job-description]',0)->find('div[class=job-info__wrapper]',0)->find('span[class=job-id job-info]',0)->find('span[class=job-info__content"]',0)->innertext;
                            $ref_id = trim($ref_id);
                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Shell",
                                        "website" => "https://jobs.shell.com/search-jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => $ref_id,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            } 
                        }
                    }
                    $fh = fopen( '/var/www/html/cronhtmlfile/ShellContent.html', 'w' );
                    fclose($fh);

                 }
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
