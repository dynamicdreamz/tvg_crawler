<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class MaerskCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'maersk:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(1094 / 20);
             for ($i=0; $i < $pagination; $i++) 
             {  
                $p = $i+1;
                $html_data = HtmlDomParser::file_get_html("https://careers.maersk.com/api/vacancies/getvacancies?search=&offset=".$i."&language=EN&region=&company=&category=&country=&searchInput=&offset=".$p."&vacanciesPage=true");
                $job_data = json_decode($html_data,true);

                foreach ($job_data['results'] as $jobs) 
                {
                   // dd($jobs);
                    $postdate = $jobs['Posted'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $source_url = $jobs['Url'];
                        $job_id = explode("?id=", $source_url);
                        $job_id = trim($job_id[1]);
                        $ref_id = trim($job_id[1]);
                        $job_title = trim($jobs['Title']);
                        $categry = trim($jobs['Category']);
                        $categry_exp = explode("/", $categry);
                        $category = $categry_exp[0];
                        //print_r($category.",");
                        $country = trim($jobs['Country']);
                        if($country == 'USA')
                        {
                           $country = "United States";
                        }
                        else
                        {
                          if($country == 'Utd.Arab Emir.')
                          {
                              $country = "United Arab Emirates";
                          }
                          else
                          {
                              $country = $country;
                          }
                        }

                        $jobdesc = file_get_contents('https://jobsearch.maersk.com/sap/opu/odata/SAP/ZEREC_CUI_SEARCH_SRV/PostingInstanceSet?$filter=Details/ExternalCode%20eq%20%27'.$job_id.'%27&$format=json');
                        $jobdesc = json_decode($jobdesc, true);
                        $desc = $jobdesc['d']['results'][0]['GeneralInfo']."\n".$jobdesc['d']['results'][0]['Requirements']."\n".$jobdesc['d']['results'][0]['AdditionalInfo'];
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Maersk",
                                        "website" => "https://www.maersk.com/careers/vacancies",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => $ref_id,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }

                        
                    }
                    
                }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
