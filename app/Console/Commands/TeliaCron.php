<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class TeliaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'telia:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(119 / 10);
             for ($i=1; $i < $pagination; $i++) 
             {  
                //$p = $i*10;
                $html_data = HtmlDomParser::file_get_html("https://www.teliacompany.com/webservices/contentservice.svc/GetWorkdayOpenPositionsList?pageId=1379&lang=en&paging=".$i."&pagesize=10");
                $job_data = json_decode($html_data,true);

                foreach ($job_data['OpenPositionList'] as $jobs) 
                {
                   // dd($jobs);
                    $postdate = $jobs['PublishDate'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    //dd($posted_date);
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = $jobs['Id'];
                        //print_r($job_id.",");
                        $job_title = html_entity_decode($jobs['LinkText']);
                        $job_title = trim($job_title);
                        $category = trim($jobs['Position']);
                        $cuntry_expl = explode("\r\n", $jobs['Country']);
                        $country = trim($cuntry_expl[0]);
                        //print_r($country.",");
                        $url = trim($jobs['Link']);
                        $source_url = "https://www.teliacompany.com".$url;
                        $enddate = $jobs['ClosingDate'];
                        $enddate = date("Y-m-d", strtotime($enddate));
                        if($job_id == '64659')
                        {
                            $job_desc = "";
                        }
                        else
                        {
                            $jobdesc = HtmlDomParser::file_get_html($source_url);
                            $desc = $jobdesc->find('div[class=Page__Content__Wrapper Job__Post__Wrapper]',0)->find('div[class=Page__Main__Content]',0)->find('div[class=openPositionBodyText]',0)->innertext;

                            $job_desc = preg_replace('/\s+/', ' ', $desc);
                            $job_desc = addslashes($job_desc);
                        }
                       
                        
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Telia",
                                        "website" => "https://www.teliacompany.com/en/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=> $enddate, 
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                    }
                }
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
