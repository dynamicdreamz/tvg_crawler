<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AlphasightsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    public $date_IST;
    protected $signature = 'alphasights:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
                $country_by_city = [
                    "Dubai"=>"United Arab Emirates",
                    "Hamburg"=>"Germany",
                    "Hong Kong"=>"Hong Kong",
                    "London"=>"United Kingdom",
                    "New York"=>"United States",
                    "San Francisco"=>"United States",
                    "Seoul"=>"South Korea",
                    "Tokyo"=>"Japan",
                    "Shanghai"=>"China"
                ];

        	    $dom = new HtmlDomParser();
                $str =HtmlDomParser::file_get_html('https://boards.greenhouse.io/embed/job_board?for=alphasights&b=https://www.alphasights.com/careers/open-positions');
                $jobs_data = $str->find('section[class=level-0]');
                foreach ($jobs_data as $key => $cat) 
                {
                    $category =  $cat->find('h2', 0)->innertext();

                    $jobs = $cat->find('div[class=opening]');
                    foreach ($jobs as $k=>$job) {
                        $job_details_url = $job->find('a', 0)->href;
                        $ref_id = explode('=', $job_details_url);
                        $job_id = $ref_id[1];
                       //-- print_r($job_id.",");
                        $job_details_url = "https://boards.greenhouse.io/embed/job_app?for=alphasights&token=" . $job_id . "&b=https://www.alphasights.com/careers/open-positions";
                        $job_details = HtmlDomParser::file_get_html($job_details_url);
                        $description = $job_details->find('div[id=content]', 0)->innertext();
                        $description = preg_replace('/\s+/', ' ', $description);
                        $description = addslashes($description);

                        $country = $job->find('span[class=location]', 0)->innertext();
                        $country = explode(", ", $country);
                        if(count($country) == 1)
                        {
                          $country = $country_by_city[trim($country[0])];
                        }
                        else
                        {
                           $country = $country_by_city[trim($country[0])];
                        }

                        $job_title = $job->find('a', 0)->innertext();

                        $row = Job::where('job_id', $job_id)->count();  
                        if($row == 0)
                        {
                            $insert_data = [
                                        "company" => "AlphaSights",
                                        "website" => "https://www.alphasights.com",
                                        "job_title" => $job_title,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $description,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $job_details_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                 
                            ];     

                           //print_r($insert_data);   
                           Job::insert($insert_data);         
                        }                        

                    }
                }
                
        
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
