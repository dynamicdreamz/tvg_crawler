<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class LorealCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'loreal:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             // $pagination = ceil(2634 / 10);
             $postdata = '{"lang":"en_global","deviceType":"desktop","country":"global","ddoKey":"refineSearch","sortBy":"","subsearch":"","from":0,"jobs":true,"counts":true,"all_fields":["category","subFunction","positionType","country","location","brands"],"pageName":"search-results","size":1690,"clearAll":true,"jdsource":"facets","location":"","isSliderEnable":false,"keywords":"","global":true,"selected_fields":{}}';
                //dd($postdata);
             $ch = curl_init('https://careers.loreal.com/widgets');
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'content-type: application/json'
                ));
             curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

             $response = curl_exec($ch);

             curl_close($ch);
             $result = json_decode($response, true);
             $job_data = $result['refineSearch']['data']['jobs'];
             foreach ($job_data as $key => $job) 
             {
                 $posted_date = date('Y-m-d', strtotime($job['postedDate']));
                 if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                 {
                     $job_id = $job['jobId'];
                     //print_r($job_id.",");
                     $posted_on = $posted_date;
                     // print_r($posted_on.",");
                     $category = trim($job['category']);
                     $job_title = trim($job['title']);
                     $job_url_title = str_replace(" ", "-", $job_title);
                     $source_url = "https://careers.loreal.com/global/en/job/".$job_id."/".$job_url_title;
                     // print_r($source_url.",");
                     $country = trim($job['country']);
                     $job_type = trim($job['type']);
                     if($job_type == 'Full - Time')
                     {
                         $job_type = '1';
                     }
                     else
                     {
                         $job_type = '1';
                     } 
                     $row = Job::where('job_id', $job_id)->count();  
                             if($row == 0)
                               {
                                      $insert_data = [
                                        "company" => "Loreal",
                                        "website" => "https://careers.loreal.com/global/en/search-results",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_on,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => '',
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>$job_type,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                                }
                 }
             
             }
             
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
