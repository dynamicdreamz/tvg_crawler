<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AccionaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'acciona:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $dom = new HtmlDomParser();
              $brk = '';
              $current_date = date("Y-m-d");
              $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d")))); 
              $dom = new HtmlDomParser();
              $pagination = 13;
              for ($i = 0; $i <= $pagination; $i++) 
              {
              	 //--print_r($i.",");
                 $dom = HtmlDomParser::file_get_html('http://employmentchannel.acciona.com/vacancies/?page='.$i);
                 $jobs_data = $dom->find('table[class=tablaOfertas]',0)->find('tr');
                 foreach ($jobs_data as $key => $job) {
                 	if($key > 0) {
                 		$job_url = 'http://employmentchannel.acciona.com' . $job->find('td a', 0)->href;
                 		$job_id = explode('=', $job_url);
                 		$job_id = $job_id[1];

                 		$job_title = $job->find('td a', 1)->innertext();
                 		$category = $job->find('td a', 0)->innertext();

                 		$location = $job->find('td', 2)->find('a span', 0)->innertext();
			            $location = explode(',', $location);
			            $country = $location[0];
			            //print_r($country.",");
                 		$detail_page = HtmlDomParser::file_get_html($job_url);
                 		$description = $detail_page->find('div[class=wysiwyg]', 0);

                 		$date = $detail_page->find('span[class=news-time]', 0)->innertext();
                        $posted_date = date('Y-m-d', strtotime(strip_tags($date)));
                        if(strtotime($current_date) == strtotime($posted_date))
                        {
                        	$jobid = $job_id;
                        	//--print_r($jobid.",");
                        	$jobtitle = $job_title;
                        	$jobcategory = $category;
                        	$jobcountry = $country;
                            $jobdesc = preg_replace('/\s+/', ' ', $description);
                        	$jobdesc = addslashes($jobdesc);
                        	$posted_on = $posted_date;
                        	$joburl = $job_url;

                        	$row = Job::where('job_id', $jobid)->count();  
		                    if($row == 0)
		                    {
		                          $insert_data = [
		                            "company" => "Acciona",
		                            "website" => "https://www.acciona.com/",
		                            "job_title" => $jobtitle,
		                            "posted_on"=> $posted_on,
		                            "category" => $jobcategory,
		                            "country" => $jobcountry,
		                            "description" => $jobdesc,
		                            "job_id" => $jobid,
		                            "reference_id" => '',
		                            "contact_name"=>'',
                                    "contact_email"=>'',
                                    "contact_phone"=>'',
		                            "source_url" => $joburl,
                                    "experience_from" => 0,
                                    "experience_to" => 0,
		                            "job_type"=>1,
                                    "points"=>0,
                                    "keywords"=>'',
                                    "keyword_ids"=>'',
                                    "keyword_points"=>'',
                                    "rating_types"=>'',
                                    "rating_points"=>'',
                                    "status"=>0,
                                    "created_at"=>date("Y-m-d H:i:s"),
                                    "updated_at"=>date("Y-m-d H:i:s")             
		                          ]; 
		                        //print_r($insert_data);
		                        Job::insert($insert_data);                
		                    }

                        }
                        else
                        {
                             $brk = "error";
                        }
                 	}
                 }
                 	
                if($brk == 'error')
                {
                    break;
                }
             }
 
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
