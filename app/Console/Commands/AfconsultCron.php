<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AfconsultCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'afconsult:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $categories = [
                   "5e97b707-834f-49cc-b74c-e17ec0357007"=>"Automation, Electrical and Robotics Engineering",
                   "9050aef7-2262-4b3d-9612-6ee50d7f330e"=>"Buildings Services Systems",
                   "67cdcaf7-4cad-494a-9885-db574da9061f"=>"Computer Aided Engineering (CAE)",
                   "d0f6fd1d-1d45-4a87-97b8-3a4bef324315"=>"Electronics System and Communication technology",
                   "2da20f65-1ed1-4e92-90f4-b4d4ecf0d117"=>"Energy (Power generation, T&D and Renewables)",
                   "6f52e3b5-b995-40f3-bdb2-d191aa42359a"=>"Health, Safety and Environment",
                   "88d07965-6e4b-48f0-a007-4f7abcc1228f"=>"IT and Industrial IT",
                   "76594f2e-b148-4558-afba-83397becdf51"=>"Lighting, Acoustics and Experience Design",
                   "6ae4269c-e637-4f46-87df-9d3886bdd82a"=>"Mechanical Engineering (Product and Machine Design)",
                   "d07c1701-d549-46b6-9e74-ac6d03da231b"=>"Plant Engineering and Piping Design",
                   "a8ecf975-12d6-4ff4-949a-d4f3b6ae4532"=>"Process and Production Engineering",
                   "c672cace-b5d1-4764-ab06-2120534d1175"=>"Project Management and Management",
                   "3ca2d437-0f81-47d6-b5ae-ba31d1ac55e8"=>"Quality Assurance, Test and Validation",
                   "5c8e2d47-dfca-4e3b-b58d-7f26bee29f1a"=>"Software Engineering and Test",
                   "cfc796a1-feed-48be-916b-e07afe4aba0b"=>"Supply Chain, Procurement and Technical Documentation",
                   "51b99940-5101-4e95-bb13-53039e158c2a"=>"Urban Planning, Civil and Structural Engineering",
              ];
              $countries = [
                      "2da2cc3d-03a2-e411-9759-0050568f2054"=>"Denmark",
                      "3ca2cc3d-03a2-e411-9759-0050568f2054"=>"Finland",
                      "a0a2cc3d-03a2-e411-9759-0050568f2054"=>"Poland",
                      "c3a2cc3d-03a2-e411-9759-0050568f2054"=>"Sweden",
                      "c4a2cc3d-03a2-e411-9759-0050568f2054"=>"Switzerland"
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($categories as $key => $cate) 
             {
                 foreach ($countries as $keys => $cuntry) 
                 {
                     $html_data = HtmlDomParser::file_get_html("https://www.afconsult.com/en/join-us/available-jobs/?SearchText=&Sca=".$key."&Sco=".$keys);
                     $job_data = $html_data->find('div[class=Content-listing Job-list]',0)->find('div[class=row js-sort-container]',0)->find('div[class=block col regular-12 js-sort-item Job-list-row]');
                     foreach ($job_data as $jobs) 
                     {
                        $url = $jobs->find('a',0)->href;
                        $source_url = "https://www.afconsult.com".$url;
                        $jobid_exp = explode("/", $url);
                        $jobid_exp = explode("-", $jobid_exp[4]);
                        $job_id = $jobid_exp[0];
                        print_r($job_id.",");
                        $job_title = trim($jobs->find('a',0)->find('div[class=Job-list-item Job-list-itemTitle col regular-6]',0)->innertext);
                        $job_title = html_entity_decode($job_title);                        
                        $country = $cuntry;
                        $category = $cate;
                        $jobdesc = HtmlDomParser::file_get_html($source_url);
                        $desc= $jobdesc->find('div[class=u-container]',0)->find('div[id=mainContent]',0)->find('section',3)->find('article',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);
                        
                        $enddate = trim($jobs->find('a',0)->find('div[class=Job-list-item Job-list-itemDate col regular-3]',0)->innertext);
                        $enddate = str_replace("/", "-", $enddate);
                        $enddate = date("Y-m-d", strtotime($enddate));
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Afconsult",
                                        "website" => "https://www.afconsult.com/en/join-us/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }     

                     }

                 }
             }
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
