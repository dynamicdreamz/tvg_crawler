<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class NetflixCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'netflix:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
              $pagination = ceil(527 / 20);
              for ($i=0; $i < 3; $i++) 
              { 
                $p = $i+1; 
                $html_data = HtmlDomParser::file_get_html("https://jobs.netflix.com/api/search?page=".$p);
                $job_data = json_decode($html_data,true);

                foreach ($job_data['records']['postings'] as $key => $jobs) 
                {
                   $postdate = trim($jobs['created_at']);
                   $posted_date = date("Y-m-d", strtotime($postdate));
                   if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                   {
                      $job_id = $jobs['external_id'];
                      //print_r($job_id.",");
                      $source_url = $jobs['url'];
                      $job_title = trim($jobs['text']);
                      $category = trim($jobs['team']);
                      $cuntry_exp = explode(",",trim($jobs['location']));
                      if(count($cuntry_exp) == 2)
                      {
                         $country = trim($cuntry_exp[1]);
                         if($country == "California") 
                         {
                            $country = "United States";
                         }
                         else
                         {
                             $country = $country;
                         }
                      }
                      else
                      { 
                         $country = trim($cuntry_exp[0]);
                         if($country == "California") 
                         {
                            $country = "United States";
                         }
                         else
                         {
                             $country = $country;
                         }
                      }
                      
                      $desc = $jobs['description']."\n".$jobs['search_text'];
                      $job_desc = preg_replace('/\s+/', ' ', $desc);
                      $job_desc = addslashes($job_desc);
                      $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Netflix",
                                        "website" => "https://jobs.netflix.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        


                   }
                   
                }
              }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
