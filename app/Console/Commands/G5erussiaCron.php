<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class G5erussiaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'g5erussia:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $categories = [
                      "programming"=>"Programming",
                      "game_design"=>"Game design",
                      "marketing_pr"=>"Marketing & PR",
                      "art_animation"=>"Art & animation",
                      "graphic"=>"Graphic",
                      "management"=>"Management",
                      "hr"=>"HR",
                      "others"=>""
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             	$html_data = HtmlDomParser::file_get_html("https://jobs.g5e.com/russia/");
             	$job_data = $html_data->find('section[class=vacancies]',0)->find('div[class=sort_box]',0)->find('ul[class=positions]',0)->find('li');
             	$count = count($job_data) - 1 ;
             	for($i = 0; $i < $count ; $i++){

                    $url = $job_data[$i]->find('a',0)->href;
                    $source_url = "https://jobs.g5e.com".$url;
                    $jobid = explode("/", $url);
                    $job_id = end($jobid);
                    //print_r($job_id.",");
                    $job_title = $job_data[$i]->find('a',0)->find('div[class=position_head]',0)->find('div[class=position_title]',0)->find('span',0)->innertext;
                    $job_title = html_entity_decode(trim($job_title));
                    $country = "Russia";
                    $p = 'data-groups';
                    $category = $job_data[$i]->$p;
                    $category = str_replace('["', '', $category);
                    $category = str_replace('"]', '', $category);
                    $category = $categories[$category];
                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[class=description]',0)->find('div[class=descripton_content]',0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);

                    $row = Job::where('job_id', $job_id)->count();
                    if($row == 0)
                    {
                        $insert_data = [
                            "company" => "G5 Entertainment",
                            "website" => "https://jobs.g5e.com",
                            "job_title" => $job_title,
                            "posted_on"=> Null,
                            "category" => $category,
                            "country" => $country,
                            "description" => $job_desc,
                            "job_id" => $job_id,
                            "reference_id" => '',
                            "contact_name"=>'',
                            "contact_email"=>'',
                            "contact_phone"=>'',
                            "source_url" => $source_url,
                            "experience_from" => 0,
                            "experience_to" => 0,
                            "job_type"=>1,
                            "points"=>0,
                            "keywords"=>'',
                            "keyword_ids"=>'',
                            "keyword_points"=>'',
                            "rating_types"=>'',
                            "rating_points"=>'',
                            "status"=>0,
                            "created_at"=>date("Y-m-d H:i:s"),
                            "updated_at"=>date("Y-m-d H:i:s")
                        ];
                        //print_r($insert_data);
                        Job::insert($insert_data);
                    }
                }
             	
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
