<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class VodafoneCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'vodafone:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
                $countries = array(
                        '510'=>'Albania',
                        '520'=>'Czech Republic',
                        '104'=>'Egypt',
                        '965'=>'France',
                        '970'=>'Germany',
                        '130'=>'Ghana',
                        '1135'=>'Greece',
                        '133'=>'Hong Kong',
                        '1219'=>'Hungary',
                        '140'=>'India',
                        '1320'=>'Italy',
                        '347'=>'Japan',
                        '1391'=>'Malta',
                        '379'=>'Mozambique',
                        '390'=>'New Zealand',
                        '443'=>'Peoples Republic of China',
                        '1459'=>'Portugal',
                        '454'=>'Qatar',
                        '1497'=>'Romania',
                        '475'=>'South Africa',
                        '500'=>'Tanzania',
                        '1848'=>'Turkey',
                        '1934'=>'USA',
                        '557'=>'England',
                        '1255'=>'Ireland',
                        '1434'=>'Northern Ireland',
                        '1616'=>'Scotland'
                    );
                $brk = '';
                $current_date = date("Y-m-d");
                $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
                $dom = new HtmlDomParser();

                foreach ($countries as $keys => $value) 
                {
                    //--print_r($keys.",");
                    $doms = HtmlDomParser::file_get_html('https://careers.vodafone.com/jobs?q=&jto=false&page=1&options=,'.$keys);
                    $job_detail = $doms->find('div[class=pagination-wrapper]',0)->find('span[class=pagination-results]',0)->innertext;
                    $job_details_spec = explode(" ", $job_detail);
                    $pagination = ceil($job_details_spec[0] / 12);

                    
                    for ($i=1; $i <= $pagination ; $i++)
                    {
                        //--print_r($i.",");
                        //print_r($keys.",");
                        $dom = HtmlDomParser::file_get_html('https://careers.vodafone.com/jobs?q=&jto=false&page='.$i.'&options=,'.$keys);
                        $job_details = $dom->find('div[class=vacancy-wrapper clearfix]',0)->find('div[class=job-block-wrapper]');
                        //print_r( $job_details );
                        //die;
                        foreach ($job_details as $key => $job) 
                        {
                           $job_spec = $job->find('div[class=job-block]',0);
                           $url = $job_spec->find('div[class=job-title job-block__title-wrapper] a',0)->href;
                           //print_r($url);
                           $job_url = "https://careers.vodafone.com".$job_spec->find('div[class=job-title job-block__title-wrapper] a',0)->href;
                           $url_spec = explode("/", $url);
                            
                           $job_id = explode("-", $url_spec[2]);
                           $job_id = end($job_id);
                           //print_r($job_id.",");

                           $job_title = $job_spec->find('div[class=job-title job-block__title-wrapper] span',0)->innertext;

                           //$country_spec = $job_spec->find('div[class=job-location job-block__location-wrapper]',0)->find('p[class=location-text job-block__location-text]',0)->innertext;                       
                           $country = $value;
                           //print_r($country.",");

                            $content = @file_get_contents($job_url);
                            if (strpos($http_response_header[0], "200")) 
                            { 

                               $myfile = fopen("/opt/lampp/htdocs/tvg/cronhtmlfile/VodafoneContent.html", "w") or die("Unable to open file!");
                               $txt = $content;
                               fwrite($myfile, $txt);

                               //use HtmlDomParser::file_get_html and please change this path when take the online and server
                               $description = HtmlDomParser::file_get_html('/opt/lampp/htdocs/tvg/cronhtmlfile/VodafoneContent.html');
                               if($description != FALSE)
                               {
                                   $job_spec = $description->find('div[class=row dragElement widget container-widget wrapper-widget free_text_wrappers]',0)->find('div[class=col-md-12 container-col ui-droppable]');
                                   $posted_date = $job_spec[0]->find('div[class=cop-widget dynamic-widget date-widget]',0)->find('div',0)->innertext;
                                   $posted_date = explode("</span>", $posted_date);
                                   $posted_date = date('Y-m-d', strtotime($posted_date[1])); 
                                   if(strtotime($current_date) == strtotime($posted_date))
                                   {
                                        $jobid = $job_id;
                                        //--print_r($jobid.",");

                                        $joburl = $job_url;
                                        $jobtitle = $job_title;
                                        $jobcountry = $country;

                                        $job_desc = $description->find('div[class=cop-widget dynamic-widget description-widget]',0)->innertext;
                                        $job_desc = addslashes($job_desc);
                                        $job_desc = preg_replace('/(\v|\s)+/', ' ', $job_desc);

        
                                        $job_ol = $description->find('div[class=col-md-12 container-col ui-droppable]',0)->find('div[class=cop-widget dynamic-widget vacancy-options-widget]',0)->find('ol');
                                        if($job_id != '17735')
                                        {
                                          $job_type = $job_ol[0]->find('li',0)->innertext;
                                          $job_type = explode("</label>", $job_type);
                                          $job_type = preg_replace('/(\v|\s)+/', ' ', $job_type[1]);
                                        }
                                        else
                                        {
                                          $job_type == ' Full Time ';
                                        }

                                        if($job_type == ' Full Time ')
                                        {
                                            $type = '1';    
                                        }
                                        else
                                        {
                                            $type = '2';
                                        }

                                        if($job_id != '17735')
                                        {

                                          $category = $job_ol[0]->find('li',1)->innertext;
                                          $category = explode("</label>", $category);
                                          $category = preg_replace('/(\v|\s)+/', ' ', $category[1]);
                                        }
                                        else
                                        {
                                            $category = '';
                                        }

                                        $job_spec = $description->find('div[class=row dragElement widget container-widget wrapper-widget free_text_wrappers]',0)->find('div[class=col-md-12 container-col ui-droppable]');
                                        //dd(count($job_spec));
                                        $reference_id = $job_spec[0]->find('div[class=cop-widget dynamic-widget text-widget reference]',0)->find('div',0)->innertext;
                                        //dd($reference_id);
                                        //print_r($reference_id.",");

                                        $posted_on = $posted_date;
                                        //--print_r($posted_on.",");

                                        $row = Job::where('job_id', $job_id)->count();
                                        if($row == 0)
                                        {  
                                          $insert_data = [
                                            "company" => "Vodafone",
                                            "website" => "https://www.vodafone.com/",
                                            "job_title" => $jobtitle,
                                            "posted_on"=> $posted_on,
                                            "category" => $category,
                                            "country" => $jobcountry,
                                            "description" => $job_desc,
                                            "job_id" => $jobid,
                                            "reference_id" => $reference_id,
                                            "contact_name"=>'',
                                            "contact_email"=>'',
                                            "contact_phone"=>'',
                                            "source_url" => $joburl,
                                            "experience_from" => 0,
                                            "experience_to" => 0,
                                            "job_type"=>$type,
                                            "points"=>0,
                                            "keywords"=>'',
                                            "keyword_ids"=>'',
                                            "keyword_points"=>'',
                                            "rating_types"=>'',
                                            "rating_points"=>'',
                                            "status"=>0,
                                            "created_at"=>date("Y-m-d H:i:s"),
                                            "updated_at"=>date("Y-m-d H:i:s")                                                       
                                          ];
                                         // print_r($insert_data);

                                          Job::insert($insert_data);
                                        }  


                                   }
                                   else
                                   {
                                        $brk = "error";
                                   }                              
                                }
                                 $fh = fopen( '/opt/lampp/htdocs/tvg/cronhtmlfile/VodafoneContent.html', 'w' );
                                 fclose($fh);
                            } else { 
                              echo "FAILED";
                            } 

                        $fh = fopen( '/opt/lampp/htdocs/tvg/cronhtmlfile/VodafoneContent.html', 'w' );
                        fclose($fh);  
                        if($brk == 'error')
                        {
                            break;
                        }

                    } 
  
                }
            }
         
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
