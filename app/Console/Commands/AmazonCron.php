<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AmazonCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'amazon:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $current_date = date("Y-m-d");

            $pagination = 2;


            for($i=0; $i <= $pagination; $i++) {
                $offset = $i * 10;
                $jobs = file_get_contents('https://www.amazon.jobs/en-gb/search.json?base_query=&city=&country=&county=&facets[]=
location&facets[]=business_category&facets[]=category&facets[]=schedule_type_id&facets[]=
employee_class&facets[]=normalized_location&facets[]=job_function_id&latitude=&loc_group_id=
&loc_query=&longitude=&offset='.$offset.'&query_options=&radius=24km&region=&result_limit=10&schedule_type_id[]=Full-Time&sort=relevant');

                $jobs_data = json_decode($jobs,true);

                for($j=0; $j <= count($jobs_data["jobs"]) -1; $j++) {
                    $posted_date = date('Y-m-d', strtotime($jobs_data["jobs"][$j]["posted_date"]));

                    if (strtotime($current_date) == strtotime($posted_date)) {

                        $country = file_get_contents('https://restcountries.eu/rest/v2/alpha/'.$jobs_data["jobs"][$j]["country_code"]);
                        $country = json_decode($country,true);

                        $description = $jobs_data["jobs"][$j]["description"];


                        $job_title = $jobs_data["jobs"][$j]["title"];
                        $category = $jobs_data["jobs"][$j]["job_category"];
                        $job_description = preg_replace('/\s+/', ' ', $description);
                        $job_id = '';
                        $job_url = 'https://www.amazon.jobs' . $jobs_data["jobs"][$j]["job_path"];
                        $reference_id = $jobs_data["jobs"][$j]["id_icims"];
                        if ($jobs_data["jobs"][$j]["job_schedule_type"] == "full-time") {
                            $job_type = 1;
                        } elseif ($jobs_data["jobs"][$j]["job_schedule_type"] == "part-time") {
                            $job_type = 2;
                        } else {
                            $job_type = 0;
                        }


                        $row = Job::where('job_id', $job_id)->count();
                        if ($row == 0) {
                            $insert_data = [
                                "company" => "Amazon",
                                "website" => "https://www.amazon.com",
                                "job_title" => $job_title,
                                "posted_on" => $posted_date,
                                "category" => $category,
                                "country" => $country["name"],
                                "description" => $job_description,
                                "job_id" => $job_id,
                                "reference_id" => $reference_id,
                                "contact_name" => '',
                                "contact_email" => '',
                                "contact_phone" => '',
                                "source_url" => $job_url,
                                "close_on" => "",
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type" => $job_type,
                                "points" => 0,
                                "keywords" => '',
                                "keyword_ids" => '',
                                "keyword_points" => '',
                                "rating_types" => '',
                                "rating_points" => '',
                                "status" => 0,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s")
                            ];
                            dd($insert_data);
                            Job::insert($insert_data);
                        }

                    }
                }

            }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
