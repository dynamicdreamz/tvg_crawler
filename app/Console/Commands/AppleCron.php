<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AppleCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'apple:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    "AFG" => "Afghanistan",
                    "ALB" => "Albania",
                    "DZA" => "Algeria",
                    "ASM" => "American Samoa",
                    "AND" => "Andorra",
                    "AGO" => "Angola",
                    "AIA" => "Anguilla",
                    "ATA" => "Antarctica",
                    "ATG" => "Antigua and Barbuda",
                    "ARG" => "Argentina",
                    "ARM" => "Armenia",
                    "ABW" => "Aruba",
                    "AUS" => "Australia",
                    "AUT" => "Austria",
                    "AZE" => "Azerbaijan",
                    "BHS" => "Bahamas",
                    "BHR" => "Bahrain",
                    "BGD" => "Bangladesh",
                    "BRB" => "Barbados",
                    "BLR" => "Belarus",
                    "BEL" => "Belgium",
                    "BLZ" => "Belize",
                    "BEN" => "Benin",
                    "BMU" => "Bermuda",
                    "BTN" => "Bhutan",
                    "BOL" => "Bolivia",
                    "BES" => "Bonaire",
                    "BIH" => "Bosnia and Herzegovina",
                    "BWA" => "Botswana",
                    "BVT" => "Bouvet Island",
                    "BRA" => "Brazil",
                    "IOT" => "British Indian Ocean Territory",
                    "VGB" => "British Virgin Islands",
                    "BRN" => "Brunei Darussalam",
                    "BGR" => "Bulgaria",
                    "BFA" => "Burkina Faso",
                    "BDI" => "Burundi",
                    "CPV" => "Cabo Verde",
                    "KHM" => "Cambodia",
                    "CMR" => "Cameroon",
                    "CAN" => "Canada",
                    "CYM" => "Cayman Islands",
                    "CAF" => "Central African Republic",
                    "TCD" => "Chad",
                    "CHL" => "Chile",
                    "CHN" => "China",
                    "HKG" => "Hong Kong",
                    "MAC" => "Macao",
                    "CXR" => "Christmas Island",
                    "CCK" => "Cocos Islands",
                    "COL" => "Colombia",
                    "COM" => "Comoros",
                    "COG" => "Congo",
                    "COK" => "Cook Islands",
                    "CRI" => "Costa Rica",
                    "HRV" => "Croatia",
                    "CUB" => "Cuba",
                    "CUW" => "Curacao",
                    "CYP" => "Cyprus",
                    "CZE" => "Czechia",
                    "CIV" => "Cote d'Ivoire",
                    "PRK" => "Democratic People's Republic of Korea",
                    "COD" => "Democratic Republic of the Congo",
                    "DNK" => "Denmark",
                    "DJI" => "Djibouti",
                    "DMA" => "Dominica",
                    "DOM" => "Dominican Republic",
                    "ECU" => "Ecuador",
                    "EGY" => "Egypt",
                    "SLV" => "El Salvador",
                    "GNQ" => "Equatorial Guinea",
                    "ERI" => "Eritrea",
                    "EST" => "Estonia",
                    "ETH" => "Ethiopia",
                    "FLK" => "Falkland Islands",
                    "FRO" => "Faroe Islands",
                    "FJI" => "Fiji",
                    "FIN" => "Finland",
                    "FRA" => "France",
                    "GUF" => "French Guiana",
                    "PYF" => "French Polynesia",
                    "ATF" => "French Southern Territories",
                    "GAB" => "Gabon",
                    "GMB" => "Gambia",
                    "GEO" => "Georgia",
                    "DEU" => "Germany",
                    "GHA" => "Ghana",
                    "GIB" => "Gibraltar",
                    "GRC" => "Greece",
                    "GRL" => "Greenland",
                    "GRD" => "Grenada",
                    "GLP" => "Guadeloupe",
                    "GUM" => "Guam",
                    "GTM" => "Guatemala",
                    "GGY" => "Guernsey",
                    "GIN" => "Guinea",
                    "GNB" => "Guinea-Bissau",
                    "GUY" => "Guyana",
                    "HTI" => "Haiti",
                    "HMD" => "Heard Island and McDonald Islands",
                    "VAT" => "Holy See",
                    "HND" => "Honduras",
                    "HUN" => "Hungary",
                    "ISL" => "Iceland",
                    "IND" => "India",
                    "IDN" => "Indonesia",
                    "IRN" => "Iran",
                    "IRQ" => "Iraq",
                    "IRL" => "Ireland",
                    "IMN" => "Isle of Man",
                    "ISR" => "Israel",
                    "ITA" => "Italy",
                    "JAM" => "Jamaica",
                    "JPN" => "Japan",
                    "JEY" => "Jersey",
                    "JOR" => "Jordan",
                    "KAZ" => "Kazakhstan",
                    "KEN" => "Kenya",
                    "KIR" => "Kiribati",
                    "KWT" => "Kuwait",
                    "KGZ" => "Kyrgyzstan",
                    "LAO" => "Lao People's Democratic Republic",
                    "LVA" => "Latvia",
                    "LBN" => "Lebanon",
                    "LSO" => "Lesotho",
                    "LBR" => "Liberia",
                    "LBY" => "Libya",
                    "LIE" => "Liechtenstein",
                    "LTU" => "Lithuania",
                    "LUX" => "Luxembourg",
                    "MDG" => "Madagascar",
                    "MWI" => "Malawi",
                    "MYS" => "Malaysia",
                    "MDV" => "Maldives",
                    "MLI" => "Mali",
                    "MLT" => "Malta",
                    "MHL" => "Marshall Islands",
                    "MTQ" => "Martinique",
                    "MRT" => "Mauritania",
                    "MUS" => "Mauritius",
                    "MYT" => "Mayotte",
                    "MEX" => "Mexico",
                    "FSM" => "Micronesia",
                    "MCO" => "Monaco",
                    "MNG" => "Mongolia",
                    "MNE" => "Montenegro",
                    "MSR" => "Montserrat",
                    "MAR" => "Morocco",
                    "MOZ" => "Mozambique",
                    "MMR" => "Myanmar",
                    "NAM" => "Namibia",
                    "NRU" => "Nauru",
                    "NPL" => "Nepal",
                    "NLD" => "Netherlands",
                    "NCL" => "New Caledonia",
                    "NZL" => "New Zealand",
                    "NIC" => "Nicaragua",
                    "NER" => "Niger",
                    "NGA" => "Nigeria",
                    "NIU" => "Niue",
                    "NFK" => "Norfolk Island",
                    "MNP" => "Northern Mariana Islands",
                    "NOR" => "Norway",
                    "OMN" => "Oman",
                    "PAK" => "Pakistan",
                    "PLW" => "Palau",
                    "PAN" => "Panama",
                    "PNG" => "Papua New Guinea",
                    "PRY" => "Paraguay",
                    "PER" => "Peru",
                    "PHL" => "Philippines",
                    "PCN" => "Pitcairn",
                    "POL" => "Poland",
                    "PRT" => "Portugal",
                    "PRI" => "Puerto Rico",
                    "QAT" => "Qatar",
                    "KOR" => "Republic of Korea",
                    "MDA" => "Republic of Moldova",
                    "ROU" => "Romania",
                    "RUS" => "Russian Federation",
                    "RWA" => "Rwanda",
                    "REU" => "Reunion",
                    "BLM" => "Saint Barthelemy",
                    "SHN" => "Saint Helena",
                    "KNA" => "Saint Kitts and Nevis",
                    "LCA" => "Saint Lucia",
                    "MAF" => "Saint Martin",
                    "SPM" => "Saint Pierre and Miquelon",
                    "VCT" => "Saint Vincent and the Grenadines",
                    "WSM" => "Samoa",
                    "SMR" => "San Marino",
                    "STP" => "Sao Tome and Principe",
                    "SAU" => "Saudi Arabia",
                    "SEN" => "Senegal",
                    "SRB" => "Serbia",
                    "SYC" => "Seychelles",
                    "SLE" => "Sierra Leone",
                    "SGP" => "Singapore",
                    "SXM" => "Sint Maarten",
                    "SVK" => "Slovakia",
                    "SVN" => "Slovenia",
                    "SLB" => "Solomon Islands",
                    "SOM" => "Somalia",
                    "ZAF" => "South Africa",
                    "SGS" => "South Georgia and the South Sandwich Islands",
                    "SSD" => "South Sudan",
                    "ESP" => "Spain",
                    "LKA" => "Sri Lanka",
                    "PSE" => "State of Palestine",
                    "SDN" => "Sudan",
                    "SUR" => "Suriname",
                    "SJM" => "Svalbard and Jan Mayen Islands",
                    "SWZ" => "Swaziland",
                    "SWE" => "Sweden",
                    "CHE" => "Switzerland",
                    "SYR" => "Syrian Arab Republic",
                    "TJK" => "Tajikistan",
                    "THA" => "Thailand",
                    "MKD" => "The former Yugoslav Republic of Macedonia",
                    "TLS" => "Timor-Leste",
                    "TGO" => "Togo",
                    "TKL" => "Tokelau",
                    "TON" => "Tonga",
                    "TTO" => "Trinidad and Tobago",
                    "TUN" => "Tunisia",
                    "TUR" => "Turkey",
                    "TKM" => "Turkmenistan",
                    "TCA" => "Turks and Caicos Islands",
                    "TUV" => "Tuvalu",
                    "UGA" => "Uganda",
                    "UKR" => "Ukraine",
                    "ARE" => "United Arab Emirates",
                    "GBR" => "United Kingdom of Great Britain and Northern Ireland",
                    "TZA" => "United Republic of Tanzania",
                    "UMI" => "United States Minor Outlying Islands",
                    "VIR" => "United States Virgin Islands",
                    "USA" => "United States of America",
                    "URY" => "Uruguay",
                    "UZB" => "Uzbekistan",
                    "VUT" => "Vanuatu",
                    "VEN" => "Venezuela",
                    "VNM" => "Viet Nam",
                    "WLF" => "Wallis and Futuna Islands",
                    "ESH" => "Western Sahara",
                    "YEM" => "Yemen",
                    "ZMB" => "Zambia",
                    "ZWE" => "Zimbabwe"];

                  
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $pagination = 197;
             for ($i=1; $i <= $pagination ; $i++) 
             { 
                //print_r($i.",");
                $postdata = '{"query":"","filters":{"range":{"standardWeeklyHours":{"start":null,"end":null}}},"page":'.$i.',"locale":"en-us","sort":"newest"}';
                //dd($postdata);
                $ch = curl_init('https://jobs.apple.com/api/role/search');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'content-type: application/json'
                ));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

                $response = curl_exec($ch);

                curl_close($ch);
                $result = json_decode($response, true);
                
                foreach ($result['searchResults'] as $key=>$job)
                {
                   $posted_date = date('Y-m-d', strtotime($job['postingDate']));
                   if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                   {
                       $job_id = $job['positionId'];
                       //print_r($job_id.",");
                       $job_title = $job['postingTitle'];
                       $posted_on = $posted_date; 
                       $job_description = preg_replace('/\s+/', ' ', $job['jobSummary']);
                       if($job_id == 200024737)
                       {
                         $country = '';
                       }
                       else
                       {
                         if($job_id == 200026808)
                         {
                            $country = '';
                         }
                         else
                         {
                             if($job_id == 200043468)
                             {
                                $country = '';
                             }
                             else
                             {
                               $country = $job['locations'][0]['countryName'];
                             }
                         }
                       }
                       
                       $source_url = "https://jobs.apple.com/en-us/details/".$job_id."/".$job['transformedPostingTitle']."?team=".$job['team']['teamCode'];
                       
                       $cat = HtmlDomParser::file_get_html($source_url);
                       
                       if($job_id == 200024737)
                       {
                         $category = '';
                       }
                       else
                       {
                         if($job_id == 200026808)
                         {
                            $category = '';
                         }
                         else
                         {
                             if($job_id == 200043468)
                             {
                                $category = '';
                             }
                             else
                             {
                                
                               $category = $cat->find('div[class=jd__header--info]',0)->find('div[class=job-team-name]',0)->innertext;
                             } 
                         }
                         
                       }

                       if($job_id == 114438328)
                       {
                          $category = "Apple Retail";
                       }

                       $row = Job::where('job_id', $job_id)->count();  
                       if($row == 0)
                       {
                              $insert_data = [
                                "company" => "Apple",
                                "website" => "https://jobs.apple.com/",
                                "job_title" => $job_title,
                                "posted_on"=> $posted_on,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_description,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
                   }
                   else
                   {
                     $brk = "error";
                   }    

                }
              
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
