<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class ParadoxCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'paradox:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Berkeley"=>"United States",
                      "Delft"=>"Netherlands",
                      "Malmö"=>"Sweden",
                      "Seattle"=>"United States",
                      "Stockholm"=>"Sweden",
                      "Umeå"=>"Sweden",
                   ];
          
                $html_data = HtmlDomParser::file_get_html("https://career.paradoxplaza.com/jobs");
                $job_data = $html_data->find('section[id=section-jobs]',0)->find('div[class=wrapper]',0)->find('div[class=jobs-section-inner]',0)->find('ul[class=jobs]',0)->find('li');
                foreach ($job_data as $key => $jobs)
                {
                    $url = $jobs->find('a',0)->href;
                    $source_url = "https://career.paradoxplaza.com".$url;
                    $jobid = explode("/", $url);
                    $jobid_exp = explode("-", $jobid[2]);
                    $job_id = $jobid_exp[0];
                    //print_r($job_id.",");
                    $job_title = $jobs->find('a',0)->find('span[class=title]',0)->innertext;
                    $job_title = trim($job_title);
                    if($job_id == '236488')
                    {
                        $category = "";
                        $country = "Sweden";
                    }
                    else
                    {
                        if($job_id == '217173')
                        {
                            $category = "";
                            $country = "Sweden";
                        }
                        else
                        {
                            if($job_id == '55981')
                            {
                                $category = "Design";
                                $country = "";
                            }
                            else
                            {
                                $categry = $jobs->find('a',0)->find('span[class=meta]',0)->innertext;
                                $categry_exp = explode("-", trim($categry));
                                $category = html_entity_decode($categry_exp[0]);
                                $country = $countries[trim($categry_exp[1])]; 
                            }
                        }
                    }
                    
                    //print_r($category.",");

                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[class=job]',0)->find('div[class=wrapper]',0)->find('div[class=body u-margin-top--medium u-primary-text-color]',0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    
                     $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Paradoxplaza",
                                        "website" => "https://career.paradoxplaza.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                    
                    

                    
                }
            
                

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
