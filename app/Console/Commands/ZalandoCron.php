<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class ZalandoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'zalando:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Ansbach"=>"Germany",
                      "Berlin"=>"Germany",
                      "Dortmund"=>"Germany",
                      "Dublin (Ireland)"=>"Ireland",
                      "Erfurt"=>"Germany",
                      "Frankfurt am Main"=>"Germany",
                      "Großbeeren"=>"Germany",
                      "Hamburg"=>"Germany",
                      "Hannover"=>"Germany",
                      "Helsinki (Finland)"=>"Finland",
                      "Köln"=>"Germany",
                      "Lahr/Schwarzwald"=>"Germany",
                      "Leipzig"=>"Germany",
                      "Łódź (Poland)"=>"Poland",
                      "Ludwigsfelde"=>"Germany",
                      "Mönchengladbach"=>"Germany",
                      "Münster"=>"Germany",
                      "Nogarole Rocca (Italy)"=>"Italy",
                      "Olsztynek (Poland)"=>"Poland",
                      "Stuttgart"=>"Germany",
                      "Szczecin (Poland)"=>"Poland",
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(683 / 10);
             for ($i=0; $i < $pagination; $i++) 
             {  
                $p = $i*10;
                $html_data = HtmlDomParser::file_get_html("https://jobs.zalando.com/api/jobs/?q=&filters=%7B%7D&offset=".$p."&limit=10");
                $job_data = json_decode($html_data,true);
                foreach ($job_data['data'] as $jobs) 
                {
                   // dd($jobs);
                    $postdate = $jobs['updated_at'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = trim($jobs['id']);
                        //print_r( $job_id .",");
                        $category = trim($jobs['job_categories'][0]);
                        $job_title = trim($jobs['title']);
                        $title = str_replace(" ", "-", $job_title);
                        $title = str_replace("(", "", $title);
                        $title = str_replace("/", "-", $title);
                        $title = str_replace(")", "", $title);
                        $title = str_replace("&-", "", $title);
                        $title = str_replace("---", "-", $title);
                        $title = strtolower($title);
                        $source_url = $job_id."-".$title;
                        $source_url = "https://jobs.zalando.com/en/jobs/".$source_url; 
                        
                        $cuntry =  trim($jobs['offices'][0]);
                        if($cuntry == 'Remote')
                        {
                            $country = "";
                        }
                        else
                        {
                            $country = $countries[$cuntry];
                           
                        }

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Zalando",
                                        "website" => "https://jobs.zalando.com/en/jobs/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => '',
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                        
                    }
                    
                }
             }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
