<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class NordeaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'nordea:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

        	 $countries = [
                   "Denmark"=>"19419",
                   "Estonia"=>"19518",
                   "Finland"=>"19420",
                   "Norway"=>"19422",
                   "Poland"=>"19508",
                   "Sweden"=>"19423"
        	 ];
              
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://www.nordea.com/en/careers/vacant-positions/?cty=0&geo=0&area=0&jobid=&p=1");
             $get_country = $html_data->find('section[class=grid-group]',0)->find('article[class=grid span-24 landmark]',0)->find('div[class=job-search block landmark folded-banner]',0)->find('select[id=job-search-select-cty]',0)->find('option');
             for ($i=1; $i < count($get_country); $i++) 
             { 
             	$cuntry = $get_country[$i]->innertext;
             	if($cuntry != 'Other countries')
             	{
             	    $cuntry_code = $countries[trim($cuntry)];
	             	$html_datas = HtmlDomParser::file_get_html("https://www.nordea.com/en/careers/vacant-positions/?cty=".$cuntry_code."&geo=0&area=0&jobid=&p=1");
	             	$job_no = $html_datas->find('div[class=js-job-listing]',0)->find('div[class=jobs-results]',0)->find('div[class=search-message]',0)->find('p',0)->innertext;
	             	$job_no_exp = explode(" ", $job_no);
	             	$job_nos = $job_no_exp[3];
	                
	                $pagination = ceil($job_nos / 20);
	                for ($j=1; $j < 3; $j++) 
	                { 
	                    $job_datas = HtmlDomParser::file_get_html("https://www.nordea.com/en/careers/vacant-positions/?cty=".$cuntry_code."&geo=0&area=0&jobid=&p=".$j);
	                    $job_data = $job_datas->find('div[class=js-job-listing]',0)->find('table[class=stackrows]',0)->find('tr[class=job-item]');
	                    foreach ($job_data as $key => $jobs) 
	                    {
	                        $url = $jobs->find('a',0)->href;
	                        $source_url = "https://www.nordea.com".$url;
	                        
	                        $job_title = trim($jobs->find('a',0)->innertext);

	                        $jobid_exp = explode("=", $source_url);
	                        $jobid_exps = explode("-", $jobid_exp[1]);
	                        $job_id = trim($jobid_exps[0]); 
	                        //print_r($job_id.",");
	                        
	                        $country = $cuntry;
	                        
	                        $categ = $jobs->find('td',1)->innertext;
	                        $categ_exp = explode(",", $categ);
	                        $category = trim($categ_exp[0]);

	                        $enddate = $jobs->find('td',3)->innertext;
	                        $enddate = date("Y-m-d", strtotime(trim($enddate)));

	                        $jobdesc = HtmlDomParser::file_get_html($source_url);
	                        $desc = $jobdesc->find('div[class=site-borders]',0)->find('div[class=grid-group main-content]',0)->find('section[class=grid-group job-details__section]',0)->find('article[class=grid span-24 span-15--lap-large span-16--desk]',0)->innertext;
	                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                            $job_desc = addslashes($job_desc);

                            $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Nordea",
                                        "website" => "https://www.nordea.com/en/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=> $enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }

	                    }	
	                }
	            }
             	
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
