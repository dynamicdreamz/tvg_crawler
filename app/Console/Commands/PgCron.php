<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class PgCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'pg:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $insert_data = array();
              $brk = '';
              $current_date = date("Y-m-d");
              $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
              $dom = new HtmlDomParser();

              $categories = [
                  "Administrative"=>"Administrative",
                  "Brand%3A+Marketing%2FBrand+Management"=>"Marketing / Brand Management",
                  "Communications"=>"Communications",
                  "Consumer+%26+Market+Knowledge"=>"Consumer & Market Knowledge",
                  "Design"=>"Design",
                  "Engineering"=>"Engineering",
                  "Finance+%26+Accounting"=>"Finance & Accounting",
                  "Finanzwesen"=>"Finance",
                  "Forschung+%26+Entwicklung"=>"Research & Development",
                  "Human+Resources"=>"Human Resources",
                  "Information+Technology"=>"Information Technology",
                  "Legal"=>"Legal",
                  "Logistics%2FSupply+Network+Operations"=>"Logistics/Supply Network Operations",
                  "Logistik"=>"Logistics",
                  "Manufacturing"=>"Manufacturing",
                  "Marktanalyse"=>"Market Analysis",
                  "Produktion"=>"Production",
                  "Purchasing"=>"Purchasing",
                  "Qualitätssicherung"=>"Quality Control",
                  "Quality+Assurance"=>"Quality Assurance",
                  "Research+%26+Development"=>"Research Development",
                  "Sales"=>"Sales",
                  "Vertrieb"=>"Distribution"

              ];

              $ActiveFacetID = [
                  "Administrative"=>"1632",
                  "Brand%3A+Marketing%2FBrand+Management"=>"1643",
                  "Communications"=>"1633",
                  "Consumer+%26+Market+Knowledge"=>"1642",
                  "Design"=>"1634",
                  "Engineering"=>"1635",
                  "Finance+%26+Accounting"=>"1636",
                  "Finanzwesen"=>"64402",
                  "Forschung+%26+Entwicklung"=>"60901",
                  "Human+Resources"=>"1637",
                  "Information+Technology"=>"3605",
                  "Legal"=>"1639",
                  "Logistics%2FSupply+Network+Operations"=>"1640",
                  "Logistik"=>"64361",
                  "Manufacturing"=>"1641",
                  "Marktanalyse"=>"64476",
                  "Produktion"=>"64967",
                  "Purchasing"=>"1644",
                  "Qualitätssicherung"=>"53210",
                  "Quality+Assurance"=>"1653",
                  "Research+%26+Development"=>"1645",
                  "Sales"=>"11694",
                  "Vertrieb"=>"35220"


              ];
              $categorys = array("Administrative","Brand%3A+Marketing%2FBrand+Management","Communications","Consumer+%26+Market+Knowledge","Design","Engineering","Finance+%26+Accounting","Finanzwesen","Forschung+%26+Entwicklung","Human+Resources","Information+Technology","Legal","Logistics%2FSupply+Network+Operations","Logistik","Manufacturing","Marktanalyse","Produktion","Purchasing","Qualitätssicherung","Quality+Assurance","Research+%26+Development","Sales","Vertrieb");
              
              foreach ($categorys as $key => $cate_val) 
              {
                  $activecode = $ActiveFacetID[$cate_val];
                  $page_json_data = HtmlDomParser::file_get_html('https://www.pgcareers.com/search-jobs/results?ActiveFacetID='.$activecode.'&CurrentPage=1&RecordsPerPage=15&Distance=50&RadiusUnitType=0&Keywords=&Location=&Latitude=&Longitude=&ShowRadius=False&CustomFacetName=&FacetTerm=&FacetType=0&FacetFilters%5B0%5D.ID='.$activecode.'&FacetFilters%5B0%5D.FacetType=1&FacetFilters%5B0%5D.Display='.$cate_val.'&FacetFilters%5B0%5D.IsApplied=true&FacetFilters%5B0%5D.FieldName=&SearchResultsModuleName=Search+Results&SearchFiltersModuleName=Search+Filters&SortCriteria=0&SortDirection=0&SearchType=5&CategoryFacetTerm=&CategoryFacetType=&LocationFacetTerm=&LocationFacetType=&KeywordType=&LocationType=&LocationPath=&OrganizationIds=1&PostalCode=&fc=&fl=&fcf=&afc=&afl=&afcf=');
                  $page_jobs_data = json_decode($page_json_data, true);
                  $page_job_list = $page_jobs_data['results'];
                  $pagejobdatafile = fopen("/var/www/html/cronhtmlfile/Pgcontent.html", "w") or die("Unable to open file!");
                  $txt = $page_job_list;
                  fwrite($pagejobdatafile, $txt);

                  $page_job_list_details = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/Pgcontent.html');

                  $pagination = $page_job_list_details->find('section[id=search-results]',0)->find('section[id=search-results-list]',0)->find('section');
                  if(count($pagination) == 2)
                  {
                      $page_total = $page_job_list_details->find('section[id=pagination-bottom]',0)->find('div[class=pagination-page-count]',0)->find('span[class=pagination-total-pages]',0)->innertext;

                      $page_total_exp = explode(" ", $page_total);
                      $page_total = trim($page_total_exp[1]);
                  }
                  else
                  {
                     $page_total = 1;
                  }
                  $pagination = $page_total;
                  //print_r($pagination.",");
                  for ($i=1; $i <= $pagination ; $i++) 
                  { 
                    $json_data = HtmlDomParser::file_get_html('https://www.pgcareers.com/search-jobs/results?ActiveFacetID='.$activecode.'&CurrentPage='.$i.'&RecordsPerPage=15&Distance=50&RadiusUnitType=0&Keywords=&Location=&Latitude=&Longitude=&ShowRadius=False&CustomFacetName=&FacetTerm=&FacetType=0&FacetFilters%5B0%5D.ID='.$activecode.'&FacetFilters%5B0%5D.FacetType=1&FacetFilters%5B0%5D.Display='.$cate_val.'&FacetFilters%5B0%5D.IsApplied=true&FacetFilters%5B0%5D.FieldName=&SearchResultsModuleName=Search+Results&SearchFiltersModuleName=Search+Filters&SortCriteria=0&SortDirection=0&SearchType=5&CategoryFacetTerm=&CategoryFacetType=&LocationFacetTerm=&LocationFacetType=&KeywordType=&LocationType=&LocationPath=&OrganizationIds=1&PostalCode=&fc=&fl=&fcf=&afc=&afl=&afcf=');
                    $jobs_data = json_decode($json_data, true);
                    $job_list = $jobs_data['results'];
                    $jobdatafile = fopen("/var/www/html/cronhtmlfile/Pgmaincontent.html", "w") or die("Unable to open file!");
                    $txt = $job_list;
                    fwrite($jobdatafile, $txt);

                    $job_list_details = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/Pgmaincontent.html');
                    $job_list = $job_list_details->find('section[id=search-results]',0)->find('ul[class=results__list]',0)->find('li[class=results__item]');

                    foreach ($job_list as $key => $job) 
                    {
                        $url = $job->find('a',0)->href;
                        $jobid_exp = explode("/", $url);
                        $job_id = trim($jobid_exp[5]);
                        //print_r($job_id.",");

                        $category = $categories[$cate_val];
                        //print_r($category.",");

                        $source_url = "https://www.pgcareers.com".$url;
                        $job_title = $job->find('a',0)->find('h2',0)->innertext;
                        $job_title = htmlspecialchars_decode($job_title);
                        $job_loc = $job->find('a',0)->find('span[class=job-location]',0)->innertext;
                        $job_loc_exp = explode(",", trim($job_loc));
                        if(count($job_loc_exp) == 3)
                        {
                            
                            $country = trim($job_loc_exp[2]);
                            
                        }


                        if(count($job_loc_exp) == 2)
                        {
                            if(trim($job_loc_exp[1]) == 'Republic of')
                            {
                               $country = trim($job_loc_exp[0]); 
                            }
                            else
                            {
                                if(trim($job_loc_exp[1]) == 'Chinese Mainland')
                                {
                                   $country = 'China'; 
                                }
                                else
                                {
                                    $country = trim($job_loc_exp[1]); 
                                }
                            }
                        }

                        if(count($job_loc_exp) == 1)
                        {
                            if(trim($job_loc_exp[0]) == 'Multiple Locations')
                            {
                               $country = ''; 
                            }
                            else
                            {
                                if(trim($job_loc_exp[0]) == 'Chinese Mainland')
                                {
                                  $country = 'China';
                                }
                                else
                                {
                                  $country = trim($job_loc_exp[0]);
                                } 
                            }
                        }
                        //print_r($country.",");
                        
                        $job_type = $job->find('a',0)->find('span[class=job-type]',0)->innertext;
                        if(trim($job_type) == 'Full-time')
                        {
                           $job_type = 1;
                        }
                        else
                        {
                           $job_type = 2; 
                        }

                        $jobdesc = HtmlDomParser::file_get_html($source_url);

                        $job_desc = $jobdesc->find('div[id=content]',0)->find('section[class=job-description]',0)->find('div[class=ats-description]',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                        $job_desc = addslashes($job_desc); 

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "P&G",
                                        "website" => "https://www.pgcareers.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>$job_type,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }

                                 
                    }
                    

                    $fh = fopen( '/var/www/html/cronhtmlfile/Pgmaincontent.html', 'w' );
                    fclose($fh);
                     
                  }
                  
                  $fh = fopen( '/var/www/html/cronhtmlfile/Pgcontent.html', 'w' );
                  fclose($fh);
                
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
