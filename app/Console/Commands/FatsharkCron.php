<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class FatsharkCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'fatshark:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {      
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://jobs.fatsharkgames.com/jobs");
             $jobs_data = $html_data->find('section[class=section-block jobs-section u-primary-text-color]',0)->find('div[class=wrapper]',0)->find('div[class=jobs-section-inner]',0)->find('ul[class=jobs card-image jobs--count-8]',0)->find('li');

             foreach ($jobs_data as $key => $jobs) 
             {
             	 $url = $jobs->find('a',0)->href;
             	 $job_id = explode("/", $url);
             	 $job_id = explode("-", $job_id[2]);
             	 $job_id = $job_id[0];
             	 //print_r($job_id.",");
             	 $source_url = "https://jobs.fatsharkgames.com".$url;
             	 $job_title = $jobs->find('a',0)->find('div[class=job-metrics]',0)->find('div',0)->find('span',0)->innertext;
             	 $job_title = trim($job_title);
             	 $category = $jobs->find('a',0)->find('div[class=job-metrics]',0)->find('div',0)->find('span',1)->innertext; 
             	 $category = explode("-", $category);
             	 $category = html_entity_decode(trim($category[0]));
             	 if($category == "Stockholm")
             	 {
                    $category = "";
             	 }
             	 else
             	 {
                    $category = $category;
             	 }
             	 $country = "Sweden";
             	 $jobdesc = HtmlDomParser::file_get_html($source_url);
             	 $desc = $jobdesc->find('div[class=job]',0)->find('div[class=wrapper]',0)->find('div[class=body u-margin-top--medium u-primary-text-color]',0)->innertext;
             	 $job_desc = preg_replace('/\s+/', ' ', $desc);
                 $job_desc = addslashes($job_desc);
                 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Fatshark",
                                        "website" => "https://jobs.fatsharkgames.com/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
