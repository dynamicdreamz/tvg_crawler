<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SkanskaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'skanska:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $countries = [
                   "Czech Republic"=>50,
                   "Germany"=>10,
                   "Norway"=>10,
                   "Slovakia"=>10,
                   "United Kingdom"=>10,
                   "Finland"=>10,
                   "Hungary"=>10,
                   "Poland"=>50,
                   "Sweden"=>30,
                   "United States"=>50
             ];

             $countries_id = [
                   "Czech Republic"=>6847665,
                   "Germany"=>6847673,
                   "Norway"=>6847676,
                   "Slovakia"=>6847681,
                   "United Kingdom"=>6847687,
                   "Finland"=>6847690,
                   "Hungary"=>6847700,
                   "Poland"=>6847704,
                   "Sweden"=>6847706,
                   "United States"=>6847710
             ];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($countries as $key => $cvalue) 
             {
                 $c_id = $countries_id[$key]; 
                 $pagination = ceil($cvalue / 10);
                 for ($i=0; $i < $pagination; $i++) 
                 {
                     $p = $i + 1;
                     $ch = curl_init('https://jobs.skanska.com/jobs/search/'.$c_id.'/page'.$p);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     curl_setopt($ch, CURLOPT_HTTPHEADER, array(                              
                              'X-Requested-With: XMLHttpRequest'
                          ));
                     $response = curl_exec($ch);
                     // $jobdesc = HtmlDomParser::file_get_html($response);
                     $myfile = fopen("/var/www/html/cronhtmlfile/SkanskaContent.html", "w") or die("Unable to open file!");
                     $txt = $response;
                     fwrite($myfile, $txt);

                     $html_data = HtmlDomParser::file_get_html("/var/www/html/cronhtmlfile/SkanskaContent.html");
                     
                     $job_data = $html_data->find('div[id=jResultsArea]',0)->find('div[class=full_content]',0)->find('div[id=jobs_filters_area]',0)->find('div[id=job_results_list_hldr]',0)->find('div[class=job_list_row]');
                     foreach ($job_data as $keys => $jobs) 
                     {
                         $source_url = $jobs->find('div[class=jlr_right_hldr]',0)->find('div[class=jlr_title]',0)->find('p',0)->find('a',0)->href;
                         $jobid_exp = explode("/", $source_url);
                         $jobid_exp = explode("-", $jobid_exp[4]);
                         $job_id = end($jobid_exp);
                         //print_r($job_id.",");
                         $job_title = $jobs->find('div[class=jlr_right_hldr]',0)->find('div[class=jlr_title]',0)->find('p',0)->find('a',0)->innertext;
                         $country = $key;
                         $categry = trim($jobs->find('div[class=jlr_right_hldr]',0)->find('div[class=jlr_title]',0)->find('p[class=jlr_cat_loc]',1)->find('span[class=category]',0)->innertext);
                         $categry_exp = explode(",", $categry);
                         $category = trim($categry_exp[0]);

                         $chs = curl_init($source_url);
                         curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
                         curl_setopt($chs, CURLOPT_HTTPHEADER, array(                              
                                  'X-Requested-With: XMLHttpRequest'
                              ));
                         $desc_response = curl_exec($chs);
                         
                         $myfile = fopen("/var/www/html/cronhtmlfile/SkanskaContent1.html", "w") or die("Unable to open file!");
                         $txt = $desc_response;
                         fwrite($myfile, $txt);

                         $jobdesc = HtmlDomParser::file_get_html("/var/www/html/cronhtmlfile/SkanskaContent1.html");
                         $desc = $jobdesc->find('div[class=full_content]',0)->find('div[id=description_box]',0)->find('div[class=job_description]',0)->innertext;
                         $job_desc = preg_replace('/\s+/', ' ', $desc);
                         $job_desc = addslashes($job_desc);

                         $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Skanska",
                                        "website" => "https://group.skanska.com/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }   

                            $fh = fopen( '/var/www/html/cronhtmlfile/SkanskaContent1.html', 'w');
                            fclose($fh);                       


                     }
                    $fh = fopen('/var/www/html/cronhtmlfile/SkanskaContent.html', 'w');
                    fclose($fh);         
                  
                 }
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
