<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class PreemCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'preem:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://www.preem.se/om-preem/karriar/lediga-tjanster/");
             $job_data = $html_data->find('table[class=Table]',0)->find('tr');

             for ($i=1; $i < count($job_data); $i++) 
             { 
                 $job_id = "19756".$i;
                 //print_r($job_id.",");
                 $url = $job_data[$i]->find('td a',0)->href;
                 $source_url = "https://www.preem.se".$url;
                 $job_title = $job_data[$i]->find('td a',0)->innertext;
                 $job_title = html_entity_decode(trim($job_title));
                 $country = "Sweden";
                 $enddate = $job_data[$i]->find('td',2)->innertext;
                 $enddate = date("Y-m-d", strtotime($enddate)); 
                 $jobdesc = HtmlDomParser::file_get_html($source_url);
                 $desc = $jobdesc->find('div[class=TextPage-main Article]',0)->innertext;
                 $job_desc = preg_replace('/\s+/', ' ', $desc);
                 $job_desc = addslashes($job_desc);
                 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Preem",
                                        "website" => "https://www.preem.com/in-english/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => '',
                                        "country" => 'Sweden',
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                 
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
