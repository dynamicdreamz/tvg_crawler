<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class KlarnaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'klarna:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                       "Munich"=>"Germany",
                       "Oslo"=>"Norway",
                       "Helsinki"=>"Finland",
                       "UK"=>"United Kingdom",
                       "Brussels"=>"Belgium",
                       "Linden"=>"Germany",
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $postdata = 'action=klarna_directory_load_hook&post_type=job&orderby=title&nonce=6f55ac6693&posts_to_load=-1&utm_parameters=&segment=department';
             $ch = curl_init('https://www.klarna.com/careers/wp-admin/admin-ajax.php');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                    'content-type: application/x-www-form-urlencoded; charset=UTF-8'
                ));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

                $response = curl_exec($ch);

                curl_close($ch);
                $result = json_decode($response, true);
                //dd($result);
                foreach ($result['objects'] as $key => $jobs) 
                {
                    $category = $jobs['name'];
                    $category = trim($category);

                    $job_details = $jobs['objects'];

                    $myfile = fopen("/var/www/html/cronhtmlfile/KlarnaContent.html", "w") or die("Unable to open file!");
                    $txt = $job_details;
                    fwrite($myfile, $txt);

                    $job_details = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/KlarnaContent.html');
                    $job_job = $job_details->find('div[class=column small-12 medium-4]');
                    foreach ($job_job as $key => $jobs) 
                    {
                        $source_url = $jobs->find('a',0)->href;
                        $jobid = explode("/", $source_url);
                        $jobid_exp = explode("-", $jobid[4]);
                        $job_id = trim($jobid_exp[0]);
                        //print_r($job_id.",");
                        
                        $job_title = $jobs->find('a',0)->find('h3',0)->innertext;
                        $job_title = trim($job_title);
                        $job_title = html_entity_decode($job_title);  
                        
                        $countrys = $jobs->find('a',0)->find('p[class=kl-card__description]',0)->innertext;
                        $countrys_exp = explode(",", trim($countrys));
                        if($job_id == 'a4b759bf')
                        {
                             $country = "";
                        }
                        else
                        {
                            if($job_id == '5f3d3481')
                            {
                                 $country = "";
                            }
                            else
                            {
                                $country = trim($countrys_exp[1]);
                                if($country == 'Munich')
                                {
                                    $country = "Germany";
                                }
                                else
                                {
                                    if($country == 'Oslo')
                                    {
                                        $country = "Norway";
                                    }
                                    else
                                    {
                                        if($country == 'Helsinki')
                                        {
                                            $country = "Finland";
                                        }
                                        else
                                        {
                                            if($country == 'UK')
                                            {
                                                $country = "United Kingdom";
                                            }
                                            else
                                            {
                                                if($country == 'Brussels')
                                                {
                                                    $country = "Belgium";
                                                }
                                                else
                                                {
                                                    if($country == 'Linden')
                                                    {
                                                        $country = "Germany";
                                                    }
                                                    else
                                                    {
                                                        $country = $country;
                                                    } 
                                                } 
                                            } 
                                        } 
                                    } 
                                }
                            }
                        }
                        //print_r($country.",");
                        $jobdesc = HtmlDomParser::file_get_html($source_url);
                        $desc = $jobdesc->find('body[class=show header-compact]',0)->find('div[class=content-wrapper posting-page]',0)->find('div[class=content]',0)->find('div[class=section-wrapper page-full-width]',0)->find('div[class=section page-centered]',0)->innertext; 
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Klarna",
                                        "website" => "https://www.klarna.com/careers/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                        

                    }

                    $fh = fopen( '/var/www/html/cronhtmlfile/KlarnaContent.html', 'w' );
                    fclose($fh);

                }


        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
