<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class KingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'king:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "barcelona"=>"Spain",
                      "stockholm"=>"Sweden",
                      "london"=>"Germany",
                      "berlin"=>"Germany",
                      "bucharest"=>"Romania",
                      "malmo"=>"Sweden",
                      "sanfrancisco"=>"United States",
                      "chicago"=>"United States",
                      "losangeles"=>"United States",
                      "newyork"=>"United States",
                   ];
          
                $html_data = HtmlDomParser::file_get_html("https://king.com/api/external/jobs/all");
                $job_data = json_decode($html_data,true);
            
                foreach ($job_data as $jobs) 
                {
                    // dd($jobs);
                    $ref_id = $jobs['id'];
                    $job_id = $jobs['ref'];
                    //print_r($job_id.",");
                    $category = trim($jobs['functionalArea']);
                    $job_title = trim($jobs['title']); 
                    $country = $countries[trim($jobs['locations'][0])];
                    $desc = trim($jobs['roleBody']);
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);

                    $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "King",
                                        "website" => "https://king.com/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => $ref_id,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => '',
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                    
                    
                }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
