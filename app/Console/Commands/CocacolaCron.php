<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class CocacolaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'cocacola:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $countries = [
                    "AFG" => "Afghanistan",
                    "ALB" => "Albania",
                    "DZA" => "Algeria",
                    "ASM" => "American Samoa",
                    "AND" => "Andorra",
                    "AGO" => "Angola",
                    "AIA" => "Anguilla",
                    "ATA" => "Antarctica",
                    "ATG" => "Antigua and Barbuda",
                    "ARG" => "Argentina",
                    "ARM" => "Armenia",
                    "ABW" => "Aruba",
                    "AUS" => "Australia",
                    "AUT" => "Austria",
                    "AZE" => "Azerbaijan",
                    "BHS" => "Bahamas",
                    "BHR" => "Bahrain",
                    "BGD" => "Bangladesh",
                    "BRB" => "Barbados",
                    "BLR" => "Belarus",
                    "BEL" => "Belgium",
                    "BLZ" => "Belize",
                    "BEN" => "Benin",
                    "BMU" => "Bermuda",
                    "BTN" => "Bhutan",
                    "BOL" => "Bolivia",
                    "BES" => "Bonaire",
                    "BIH" => "Bosnia and Herzegovina",
                    "BWA" => "Botswana",
                    "BVT" => "Bouvet Island",
                    "BRA" => "Brazil",
                    "IOT" => "British Indian Ocean Territory",
                    "VGB" => "British Virgin Islands",
                    "BRN" => "Brunei Darussalam",
                    "BGR" => "Bulgaria",
                    "BFA" => "Burkina Faso",
                    "BDI" => "Burundi",
                    "CPV" => "Cabo Verde",
                    "KHM" => "Cambodia",
                    "CMR" => "Cameroon",
                    "CAN" => "Canada",
                    "CYM" => "Cayman Islands",
                    "CAF" => "Central African Republic",
                    "TCD" => "Chad",
                    "CHL" => "Chile",
                    "CHN" => "China",
                    "HKG" => "Hong Kong",
                    "MAC" => "Macao",
                    "CXR" => "Christmas Island",
                    "CCK" => "Cocos Islands",
                    "COL" => "Colombia",
                    "COM" => "Comoros",
                    "COG" => "Congo",
                    "COK" => "Cook Islands",
                    "CRI" => "Costa Rica",
                    "HRV" => "Croatia",
                    "CUB" => "Cuba",
                    "CUW" => "Curacao",
                    "CYP" => "Cyprus",
                    "CZE" => "Czechia",
                    "CIV" => "Cote d'Ivoire",
                    "PRK" => "Democratic People's Republic of Korea",
                    "COD" => "Congo, Democratic Republic",
                    "DNK" => "Denmark",
                    "DJI" => "Djibouti",
                    "DMA" => "Dominica",
                    "DOM" => "Dominican Republic",
                    "ECU" => "Ecuador",
                    "EGY" => "Egypt",
                    "SLV" => "El Salvador",
                    "GNQ" => "Equatorial Guinea",
                    "ERI" => "Eritrea",
                    "EST" => "Estonia",
                    "ETH" => "Ethiopia",
                    "FLK" => "Falkland Islands",
                    "FRO" => "Faroe Islands",
                    "FJI" => "Fiji",
                    "FIN" => "Finland",
                    "FRA" => "France",
                    "GUF" => "French Guiana",
                    "PYF" => "French Polynesia",
                    "ATF" => "French Southern Territories",
                    "GAB" => "Gabon",
                    "GMB" => "Gambia",
                    "GEO" => "Georgia",
                    "DEU" => "Germany",
                    "GHA" => "Ghana",
                    "GIB" => "Gibraltar",
                    "GRC" => "Greece",
                    "GRL" => "Greenland",
                    "GRD" => "Grenada",
                    "GLP" => "Guadeloupe",
                    "GUM" => "Guam",
                    "GTM" => "Guatemala",
                    "GGY" => "Guernsey",
                    "GIN" => "Guinea",
                    "GNB" => "Guinea-Bissau",
                    "GUY" => "Guyana",
                    "HTI" => "Haiti",
                    "HMD" => "Heard Island and McDonald Islands",
                    "VAT" => "Holy See",
                    "HND" => "Honduras",
                    "HUN" => "Hungary",
                    "ISL" => "Iceland",
                    "IND" => "India",
                    "IDN" => "Indonesia",
                    "IRN" => "Iran",
                    "IRQ" => "Iraq",
                    "IRL" => "Ireland",
                    "IMN" => "Isle of Man",
                    "ISR" => "Israel",
                    "ITA" => "Italy",
                    "JAM" => "Jamaica",
                    "JPN" => "Japan",
                    "JEY" => "Jersey",
                    "JOR" => "Jordan",
                    "KAZ" => "Kazakhstan",
                    "KEN" => "Kenya",
                    "KIR" => "Kiribati",
                    "KWT" => "Kuwait",
                    "KGZ" => "Kyrgyzstan",
                    "LAO" => "Lao People\'s Democratic Republic",
                    "LVA" => "Latvia",
                    "LBN" => "Lebanon",
                    "LSO" => "Lesotho",
                    "LBR" => "Liberia",
                    "LBY" => "Libya",
                    "LIE" => "Liechtenstein",
                    "LTU" => "Lithuania",
                    "LUX" => "Luxembourg",
                    "MDG" => "Madagascar",
                    "MWI" => "Malawi",
                    "MYS" => "Malaysia",
                    "MDV" => "Maldives",
                    "MLI" => "Mali",
                    "MLT" => "Malta",
                    "MHL" => "Marshall Islands",
                    "MTQ" => "Martinique",
                    "MRT" => "Mauritania",
                    "MUS" => "Mauritius",
                    "MYT" => "Mayotte",
                    "MEX" => "Mexico",
                    "FSM" => "Micronesia",
                    "MCO" => "Monaco",
                    "MNG" => "Mongolia",
                    "MNE" => "Montenegro",
                    "MSR" => "Montserrat",
                    "MAR" => "Morocco",
                    "MOZ" => "Mozambique",
                    "MMR" => "Myanmar",
                    "NAM" => "Namibia",
                    "NRU" => "Nauru",
                    "NPL" => "Nepal",
                    "NLD" => "Netherlands",
                    "NCL" => "New Caledonia",
                    "NZL" => "New Zealand",
                    "NIC" => "Nicaragua",
                    "NER" => "Niger",
                    "NGA" => "Nigeria",
                    "NIU" => "Niue",
                    "NFK" => "Norfolk Island",
                    "MNP" => "Northern Mariana Islands",
                    "NOR" => "Norway",
                    "OMN" => "Oman",
                    "PAK" => "Pakistan",
                    "PLW" => "Palau",
                    "PAN" => "Panama",
                    "PNG" => "Papua New Guinea",
                    "PRY" => "Paraguay",
                    "PER" => "Peru",
                    "PHL" => "Philippines",
                    "PCN" => "Pitcairn",
                    "POL" => "Poland",
                    "PRT" => "Portugal",
                    "PRI" => "Puerto Rico",
                    "QAT" => "Qatar",
                    "KOR" => "Korea",
                    "MDA" => "Moldova",
                    "ROU" => "Romania",
                    "RUS" => "Russian Federation",
                    "RWA" => "Rwanda",
                    "REU" => "Reunion",
                    "BLM" => "Saint Barthelemy",
                    "SHN" => "Saint Helena",
                    "KNA" => "Saint Kitts and Nevis",
                    "LCA" => "Saint Lucia",
                    "MAF" => "Saint Martin",
                    "SPM" => "Saint Pierre and Miquelon",
                    "VCT" => "Saint Vincent and Grenadines",
                    "WSM" => "Samoa",
                    "SMR" => "San Marino",
                    "STP" => "Sao Tome and Principe",
                    "SAU" => "Saudi Arabia",
                    "SEN" => "Senegal",
                    "SRB" => "Serbia",
                    "SYC" => "Seychelles",
                    "SLE" => "Sierra Leone",
                    "SGP" => "Singapore",
                    "SXM" => "Sint Maarten",
                    "SVK" => "Slovakia",
                    "SVN" => "Slovenia",
                    "SLB" => "Solomon Islands",
                    "SOM" => "Somalia",
                    "ZAF" => "South Africa",
                    "SGS" => "South Georgia And Sandwich Isl.",
                    "SSD" => "South Sudan",
                    "ESP" => "Spain",
                    "LKA" => "Sri Lanka",
                    "PSE" => "State of Palestine",
                    "SDN" => "Sudan",
                    "SUR" => "Suriname",
                    "SJM" => "Svalbard and Jan Mayen Islands",
                    "SWZ" => "Swaziland",
                    "SWE" => "Sweden",
                    "CHE" => "Switzerland",
                    "SYR" => "Syrian Arab Republic",
                    "TJK" => "Tajikistan",
                    "THA" => "Thailand",
                    "MKD" => "Macedonia",
                    "TLS" => "Timor-Leste",
                    "TGO" => "Togo",
                    "TKL" => "Tokelau",
                    "TON" => "Tonga",
                    "TTO" => "Trinidad and Tobago",
                    "TUN" => "Tunisia",
                    "TUR" => "Turkey",
                    "TKM" => "Turkmenistan",
                    "TCA" => "Turks and Caicos Islands",
                    "TUV" => "Tuvalu",
                    "UGA" => "Uganda",
                    "UKR" => "Ukraine",
                    "ARE" => "United Arab Emirates",
                    "GBR" => "United Kingdom",
                    "TZA" => "Tanzania",
                    "UMI" => "United States Minor Outlying Islands",
                    "VIR" => "United States Virgin Islands",
                    "USA" => "United States",
                    "URY" => "Uruguay",
                    "UZB" => "Uzbekistan",
                    "VUT" => "Vanuatu",
                    "VEN" => "Venezuela",
                    "VNM" => "Viet Nam",
                    "WLF" => "Wallis and Futuna Islands",
                    "ESH" => "Western Sahara",
                    "YEM" => "Yemen",
                    "ZMB" => "Zambia",
                    "ZWE" => "Zimbabwe"
                ];

            $categoris = [
                    'Aviation'=>'e707ae97-f766-4bb5-b8eb-ed082c4ceec1',
                    'Business - Administrative Services'=>'73ef5b30-ac6a-42b2-8ce8-ca1ccd64a8f4',
                    'Business Management and Development'=>'32e6fce6-ddce-4807-849d-403917c3058a',
                    'Finance'=>'5df22303-01dc-405a-a95f-b7e2c8e0446a',
                    'General Management'=>'32fce8f2-f882-4b97-9d23-013fa8727489',
                    'Human Resources'=>'a375ee01-0e40-499b-a637-ef1328f8dae4',
                    'Information Technology'=>'b308f88f-47bb-417f-8a8a-35f2c3bccc21',
                    'Legal'=>'c56ae044-330a-4539-8962-ab49191c1f1f',
                    'Marketing'=>'c8cd769d-f5e6-44a8-919a-5c108c927989',
                    'Public Affairs'=>'48f0ebc3-9dad-420b-b8ab-8ed2d6072387',
                    'Retail and Attractions'=>'110ff12a-91c7-41cf-80eb-1c9feff25c17',
                    'Sales and Account Management'=>'eb5ecdde-518a-4118-abe1-bdbbd6359ea0',
                    'Security'=>'f88a7343-708a-4d64-8ec9-d9ac85f12e46',
                    'Supply Chain - Logistics'=>'8d82d40e-4842-4028-9603-57d923ee449f',
                    'Technical Sciences - Engineering'=>'148cc1ec-9787-4ea1-b63f-1c6a1ce0a0eb'
            ];

            $categoris_array = array("Aviation","Business - Administrative Services","Business Management and Development","Finance","General Management","Human Resources","Information Technology","Legal","Marketing","Public Affairs","Retail and Attractions","Sales and Account Management","Security","Supply Chain - Logistics","Technical Sciences - Engineering");

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             

             foreach ($categoris_array as $key => $cates)
             {
                 $selectCats = $categoris[$cates];
                 $ch = curl_init('https://useast.appvault.com/jobs/handlers/av_iSearchNew.aspx?v=cocacolacompanyrms');
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'content-type: application/x-www-form-urlencoded; charset=UTF-8',

                 ));
                 curl_setopt($ch, CURLOPT_POSTFIELDS, 'txtKeywords=&selectCats='.$selectCats.'&txtCity=&zip=&zipNoCity=&cStateZip=00000&distance=0&country=ALL&jobItem1=%220%22&jobItemsel1=JOBSTATUS&jobItem2=%5B%5D&jobItemsel2=&page=1&pageSize=25&fromPage=None&avprettyurl=true&newPage=1');
                 $response = curl_exec($ch);
                 curl_close($ch);
                 $result = $response;
                 $xml = str_replace("<![CDATA[", "", $result);
                 $xml = str_replace("]]>", "", $xml);
                 
                 $myfile = fopen("/var/www/html/cronhtmlfile/CokacolaContent.html", "w") or die("Unable to open file!");
                 $txt = $xml;
                 fwrite($myfile, $txt);
                 $job_details = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/CokacolaContent.html');
                 $job_datas = $job_details->find('totalCount',0)->innertext;
                 $pagination = ceil($job_datas / 25);
                 for($i=1;$i<=2;$i++)
                 {
                     $ch = curl_init('https://useast.appvault.com/jobs/handlers/av_iSearchNew.aspx?v=cocacolacompanyrms');
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'content-type: application/x-www-form-urlencoded; charset=UTF-8',

                     ));
                     curl_setopt($ch, CURLOPT_POSTFIELDS, 'txtKeywords=&selectCats='.$selectCats.'&txtCity=&zip=&zipNoCity=&cStateZip=00000&distance=0&country=ALL&jobItem1=%220%22&jobItemsel1=JOBSTATUS&jobItem2=%5B%5D&jobItemsel2=&page='.$i.'&pageSize=25&fromPage=None&avprettyurl=true&newPage='.$i);
                     $responses = curl_exec($ch);
                     curl_close($ch);
                     $results = $responses;
                     $xmls = str_replace("<![CDATA[", "", $results);
                     $xmls = str_replace("]]>", "", $xmls);
                                  
                     $myfiles = fopen("/var/www/html/cronhtmlfile/CokacolaContent1.html", "w") or die("Unable to open file!");
                     $txts = $xmls;
                     fwrite($myfiles, $txts);
                     $job_detail = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/CokacolaContent1.html');
                     $job_data = $job_detail->find('li');

                     foreach ($job_data as $key => $job) 
                     {
                         $url = $job->find('div[class=job_listingDefault]',0)->find('a',0)->href;
                         $source_url = "https://cocacola.appvault.com/".$url;

                         $category = $cates;

                         $jobid_exp = explode("/", $url);
                         $job_id = $jobid_exp[1];

                         $job_title = $job->find('div[class=job_listingDefault]',0)->find('a',0)->innertext;
                         $job_title = trim( $job_title );

                         

                         $job_details = HtmlDomParser::file_get_html($source_url);

                         $postdate = $job_details->find('div[id=jobDetails]',0)->find('header[class=jobHeader]',0)->find('ul[class=jobInfoList]',0)->find('li[class=jobPostDate]',0)->innertext;
                         $postdate_exp = explode("</span>", $postdate);
                         $posted_date = date('Y-m-d', strtotime($postdate_exp[1]));

                         $location = $job_details->find('div[id=jobDetails]',0)->find('header[class=jobHeader]',0)->find('ul[class=jobInfoList]',0)->find('li[class=jobLocation]',0)->innertext;
                         $location_exp = explode("</span>", $location);
                         $location_exp = trim($location_exp[1]);
                         $loca_exp = explode("-", $location_exp);
                         //dd(trim($loca_exp[1]));
                         if(trim($loca_exp[1]) == 'TWN')
                         {
                           $country = "Taiwan";
                         }
                         else
                         {
                           $country = $countries[trim($loca_exp[1])];
                         }
                         //print_r($country.",");

                         $job_desc = $job_details->find('div[id=jobDetails]',0)->find('section[id=jobCopy]',0)->innertext;
                         $job_desc = preg_replace('/\s+/', ' ', $job_desc);
                         $job_desc = addslashes($job_desc);

                         if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                         {
                             //print_r($posted_date.",");
                             $job_id = $job_id;
                             //print_r($job_id.",");
                             $job_title = $job_title;
                             $posted_on = $posted_date;
                             $category = $category;
                             //print_r($category.",");
                             $country = $country;
                             $description = $job_desc;
                             $source_url = $source_url;

                             $row = Job::where('job_id', $job_id)->count();  
                             if($row == 0)
                               {
                                      $insert_data = [
                                        "company" => "Cocacola",
                                        "website" => "https://www.coca-colacompany.com/careers",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_on,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $description,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                                }

                         }
     
                     }

                     $fh = fopen( '/var/www/html/cronhtmlfile/CokacolaContent1.html', 'w' );
                     fclose($fh);

                        
                 }
                 $fh = fopen( '/var/www/html/cronhtmlfile/CokacolaContent.html', 'w' );
                 fclose($fh);
         
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
