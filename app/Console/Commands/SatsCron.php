<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SatsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'sats:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Ansbach"=>"Germany",
                      "Berlin"=>"Germany",
                      "Dortmund"=>"Germany",
                      "Dublin (Ireland)"=>"Ireland",
                      "Erfurt"=>"Germany",
                      "Frankfurt am Main"=>"Germany",
                      "Großbeeren"=>"Germany",
                      "Hamburg"=>"Germany",
                      "Hannover"=>"Germany",
                      "Helsinki (Finland)"=>"Finland",
                      "Köln"=>"Germany",
                      "Lahr/Schwarzwald"=>"Germany",
                      "Leipzig"=>"Germany",
                      "Łódź (Poland)"=>"Poland",
                      "Ludwigsfelde"=>"Germany",
                      "Mönchengladbach"=>"Germany",
                      "Münster"=>"Germany",
                      "Nogarole Rocca (Italy)"=>"Italy",
                      "Olsztynek (Poland)"=>"Poland",
                      "Stuttgart"=>"Germany",
                      "Szczecin (Poland)"=>"Poland",
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(54 / 10);
             for ($i=0; $i < $pagination; $i++) 
             {  
                $p = $i*10;
                $ch = curl_init('https://global3.recruitmentplatform.com/fo/rest/jobs?firstResult='.$p.'&maxResults=10&sortBy=DPOSTINGSTART&sortOrder=desc');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  'Accept: application/json, text/javascript, */*; q=0.01',
                  'username: QD9FK026203F3VBQB688M8M77:guest:FO',
                  'password: guest'
                ));
                $response = curl_exec($ch);
                $result = json_decode($response,true);
                // dd($result);

                foreach ($result['jobs'] as $jobs) 
                {
                    $postdate = $jobs['jobFields']['DPOSTINGSTART'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = trim($jobs['jobFields']['id']);
                        //print_r( $job_id .",");
                        $job_title = trim($jobs['jobFields']['jobTitle']);
                        $category = trim($jobs['jobFields']['SLOVLIST1']);
                        $country = "Singapore";
                        $source_url = "https://www.sats.com.sg/Careers/latest/components/details.html?jobId=".$job_id;
                        if($job_id == "2032")
                        {
                          $desc = trim($jobs['customFields'][0]['title'])."\n".trim($jobs['customFields'][0]['content'])."\n".trim($jobs['customFields'][1]['title'])."\n".trim($jobs['customFields'][1]['content']);
                        }
                        else
                        {
                          $desc = trim($jobs['customFields'][0]['title'])."\n".trim($jobs['customFields'][0]['content'])."\n".trim($jobs['customFields'][1]['title'])."\n".trim($jobs['customFields'][1]['content'])."\n".trim($jobs['customFields'][2]['title'])."\n".trim($jobs['customFields'][2]['content']);
                        }
                        $job_desc = addslashes($desc);
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Sats",
                                        "website" => "https://www.sats.com.sg/Careers/latest/components/index.html",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                        
                        
                    }
                    
                }
             }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
