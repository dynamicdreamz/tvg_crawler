<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AlfalavalCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'alfalaval:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $json_data = HtmlDomParser::file_get_html("https://www.alfalaval.com/webapi/Vacancy/Get/?pageid=8173&lang=en");
             $jobs_content = json_decode($json_data, true);
             foreach ($jobs_content['AllVacancy'] as $key => $jobs) 
             {
                 $url = $jobs['Url'];
                 $source_url = "https://www.alfalaval.com".$url;
                 $job_id = explode("=", $url);
                 $job_id = trim($job_id[1]);
                 //print_r($job_id.",");
                 $job_title = trim($jobs['Title']);
                 $category = trim($jobs['Function']);
                 $country = explode(",", $jobs['Country']);
                 $country = $country[0];
                 $enddate = $jobs['ClosingDate'];
                 $enddate = date("Y-m-d", strtotime($enddate));
                 $jobdesc = HtmlDomParser::file_get_html($source_url);
                 $desc = $jobdesc->find('section[class=page-content l-page]',0)->find('div[class=l-standard-page]',0)->find('div[class=l-content]',0)->innertext;
                 $job_desc = preg_replace('/\s+/', ' ', $desc);
                 $job_desc = addslashes($job_desc);
                 
                 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Alfalaval",
                                        "website" => "https://www.alfalaval.com/career/vacancies/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
