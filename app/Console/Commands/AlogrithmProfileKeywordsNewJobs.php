<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Admin;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use App\ExperienceRating;
use App\JobtypeRating;
use App\LocationRating;
use App\CompanyRating;
use Illuminate\Support\Facades\Log;

class AlogrithmProfileKeywordsNewJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algorithm:profilekeywordsnewjobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
            //Get all users
            $users = Admin::all();
            foreach ($users as $user) {
                    $generic_keywords = Keyword::leftJoin('weights', function ($join) {
                        $join->on('keywords.generic_weight', '=', 'weights.id');
                    })->where('keywords.profile_id', '>', 0)->where('keywords.user_id', $user->id)->select('keywords.*', 'weights.rating')->get();

                    $jobs = Job::where('user_id', $user->id)->where('points', '>', $user->threshold)->orWhere('status', 1)->get();

                    foreach ($jobs as $job) {
                        $rating_types = [];
                        $rating_points = [];
                        foreach ($generic_keywords as $k => $keyword) {
                            $rating = 0;

                            //calculate ratings based on company, location, job type & experience
                            $company_ratings = CompanyRating::where("profile_id", $keyword->profile_id)->where('user_id', $user->id)->get()->toArray();
                            if (count($company_ratings) > 0) {
                                $company_key = array_search($job->company, array_column($company_ratings, 'company'));
                                if ($company_key !== false) {
                                    $company_rating = $company_ratings[$company_key]["rating"];

                                    array_push($rating_types, "company");
                                    array_push($rating_points, $company_rating);
                                    $rating = $company_rating;
                                }
                            }

                            $jobtype_ratings = JobtypeRating::where("profile_id", $keyword->profile_id)->where('user_id', $user->id)->get()->toArray();
                            if (count($jobtype_ratings) > 0) {
                                $jobtype_key = array_search($job->job_type, array_column($jobtype_ratings, 'jobtype'));
                                if ($jobtype_key !== false) {
                                    $jobtype_rating = $jobtype_ratings[$jobtype_key]["rating"];
                                    array_push($rating_types, "jobtype");
                                    array_push($rating_points, $jobtype_rating);
                                    $rating += $jobtype_rating;
                                }
                            }

                            $location_ratings = LocationRating::where("profile_id", $keyword->profile_id)->where('user_id', $user->id)->get()->toArray();
                            if (count($location_ratings) > 0) {
                                $location_key = array_search($job->country, array_column($location_ratings, 'country'));
                                if ($location_key !== false) {
                                    $location_rating = $location_ratings[$location_key]["rating"];
                                    array_push($rating_types, "location");
                                    array_push($rating_points, $location_rating);
                                    $rating += $location_rating;
                                }
                            }

                            $exp_ratings = ExperienceRating::where("profile_id", $keyword->profile_id)->where('user_id', $user->id)->get()->toArray();
                            if (count($exp_ratings) > 0) {
                                if ($job->experience_from > 0 && $job->experience_to > 0) {
                                    $exp_key_from = array_search($job->experience_from, array_column($exp_ratings, 'experience_from'));
                                    $exp_key_to = array_search($job->experience_to, array_column($exp_ratings, 'experience_to'));

                                    if ($exp_ratings[$exp_key_from]["experience_from"] == $job->experience_from && $exp_ratings[$exp_key_to]["experience_to"] == $job->experience_to) {
                                        $exp_rating = $exp_ratings[$exp_key_from]["rating"];

                                        array_push($rating_types, "experience");
                                        array_push($rating_points, $exp_rating);
                                        $rating += $exp_rating;
                                    }
                                }
                            }

                            if ($rating > 0) {
                                JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->update(['profile_rating_types' => serialize($rating_types), 'profile_rating_points' => serialize($rating_points)]);
                                $rating_types = [];
                                $rating_points = [];
                            }


                            $keyword_exists = JobMatchedProfile::where(['job_id' => $job->id, 'keyword_id' => $keyword->id, 'profile_id' => $keyword->profile_id])->first();
                            if (!$keyword_exists) {
                                if ($job->category != '' && $job->description != '') {
                                    if (preg_match("~\b".$keyword->generic_title."\b~", $job->job_title) == 1 ||
                                        preg_match("~\b".$keyword->generic_title."\b~", $job->category) == 1 ||
                                        preg_match("~\b".$keyword->generic_title."\b~", $job->description) == 1) {
                                        $matchedjob = new JobMatchedProfile();
                                        $matchedjob->job_id = $job->id;
                                        $matchedjob->profile_id = $keyword->profile_id;
                                        $matchedjob->keyword_id = $keyword->id;
                                        $matchedjob->points = $keyword->rating * $keyword->generic_balance;
                                        $matchedjob->save();

                                        $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->pluck('points')->toArray();
                                        $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                                            $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                                        })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);


                                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)
                                            ->update(['total_points' => array_sum($points) + $rating, 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'profile_keyword_points' => serialize($points)]);

                                    }
                                } elseif ($job->category == '' && $job->description != '') {
                                    if (preg_match("~\b".$keyword->generic_title."\b~", $job->category) == 1 ||
                                        preg_match("~\b".$keyword->generic_title."\b~", $job->description) == 1) {
                                        $matchedjob = new JobMatchedProfile();
                                        $matchedjob->job_id = $job->id;
                                        $matchedjob->profile_id = $keyword->profile_id;
                                        $matchedjob->keyword_id = $keyword->id;
                                        $matchedjob->points = $keyword->rating * $keyword->generic_balance;
                                        $matchedjob->save();

                                        $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->pluck('points')->toArray();
                                        $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                                            $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                                        })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);


                                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)
                                            ->update(['total_points' => array_sum($points) + $rating, 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'profile_keyword_points' => serialize($points)]);
                                    }
                                } elseif ($job->description == '' && $job->category != '') {
                                    if (preg_match("~\b".$keyword->generic_title."\b~", $job->job_title) == 1 ||
                                        preg_match("~\b".$keyword->generic_title."\b~", $job->category) == 1) {
                                        $matchedjob = new JobMatchedProfile();
                                        $matchedjob->job_id = $job->id;
                                        $matchedjob->profile_id = $keyword->profile_id;
                                        $matchedjob->keyword_id = $keyword->id;
                                        $matchedjob->points = $keyword->rating * $keyword->generic_balance;
                                        $matchedjob->save();

                                        $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->pluck('points')->toArray();
                                        $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                                            $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                                        })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);


                                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)
                                            ->update(['total_points' => array_sum($points) + $rating, 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'profile_keyword_points' => serialize($points)]);
                                    }
                                } else {
                                    if (preg_match("~\b".$keyword->generic_title."\b~", $job->job_title) == 1) {
                                        $matchedjob = new JobMatchedProfile();
                                        $matchedjob->job_id = $job->id;
                                        $matchedjob->profile_id = $keyword->profile_id;
                                        $matchedjob->keyword_id = $keyword->id;
                                        $matchedjob->points = $keyword->rating * $keyword->generic_balance;
                                        $matchedjob->save();

                                        $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->pluck('points')->toArray();
                                        $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                                            $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                                        })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);


                                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)
                                            ->update(['total_points' => array_sum($points) + $rating, 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'profile_keyword_points' => serialize($points)]);
                                    }
                                }
                            } else {
                                $all_generic_keywords = Keyword::where("profile_id", $keyword->profile_id)->where("user_id", $user->id)->pluck('id')->all();

                                $matched_keywords = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->get();

                                foreach ($matched_keywords as $v) {
                                    if (!in_array($v->keyword_id, $all_generic_keywords)) {
                                        JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('keyword_id', $v->keyword_id)->delete();
                                    }
                                }

                                JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('keyword_id', $keyword->id)->update(['points' => $keyword->rating * $keyword->generic_balance]);

                                $points = JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0)->pluck('points')->toArray();
                                $keywords = JobMatchedProfile::leftJoin('keywords', function ($join) {
                                    $join->on('job_matched_profiles.keyword_id', '=', 'keywords.id');
                                })->where('job_matched_profiles.job_id', $job->id)->where('keywords.profile_id', $keyword->profile_id)->where('job_matched_profiles.remove', 0);

                                JobMatchedProfile::where('job_id', $job->id)->where('profile_id', $keyword->profile_id)->where('keyword_id', $keyword->id)
                                    ->update(['points' => $keyword->rating * $keyword->generic_balance, 'total_points' => array_sum($points) + $rating, 'profile_keywords' => serialize($keywords->pluck('keywords.generic_title')->toArray()), 'profile_keyword_ids' => serialize($keywords->pluck('keywords.id')->toArray()), 'profile_keyword_points' => serialize($points)]);
                            }
                        }
                    }
                    Admin::where('id', $user->id)->update(["profile_cron" => 2]);

            }
            echo "Profile cron executed successfully";
            exit;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
