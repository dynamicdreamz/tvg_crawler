<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class IbmCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'ibm:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
        	 $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];

             $insert_data = array();
             $pagination = ceil(6468 / 30);
             //dd($pagination);
             for ($i=1; $i <= 2; $i++) 
             { 
             	//--print_r($i.",");
                $dom = new HtmlDomParser();
                $dom = HtmlDomParser::file_get_html('https://careers.ibm.com/ListJobs/All/lang%3den/Page-'.$i.'/?lang=en');
                $jobs_data = $dom->find('table[class=JobListTable]',0)->find('tr');
                //dd($jobs_data);
                for ($j=2; $j < count($jobs_data) ; $j++) 
                { 
                   $job_url = "https://careers.ibm.com" . $jobs_data[$j]->find('td[class=coloriginaljobtitle] a',0)->href."?lang=en";
                   $job_id = explode('/', $job_url);
                   $job_ids = $job_id[5];
                   //--print_r($job_ids.",");

           
                   $job_title = $jobs_data[$j]->find('td[class=coloriginaljobtitle] a',0)->innertext;
                   $category = $jobs_data[$j]->find('td[class=colshorttextfield1]',0)->innertext;
                   $category = preg_replace('/(\v|\s)+/', '', $category);

                   if($job_ids != '497799')
                   {
                               
                    $content = @file_get_contents($job_url);
                    if (strpos($http_response_header[0], "200")) 
                    {
                   	    $myfile = fopen("/opt/lampp/htdocs/tvg/cronhtmlfile/IbmContent.html", "w") or die("Unable to open file!");
                        $txt = $content;
                        fwrite($myfile, $txt);

                        $description = HtmlDomParser::file_get_html('/opt/lampp/htdocs/tvg/cronhtmlfile/IbmContent.html');
                        if($description != FALSE)
	                    {
                           // if($job_ids != '437275')
                           // {
                           //    if($job_ids != '500354')
                           //    {
	                          //   $job_info = $description->find('div[id=job-description]',0)->innertext;
	                          //   $job_description = addslashes($job_info);
                           //    }
                           //    else
                           //    {
                           //      $job_description = '';
                           //    }
                           
                           // }
                           // else
                           // {
                           //    $job_description = '';
                           // }

	                    	$job_info = $description->find('div[id=job-description]',0)->innertext;
	                    	$job_description = preg_replace('/\s+/', ' ', $job_info);
	                        $job_description = addslashes($job_description);


	                       $job_spec = $description->find('section[class=job-specs desktop-only]',0)->find('ul');
	                       foreach ($job_spec as $key => $value) 
	                       {
	                           $job_cntry = $value->find('li',0)->innertext;
	                           $job_cntry = explode("</span>", $job_cntry);
	                           if(strlen($job_cntry[1]) == '2')
	                           {
                                  $job_cntry = $countries[trim($job_cntry[1])];
	                           }
	                           else
	                           {
	                           	  $job_cntry = $job_cntry[1];
	                           }
	                           
	                           //print_r($job_cntry.",");

	                           $job_state = $value->find('li',1)->innertext;
	                           $job_state = explode("</span>", $job_state);
	                           $job_state = $job_state[1];
	                           
	                           $job_city = $value->find('li',2)->innertext;
	                           $job_city = explode("</span>", $job_city);
	                           $job_city = $job_city[1];

	                           $country = $job_cntry;
	                           
	                           $job_type = $value->find('li',6)->innertext;
	                           $job_type = explode("</span>", $job_type);
	                           $job_type = $job_type[1];

                               if($job_type == 'Full-Time')
                               {
                                  $job_type = '1';
                               }
                               else
                               {
                                  if($job_type == 'Part-Time')
                                  {
                                    $job_type = '2';
                                  }
                                  else
                                  {
                                    $job_type = '1';
                                  }
                                  
                               }
                               //print_r($job_type.",");

	                           $reference_id = $value->find('li',8)->innertext;
	                           $reference_id = explode("</span>", $reference_id);
	                           $reference_id = $reference_id[1];
	                           //dd($reference_id);	                          
	                       }

	                    }
	                    $row = Job::where('job_id', $job_ids)->count();
	                    if($row == 0)
	                    {  
	                        $insert_data = [
	                            "company" => "IBM",
	                            "website" => "https://www.ibm.com/us-en/?lnk=m",
	                            "job_title" => $job_title,
	                            "category" => $category,
	                            "country" => $country,
	                            "description" => $job_description,
	                            "job_id" => $job_ids,
	                            "reference_id" => $reference_id,
	                            "contact_name"=>'',
	                            "contact_email"=>'',
	                            "contact_phone"=>'',
	                            "source_url" => $job_url,
	                            "experience_from" => 0,
                            	"experience_to" => 0,
	                            "job_type"=> $job_type,
	                            "points"=>0,
	                            "keywords"=>'',
	                            "keyword_ids"=>'',
	                            "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
	                            "status"=>0,
	                            "created_at"=>date("Y-m-d H:i:s"),
	                            "updated_at"=>date("Y-m-d H:i:s")                                               
	                          ];
                            //print_r($insert_data);
	                        Job::insert($insert_data);
	                    }
	                    $fh = fopen( '/opt/lampp/htdocs/tvg/cronhtmlfile/IbmContent.html', 'w' );
                        fclose($fh);
                    }
                   
                  }
                 
                  $fh = fopen( '/opt/lampp/htdocs/tvg/cronhtmlfile/IbmContent.html', 'w' );
                  fclose($fh);
    
                }
 
             }
                          
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
