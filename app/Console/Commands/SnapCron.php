<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SnapCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'snap:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Ansbach"=>"Germany",
                      "Berlin"=>"Germany",
                      "Dortmund"=>"Germany",
                      "Dublin (Ireland)"=>"Ireland",
                      "Erfurt"=>"Germany",
                      "Frankfurt am Main"=>"Germany",
                      "Großbeeren"=>"Germany",
                      "Hamburg"=>"Germany",
                      "Hannover"=>"Germany",
                      "Helsinki (Finland)"=>"Finland",
                      "Köln"=>"Germany",
                      "Lahr/Schwarzwald"=>"Germany",
                      "Leipzig"=>"Germany",
                      "Łódź (Poland)"=>"Poland",
                      "Ludwigsfelde"=>"Germany",
                      "Mönchengladbach"=>"Germany",
                      "Münster"=>"Germany",
                      "Nogarole Rocca (Italy)"=>"Italy",
                      "Olsztynek (Poland)"=>"Poland",
                      "Stuttgart"=>"Germany",
                      "Szczecin (Poland)"=>"Poland",
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $ch = curl_init('https://www.snap.com/api/jobs/');
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                      'accept: */*',
                      'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
                  ));
                $response = curl_exec($ch);
                $result = json_decode($response,true);

                foreach ($result['Report_Entry'] as $key => $jobs) 
                {
                   $job_id = trim($jobs['id']);
                   //print_r($job_id.",");
                   $job_title = trim($jobs['title']);
                   $source_url = trim($jobs['absolute_url']);
                   $category = trim($jobs['role']);
                   $desc = trim($jobs['content']);
                   $job_desc = preg_replace('/\s+/', ' ', $desc);
                   $job_desc = addslashes($job_desc); 
                   $cuntry = trim($jobs['offices'][0]['location']);
                   $cuntry_exp = explode(",", $cuntry);
                   $country = trim($cuntry_exp[1]);
                   $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Snap",
                                        "website" => "https://www.snap.com/en-US/jobs/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        
                   
                }
                

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
