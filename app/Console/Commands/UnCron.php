<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class UnCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'un:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      'ACCRA'=>'Ghana',
                      'ADDIS ABABA'=>'Ethiopia',
                      'ALMATY (ALMA ATA)'=>'Kazakhstan',
                      'AMMAN'=>'Jordan',
                      'ANKARA'=>'Turkey',
                      'ARUSHA'=>'Tanzania',
                      'ASHKHABAD'=>'Turkmenistan',
                      'BAGHDAD'=>'Iraq',
                      'BAKU'=>'Azerbaijan',
                      'BAMAKO'=>'Mali',
                      'BANGKOK'=>'Thailand',
                      'BANGUI'=>'Africa',
                      'BARCELONA'=>'Spain',
                      'BEIJING'=>'China',
                      'BEIRUT'=>'Lebanon',
                      'BENTIU'=>'South Sudan',
                      'BOGOTA'=>'Colombia',
                      'BONN'=>'Germany',
                      'Bria'=>'Africa',
                      'BRINDISI'=>'Italy',
                      'BRUSSELS'=>'Belgium',
                      'BUDAPEST'=>'Hungary',
                      'BUENOS AIRES'=>'Argentina',
                      'BUSAN'=>'South Korea',
                      'CAIRO'=>'Egypt',
                      'DAKAR'=>'Senegal',
                      'DILI (EAST TIMOR)'=>'Timor-Leste',
                      'DONETSK'=>'Ukraine',
                      'EL FASHER'=>'Sudan',
                      'ENTEBBE'=>'Uganda',
                      'ERBIL'=>'Kurdistan',
                      'Field Locations'=>'',
                      'GENEVA'=>'Switzerland',
                      'GEORGETOWN'=>'Guyana',
                      'GOMA'=>'Democratic Republic of the Congo',
                      'HERAT'=>'Afghanistan',
                      'INCHEON CITY'=>'South Korea',
                      'ISTANBUL'=>'Turkey',
                      'JAKARTA'=>'Indonesia',
                      'JAM - Other cities'=>'',
                      'Juba'=>'South Sudan',
                      'JUBA'=>'South Sudan',
                      'KABUL'=>'Afghanistan',
                      'KAGA BANDORO'=>'African',
                      'KAMPALA'=>'Uganda',
                      'KASS'=>'',
                      'KHARTOUM'=>'Sudan',
                      'KIGALI'=>'Rwanda',
                      'KINGSTON'=>'Jamaica',
                      'KINSHASA'=>'Democratic Republic of the Congo',
                      'KOBE'=>'Japan',
                      'KUWAIT'=>'Kuwait',
                      'LA PAZ'=>'Bolivia',
                      'LAGOS'=>'Nigeria',
                      'LIMA'=>'Peru',
                      'LOME'=>'Togo',
                      'LUSAKA_ISC'=>'Zambia',
                      'MANAMA'=>'Bahrain',
                      'Menaka'=>'',
                      'MEXICO CITY'=>'Mexico',
                      'MOGADISHU'=>'Somalia',
                      'MONTEVIDEO'=>'Uruguay',
                      'MONTREAL'=>'Canada',
                      'NAIROBI'=>'Kenya',
                      'NAQOURA'=>'Lebanon',
                      'NAY PYI DAW_ISC'=>'',
                      'NEW DELHI'=>'India',
                      'NEW YORK'=>'United States',
                      'NIAMEY'=>'Niger',
                      'NICOSIA'=>'Cyprus',
                      'NOUAKCHOTT'=>'Mauritania',
                      'OSAKA'=>'Japan',
                      'OUAGADOUGOU'=>'Burkina Faso',
                      'PANAMA CITY'=>'Panama',
                      'PARIS'=>'France',
                      'PARIS (2000)'=>'France',
                      'PHNOM-PENH'=>'Cambodia',
                      'PRETORIA'=>'South Africa',
                      'PRISTINA_ISC'=>'',
                      'RABAT'=>'Morocco',
                      'RIO DE JANEIRO'=>'Brazil',
                      'SAN JOSE'=>'United States',
                      "SANA'A"=>'Yemen',
                      'SANTIAGO'=>'Chile',
                      'SARAJEVO'=>'Bosnia and Herzegovina',
                      'SITTWAY (SITTWE)'=>'Myanmar',
                      'SUVA'=>'Fiji',
                      'THE HAGUE'=>'Netherlands',
                      'TOKYO'=>'Japan',
                      'TOMBOUCTOU'=>'Mali',
                      'TOYAMA'=>'Japan',
                      'TRIPOLI (LIBYA)'=>'Libya',
                      'VIENNA'=>'Austria',
                      'WASHINGTON'=>'United State',
                      'Yambio'=>'South Sudan',
                      'YANGON'=>'Myanmar',
                      'YAOUNDE'=>'Cameroon',
                      'ZALINGUEI'=>'Sudan',
                      'MOGADISCIO'=>'Somalia',
                      'MULTIPLE DUTY STATIONS'=>'',
                      'NAY PYI TAW'=>'Myanmar',

                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $html_data = HtmlDomParser::file_get_html("https://careers.un.org/lbw/jobfeed.aspx?lang=en-US");
             $job_data = $html_data->find('rss',0)->find('channel',0)->find('item');
             foreach ($job_data as $key => $jobs) 
             {
                $jdata = $jobs->find('description',0)->innertext;
                $jdata = html_entity_decode($jdata);
                $jdata_exp = explode("<br/>", $jdata);
                $post_date = explode(":", $jdata_exp[7]);
                $posted_date = date("Y-m-d", strtotime(trim($post_date[1])));
                if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                {
                    $job_title = $jobs->find('title',0)->innertext;
                    $job_title = html_entity_decode($job_title);
                    $job_title = trim($job_title);
                    $job_id = explode(":", $jdata_exp[1]);
                    $job_id = trim($job_id[1]);
                    //print_r($job_id.",");
                    $country = explode(":", $jdata_exp[5]);
                    $country = $countries[trim($country[1])];
                    $category = explode(":", $jdata_exp[3]);
                    $category = trim($category[1]);
                    $enddate = explode(":", $jdata_exp[8]);
                    $enddate = date("Y-m-d", strtotime(trim($enddate[1])));
                    $source_url = "https://careers.un.org/lbw/jobdetail.aspx?id=".$job_id;
                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[id=ctl00_ContentPlaceHolder1_cJobDescription]',0)->find('div[id=jd_content]',0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "United Nations",
                                        "website" => "https://careers.un.org/lbw/Home.aspx",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        

                }
             }


        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
