<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class EricssonCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'ericsson:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(1548 / 10);
             for ($i=0; $i < $pagination; $i++) 
             {  
                $p = $i+1;
                //print_r($p.",");
                $html_data = HtmlDomParser::file_get_html("https://jobs.ericsson.com/api/jobs?page=".$p."&internal=false&userId=e3268ebe-10e3-4979-894c-f329ac767929&sessionId=b359574c-dce6-44db-8bdf-8f1788e90ae4&deviceId=2582553656&domain=ericsson.jibeapply.com");
                $job_data = json_decode($html_data,true);
                //dd($html_data);
                foreach ($job_data['jobs'] as $jobs) 
                {                  
                    $postdate = $jobs['data']['posted_date'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = trim($jobs['data']['req_id']);
                        //print_r( $job_id .",");
                        $category = $jobs['data']['category'];
                        if(empty($category))
                        {
                            $category = "";
                        }
                        else
                        {
                            $category = trim($jobs['data']['categories'][0]['name']);                           
                        }
                        $job_title = trim($jobs['data']['title']);
                        $country = addslashes(trim($jobs['data']['country']));
                        $source_url = $jobs['data']['meta_data']['canonical_url'];                        
                        $desc = $jobs['data']['description'];
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);

                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Ericsson",
                                        "website" => "https://www.ericsson.com/en/careers",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                        
                    }
                    
                }
             }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
