<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class KmdCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'kmd:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
                $current_date = date("Y-m-d");
                $dom = new HtmlDomParser();
                $dom = HtmlDomParser::file_get_html('https://www.kmd.net/career/available-jobs');
                $job_details = $dom->find('div[class=container]',0)->find('div[class=section-inner]',0)->find('div[class=vacancy-item]');
                foreach ($job_details as $key => $job) 
                {
                   $job_urls = $job->find('div[class=vacany-jobtitle] a',0)->href;
                   //print_r($job_urls.",");
                   $job_url = explode("=", $job_urls);
                   $job_url = $job_url[3];
                   //print_r($job_url); 
                   $job_title = $job->find('div[class=vacany-jobtitle] a',0)->innertext;
                   $close_on = $job->find('div[class=vacancy-applicationdeadline]',0)->innertext;
                   $close_on = date('Y-m-d', strtotime($close_on)); 
                   
                   $description = HtmlDomParser::file_get_html('https://career5.successfactors.eu/career?career_ns=job_listing&company=kmd&career_job_req_id='.$job_url);
                   if($description != FALSE)
                   {
                      $desc_spec = $description->find('div[id=jobAppPageTitle]',0);

                      $job_id = $desc_spec->find('div[tabindex=0]',0)->find('b',0)->innertext;
                      $reference_id = $job_id;
                      //--print_r($reference_id.",");
                      $posted_on = $desc_spec->find('div[tabindex=0]',0)->find('b',1)->innertext;
                      $posted_on = str_replace("/", "-", $posted_on);
                      $posted_on = date('Y-m-d', strtotime($posted_on)); 
                      
                      
                      $category = ($desc_spec->find('div[tabindex=0]',0)->find('b',3)) ? 'Truthy Value' : 'Falsey Value';
                      if($category == 'Falsey Value')
                      {
                         $category = '';
                      }
                      else
                      {
                         $category = $desc_spec->find('div[tabindex=0]',0)->find('b',3)->innertext;
                      }

                      $country = 'Denmark';
                      
                      $desc = $description->find('div[class=content]',0)->find('div[class=joqReqDescription]',0)->innertext;
                      $job_description = preg_replace('/\s+/', ' ', $desc);
                      $job_description = addslashes($job_description);
                      
                     
                   }
                   if(strtotime($current_date) == strtotime($posted_on))
                   {
                     $row = Job::where('job_id', $job_id)->count();
                     if($row == 0)
                     {  
                            $insert_data = [
                              "company" => "Kmd",
                              "website" => "https://www.kmd.net/",
                              "job_title" => $job_title,
                              "posted_on"=> $posted_on,
                              "category" => $category,
                              "country" => $country,
                              "description" => $job_description,
                              "job_id" => $job_id,
                              "reference_id" => $reference_id,
                              "contact_name"=>'',
                              "contact_email"=>'',
                              "contact_phone"=>'',
                              "source_url" => $job_urls,
                              "close_on" => $close_on,
                              "experience_from" => 0,
                              "experience_to" => 0,
                              "job_type"=>1,
                              "points"=>0,
                              "keywords"=>'',
                              "keyword_ids"=>'',
                              "keyword_points"=>'',
                              "rating_types"=>'',
                              "rating_points"=>'',
                              "status"=>0,
                              "created_at"=>date("Y-m-d H:i:s"),
                              "updated_at"=>date("Y-m-d H:i:s")                                                          
                            ];

                           Job::insert($insert_data);
                     }
                  }
                  else
                  {
                     break;
                  }       
                }                
                          
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
