<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SwecoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'sweco:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $pagination = ceil(40 / 12);
             for ($i=0; $i < $pagination; $i++) 
             {

                 $html_data = HtmlDomParser::file_get_html("https://careers.sweco.co.uk/vacancies.aspx?txtLocation=&lstLocation=&txtKeywords=&chkCategory=&lstRegion=&chkSalary=&lstDepartment=&optMatch=&chkDivision=&PageNo=".$i."&AttachedSAF=0&chkdepartment=&chkEmploymentType=&cmbSearchwithin=&txtpostcode=");
                 $job_data = $html_data->find('div[class=JT-column]',0)->find('div[class=JT-ui JT-cards]',0)->find('div[class=JT-ui JT-fluid JT-card]');
                 foreach ($job_data as $key => $jobs) 
                 {
                    $source_url = $jobs->find('div[class=JT-content]',0)->find('a',0)->href;
                    $source_url = "https://careers.sweco.co.uk/".$source_url;
                    $jobid_exp = explode("?jobid=", $source_url);
                    $job_id = trim($jobid_exp[1]);
                    print_r($job_id.",");
                    $job_title = trim($jobs->find('div[class=JT-content]',0)->find('a',0)->innertext);
                    $country = "United Kingdom";
                    $enddate =  $jobs->find('div[class=JT-content]',0)->find('div[class=JT-ui JT-three JT-stackable JT-cards]',0)->find('div[class=JT-description]',0)->find('p[class=jobdetailsitem closingdate]',0)->innertext;
                    $enddate = explode("</strong>", $enddate);
                    $enddate = date("Y-m-d", strtotime(trim($enddate[1])));
                    $category = $jobs->find('div[class=JT-content]',0)->find('div[class=JT-ui JT-three JT-stackable JT-cards]',0)->find('div[class=JT-ui JT-card JT-no-border]',2)->find('div[class=JT-description]',0)->find('p[class=jobdetailsitem division]',0)->innertext;
                    $category = explode("</strong>", $category);
                    $category = trim($category[1]);
                    $jobdesc = HtmlDomParser::file_get_html($source_url);
                    $desc = $jobdesc->find('div[class=JT-column JT-intro-copy JT-padded]',0)->find('div[id=JobDescription]',0)->innertext;
                    $job_desc = preg_replace('/\s+/', ' ', $desc);
                    $job_desc = addslashes($job_desc);
                    $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Sweco",
                                        "website" => "https://careers.sweco.co.uk/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }        
                    
                 }
              
             }
             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
