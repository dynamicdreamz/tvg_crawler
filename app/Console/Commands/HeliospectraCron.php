<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class HeliospectraCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'heliospectra:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {      
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-1 month", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://www.heliospectra.com/about-heliospectra/#career");
             $job_data = $html_data->find('div[id="career"]',0)->find('div[class=layout__section]',2)->find('div[class=layout__row]',0)->find('div[class=layout__col layout__col--half]',1)->find('ul[class=widget__list widget__box]',0)->find('li');
             
             for ($i=0; $i <count($job_data) ; $i++) 
             { 
                $job_id = "57912".$i;
                //print_r($job_id.",");
                $source_url = $job_data[$i]->find('a',0)->href;
                $job_title = $job_data[$i]->find('a',0)->innertext;
                $job_title = trim($job_title);
                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('div[id=main]',0)->find('div[class=layout__section news-article]',0)->find('div[class=layout__inner]',0)->find('div[class=layout__col]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);
                $job_desc = html_entity_decode($job_desc);
                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Heliospectra",
                                        "website" => "https://www.heliospectra.com/about-heliospectra/#career",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => '',
                                        "country" => '',
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                      //print_r($insert_data);
                                      Job::insert($insert_data);                
                            }   
             }           

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
