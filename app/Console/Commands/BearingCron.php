<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BearingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'Bearing:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                  "AT"=>"Austria",
                  "BE"=>"Belgium",
                  "CN"=>"China",
                  "DK"=>"Denmark",
                  "FI"=>"Finland",
                  "FR"=>"France",
                  "DE"=>"Germany",
                  "IE"=>"Ireland",
                  "IT"=>"Italy",
                  "LU"=>"Luxembourg",
                  "MA"=>"Morocco",
                  "NL"=>"Netherlands",
                  "NO"=>"Norway",
                  "PL"=>"Poland",
                  "RO"=>"Romania",
                  "RU"=>"Russia",
                  "SG"=>"Singapore",
                  "SE"=>"Sweden",
                  "CH"=>"Switzerland",
                  "TR"=>"Turkey",
                  "UA"=>"Ukraine",
                  "AE"=>"United Arab Emirates",
                  "GB"=>"United Kingdom",
                  "US"=>"United States"
              ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($countries as $key => $cuntry) 
             {
                 $html_data = HtmlDomParser::file_get_html("https://www.bearingpoint.com/en-africa/our-people/careers/open-roles/?country=".$key);
                 $job_data = $html_data->find('div[class=main-wrapper]',5)->find('div[class=jobs]',0)->find('a');
                 foreach ($job_data as $key => $jobs) 
                 {
                     $url = $jobs->href;
                     $source_url = "https://www.bearingpoint.com".$url;
                     $jobid_exp = explode("=", $url);
                     $job_id = end($jobid_exp);
                     //print_r($job_id.",");
                     $job_title = $jobs->find('div[class=row]',0)->find('div[class=columns job-title]',0)->find('h3',0)->innertext;
                     $job_title = trim($job_title);
                     $country = $cuntry; 
                     $jobdesc = HtmlDomParser::file_get_html($source_url);
                     $desc = $jobdesc->find('section[class=content-section success-factors]',0)->find('div[class=main-wrapper]',0)->find('div[class=inner-wrapper]',0)->innertext;
                     $job_desc = preg_replace('/\s+/', ' ', $desc);
                     $job_desc = addslashes($job_desc);
                     
                     $row = Job::where('job_id', $job_id)->count();  
                        if($row == 0)
                        {
                              $insert_data = [
                                "company" => "Bearingpoint",
                                "website" => "https://www.bearingpoint.com/en/our-people/careers/",
                                "job_title" => $job_title,
                                "posted_on"=> Null,
                                "category" => '',
                                "country" => $country,
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
                 }
             }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
