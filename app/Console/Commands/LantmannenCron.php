<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class LantmannenCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'lantmannen:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                       "204"=>"Sweden",
                       "67"=>"Estonia",
                       "58"=>"Denmark",
                       "72"=>"Finland",
                       "160"=>"Norway",
                       "224"=>"United Kingdom",
                       "21"=>"Belgium",
                       "150"=>"Nederland"
                    ];
             
             $categories = [
                  "764"=>"Sales",
                  "766"=>"Maintenance",
                  "770"=>"Production/Operation",
                  "773"=>"Research & Development",
                  "777"=>"Administration/Support/Service",
                  "778"=>"Logistics",
                  "780"=>"Customer Services",
                  "781"=>"IT/Telecom",
                  "784"=>"Quality/Environment/Sustainability",
                  "790"=>"Human Resources"
             ];

             $totaljob = [
                  "764"=>"12",
                  "766"=>"6",
                  "770"=>"6",
                  "773"=>"6",
                  "777"=>"6",
                  "778"=>"6",
                  "780"=>"12",
                  "781"=>"6",
                  "784"=>"6",
                  "790"=>"6"
             ];
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             foreach ($categories as $key => $job)
             {
                 $jobtotal = $totaljob[$key];
                 $pagination = ceil($jobtotal / 6);
                 //print_r( $pagination.",");
                 for ($i=1; $i <= 2; $i++) 
                 {               
                    $html_data = HtmlDomParser::file_get_html("https://lantmannen.com/en/career/find-my-next-job/?page=".$i."&c3=".$key);
                    $job_data = $html_data->find('div[class=list-wrapper list-onecolumn]',0)->find('div[class=easy-item none]');
                    foreach ($job_data as $keys => $jobs) 
                    {
                        $url = $jobs->find('a',0)->href;
                        $source_url = "https://lantmannen.com".$url;
                        $cntry = 'data-country';
                        $countryid = $jobs->find('div[class=career-item__body]',0)->$cntry;
                        if($countryid == '')
                        {
                           $country = "";
                        } 
                        else
                        {
                           $country = $countries[trim($countryid)];

                        }
                        $jcode = "data-organization";
                        $jobid = $jobs->find('div[class=career-item__body]',0)->$jcode;
                        $job_id = trim( $jobid );
                        //print_r($job_id.",");

                        $job_title = $jobs->find('a',0)->innertext;
                        $job_title = html_entity_decode( $job_title );

                        $category = $job;
                        
                        if($job_id == '61141')
                        {
                           $enddate = Null;
                        }
                        else
                        {   
                            if($job_id == '24746')
                            {
                               $enddate = Null;
                            }
                            else
                            {
                                $enddate = $jobs->find('div[class=easy-item__deadline]',0)->find('span[class=easy-item__data]',0)->innertext;
                                $enddate = str_replace("/", "-", $enddate);
                                $enddate = date("Y-m-d", strtotime($enddate));
                            }
                           
                        }

                        $jobdesc = HtmlDomParser::file_get_html($source_url);
                        $desc = $jobdesc->find('section[id=main]',0)->find('div[class=container]',0)->find('div[class=careeritem]',0)->find('div[class=careeritem-content]',0)->innertext;
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);
                        
                        
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Lantmannen",
                                        "website" => "https://lantmannen.com/en/career/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                        

                        
                     } 
                                       
                 }
                 
                 
             }
           
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
