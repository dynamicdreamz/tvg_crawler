<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class SsabCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'ssab:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        set_time_limit(0);
        date_default_timezone_set('Asia/Kolkata');
        $ist = date("Y-m-d g:i:s");
        $this->date_IST = date("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $insert_data = array();
            $brk = '';
            $current_date = date("Y-m-d");
            $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
            $dom = new HtmlDomParser();

            $html_data = HtmlDomParser::file_get_html("https://www.ssab.com/company/careers/job-opportunities");
            $job_data = $html_data->find('div[class=main-content-wrapper frame ssab-grey-15]', 0)->find('div[class=row]', 0)->find('div[class=block-container col-sm-12]', 0)->find('div[class=col-sm-12]', 0)->find('div[class=filter-row]');

            foreach ($job_data as $key => $value) {

                $url = $job_data[$key]->find('div[class=col-sm-2]', 0)->find('div[class=vacancy-data]', 0)->find('a', 0)->href;
                $source_url = "https://www.ssab.com" . $url;

                $job_title = $job_data[$key]->find('div[class=col-sm-2]', 0)->find('div[class=vacancy-data]', 0)->find('a', 0)->innertext;
                $job_title = html_entity_decode($job_title);
                $jobid = explode("/", $url);
                $jobid = explode("-", $jobid[5]);
                $job_id = end($jobid);

                $category = $job_data[$key]->find('div[class=col-sm-2 filter-col-job-area]', 0)->find('div[class=vacancy-data]', 0)->innertext;
                $category = explode("</h3>", trim($category));
                $category = trim($category[1]);

                $country = $job_data[$key]->find('div[class=col-sm-2 filter-col-country]', 0)->find('div[class=vacancy-data]', 0)->innertext;
                $country = explode("</h3>", $country);
                $country = trim($country[1]);

                $enddate = $job_data[$key]->find('div[class=col-sm-1]', 0)->find('div[class=vacancy-data]', 0)->innertext;
                $enddate = explode("</h3>", $enddate);
                $enddate = trim($enddate[1]);
                $enddate = date("Y-m-d", strtotime($enddate));

                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('div[class=main-content-wrapper frame ssab-grey-15]', 0)->find('div[class=row]', 0)->find('div[class=block-container col-sm-12]', 0)->find('div[class=standard-page-content-wrapper col-sm-12]', 0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);

                $row = Job::where('job_id', $job_id)->count();
                if ($row == 0) {
                    $insert_data = [
                        "company" => "SSAB",
                        "website" => "https://www.ssab.com/company/careers/job-opportunities/",
                        "job_title" => $job_title,
                        "posted_on" => Null,
                        "category" => $category,
                        "country" => $country,
                        "description" => $job_desc,
                        "job_id" => $job_id,
                        "reference_id" => '',
                        "contact_name" => '',
                        "contact_email" => '',
                        "contact_phone" => '',
                        "source_url" => $source_url,
                        "close_on" => $enddate,
                        "experience_from" => 0,
                        "experience_to" => 0,
                        "job_type" => 1,
                        "points" => 0,
                        "keywords" => '',
                        "keyword_ids" => '',
                        "keyword_points" => '',
                        "rating_types" => '',
                        "rating_points" => '',
                        "status" => 0,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s")
                    ];


//                    print_r($insert_data);
                    Job::insert($insert_data);
                }

            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
