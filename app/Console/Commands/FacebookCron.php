<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class FacebookCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'facebook:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'Altoona'=>'United States',
                    'Ashburn'=>'United States',
                    'Atlanta'=>'United States',
                    'Austin'=>'United States',
                    'Boston'=>'United States',
                    'Chicago'=>'United States',
                    'Dallas'=>'United States',
                    'Denver'=>'United States',
                    'Detroit'=>'United States',
                    'Eagle Mountain'=>'United States',
                    'Forest City'=>'United States',
                    'Fort Worth'=>'United States',
                    'Fremont'=>'United States',
                    'Henrico'=>'United States',
                    'Huntsville'=>'United States',
                    'Los Angeles'=>'United States',
                    'Los Lunas'=>'United States',
                    'Menlo Park'=>'United States',
                    'Miami'=>'United States',
                    'Montreal'=>'United States',
                    'Mountain View'=>'United States',
                    'New Albany'=>'United States',
                    'New York'=>'United States',
                    'Newton County'=>'United States',
                    'Northridge'=>'United States',
                    'Papillion'=>'United States',
                    'Pittsburgh'=>'United States',
                    'Prineville'=>'United States',
                    'Redmond'=>'United States',
                    'San Francisco'=>'United States',
                    'Santa Clara'=>'United States',
                    'Sausalito'=>'United States',
                    'Seattle'=>'United States',
                    'Sunnyvale'=>'United States',
                    'Toronto'=>'United States',
                    'Vancouver'=>'United States',
                    'Washington'=>'United States',
                    'Woodland Hills'=>'United States',
                    'Amsterdam'=>'Netherlands',
                    'Berlin'=>'Germany',
                    'Brussels'=>'Belgium',
                    'Clonee'=>'Ireland',
                    'Copenhagen'=>'Denmark',
                    'Cork'=>'Ireland',
                    'Dubai'=>'United Arab Emirates',
                    'Dublin'=>'Ireland',
                    'Geneva'=>'Switzerland',
                    'Hamburg'=>'Germany',
                    'Johannesburg'=>'South Africa',
                    'London'=>'United Kingdom',
                    'Luleå'=>'Sweden',
                    'Madrid'=>'Spain',
                    'Milan'=>'Italy',
                    'Odense'=>'Denmark',
                    'Oslo'=>'Norway',
                    'Paris'=>'France',
                    'Stockholm'=>'Sweden',
                    'Tel Aviv'=>'Israel',
                    'Warsaw'=>'Poland',
                    'Zurich'=>'Switzerland',
                    'Auckland'=>'New Zealand',
                    'Bangalore'=>'India',
                    'Bangkok'=>'Thailand',
                    'Gurgaon'=>'India',
                    'Hong Kong'=>'Hong Kong',
                    'Hyderabad'=>'India',
                    'Jakarta'=>'Indonesia',
                    'Kuala Lumpur'=>'Malaysia',
                    'Manila'=>'Philippines',
                    'Melbourne'=>'Australia',
                    'Mumbai'=>'India',
                    'New Delhi'=>'India',
                    'Seoul'=>'South Korea',
                    'Shanghai'=>'China',
                    'Singapore'=>'Singapore',
                    'Sydney'=>'Australia',
                    'Taipei'=>'Taiwan',
                    'Tokyo'=>'Japan',
                    'Bogotá'=>'Colombia',
                    'Brasilia'=>'Brazil',
                    'Buenos Aires'=>'Argentina',
                    'Mexico City'=>'Mexico',
                    'São Paulo'=>'Brazil',
                    ];

             // $categorie = array("Advertising Technology","Business Development & Partnerships","Communications & Public Policy","Creative","Data & Analytics","Design & User Experience","Enterprise Engineering","Facebook Reality Labs","Infrastructure","Instagram","Internship - Business","Internship - Engineering, Tech & Design","Internship - PhD","Legal, Finance, Facilities & Admin","Oculus");

             $categories = ["Advertising%20Technology"=>"Advertising Technology","Business%20Development%20%26%20Partnerships"=>"Business Development & Partnerships","Communications%20%26%20Public%20Policy"=>"Communications & Public Policy","Creative"=>"Creative","Data%20%26%20Analytics"=>"Data & Analytics","Design%20%26%20User%20Experience"=>"Design & User Experience","Enterprise%20Engineering"=>"Enterprise Engineering","Facebook%20Reality%20Labs"=>"Facebook Reality Labs","Infrastructure"=>"Infrastructure","Instagram"=>"Instagram","Internship%20-%20Business"=>"Internship - Business","Internship%20-%20Engineering%2C%20Tech%20%26%20Design"=>"Internship - Engineering, Tech & Design","Internship%20-%20PhD"=>"Internship - PhD","Legal%2C%20Finance%2C%20Facilities%20%26%20Admin"=>"Legal, Finance, Facilities & Admin",'Oculus'=>"Oculus","Online%20Operations"=>"Online Operations","People%20%26%20Recruiting"=>"People & Recruiting","Portal"=>"Portal","Product%20Management"=>"Product Management","Research"=>"Research","Rotational%20Engineering%20Program"=>"Rotational Engineering Program","Sales%20%26%20Marketing"=>"Sales & Marketing","Security"=>"Security","Social%20VR"=>"Social VR","Software%20Engineering"=>"Software Engineering","Technical%20Program%20Management"=>"Technical Program Management","University%20Grad%20-%20Business"=>"University Grad - Business","University%20Grad%20-%20Engineering%2C%20Tech%20%26%20Design"=>"University Grad - Engineering, Tech & Design","University%20Grad%20-%20PhD%20%26%20Postdoc"=>"University Grad - PhD & Postdoc","WhatsApp"=>"WhatsApp"];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             //$pagination = ceil(2811 / 10);
             foreach ($categories as $key => $value) 
             {
                 //print_r($value.",");
             	 $ch = curl_init('https://www.facebook.com/careers/search/filter/?search_result_section_id=search_result&page=1&teams[0]='.$key);
	             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                    'content-type: application/x-www-form-urlencoded',
	                    'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
	                ));
	             curl_setopt($ch, CURLOPT_POSTFIELDS, '__user=0&__a=1&__dyn=7xe6Fo4OQ1PyUhxOnFwn84a2i5U4e0yoW3q327E2vwXx61rwf24o29wmU1upE4W0OE2WxO2u0Io5u1Aw60KdwnU1oU88&__req=b&__be=1&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1000730275&__s=%3Ad231b8%3Al1dju5&lsd=AVqlAodG&jazoest=2719');

	             $response = curl_exec($ch);
	             curl_close($ch);
	             $result = json_encode($response, true);
	             $gg = stripslashes($result);
	             $first10chars = substr($gg, +10);
	             $last1chars = substr($first10chars, 0, -1);
					// $dd =  htmlspecialchars_decode($last1chars);
					// dd($dd);
	             $results = json_decode($last1chars, true);
	             $ddddd = html_entity_decode( html_entity_decode($results['domops'][0][3]['__html']));
	             $html_encode = str_replace("u003E", ">", $ddddd);

	             $myfile = fopen("/var/www/html/cronhtmlfile/FacebookContent.html", "w") or die("Unable to open file!");
	             $txt = $html_encode;
	             fwrite($myfile, $txt);
                 
	             $html_data = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/FacebookContent.html');
	             $pagina = $html_data->find('div[id=search_result]',0)->find('div[class=_6ci_]',0)->innertext;
	             $pagina_exp = explode(" ", $pagina);
	             $pagination = trim($pagina_exp[1]);
	             $pagination = ceil($pagination / 10);
	             for ($i=1; $i <= 3 ; $i++) 
	             { 
	             	 //print_r($i.",");
	             	 $ch = curl_init('https://www.facebook.com/careers/search/filter/?page='.$i.'&search_result_section_id=search_result&teams[0]='.$key);
		             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		             curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		                    'content-type: application/x-www-form-urlencoded',
		                    'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
		                ));
		             curl_setopt($ch, CURLOPT_POSTFIELDS, '__user=0&__a=1&__dyn=7xe6Fo4OQ1PyUhxOnFwn84a2i5U4e0yoW3q327E2vwXx61rwf24o29wmU1upE4W0OE2WxO2u0Io5u1Aw60KdwnU1oU88&__req=b&__be=1&__pc=PHASED%3ADEFAULT&dpr=1&__rev=1000730275&__s=%3Ad231b8%3Al1dju5&lsd=AVqlAodG&jazoest=2719');

		             $response = curl_exec($ch);
		             curl_close($ch);
		             $result = json_encode($response, true);
		             $gg = stripslashes($result);
		             $first10chars = substr($gg, +10);
		             $last1chars = substr($first10chars, 0, -1);
					 // $dd =  htmlspecialchars_decode($last1chars);
					 // dd($dd);
		             $results = json_decode($last1chars, true);
		             $ddddd = html_entity_decode( html_entity_decode($results['domops'][0][3]['__html']));
		             $html_encode = str_replace("u003E", ">", $ddddd);

		             $myfiles = fopen("/var/www/html/cronhtmlfile/FacebookContent1.html", "w") or die("Unable to open file!");
		             $txts = $html_encode;
		             fwrite($myfiles, $txts);

		             $html_data = HtmlDomParser::file_get_html('/var/www/html/cronhtmlfile/FacebookContent1.html');
		             $jobs = $html_data->find('div[id=search_result]',0)->find('a[class=_69jm]');
		             foreach ($jobs as $job) 
		             {
		             	 $source_url = $job->href;
		             	 $source_url = "https://www.facebook.com".$source_url;
		             	 $jobid_exp = explode("/", $source_url);
		             	 $job_id = $jobid_exp[5];
		             	 //print_r($job_id.",");
		             	 $title = $job->find('div[class=_69jn]',0)->find('div[class=_69jo]',0)->innertext;
		             	 $title_exp = explode("<i", $title);
		             	 $job_title = $title_exp[0];
		             	 $category = $categories[$key];
		             	 $location = $job->find('div[class=_69jn]',0)->find('div[class=_3gel _3gfe _3gef _3gee]',0)->find('div[class=_3m9]',0)->innertext;
                        // dd( $location );
		             	 $location_exp = explode(",", $location);
		             	 if($job_id == '2244161488976062')
		             	 {
                            $country = "";
		             	 }
		             	 else
		             	 {
		             	    $country = $countries[trim($location_exp[0])];
                         }
		             	 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Facebook",
                                        "website" => "https://www.facebook.com/careers/jobs",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => '',
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                           
		             	 
		             }

		             $fh = fopen('/var/www/html/cronhtmlfile/FacebookContent1.html', 'w' );
                     fclose($fh);

	             	 
	             }

	              $fh = fopen('/var/www/html/cronhtmlfile/FacebookContent.html', 'w' );
                  fclose($fh);
            	
             }

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
