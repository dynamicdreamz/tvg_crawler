<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class KryCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'kry:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "33130"=>"England",
                      "7817"=>"Norway",
                      "34729"=>"Germany",
                      "51458"=>"Sweden",
                      "35044"=>"Sweden"
                   ];

              $categories = [
                  "16515"=>"Clinicians",

              ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $ch = curl_init('https://api.teamtailor.com/v1/jobs?api_version=20161108&include=department,locations&fields[departments]=name&fields[locations]=name,city&page[size]=10&filter[feed]=public&filter[department]=16515&api_key=xry3OY11CQiOgPLBNW8uVM8dhr6malxCtAjIBOFB&');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.90 Safari/537.36',
                ));
                $response = curl_exec($ch);
                $result = json_decode($response,true);

                foreach ($result['data'] as $jobs) 
                {
                   $job_id = $jobs['id'];
                   //print_r($job_id.",");
                   $job_title = $jobs['attributes']['title'];
                   $category = $jobs['relationships']['department']['data']['id'];
                   $category = $categories[trim($category)];
                   if(empty($jobs['relationships']['locations']['data']))
                   {
                      $country = "";
                   }
                   else
                   {
                      $country = $jobs['relationships']['locations']['data'][0]['id'];
                      $country = $countries[trim($country)];
                   }
                   
                   $source_url = $jobs['links']['careersite-job-url'];
                   $jobdesc = HtmlDomParser::file_get_html($source_url);
                   $desc = $jobdesc->find('div[class=job]',0)->find('div[class=wrapper]',0)->find('div[class=body u-margin-top--medium u-primary-text-color]',0)->innertext;
                   $job_desc = preg_replace('/\s+/', ' ', $desc);
                   $job_desc = addslashes($job_desc);
                   $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Kry",
                                        "website" => "https://www.kry.se/en/for-kliniker/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }

                }
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
