<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BakermckenzieCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'bakermckenzie:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             
             $pagination = ceil(357 / 9);
             for ($i=1; $i <= 3 ; $i++) 
             { 
               $p = $i * 9;
               print_r($p.",");
               $postdata = '{"IsAlphaSort":false,"Skip":'.$p.',"Take":9,"InitialPageSize":9,"ReloadFilters":false,"KeywordFilter":"","UseFallback":false,"PracticeFilters":[],"IndustryFilters":[],"AsiaPacificFilters":{"ID":"b3875503-8bac-4c09-b959-d37349c9318e","Name":"Asia Pacific","Url":null,"IsSelected":false,"IsEnabled":true,"IsSelectable":true,"IsDisplayed":true,"CustomTitle":null,"TemplateId":"f32c1cb2-8eee-4845-8df6-358e3794d6b4","Filter":[]},"EMEAFilters":{"ID":"d564c2c5-fcb5-48c7-87ab-e84a00aeeff8","Name":"Europe, Middle East and Africa","Url":null,"IsSelected":false,"IsEnabled":true,"IsSelectable":true,"IsDisplayed":true,"CustomTitle":null,"TemplateId":"f32c1cb2-8eee-4845-8df6-358e3794d6b4","Filter":[]},"LatinAmericaFilters":{"ID":"280ed807-4fba-4e2f-8f3b-f1bee2671749","Name":"Latin America","Url":null,"IsSelected":false,"IsEnabled":true,"IsSelectable":true,"IsDisplayed":true,"CustomTitle":null,"TemplateId":"f32c1cb2-8eee-4845-8df6-358e3794d6b4","Filter":[]},"NorthAmericaFilters":{"ID":"b5eb49ce-aa94-4658-bcd9-001b1fb80f4f","Name":"North America","Url":null,"IsSelected":false,"IsEnabled":true,"IsSelectable":true,"IsDisplayed":true,"CustomTitle":null,"TemplateId":"f32c1cb2-8eee-4845-8df6-358e3794d6b4","Filter":[]},"TypeFilters":[],"Officefilters":[],"MobileFilterByField":"keyword","MobileFilterByFieldLabel":"Keyword"}';
                  //dd($postdata);
               $ch = curl_init('https://www.bakermckenzie.com/en/api/sitecore/careers/search');
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'X-Requested-With: XMLHttpRequest',
                      'Content-Type: application/json; charset=UTF-8'
                  ));
               curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

               $result = curl_exec($ch);

               $response = json_decode($result,TRUE);
               foreach($response['GridData'] as $key => $jobs) 
               {
                   $posted_date = date('Y-m-d', strtotime($jobs['PostedDate']));
                   if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                   {
                      $job_id = $jobs['ID'];
                      //print_r($job_id.",");
                      $job_title = $jobs['Title'];
                      $category = $jobs['Level'];
                      $source_url = $jobs['NavigateLink']['Url'];
                      $jobdesc = HtmlDomParser::file_get_html($source_url);
                      $desc = $jobdesc->find('section[class=two-column-page-wrapper]',0)->find('div[class=page-wrapper]',0)->find('div[class=main-column has-right-column js-border-adjust]',0)->innertext;
                      $job_desc = preg_replace('/\s+/', ' ', $desc);
                      $job_desc = addslashes($job_desc);

                      $country = $jobdesc->find('section[class=two-column-page-wrapper]',0)->find('aside[class=right-column]',0)->find('section[class=related-content-module link-list-content-module global-module]',1)->find('div[class=related-list]',0)->find('ul[class=related-list-container]',0)->find('li[class=related-list-item]',0)->find('a',0)->find('span',0)->innertext;
                      $country = trim($country);

                      if($country == 'Belfast Center')
                      {
                         $country = 'United Kingdom';
                      }
                      else
                      {
                         $country = $country;

                      }
                      $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Bakermckenzie",
                                        "website" => "https://www.bakermckenzie.com/en/careers",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => '',
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }

                   }
                   else
                   {
                      $brk = "error";
                   }
                  
               }

               if($brk == 'error')
                {
                    break;
                }
             }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
