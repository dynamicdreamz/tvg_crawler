<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class BcgCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'bcg:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = ["35972779"=>"Angola","35972756"=>"Argentina","35972746"=>"Australia","35972763"=>"Austria","35972749"=>"Belgium","35972766"=>"Brazil","35972750"=>"Canada","35972771"=>"Chile","35972747"=>"China","35972777"=>"Colombia","35972769"=>"Czech Republic","35972767"=>"Denmark","35972738"=>"England","35972757"=>"Finland","35972739"=>"France","35972740"=>"Germany","35972768"=>"Greece","35972764"=>"Hungary","35972761"=>"India","35972760"=>"Indonesia","35972775"=>"Israel","35972741"=>"Italy","35972737"=>"Japan","37265164"=>"Kazakhstan","35972748"=>"Malaysia","35972751"=>"Mexico","35972774"=>"Morocco","35972752"=>"Netherlands","35972745"=>"New Zealand","35972781"=>"Nigeria","35972762"=>"Norway","35972782"=>"Peru","37034921"=>"Philippines","35972765"=>"Poland","35972758"=>"Portugal","35972754"=>"Russia","35972780"=>"Saudia Arabia","35972759"=>"Singapore","35972776"=>"South Africa","35972753"=>"South Korea","35972742"=>"Spain","35972743"=>"Sweden","35972744"=>"Switzerland","35972770"=>"Taiwan","35972755"=>"Thailand","35972773"=>"Turkey","35972772"=>"United Arab Emirates","35972736"=>"United States","35972778"=>"Vietnam"];

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();
             $countrys = array("35972779","35972756","35972746","35972763","35972749","35972766","35972750","35972771","35972747","35972777","35972769","35972767","35972738","35972757","35972739","35972740","35972768","35972764","35972761","35972760","35972775","35972741","35972737","37265164","35972748","35972751","35972774","35972752","35972745","35972781","35972762","35972782","37034921","35972765","35972758","35972754","35972780","35972759","35972776","35972753","35972742","35972743","35972744","35972770","35972755","35972773","35972772","35972736","35972778");
             foreach ($countrys as $cutry_code) 
             {
                $arrContextOptions=array(
                        "ssl"=>array(
                            "verify_peer"=>false,
                            "verify_peer_name"=>false,
                        ),
                    );  
                $url = 'https://talent.bcg.com/apply/SearchJobs/?3_131_3=%5B%22'.$cutry_code.'%22%5D';
                $html = HtmlDomParser::file_get_html($url,false,stream_context_create($arrContextOptions));
                // /dd($html);
                $job_data = $html->find('div[class=mainWrapper]',0)->find('main[id=mainContent]',0)->find('ul[class=listSingleColumn]',0)->find('li[class=listSingleColumnItem]',0)->innertext;
                if(trim($job_data) != 'No jobs found')
                {
                   $pageno = $html->find('div[class=mainWrapper]',0)->find('main[id=mainContent]',0)->find('div[class=listControls listControlsBottom autoClearer]',0)->find('div[class=listPagination]',0)->innertext;
                   if(trim($pageno) == "")
                   {
                       $job_details = $html->find('div[class=mainWrapper]',0)->find('main[id=mainContent]',0)->find('ul[class=listSingleColumn]',0)->find('li');
                       foreach ($job_details as $key => $jobs) 
                       {
                           $urls = $jobs->find('h3[class=listSingleColumnItemTitle]',0)->find('a',0)->href;
                           $source_url = trim($urls);

                           $jobid_exp = explode("/", $source_url);
                           $job_id = $jobid_exp[6];
                           

                           $job_title = $jobs->find('h3[class=listSingleColumnItemTitle]',0)->find('a',0)->innertext;
                           $job_title = trim($job_title);

                           $country = $countries[$cutry_code];

                           $refno = $jobs->find('p[class=listSingleColumnItemMiscData]',0)->find('span[class=listSingleColumnItemMiscDataItem]',1)->innertext;
                           $refno_exp = explode("#", trim($refno));
                           $refno_exp1 = explode(".", $refno_exp[1]);
                           $ref_no = $refno_exp1[0];

                           $job_details = HtmlDomParser::file_get_html($source_url,false,stream_context_create($arrContextOptions));
                           $job_detail = $job_details->find('div[class=detailArea-one jobDetail]',0)->innertext;
                           $job_desc = preg_replace('/\s+/', ' ', $job_detail);
                           $job_desc = addslashes($job_desc);

                           $postdate = $jobs->find('p[class=listSingleColumnItemMiscData]',0)->find('span',2)->innertext;
                           $postdate_exp = explode(" ", $postdate);
                           $posted_date = date('Y-m-d', strtotime($postdate_exp[1]));
                           if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                           {
                              // print_r($job_id.",");
                               $posted_on = $posted_date;
                               $row = Job::where('job_id', $job_id)->count();  
                               if($row == 0)
                               {
                                      $insert_data = [
                                        "company" => "Bcg",
                                        "website" => "https://www.bcg.com/careers/default.aspx",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_on,
                                        "category" => '',
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => $ref_no,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                                }
                                                   
                           }

                       }
                   }
                   else
                   {
                        $pageno = $html->find('div[class=mainWrapper]',0)->find('main[id=mainContent]',0)->find('div[class=listControls listControlsBottom autoClearer]',0)->find('div[class=listPagination]',0)->find('span[class=jobPaginationLegend]',0)->innertext;
                        $pageno_exp = explode(" ",$pageno);
                        $pagination = ceil($pageno_exp[3] / 20);
                        for ($i=0; $i < $pagination; $i++) 
                        { 
                           $p = $i*20;
                           $url = 'https://talent.bcg.com/apply/SearchJobs/?3_131_3=%5B%22'.$cutry_code.'%22%5D&folderOffset='.$p;
                           $html = HtmlDomParser::file_get_html($url,false,stream_context_create($arrContextOptions));
                           $job_data = $html->find('div[class=mainWrapper]',0)->find('main[id=mainContent]',0)->find('ul[class=listSingleColumn]',0)->find('li');
                           foreach ($job_data as $key => $job) 
                           {
                               $urls = $jobs->find('h3[class=listSingleColumnItemTitle]',0)->find('a',0)->href;
                               $source_url = trim($urls);

                               $jobid_exp = explode("/", $source_url);
                               $job_id = $jobid_exp[6];
                               //print_r($job_id.",");

                               $job_title = $jobs->find('h3[class=listSingleColumnItemTitle]',0)->find('a',0)->innertext;
                               $job_title = trim($job_title);

                               $refno = $jobs->find('p[class=listSingleColumnItemMiscData]',0)->find('span[class=listSingleColumnItemMiscDataItem]',1)->innertext;
                               $refno_exp = explode("#", trim($refno));
                               $refno_exp1 = explode(".", $refno_exp[1]);
                               $ref_no = $refno_exp1[0];

                               $job_details = HtmlDomParser::file_get_html($source_url,false,stream_context_create($arrContextOptions));
                               $job_detail = $job_details->find('div[class=detailArea-one jobDetail]',0)->innertext;
                               $job_desc = preg_replace('/\s+/', ' ', $job_detail);
                               $job_desc = addslashes($job_desc);

                               $postdate = $jobs->find('p[class=listSingleColumnItemMiscData]',0)->find('span',2)->innertext;
                               $postdate_exp = explode(" ", $postdate);
                               $posted_date = date('Y-m-d', strtotime($postdate_exp[1]));

                               if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                               {
                                  // print_r($job_id.",");
                                   $posted_on = $posted_date;
                                   $row = Job::where('job_id', $job_id)->count();  
                                   if($row == 0)
                                   {
                                          $insert_data = [
                                            "company" => "Bcg",
                                            "website" => "https://www.bcg.com/careers/default.aspx",
                                            "job_title" => $job_title,
                                            "posted_on"=> $posted_on,
                                            "category" => '',
                                            "country" => $country,
                                            "description" => $job_desc,
                                            "job_id" => $job_id,
                                            "reference_id" => $ref_no,
                                            "contact_name"=>'',
                                            "contact_email"=>'',
                                            "contact_phone"=>'',
                                            "source_url" => $source_url,
                                            "experience_from" => 0,
                                            "experience_to" => 0,
                                            "job_type"=>1,
                                            "points"=>0,
                                            "keywords"=>'',
                                            "keyword_ids"=>'',
                                            "keyword_points"=>'',
                                            "rating_types"=>'',
                                            "rating_points"=>'',
                                            "status"=>0,
                                            "created_at"=>date("Y-m-d H:i:s"),
                                            "updated_at"=>date("Y-m-d H:i:s")                                       
                                          ]; 
                                        //print_r($insert_data);
                                        Job::insert($insert_data);                
                                    }

                               }

                           }

                        }
                   }
                }
             }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
