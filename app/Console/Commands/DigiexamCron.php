<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class DigiexamCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'digiexam:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $html_data = HtmlDomParser::file_get_html("https://www.digiexam.com/career/");
                $job_data = $html_data->find('section[id=section-career-jobs]',0)->find('div[class=inner-content row]',0)->find('div[class=job-term-wrap medium-12 large-12 column]');
                foreach ($job_data as $jobs) 
                {
                   $category = $jobs->find('div[class=large-5 column]',0)->find('h3',0)->innertext;
                   $category = html_entity_decode(trim($category));
                   $job_list = $jobs->find('div[class=large-7 column]',0)->find('div[class=job-item content-block]');
                   foreach ($job_list as $key => $job) 
                   {
                       $job_title = $job->find('h4',0)->innertext;
                       $job_title = trim($job_title);
                       $category = $category;
                       $source_url = $job->find('a',0)->href;
                       $job_id = explode("/", $source_url);
                       $job_id = explode("-", $job_id[4]);
                       $job_id = trim($job_id[0]);
                       //print_r($job_id.",");
                       $job_desc = HtmlDomParser::file_get_html($source_url);
                       $jid = 'job-id-'.$job_id;
                       $desc = $job_desc->find('div[class='.$jid.']',0)->innertext;
                       $job_desc = preg_replace('/\s+/', ' ', $desc);
                       $job_desc = addslashes($job_desc);
                       $row = Job::where('job_id', $job_id)->count();  
                       if($row == 0)
                       {
                              $insert_data = [
                                "company" => "Digiexam",
                                "website" => "https://www.digiexam.com/career/",
                                "job_title" => $job_title,
                                "posted_on"=> Null,
                                "category" => $category,
                                "country" => 'Sweden',
                                "description" => $job_desc,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $source_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                                       
                              ]; 
                            //print_r($insert_data);
                            Job::insert($insert_data);                
                        }
                   }
                  
                    
                }

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
