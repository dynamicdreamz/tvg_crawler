<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class MindoktorCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'mindoktor:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                $html_data = HtmlDomParser::file_get_html("https://karriar.mindoktor.se/#page-block-11800");
                $job_data = $html_data->find('div[id=items]',0)->find('div[class=position-container filter-item]');
                
                foreach ($job_data as $jobs) 
                {
                   $url = $jobs->find('div[class=item col-md-12  box-shadow]',0)->find('div[class=row inner-row]',0)->find('div[class=inner-row-container col-md-12 col-xs-12 col-sm-12 col-lg-12]',0)->find('div[class=position-name]',0)->find('a',0)->href;
                   $source_url = "https://karriar.mindoktor.se".$url;
                   $job_title = $jobs->find('div[class=item col-md-12  box-shadow]',0)->find('div[class=row inner-row]',0)->find('div[class=inner-row-container col-md-12 col-xs-12 col-sm-12 col-lg-12]',0)->find('div[class=position-name]',0)->find('a',0)->innertext;
                   $job_title = trim($job_title);
                  
                   $job_id = explode("/",  $url );
                   $job_id = explode("-", $job_id[2]);
                   $job_id = $job_id[0];
                   //print_r($job_id.",");

                   $country = "Sweden";

                   if($job_id != 74344)
                   {
                     $enddate = $jobs->find('div[class=item col-md-12  box-shadow]',0)->find('div[class=row inner-row]',0)->find('div[class=inner-row-container col-md-12 col-xs-12 col-sm-12 col-lg-12]',0)->find('div[class=meta-container]',0)->find('div[class=date]',0)->find('span[class=last-apply-date]',0)->innertext;
                     $enddate = date("Y-m-d", strtotime($enddate));
                   }
                   else
                   {
                      $enddate = "";
                   }
                   
                   $jobdesc = HtmlDomParser::file_get_html($source_url);
                   $desc = $jobdesc->find('div[class=large wb-section position-description page-block-11797 block-odd]',0)->find('div[class=wb-block-inner]',0)->find('div[class=row]',0)->find('div[class=col-md-12]',0)->innertext;
                   $job_desc = preg_replace('/\s+/', ' ', $desc);
                   $job_desc = addslashes($job_desc);
                   $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Mindoktor",
                                        "website" => "https://karriar.mindoktor.se/",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => "",
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                   
                }

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
