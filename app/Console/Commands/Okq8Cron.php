<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class Okq8Cron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'okq8:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $html_data = HtmlDomParser::file_get_html("https://www.okq8.se/jobba-hos-okq8/lediga-jobb/");
              $job_data = $html_data->find('article[class=shared-module clean-top columns-one bg-color-white]',0)->find('div[class=inside-wrapper]',0)->find('div[class=crate-container]',0)->find('div[class=crate-content]',1)->find('div[class=text]',0)->find('p');
              for ($i=0; $i <count($job_data) ; $i++) 
              { 
                  $job_id = "04578".$i;
                  //print_r($job_id.",");
                  $url = $job_data[$i]->find('a',0)->href;
                  $source_url = "https://www.okq8.se".$url;
                  
                  $jobdesc = HtmlDomParser::file_get_html($source_url);
                  $desc = $jobdesc->find('article[class=shared-module clean-top columns-one bg-color-light-grey]',0)->find('div[class=inside-wrapper]',0)->find('div[class=crate-container]',0)->innertext;
                  $job_desc = preg_replace('/\s+/', ' ', $desc);
                  $job_desc = addslashes($job_desc);
                  $job_title = $jobdesc->find('article[class=shared-module no-height clean columns-one bg-color-white bg-position-y-top]',0)->find('div[class=inside-wrapper]',0)->find('div[class=crate-container]',0)->find('div[class=crate center-text hero-top]',0)->find('div[class=headline]',0)->find('h1',0)->innertext;
                  $job_title = html_entity_decode(trim($job_title));
                  $country = "Sweden";
                  $enddate =  $jobdesc->find('article[class=shared-module no-height clean columns-one bg-color-white bg-position-y-top]',0)->find('div[class=inside-wrapper]',0)->find('div[class=crate-container]',0)->find('div[class=crate center-text hero-top]',0)->find('p',1)->innertext;
                  $enddate = explode("</strong>", $enddate);
                  $enddate = trim($enddate[1]);
                  $enddate = str_replace("&nbsp;", "", $enddate);
                  $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Okq8",
                                        "website" => "https://www.okq8.se/jobba-hos-okq8/lediga-jobb/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => "",
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "close_on"=>$enddate,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                 
                  
              }
             
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
