<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class ViasatCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'viasat:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                    'AF' => 'Afghanistan',
                    'AX' => 'Aland Islands',
                    'AL' => 'Albania',
                    'DZ' => 'Algeria',
                    'AS' => 'American Samoa',
                    'AD' => 'Andorra',
                    'AO' => 'Angola',
                    'AI' => 'Anguilla',
                    'AQ' => 'Antarctica',
                    'AG' => 'Antigua And Barbuda',
                    'AR' => 'Argentina',
                    'AM' => 'Armenia',
                    'AW' => 'Aruba',
                    'AU' => 'Australia',
                    'AT' => 'Austria',
                    'AZ' => 'Azerbaijan',
                    'BS' => 'Bahamas',
                    'BH' => 'Bahrain',
                    'BD' => 'Bangladesh',
                    'BB' => 'Barbados',
                    'BY' => 'Belarus',
                    'BE' => 'Belgium',
                    'BZ' => 'Belize',
                    'BJ' => 'Benin',
                    'BM' => 'Bermuda',
                    'BT' => 'Bhutan',
                    'BO' => 'Bolivia',
                    'BA' => 'Bosnia And Herzegovina',
                    'BW' => 'Botswana',
                    'BV' => 'Bouvet Island',
                    'BR' => 'Brazil',
                    'IO' => 'British Indian Ocean Territory',
                    'BN' => 'Brunei Darussalam',
                    'BG' => 'Bulgaria',
                    'BF' => 'Burkina Faso',
                    'BI' => 'Burundi',
                    'KH' => 'Cambodia',
                    'CM' => 'Cameroon',
                    'CA' => 'Canada',
                    'CV' => 'Cape Verde',
                    'KY' => 'Cayman Islands',
                    'CF' => 'Central African Republic',
                    'TD' => 'Chad',
                    'CL' => 'Chile',
                    'CN' => 'China',
                    'CX' => 'Christmas Island',
                    'CC' => 'Cocos (Keeling) Islands',
                    'CO' => 'Colombia',
                    'KM' => 'Comoros',
                    'CG' => 'Congo',
                    'CD' => 'Congo, Democratic Republic',
                    'CK' => 'Cook Islands',
                    'CR' => 'Costa Rica',
                    'CI' => 'Cote D\'Ivoire',
                    'HR' => 'Croatia',
                    'CU' => 'Cuba',
                    'CY' => 'Cyprus',
                    'CZ' => 'Czech Republic',
                    'DK' => 'Denmark',
                    'DJ' => 'Djibouti',
                    'DM' => 'Dominica',
                    'DO' => 'Dominican Republic',
                    'EC' => 'Ecuador',
                    'EG' => 'Egypt',
                    'SV' => 'El Salvador',
                    'GQ' => 'Equatorial Guinea',
                    'ER' => 'Eritrea',
                    'EE' => 'Estonia',
                    'ET' => 'Ethiopia',
                    'FK' => 'Falkland Islands (Malvinas)',
                    'FO' => 'Faroe Islands',
                    'FJ' => 'Fiji',
                    'FI' => 'Finland',
                    'FR' => 'France',
                    'GF' => 'French Guiana',
                    'PF' => 'French Polynesia',
                    'TF' => 'French Southern Territories',
                    'GA' => 'Gabon',
                    'GM' => 'Gambia',
                    'GE' => 'Georgia',
                    'DE' => 'Germany',
                    'GH' => 'Ghana',
                    'GI' => 'Gibraltar',
                    'GR' => 'Greece',
                    'GL' => 'Greenland',
                    'GD' => 'Grenada',
                    'GP' => 'Guadeloupe',
                    'GU' => 'Guam',
                    'GT' => 'Guatemala',
                    'GG' => 'Guernsey',
                    'GN' => 'Guinea',
                    'GW' => 'Guinea-Bissau',
                    'GY' => 'Guyana',
                    'HT' => 'Haiti',
                    'HM' => 'Heard Island & Mcdonald Islands',
                    'VA' => 'Holy See (Vatican City State)',
                    'HN' => 'Honduras',
                    'HK' => 'Hong Kong',
                    'HU' => 'Hungary',
                    'IS' => 'Iceland',
                    'IN' => 'India',
                    'ID' => 'Indonesia',
                    'IR' => 'Iran, Islamic Republic Of',
                    'IQ' => 'Iraq',
                    'IE' => 'Ireland',
                    'IM' => 'Isle Of Man',
                    'IL' => 'Israel',
                    'IT' => 'Italy',
                    'JM' => 'Jamaica',
                    'JP' => 'Japan',
                    'JE' => 'Jersey',
                    'JO' => 'Jordan',
                    'KZ' => 'Kazakhstan',
                    'KE' => 'Kenya',
                    'KI' => 'Kiribati',
                    'KR' => 'Korea',
                    'KW' => 'Kuwait',
                    'KG' => 'Kyrgyzstan',
                    'LA' => 'Lao People\'s Democratic Republic',
                    'LV' => 'Latvia',
                    'LB' => 'Lebanon',
                    'LS' => 'Lesotho',
                    'LR' => 'Liberia',
                    'LY' => 'Libyan Arab Jamahiriya',
                    'LI' => 'Liechtenstein',
                    'LT' => 'Lithuania',
                    'LU' => 'Luxembourg',
                    'MO' => 'Macao',
                    'MK' => 'Macedonia',
                    'MG' => 'Madagascar',
                    'MW' => 'Malawi',
                    'MY' => 'Malaysia',
                    'MV' => 'Maldives',
                    'ML' => 'Mali',
                    'MT' => 'Malta',
                    'MH' => 'Marshall Islands',
                    'MQ' => 'Martinique',
                    'MR' => 'Mauritania',
                    'MU' => 'Mauritius',
                    'YT' => 'Mayotte',
                    'MX' => 'Mexico',
                    'FM' => 'Micronesia, Federated States Of',
                    'MD' => 'Moldova',
                    'MC' => 'Monaco',
                    'MN' => 'Mongolia',
                    'ME' => 'Montenegro',
                    'MS' => 'Montserrat',
                    'MA' => 'Morocco',
                    'MZ' => 'Mozambique',
                    'MM' => 'Myanmar',
                    'NA' => 'Namibia',
                    'NR' => 'Nauru',
                    'NP' => 'Nepal',
                    'NL' => 'Netherlands',
                    'AN' => 'Netherlands Antilles',
                    'NC' => 'New Caledonia',
                    'NZ' => 'New Zealand',
                    'NI' => 'Nicaragua',
                    'NE' => 'Niger',
                    'NG' => 'Nigeria',
                    'NU' => 'Niue',
                    'NF' => 'Norfolk Island',
                    'MP' => 'Northern Mariana Islands',
                    'NO' => 'Norway',
                    'OM' => 'Oman',
                    'PK' => 'Pakistan',
                    'PW' => 'Palau',
                    'PS' => 'Palestinian Territory, Occupied',
                    'PA' => 'Panama',
                    'PG' => 'Papua New Guinea',
                    'PY' => 'Paraguay',
                    'PE' => 'Peru',
                    'PH' => 'Philippines',
                    'PN' => 'Pitcairn',
                    'PL' => 'Poland',
                    'PT' => 'Portugal',
                    'PR' => 'Puerto Rico',
                    'QA' => 'Qatar',
                    'RE' => 'Reunion',
                    'RO' => 'Romania',
                    'RU' => 'Russian Federation',
                    'RW' => 'Rwanda',
                    'BL' => 'Saint Barthelemy',
                    'SH' => 'Saint Helena',
                    'KN' => 'Saint Kitts And Nevis',
                    'LC' => 'Saint Lucia',
                    'MF' => 'Saint Martin',
                    'PM' => 'Saint Pierre And Miquelon',
                    'VC' => 'Saint Vincent And Grenadines',
                    'WS' => 'Samoa',
                    'SM' => 'San Marino',
                    'ST' => 'Sao Tome And Principe',
                    'SA' => 'Saudi Arabia',
                    'SN' => 'Senegal',
                    'RS' => 'Serbia',
                    'SC' => 'Seychelles',
                    'SL' => 'Sierra Leone',
                    'SG' => 'Singapore',
                    'SK' => 'Slovakia',
                    'SI' => 'Slovenia',
                    'SB' => 'Solomon Islands',
                    'SO' => 'Somalia',
                    'ZA' => 'South Africa',
                    'GS' => 'South Georgia And Sandwich Isl.',
                    'ES' => 'Spain',
                    'LK' => 'Sri Lanka',
                    'SD' => 'Sudan',
                    'SR' => 'Suriname',
                    'SJ' => 'Svalbard And Jan Mayen',
                    'SZ' => 'Swaziland',
                    'SE' => 'Sweden',
                    'CH' => 'Switzerland',
                    'SY' => 'Syrian Arab Republic',
                    'TW' => 'Taiwan',
                    'TJ' => 'Tajikistan',
                    'TZ' => 'Tanzania',
                    'TH' => 'Thailand',
                    'TL' => 'Timor-Leste',
                    'TG' => 'Togo',
                    'TK' => 'Tokelau',
                    'TO' => 'Tonga',
                    'TT' => 'Trinidad And Tobago',
                    'TN' => 'Tunisia',
                    'TR' => 'Turkey',
                    'TM' => 'Turkmenistan',
                    'TC' => 'Turks And Caicos Islands',
                    'TV' => 'Tuvalu',
                    'UG' => 'Uganda',
                    'UA' => 'Ukraine',
                    'AE' => 'United Arab Emirates',
                    'GB' => 'United Kingdom',
                    'UK' => 'United Kingdom',
                    'US' => 'United States',
                    'USA' => 'United States',
                    'UM' => 'United States Outlying Islands',
                    'UY' => 'Uruguay',
                    'UZ' => 'Uzbekistan',
                    'VU' => 'Vanuatu',
                    'VE' => 'Venezuela',
                    'VN' => 'Viet Nam',
                    'VG' => 'Virgin Islands, British',
                    'VI' => 'Virgin Islands, U.S.',
                    'WF' => 'Wallis And Futuna',
                    'EH' => 'Western Sahara',
                    'YE' => 'Yemen',
                    'ZM' => 'Zambia',
                    'ZW' => 'Zimbabwe'];

                $dom = new HtmlDomParser();
                $brk = '';
                $current_date = date("Y-m-d");
                $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
                $category = array(
                      "417"=>"Contracts / Legal",
                      "419"=>"Customer Service",
                      "421"=>"Engineering",
                      "81469"=>"Facilities",
                      "422"=>"Finance",
                      "424"=>"Information Technology",
                      "425"=>"Marketing",
                      "426"=>"Operations / Manufacturing",
                      "423"=>"People & Culture",
                      "427"=>"Quality",
                      "418"=>"Sales / Business Development",
                      "420"=>"Security",
                      "428"=>"Technical Operations",
                    );
                foreach ($category as $key => $data) 
                {
                    //--print_r($key.",");
                    if($key == '421')
                    {
                       $pagination = ceil(160 / 20);
                       for ($i=0; $i < $pagination; $i++) 
                       { 
                           $n=$i*20;
                           //print_r($n.",");
                           $arrContextOptions=array(
                            "ssl"=>array(
                                "verify_peer"=>false,
                                "verify_peer_name"=>false,
                                ),
                            );  

                            $dom = HtmlDomParser::file_get_html("https://careers.viasat.com/careers/SearchJobs/?3_32_3=%5B%22421%22%5D&folderOffset=".$n, false, stream_context_create($arrContextOptions));
                            $jobs_data = $dom->find('ul[class=listSingleColumn]',0)->find('li');
                            foreach ($jobs_data as $jobs) 
                            {
                              $posted_date = $jobs->find('span',2)->innertext;
                              $posted_date = explode(" ",$posted_date);
                              $posted_date =  date('Y-m-d', strtotime($posted_date[1]));

                              if(strtotime($current_date) == strtotime($posted_date))
                              {
                                   $job_url = $jobs->find('h3[class=listSingleColumnItemTitle] a',0)->href;
                                   $job_id = explode("/", $job_url);
                                   $job_id = $job_id[6];
                                   //--print_r($job_id.",");

                                   $job_title = $jobs->find('h3[class=listSingleColumnItemTitle] a',0)->innertext;
                                   $country_spec = $jobs->find('span[class=listSingleColumnItemMiscDataItem]',0)->innertext;
                                   $country_spec = str_replace('.', '', $country_spec);
                                   $country_spec = explode("-", $country_spec);
                                   if(strlen($country_spec[0]) == 3)
                                   {
                                       $country = $countries[preg_replace('/\s+/', '', $country_spec[0])];
                                   }
                                   else
                                   {
                                        $country = preg_replace('/\s+/', '', $country_spec[0]);
                                   }

                                   //print_r($country.",");
                                           
                                   $reference_id = $jobs->find('span[class=listSingleColumnItemMiscDataItem]',1)->innertext;
                                   $reference_id = explode("#",$reference_id);
                                   $reference_id = str_replace('.', '', $reference_id[1]);
                                     
                                   $posted_on = $posted_date;
                                   //print_r($posted_on.",");

                                   $description = HtmlDomParser::file_get_html($job_url, false, stream_context_create($arrContextOptions));
                                   if($description != FALSE)
                                   {
                                     $job_details = $description->find('div[class=jobDetail]',0)->innertext;
                                     $job_description = preg_replace('/\s+/', ' ', $job_details);
                                     $job_description = addslashes($job_description);
                                    
                                     $experience_spec = $description->find('div[class=jobDetail]',0)->find('div[class=fieldSet]',1)->find('div[class=fieldSetValue]',0)->innertext;
                                     $experience_spec = explode(" ", $experience_spec);
                                     if(count($experience_spec) == '2')
                                     {
                                         $expe = explode("-", $experience_spec[0]);
                                         if(count($expe) == '2')
                                         {
                                             $experience_1 = $expe[0];
                                             $experience_2 = $expe[1];
                                         }
                                         else
                                         {
                                             $experience_1 = $expe[0];
                                             $experience_2 = 0;
                                         }
                                        
                                     }
                                     else
                                     {
                                       if(count($experience_spec) == '1')
                                       {
                                          $experience_1 = $experience_spec[0];
                                          $experience_2 = 0;
                                       }
                                       else
                                       {
                                         if(count($experience_spec) == '3')
                                         {
                                             if($experience_spec[1] == 'plus')
                                             {
                                                $experience_1 = $experience_spec[0];
                                                $experience_2 = 0;
                                             }
                                             else
                                             {
                                                $experience_1 = $experience_spec[0];
                                                $experience_2 = 0;
                                             }
                                 
                                         }
                                        
                                       }
                                            
                                     }
                                   
                                   }
                                   $row = Job::where('job_id', $job_id)->count();  
                                   if($row == 0)
                                   {
                                      $insert_data = [
                                        "company" => "Viasat",
                                        "website" => "https://www.viasat.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_on,
                                        "category" => $data,
                                        "country" => $country,
                                        "description" => $job_description,
                                        "job_id" => $job_id,
                                        "reference_id" => $reference_id,
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $job_url,
                                        "experience_from" => $experience_1,
                                        "experience_to" => $experience_2,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")            
                                      ];     

                                     // print_r($insert_data);   
                                     Job::insert($insert_data);         
                                   }


                              }
                              else
                              {
                                  $brk = "error";
                              }
                             

                           }
                           if($brk == 'error')
                           {
                              break;
                           }
                           
                       }
                    }
                    else
                    {

                        $arrContextOptions=array(
                            "ssl"=>array(
                                "verify_peer"=>false,
                                "verify_peer_name"=>false,
                            ),
                        );  

                        $dom = HtmlDomParser::file_get_html("https://careers.viasat.com/careers/SearchJobs/?3_32_3=%5B%22".$key."%22%5D", false, stream_context_create($arrContextOptions));
                        $jobs_data = $dom->find('ul[class=listSingleColumn]',0)->find('li');
                        foreach ($jobs_data as $key => $jobs) 
                         {
                              $posted_date = $jobs->find('span',2)->innertext;
                              $posted_date = explode(" ",$posted_date);
                              $posted_date = date('Y-m-d', strtotime($posted_date[1]));

                              if(strtotime($current_date) == strtotime($posted_date))
                              {
                                   $job_url = $jobs->find('h3[class=listSingleColumnItemTitle] a',0)->href;
                                   $job_id = explode("/", $job_url);
                                   $job_id = $job_id[6];
                                   //--print_r($job_id.",");
                                   
                                   $job_title = $jobs->find('h3[class=listSingleColumnItemTitle] a',0)->innertext;
                                   $country_spec = $jobs->find('span[class=listSingleColumnItemMiscDataItem]',0)->innertext;
                                   $country_spec = str_replace('.', '', $country_spec);
                                   $country_spec = explode("-", $country_spec);
                                   if(strlen($country_spec[0]) == 3)
                                   {
                                       $country = $countries[preg_replace('/\s+/', '', $country_spec[0])];
                                   }
                                   else
                                   {
                                        $country = preg_replace('/\s+/', '', $country_spec[0]);
                                   }

                                   //print_r($country.",");

                                   $reference_id = $jobs->find('span[class=listSingleColumnItemMiscDataItem]',1)->innertext;
                                   $reference_id = explode("#",$reference_id);
                                   $reference_id = str_replace('.', '', $reference_id[1]);
                                     
                                   $posted_on = $posted_date;
                                   //print_r($posted_on.",");
                                   
                                   $description = HtmlDomParser::file_get_html($job_url, false, stream_context_create($arrContextOptions));

                                   if($description != FALSE)
                                   {
                                     $job_details = $description->find('div[class=jobDetail]',0)->innertext;
                                     $job_description = preg_replace('/\s+/', ' ', $job_details);
                                     $job_description = addslashes($job_description);
                                     
                                     $experience_spec = $description->find('div[class=jobDetail]',0)->find('div[class=fieldSet]',1)->find('div[class=fieldSetValue]',0)->innertext;
                                     $experience_spec = explode(" ", $experience_spec);
                                     if(count($experience_spec) == '2')
                                     {
                                         $expe = explode("-", $experience_spec[0]);
                                         if(count($expe) == '2')
                                         {
                                             $experience_1 = $expe[0];
                                             $experience_2 = $expe[1];
                                         }
                                         else
                                         {
                                             $experience_1 = $expe[0];
                                             $experience_2 = 0;
                                         }
                                        
                                     }
                                     else
                                     {
                                       if(count($experience_spec) == '1')
                                       {
                                          $experience_1 = 0;
                                          $experience_2 = 0;
                                       }
                                       else
                                       {
                                         if(count($experience_spec) == '3')
                                         {
                                             if($experience_spec[1] == 'plus')
                                             {
                                                $experience_1 = $experience_spec[0];
                                                $experience_2 = 0;
                                             }
                                             else
                                             {
                                                $experience_1 = $experience_spec[0];
                                                $experience_2 = 0;
                                             }
                                 
                                         }
                                        
                                       }
                                            
                                     }
                                   
                                   }

                                   if($job_id == 1671)
                                   {
                                        $job_details = ''; 
                                   }
                                   if($job_id == 1445)
                                   {
                                         $job_details = ''; 
                                   }
                                   if($job_id == 1351)
                                   {
                                         $job_details = ''; 
                                   }

                                   $row = Job::where('job_id', $job_id)->count();  
                                   if($row == 0)
                                   {
                                        $insert_data = [
                                            
                                             "company" => "Viasat",
                                             "website" => "https://www.viasat.com/",
                                             "job_title" => $job_title,
                                             "posted_on"=> $posted_on,
                                             "category" => $data,
                                             "country" => $country,
                                             "description" => $job_description,
                                             "job_id" => $job_id,
                                             "reference_id" => $reference_id,
                                             "contact_name"=>'',
                                             "contact_email"=>'',
                                             "contact_phone"=>'',
                                             "source_url" => $job_url,
                                             "experience_from" => $experience_1,
                                             "experience_to" => $experience_2,
                                             "job_type"=>1,
                                             "points"=>0,
                                             "keywords"=>'',
                                             "keyword_ids"=>'',
                                             "keyword_points"=>'',
                                             "rating_types"=>'',
                                              "rating_points"=>'',
                                             "status"=>0,
                                             "created_at"=>date("Y-m-d H:i:s"),
                                             "updated_at"=>date("Y-m-d H:i:s")
                                        ];    

                                        //print_r($insert_data);      
                                        Job::insert($insert_data);         
                                   
                                   }

                              }
                              else
                              {
                                  $brk = "error";
                              }
                                                     
                         }

                    }
                   
                } 
            
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
