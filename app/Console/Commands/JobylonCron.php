<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class JobylonCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'jobylon:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

                 $ch = curl_init('https://feed.jobylon.com/feeds/06e6c52a4f414210bae41b177c7a4fae/?format=json');
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                      'content-type: application/json',
                      'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
                  ));
                $response = curl_exec($ch);
                $result = json_decode($response,true);
               
                foreach ($result as $jobs) 
                {
                    $postdate = $jobs['from_date'];
                    $posted_date = date("Y-m-d", strtotime($postdate));
                    if(strtotime($current_date) >= strtotime($posted_date) && strtotime($end_date) <= strtotime($posted_date))
                    {
                        $job_id = trim($jobs['id']);
                        //print_r( $job_id .",");
                        $category = trim($jobs['function']);
                        $job_title = trim($jobs['title']);
                        if(!empty($jobs['locations']))
                        {
                          $country = trim($jobs['locations'][0]['location']['country']);
                        }
                        else
                        {
                          $country = "";
                        }
                        $enddate = date("Y-m-d", strtotime($jobs['to_date']));
                        $source_url = trim($jobs['urls']['ad']);
                        $contact_name = trim($jobs['contact']['name']);
                        $contact_phone = trim($jobs['contact']['phone']);
                        $contact_email = trim($jobs['contact']['email']);
                        $desc = $jobs['descr']."\n". $jobs['skills'];
                        $job_desc = preg_replace('/\s+/', ' ', $desc);
                        $job_desc = addslashes($job_desc);
                        
                        $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Schibsted",
                                        "website" => "https://schibsted.com/career/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>$contact_name,
                                        "contact_email"=>$contact_email,
                                        "contact_phone"=>$contact_phone,
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }                

                        
                       
                    }
                    
                }           

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
