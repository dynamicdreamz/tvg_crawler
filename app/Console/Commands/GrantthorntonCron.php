<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class GrantthorntonCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'grantthornton:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Virginia"=>"United States",
                      "Kansas"=>"United States",
                      "Missouri"=>"United States",
                      "New Jersey"=>"United States"
                   ];            
              $html_data = HtmlDomParser::file_get_html("http://jobs.grantthornton.com/");
              $job_data = $html_data->find('div[class=listingFront text]');
              foreach ($job_data as $key => $jobs) 
              {
                 $url = $jobs->find('a',0)->href;
                 $source_url = "http://jobs.grantthornton.com/jobs/".$url;

                 // $jobid_exp = explode("/", $url);
                 // $jobid_exp = explode("-", $jobid_exp[1]);
                 // $job_id = $jobid_exp[0];

                 $job_title = trim($jobs->find('a b',0)->innertext);

                 $countrys = trim($jobs->find('span[class=location text right]',0)->find('span',1)->innertext);
                 $country = $countries[$countrys];
                 
                 $jobdesc = HtmlDomParser::file_get_html($source_url);
                 $desc= $jobdesc->find('span[id=ctl00_Content_LabelJobDescription]',0)->innertext;
                 $job_desc = preg_replace('/\s+/', ' ', $desc);
                 $job_desc = addslashes($job_desc);
                 
                 $postdate = $jobdesc->find('span[id=ctl00_Content_LabelFirstExtraction]',0)->innertext;
                 $posted_date = date("Y-m-d", strtotime($postdate));
                 
                 $job_id = trim($jobdesc->find('span[id=ctl00_Content_LabelJobCode]',0)->innertext);
                 //print_r($job_id.",");

                 $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "Grantthornton",
                                        "website" => "http://jobs.grantthornton.com/",
                                        "job_title" => $job_title,
                                        "posted_on"=> $posted_date,
                                        "category" => '',
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }     

              }
              
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
