<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class GideCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'gide:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
              $countries = [
                      "Ansbach"=>"Germany",
                      "Berlin"=>"Germany",
                      "Dortmund"=>"Germany",
                      "Dublin (Ireland)"=>"Ireland",
                      "Erfurt"=>"Germany",
                      "Frankfurt am Main"=>"Germany",
                      "Großbeeren"=>"Germany",
                      "Hamburg"=>"Germany",
                      "Hannover"=>"Germany",
                      "Helsinki (Finland)"=>"Finland",
                      "Köln"=>"Germany",
                      "Lahr/Schwarzwald"=>"Germany",
                      "Leipzig"=>"Germany",
                      "Łódź (Poland)"=>"Poland",
                      "Ludwigsfelde"=>"Germany",
                      "Mönchengladbach"=>"Germany",
                      "Münster"=>"Germany",
                      "Nogarole Rocca (Italy)"=>"Italy",
                      "Olsztynek (Poland)"=>"Poland",
                      "Stuttgart"=>"Germany",
                      "Szczecin (Poland)"=>"Poland",
                   ];
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://www.gi-de.com/en/ag/career/jobs/");
             // $job_data = $html_data->find('section[class=row content-wrapper]',0)->find('div[class=col-md-10 col-sm-9]',0)->find('div[id=jumpToModule]',0)->find('div[class=row]',1)->innertext;
             $job_data = $html_data->find('section[class=row content-wrapper]',0)->find('div[class=col-md-10 col-sm-9]',0)->find('div[class=tx-website]',0)->find('div[class=row]',1)->find('div[class=col-sm ce--selectionmodule]',0)->find('div[class=ce ce__selectiontext]');

             foreach ($job_data as $key => $jobs) 
             {
                $job_title = $jobs->find('div[class=col]',0)->find('h3[class=ce--topic]',0)->innertext;
                $job_title = trim($job_title);
                $categorys = $jobs->find('div[class=col]',0)->find('p[class=ce--caption]',0)->innertext;
                $categorys_exp = explode("<span", $categorys);
                $category = $categorys_exp[0];
                $countrie = $categorys_exp[2];
                $countrie_exp =explode(",", $countrie); 
                if(count($countrie_exp) == 3)
                {
                   $country = $countrie_exp[2];
                   $country = trim($country);
                }
                else
                {
                  if(count($countrie_exp) == 2)
                  {
                    $country = $countrie_exp[1];
                    $country = trim($country);
                  }
                  else
                  {

                     $countryes = explode("</span>", $countrie_exp[0]);
                     $country = trim($countryes[1]);
                  }
                }
                
                $url = $jobs->find('div[class=col]',0)->find('a',0)->href;
                $source_url = "https://www.gi-de.com/".$url;

                $job_id_exp = explode("/", $url);
                $job_id = $job_id_exp[6];
                //print_r($job_id.",");
                
                $jobdesc = HtmlDomParser::file_get_html($source_url);
                $desc = $jobdesc->find('section[class=row content-wrapper]',0)->find('div[class=col-md-10 col-sm-9]',0)->find('div[class=tx-website]',0)->find('div[class=plugin--detail]',0)->innertext;
                $job_desc = preg_replace('/\s+/', ' ', $desc);
                $job_desc = addslashes($job_desc);

                $row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "G&D",
                                        "website" => "https://www.gi-de.com/en/ag/career/",
                                        "job_title" => $job_title,
                                        "posted_on"=> Null,
                                        "category" => $category,
                                        "country" => $country,
                                        "description" => $job_desc,
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
                
             }
            
        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
