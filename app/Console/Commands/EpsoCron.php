<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class EpsoCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'epso:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
             
             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));
             $dom = new HtmlDomParser();

             $html_data = HtmlDomParser::file_get_html("https://epso.europa.eu/job-opportunities_en?keyword=&contract=All&grade=All&institution=All&location=All");
             $job_data = $html_data->find('table',0)->find('tbody tr');
             
             for ($i=1; $i < count($job_data); $i++)
             { 
             	$job_id = '17886'.$i;
             	//print_r($job_id.",");
             	$source_url = $job_data[$i]->find('td',0)->find('a',0)->href;
                $source_url = "https://epso.europa.eu".$source_url;
             	$job_title = $job_data[$i]->find('td',0)->find('a',0)->innertext;
             	$job_title = html_entity_decode(trim($job_title));
                $enddate = $job_data[$i]->find('td[class=views-field views-field-field-epso-deadline]',0)->find('span[class=date-display-single]',0)->innertext;
                $enddate = explode("-", trim($enddate));
                $enddate = date("Y-d-m", strtotime($enddate[0]));

                $cuntry = $job_data[$i]->find('td[class=views-field views-field-field-epso-locations]',0)->innertext;
                $cuntry = explode(",", trim($cuntry));

                $cuntry_exp = explode("(", trim($cuntry[0]));
                $cuntry_exp = explode(")", trim($cuntry_exp[1]));
                $country = $cuntry_exp[0];
                //print_r($country);
                
             	$row = Job::where('job_id', $job_id)->count();  
                            if($row == 0)
                            {
                                      $insert_data = [
                                        "company" => "European Commission",
                                        "website" => "https://epso.europa.eu/job-opportunities_en",
                                        "job_title" => $job_title,
                                        "posted_on"=> null,
                                        "category" => '',
                                        "country" => $country,
                                        "description" => '',
                                        "job_id" => $job_id,
                                        "reference_id" => '',
                                        "contact_name"=>'',
                                        "contact_email"=>'',
                                        "contact_phone"=>'',
                                        "source_url" => $source_url,
                                        "experience_from" => 0,
                                        "experience_to" => 0,
                                        "job_type"=>1,
                                        "points"=>0,
                                        "keywords"=>'',
                                        "keyword_ids"=>'',
                                        "keyword_points"=>'',
                                        "rating_types"=>'',
                                        "rating_points"=>'',
                                        "status"=>0,
                                        "created_at"=>date("Y-m-d H:i:s"),
                                        "updated_at"=>date("Y-m-d H:i:s")                                       
                                      ]; 
                                    //print_r($insert_data);
                                    Job::insert($insert_data);                
                            }
             }
             

             

        } catch (Exception $e) {
            return $e->getMessage();
        }    
    }
}
