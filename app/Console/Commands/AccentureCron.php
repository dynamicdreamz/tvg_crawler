<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Job;
use App\Keyword;
use App\JobMatchedProfile;
use Sunra\PhpSimple\HtmlDomParser;
use Curl;
use DB;

class AccentureCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    public $date_IST;
    protected $signature = 'accenture:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Match profile keywords with jobs and assign profile and it\'s keywords with its weight and balance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
         set_time_limit(0);
       date_default_timezone_set('Asia/Kolkata');
       $ist = date("Y-m-d g:i:s");
       $this->date_IST = date ("Y-m-d H:i:s", strtotime($ist));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {            

             $insert_data = array();
             $brk = '';
             $current_date = date("Y-m-d");
             $end_date = date("Y-m-d", strtotime("-14 days", strtotime(date("Y-m-d"))));

             $pagination = ceil(4271 / 24);
             $prev = 0;
             $offset = 1;


             for($i=0; $i <= 2; $i++) {
              $total = $prev + $offset;
              $postdata = json_encode(
                  array(
                      'c' => 'India',
                      'cs' => 'in-en',
                      'df' => '[{"metadatafieldname":"location","items":[]},{"metadatafieldname":"skill","items":[]},{"metadatafieldname":"jobTypeDescription","items":[]},{"metadatafieldname":"orgUnitLevel1Desc","items":[]},{"metadatafieldname":"orgUnitLevel2Desc","items":[]}]',
                      "f" => $total,
                      "isPk" => false,
                      "k" => '',
                      "lang" => "en",
                      "s" => 24,
                      "sf" => 1,
                      "syn" => false,
                      "userId" => "",
                      "wordDistance" => 0
                  )
              );
              $ch = curl_init('https://www.accenture.com/in-en/careers/jobsearchkeywords.query');
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                  'content-type: application/json'
              ));
              curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

              $response = curl_exec($ch);

              curl_close($ch);
              $result = json_decode($response, true);
              // print_r($result);
              
              foreach ($result['documents'] as $key=>$job) {
                $posted_date = preg_replace('/[^0-9]/', '', $job['postedDate']);
                $posted_date = date('Y-m-d', substr($posted_date, 0, -3));
                if(strtotime($current_date) == strtotime($posted_date))
                {
                  $country = explode('_', $job['id']);
                  $country = ucfirst($country[1]);
                  $posted_on = $posted_date;
                  $job_title = $job['title'];
                  $category = $job['skill'];
                  $description = $job['jobDescription'];
                  $job_description = preg_replace('/\s+/', ' ', $description);
                  $job_id = $job['requisitionId'];
                  $job_url = $job['jobDetailUrl'];
                  $job_url = str_replace('{0}','us-en',$job_url);
                  //die;
                  //--print_r($job_id.",");
                    //print_r($type.",");
                    $row = Job::where('job_id', $job_id)->count();  
                        if($row == 0)
                        {
                              $insert_data = [
                                "company" => "Accenture",
                                "website" => "https://www.accenture.com",
                                "job_title" => $job_title,
                                "posted_on"=> $posted_on,
                                "category" => $category,
                                "country" => $country,
                                "description" => $job_description,
                                "job_id" => $job_id,
                                "reference_id" => '',
                                "contact_name"=>'',
                                "contact_email"=>'',
                                "contact_phone"=>'',
                                "source_url" => $job_url,
                                "experience_from" => 0,
                                "experience_to" => 0,
                                "job_type"=>1,
                                "points"=>0,
                                "keywords"=>'',
                                "keyword_ids"=>'',
                                "keyword_points"=>'',
                                "rating_types"=>'',
                                "rating_points"=>'',
                                "status"=>0,
                                "created_at"=>date("Y-m-d H:i:s"),
                                "updated_at"=>date("Y-m-d H:i:s")                  
                              ];
                              //print_r($insert_data);
                              Job::insert($insert_data);
                        } 
 
                }

              }

          }
             
             
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
