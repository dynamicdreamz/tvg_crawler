<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\AlogrithmGenericKeywords',
        'App\Console\Commands\AlogrithmProfileKeywords',
        'App\Console\Commands\AlgorithmArchiveJobs',
        'App\Console\Commands\AlgorithmCloneJobs',
        'App\Console\Commands\AirbusCron',
        'App\Console\Commands\BmwCron',
        'App\Console\Commands\GoogleCron',
        'App\Console\Commands\TeliaCron',
        'App\Console\Commands\VestasCron',
        'App\Console\Commands\VodafoneCron',
        'App\Console\Commands\ViasatCron',
        'App\Console\Commands\TruecallerCron',
        'App\Console\Commands\AlphasightsCron',
        'App\Console\Commands\DsvCron',
        'App\Console\Commands\CiscoCron',
        'App\Console\Commands\KmdCron',
        'App\Console\Commands\AccentureCron',
        'App\Console\Commands\AccionaCron',
        'App\Console\Commands\AstrazenecaCron',
        'App\Console\Commands\IbmCron',
        'App\Console\Commands\SapCron',
        'App\Console\Commands\AppleCron',
        'App\Console\Commands\ButtonsCron',
        'App\Console\Commands\GeoblinkCron',
        'App\Console\Commands\OntruckCron',
        'App\Console\Commands\TypeformCron',
        'App\Console\Commands\CornerCron',
        'App\Console\Commands\CabifyCron',
        'App\Console\Commands\HeinekenCron',
        'App\Console\Commands\DanoneCron',
        'App\Console\Commands\MondelezCron',
        'App\Console\Commands\BcgCron',
        'App\Console\Commands\MckinseyCron',
        'App\Console\Commands\PgCron',
        'App\Console\Commands\GlovoappCron',
        'App\Console\Commands\JobandtalentCron',
        'App\Console\Commands\CocacolaCron',
        'App\Console\Commands\RedbullCron',
        'App\Console\Commands\NestleCron',
        'App\Console\Commands\LorealCron',
        'App\Console\Commands\JpmorganCron',
        'App\Console\Commands\GoldmansachsCron',
        'App\Console\Commands\BainCron',
        'App\Console\Commands\LinkedinCron',
        'App\Console\Commands\FacebookCron',
        'App\Console\Commands\UnileverCron',
        'App\Console\Commands\AtkCron',
        'App\Console\Commands\MicrosoftCron',
        'App\Console\Commands\SandvikCron',
        'App\Console\Commands\AtlascopcogroupCron',
        'App\Console\Commands\YaraCron',
        'App\Console\Commands\VolvocarsCron',
        'App\Console\Commands\OrklaCron',
        'App\Console\Commands\IkeaCron',
        'App\Console\Commands\ElectroluxCron',
        'App\Console\Commands\IcagruppenCron',
        'App\Console\Commands\BacardiCron',
        'App\Console\Commands\NordeaCron',
        'App\Console\Commands\AvanzaCron',
        'App\Console\Commands\LorealCron',
        'App\Console\Commands\CapcoCron',
        'App\Console\Commands\DeloitteCron',
        'App\Console\Commands\EvryCron',
        'App\Console\Commands\AcandoCron',
        'App\Console\Commands\SemconCron',
        'App\Console\Commands\RambollCron',
        'App\Console\Commands\BearingCron',
        'App\Console\Commands\CederquistCron',
        'App\Console\Commands\AssaabloyCron',
        'App\Console\Commands\AbbCron',
        'App\Console\Commands\VattenfallCron',
        'App\Console\Commands\ScaniaCron',
        'App\Console\Commands\LantmannenCron',
        'App\Console\Commands\DanielCron',
        'App\Console\Commands\ZalandoCron',
        'App\Console\Commands\GrantthorntonCron',
        'App\Console\Commands\AfconsultCron',
        'App\Console\Commands\GideCron',
        'App\Console\Commands\BakermckenzieCron',
        'App\Console\Commands\UberCron',
        'App\Console\Commands\EricssonCron',
        'App\Console\Commands\KingCron',
        'App\Console\Commands\ParadoxCron',
        'App\Console\Commands\BlizzardCron',
        'App\Console\Commands\BonnierCron',
        'App\Console\Commands\JobylonCron',
        'App\Console\Commands\NentgroupCron',
        'App\Console\Commands\NetflixCron',
        'App\Console\Commands\SnapCron',
        'App\Console\Commands\UnCron',
        'App\Console\Commands\TrafikverketCron',
        'App\Console\Commands\SwecoCron',
        'App\Console\Commands\SkanskaCron',
        'App\Console\Commands\IccwboCron',
        'App\Console\Commands\EpsoCron',
        'App\Console\Commands\VegvesenCron',
        'App\Console\Commands\MaerskCron',
        'App\Console\Commands\MsgCron',
        'App\Console\Commands\ShellCron',
        'App\Console\Commands\PreemCron',
        'App\Console\Commands\NovonordiskCron',
        'App\Console\Commands\GskCron',
        'App\Console\Commands\SatsCron',
        'App\Console\Commands\KryCron',
        'App\Console\Commands\MindoktorCron',
        'App\Console\Commands\ReadlyCron',
        'App\Console\Commands\ElkCron',
        'App\Console\Commands\MagineCron',
        'App\Console\Commands\SerstechCron',
        'App\Console\Commands\ThunderkickCron',
        'App\Console\Commands\FingerprintsCron',
        'App\Console\Commands\ShakeshackCron',
        'App\Console\Commands\HeliospectraCron',
        'App\Console\Commands\PlejdCron',
        'App\Console\Commands\ScriveCron',
        'App\Console\Commands\StorytelCron',
        'App\Console\Commands\BimobjectCron',
        'App\Console\Commands\JobviteCron',
        'App\Console\Commands\FatsharkCron',
        'App\Console\Commands\CherryCron',
        'App\Console\Commands\VisageCron',
        'App\Console\Commands\NepaCron',
        'App\Console\Commands\G5ecaliforniaCron',
        'App\Console\Commands\G5emaltaCron',
        'App\Console\Commands\G5erussiaCron',
        'App\Console\Commands\G5eswedenCron',
        'App\Console\Commands\G5eukraineCron',
        'App\Console\Commands\TrustlyCron',
        'App\Console\Commands\DigiexamCron',
        'App\Console\Commands\IzettleCron',
        'App\Console\Commands\ZimplerCron',
        'App\Console\Commands\MsoftCron',
        'App\Console\Commands\SecuritasCron',
        'App\Console\Commands\SsabCron',
        'App\Console\Commands\AutolivCron',
        'App\Console\Commands\AxfoodCron',
        'App\Console\Commands\AlfalavalCron',
        'App\Console\Commands\SpotifyCron',
        'App\Console\Commands\Okq8Cron',
        'App\Console\Commands\TrelleborgCron',
        'App\Console\Commands\SaabgroupCron',
        'App\Console\Commands\VolkswagengroupCron',
        'App\Console\Commands\StenaCron',
        'App\Console\Commands\PostnordCron'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('airbus:cron')
            ->daily();

        $schedule->command('bmw:cron')
            ->daily();

        $schedule->command('google:cron')
            ->daily();

        $schedule->command('telia:cron')
            ->daily();

        $schedule->command('vestas:cron')
            ->daily();

        $schedule->command('vodafone:cron')
            ->daily();

        $schedule->command('viasat:cron')
            ->daily();

        $schedule->command('truecaller:cron')
            ->daily();

        $schedule->command('alphasights:cron')
            ->daily();

        $schedule->command('dsv:cron')
            ->daily();

        $schedule->command('cisco:cron')
            ->daily();

        $schedule->command('kmd:cron')
            ->daily();

        $schedule->command('accenture:cron')
            ->daily();

        $schedule->command('acciona:cron')
            ->daily();

        $schedule->command('astrazeneca:cron')
            ->daily();

        $schedule->command('ibm:cron')
            ->daily();

        $schedule->command('sap:cron')
            ->daily();

        $schedule->command('apple:cron')
            ->daily();

        $schedule->command('buttons:cron')
            ->daily();

        $schedule->command('geoblink:cron')
            ->daily();

        $schedule->command('ontruck:cron')
            ->daily();

        $schedule->command('typeform:cron')
            ->daily();

        $schedule->command('corner:cron')
            ->daily();

        $schedule->command('cabify:cron')
            ->daily();

        $schedule->command('heineken:cron')
            ->daily();

        $schedule->command('danone:cron')
            ->daily();

        $schedule->command('mondelez:cron')
            ->daily();

        $schedule->command('bcg:cron')
            ->daily();

        $schedule->command('mckinsey:cron')
            ->daily();

        $schedule->command('pg:cron')
            ->daily();

        $schedule->command('glovoapp:cron')
            ->daily();

        $schedule->command('Jobandtalent:cron')
            ->daily();

        $schedule->command('cocacola:cron')
            ->daily();

        $schedule->command('redbull:cron')
            ->daily();

        $schedule->command('nestle:cron')
            ->daily();

        $schedule->command('loreal:cron')
            ->daily();

        $schedule->command('jpmorgan:cron')
            ->daily();

        $schedule->command('goldmansachs:cron')
            ->daily();

        $schedule->command('bain:cron')
            ->daily();

        $schedule->command('linkedin:cron')
            ->daily();

        $schedule->command('facebook:cron')
            ->daily();

        $schedule->command('unilever:cron')
            ->daily();

        $schedule->command('atk:cron')
            ->daily();

        $schedule->command('microsoft:cron')
            ->daily();

       $schedule->command('sandvik:cron')
            ->daily();

        $schedule->command('atlascopcogroup:cron')
            ->daily();

        $schedule->command('yara:cron')
            ->daily();

        $schedule->command('orkla:cron')
            ->daily();

        $schedule->command('volvocars:cron')
            ->daily();

        $schedule->command('ikea:cron')
            ->daily();

        $schedule->command('electrolux:cron')
            ->daily();

        $schedule->command('icagruppen:cron')
            ->daily();

        $schedule->command('bacardi:cron')
            ->daily();

        $schedule->command('nordea:cron')
            ->daily();

        $schedule->command('avanza:cron')
            ->daily();

        $schedule->command('loreal:cron')
            ->daily();

        $schedule->command('capco:cron')
            ->daily();

        $schedule->command('deloitte:cron')
            ->daily();

        $schedule->command('evry:cron')
            ->daily();

        $schedule->command('acando:cron')
            ->daily();

        $schedule->command('semcon:cron')
            ->daily();

        $schedule->command('ramboll:cron')
            ->daily();

        $schedule->command('bearing:cron')
            ->daily();

        $schedule->command('cederquist:cron')
            ->daily();

        $schedule->command('assaabloy:cron')
            ->daily();

        $schedule->command('abb:cron')
            ->daily();

        $schedule->command('vattenfall:cron')
            ->daily();

         $schedule->command('scania:cron')
            ->daily();

         $schedule->command('lantmannen:cron')
            ->daily();

        $schedule->command('daniel:cron')
            ->daily();

        $schedule->command('zalando:cron')
            ->daily();

        $schedule->command('grantthornton:cron')
            ->daily();

        $schedule->command('afconsult:cron')
            ->daily();

        $schedule->command('gide:cron')
            ->daily();    
        
        $schedule->command('bakermckenzie:cron')
            ->daily();

        $schedule->command('uber:cron')
            ->daily();

        $schedule->command('ericsson:cron')
            ->daily();

        $schedule->command('king:cron')
            ->daily();

         $schedule->command('paradox:cron')
            ->daily();

         $schedule->command('blizzard:cron')
            ->daily();

         $schedule->command('bonnier:cron')
            ->daily();

         $schedule->command('jobylon:cron')
            ->daily();

         $schedule->command('nentgroup:cron')
            ->daily();

         $schedule->command('netflix:cron')
            ->daily();

         $schedule->command('snap:cron')
            ->daily();

         $schedule->command('un:cron')
            ->daily();

         $schedule->command('trafikverket:cron')
            ->daily();

         $schedule->command('sweco:cron')
            ->daily();

         $schedule->command('skanska:cron')
            ->daily();

         $schedule->command('iccwbo:cron')
            ->daily();

         $schedule->command('epso:cron')
            ->daily();

         $schedule->command('vegvesen:cron')
            ->daily();

         $schedule->command('maersk:cron')
            ->daily();

         $schedule->command('msg:cron')
            ->daily();

         $schedule->command('shell:cron')
            ->daily();

         $schedule->command('preem:cron')
            ->daily();

         $schedule->command('novonordisk:cron')
            ->daily();

         $schedule->command('gsk:cron')
            ->daily();

         $schedule->command('sats:cron')
            ->daily();

         $schedule->command('kry:cron')
            ->daily();

         $schedule->command('mindoktor:cron')
            ->daily();

         $schedule->command('readly:cron')
            ->daily();

         $schedule->command('elk:cron')
            ->daily();

         $schedule->command('magine:cron')
            ->daily();

         $schedule->command('serstech:cron')
            ->daily();

         $schedule->command('thunderkick:cron')
            ->daily();

         $schedule->command('fingerprints:cron')
            ->daily();

         $schedule->command('shakeshack:cron')
            ->daily();

         $schedule->command('heliospectra:cron')
            ->daily();

         $schedule->command('plejd:cron')
            ->daily();

         $schedule->command('scrive:cron')
            ->daily();

         $schedule->command('storytel:cron')
            ->daily();

         $schedule->command('bimobject:cron')
            ->daily();

         $schedule->command('jobvite:cron')
            ->daily();

         $schedule->command('fatshark:cron')
            ->daily();

         $schedule->command('cherry:cron')
            ->daily();

         $schedule->command('visage:cron')
            ->daily();

         $schedule->command('nepa:cron')
            ->daily();

         $schedule->command('g5ecalifornia:cron')
            ->daily();

         $schedule->command('g5emalta:cron')
            ->daily();

         $schedule->command('g5erussia:cron')
            ->daily();

         $schedule->command('g5esweden:cron')
            ->daily();

         $schedule->command('g5eukraine:cron')
            ->daily();

         $schedule->command('trustly:cron')
            ->daily();

         $schedule->command('digiexam:cron')
            ->daily();

         $schedule->command('izettle:cron')
            ->daily();

         $schedule->command('zimpler:cron')
            ->daily();

         $schedule->command('msoft:cron')
            ->daily();

         $schedule->command('securitas:cron')
            ->daily();

         $schedule->command('ssab:cron')
            ->daily();

         $schedule->command('autoliv:cron')
            ->daily();

         $schedule->command('axfood:cron')
            ->daily();

         $schedule->command('alfalaval:cron')
            ->daily();

         $schedule->command('spotify:cron')
            ->daily();

         $schedule->command('okq8:cron')
            ->daily();

         $schedule->command('trelleborg:cron')
            ->daily();

         $schedule->command('saabgroup:cron')
            ->daily();

         $schedule->command('volkswagengroup:cron')
            ->daily();

         $schedule->command('stena:cron')
            ->daily();

         $schedule->command('postnord:cron')
            ->daily();
        
        $schedule->command('algorithm:generickeywords')
            ->everyTenMinutes();

        $schedule->command('algorithm:profilekeywords')
            ->everyTenMinutes();

        /*$schedule->command('algorithm:archivejobs')
            ->dailyAt("3:00");*/

        $schedule->command('algorithm:clonejobs')
            ->dailyAt("5:00");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
