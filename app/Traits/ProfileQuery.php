<?php

namespace App\Traits;

//use Auth;
//use App\User;
use App\Profile;
use Illuminate\Http\Request;
use DB;

trait ProfileQuery
{
    /**
     * Return a list of products, sortBy and/or filter.
     *
     * @param \Illuminate\Database\Eloquent\Collection
     */
    public function profilesQuery(Request $request, Profile $Profile)
    {
        try{
            $data = $request->except("_method", "_token");

            $search_text = trim($data["search_text"]);
            // Start a new query


            // Filter by Profile title
            if ($search_text  != '') {

                $Profiles = DB::table('Profiles')->where('name','LIKE','%'.$search_text.'%')
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            }else{
                $Profiles = $Profile->newQuery();
                // Return records
                $Profiles = $Profiles
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            }


            return $Profiles;


        }catch (Exception $e){
            return response()->toJson([$e->getMessage()], $e->getCode());
        }
    }
}
