<?php

namespace App\Traits;

//use Auth;
//use App\User;
use App\Job;
use App\JobMatchedKeyword;
use Illuminate\Http\Request;
use DB;

trait JobQuery
{
    /**
     * Return a list of products, sortBy and/or filter.
     *
     * @param \Illuminate\Database\Eloquent\Collection
     */
    public function indexQuery(Request $request, Job $job)
    {
        try {
            $data = $request->except("_method", "_token");

            $search_text = trim($data["search_text"]);
            // Start a new query
            $jobs = $job->newQuery();

            // Filter by job title
            if ($search_text != '') {

                $jobs->where('job_title', 'LIKE', '%' . $search_text . '%');
            }

            if ($data["relevance"] != '') {

                $jobs->where('points', $data["relevance"]);
            }

            if ($data["post_date"] != '') {

                $jobs->whereDate('posted_on', '=', date('Y-m-d',strtotime($data["post_date"])));
            }

            if ($data["country"] != '') {

                $jobs->where('country', '=', $data["country"]);
            }

            if ($data['job_type'] != "") {
                $jobs->whereIn('jobs.job_type', explode(',', $data['job_type']));
            }

            $jobs->where('status','=',1);
            $total_jobs = $jobs->count();
            // Return records
            $jobs = $jobs
                ->offset($data["offset"])
                ->limit($data["pagesize"])
                ->orderBy('points','desc')->get();


            foreach ($jobs as $key => $job) {
                $matches = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                    $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
                })->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('job_matched_keywords.job_id', $job->id)->select('keywords.*', 'weights.rating','job_matched_keywords.points')->get();

                $rel_match = 0;
                $keyowrds = [];
                foreach ($matches as $k => $match) {
                    $balance = $match->generic_balance == 1? '+':'-';
                    $keyowrds[]= $balance.$match->generic_title;
                    $rel_match += $match->points;
                }
                $jobs[$key]->relevant_match = $jobs[$key]->points;
                $jobs[$key]->keywords = $keyowrds;
                $jobs[$key]->description = str_replace("â€™", '’', $jobs[$key]->description);
                $jobs[$key]->description = str_replace("â€¢", "", $jobs[$key]->description);
                $jobs[$key]->description = str_replace("â€“", "-", $jobs[$key]->description);
                $jobs[$key]->description =  substr(stripslashes(strip_tags($jobs[$key]->description)), 0,300).'...';
                $jobs[$key]->posted_on = date('d/m/Y', strtotime($jobs[$key]->posted_on));
                $jobs[$key]->close_on = date('d/m/Y', strtotime($jobs[$key]->close_on));
            }
            //dd($jobs);
            $jobs_data["total_jobs"]  = $total_jobs;
            $jobs_data["jobs"]  = $jobs;
            return $jobs_data;


        } catch (Exception $e) {
            return response()->toJson([$e->getMessage()], $e->getCode());
        }
    }
}
