<?php

namespace App\Traits;

//use Auth;
//use App\User;
use App\Job;
use App\JobMatchedKeyword;
use Illuminate\Http\Request;
use DB;

trait AdminJobQuery
{
    /**
     * Return a list of products, sortBy and/or filter.
     *
     * @param \Illuminate\Database\Eloquent\Collection
     */
    public function indexQuery(Request $request, Job $job)
    {
        try {
            $data = $request->except("_method", "_token");

            $search_text = trim($data["search_text"]);
            // Start a new query


            // Filter by job title
            if ($search_text != '') {

                $jobs = DB::table('jobs')->where('job_title', 'LIKE', '%' . $search_text . '%')
                    ->orWhere('company', 'LIKE', '%' . $search_text . '%')
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            } else {
                $jobs = $job->newQuery();
                // Return records
                $jobs = $jobs
                    ->offset($data["offset"])
                    ->limit($data["pagesize"])
                    ->get();
            }


            foreach ($jobs as $key => $job) {
                $matches = JobMatchedKeyword::leftJoin('keywords', function ($join) {
                    $join->on('job_matched_keywords.keyword_id', '=', 'keywords.id');
                })->leftJoin('weights', function ($join) {
                    $join->on('keywords.generic_weight', '=', 'weights.id');
                })->where('job_matched_keywords.job_id', $job->id)->select('keywords.*', 'weights.rating')->get();

                $rel_match = 0;
                foreach ($matches as $k => $match) {
                    $rel_match += $match->generic_weight * $match->generic_balance;
                }
                $jobs[$key]->relevant_match = $rel_match;
            }
            return $jobs;


        } catch (Exception $e) {
            return response()->toJson([$e->getMessage()], $e->getCode());
        }
    }
}
