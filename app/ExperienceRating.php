<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExperienceRating extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','user_id', 'experience_from', 'experience_to', 'rating'
    ];
}
