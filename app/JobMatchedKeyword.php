<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobMatchedKeyword extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'job_id','user_id', 'keyword_id', 'points'
    ];
}
