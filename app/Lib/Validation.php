<?php

namespace App\lib;

class Validation
{

    private static $rules = array(

        "admin"=>array(
            "login"=>array(
                'username'=>'required',
                'password'=>'required|min:6'
            ),
            "reset_password"=>array(
                'new_password'=>'required|min:6',
                'cnf_password'=>'required_with:password|same:new_password|min:6'
            ),
            "reset_admin_password"=>array(
                'new_password'=>'required|min:6',
                'confirmation_password'=>'required_with:password|same:new_password|min:6'
            ),
            "create"=>array(
                'username'=>'required|unique:admin,username',
                'user_first_name'=>'required',
                'user_last_name'=>'required',
                'user_email'=>'required|email',
                'user_password'=>'required|min:6',
                'user_image'=>'required|mimes:jpeg,jpg,png|max:1024',
            ),
            "update_account"=>array(
                'profile_pic' => 'mimes:jpeg,jpg,png|max:1024',
            ),
            "update_user"=>array(
                'user_image' => 'mimes:jpeg,jpg,png|max:1024',
            ),
        ),
    );

    public static function get_rules($type, $rules_name)
    {
        if (isset(self::$rules[$type][$rules_name]))
            return self::$rules[$type][$rules_name];

        return array();
    }
}
?>
